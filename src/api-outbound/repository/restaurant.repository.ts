// import { RestaurantEntity } from '../entity/restaurant.entity';

export default interface RestaurantRepository {

    save(restaurant: any): Promise<any>;

    delete(): Promise<void>;

    get(): Promise<any>;


}