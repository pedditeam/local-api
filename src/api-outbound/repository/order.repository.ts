import {Document} from 'mongoose'
import {ConsumerModel} from '../../api-core/commons/model/order.model'

export default interface OrderRepository {

    getById(id: String): Promise<any>;

    getAll(): Promise<any>;

    save(order: any): Promise<String>;

    update(id: String, update: Array<ConsumerModel>): Promise<any>;

    updateDocument(doc: any): Promise<any>;

    delete(id: String): Promise<object>

}