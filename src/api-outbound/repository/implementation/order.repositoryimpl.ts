import OrderRepository from "../order.repository";
import { OrderEntity } from "../../entity/order.entity";
import { ConsumerModel } from "../../../api-core/commons/model/order.model"
import { injectable } from "inversify";
import { Document } from "mongoose";

@injectable()
export default class OrderRepositoryImpl implements OrderRepository {
    
    getById = async (id: String): Promise<any> => {
        return new Promise((resolve, reject) => {
            OrderEntity.findById(id).lean().exec((err, order) => {
                if (err) {
                    reject(err)
                }
                resolve(order)
            })
        })
    }

    getAll = async (): Promise<any> => {
        return new Promise((resolve, reject) => {
            OrderEntity.find().lean().exec((err, orders) => {
                if (err) {
                    reject(err)
                }
                resolve(orders)
            })
        })
    }
    
    save = (order: Document): Promise<String> => {
        return order.save()
            .then(order => order._id)
            .catch(err => {throw err})
    }

    update = async (id: String, update: Array<ConsumerModel>): Promise<any> => {
        return new Promise((resolve, reject) => {
            // args: condition, update, options, callback
            OrderEntity
                .findByIdAndUpdate(id, {consumers: update}, {new: true})
                .lean().exec((err, order) => {
                    if (err) {
                        reject(err)
                    }
                    resolve(order)
                })
            })
    }

    updateDocument = async (doc: any): Promise<any> => {
        return new Promise((resolve, reject) => {
            OrderEntity.updateOne({ _id: doc._id }, doc)
                .lean().exec((err, order) => {
                    if (err) {
                        reject(err)
                    }
                    resolve(order)
                })
        })
    }

    delete = async (id: String): Promise<object> => {
        return new Promise((resolve, reject) => {
            OrderEntity.findByIdAndDelete(id).exec((err, order) => {
                if (err) {
                    reject(err)
                }
                resolve(order)
            })
        })
    }
}