import RestaurantRepository from "../restaurant.repository";
import { injectable } from "inversify";
import { RestaurantEntity } from "../../entity/restaurant.entity";

@injectable()
export default class RestaurantRepositoryImpl implements RestaurantRepository {

    save(restaurantDTO: any): Promise<any> {
        return restaurantDTO.save()
            .then(restaurant => { return { 
                restaurant_cloud_id: restaurant.restaurant_cloud_id,
                token: restaurant.token
            }})
            .catch(err => { throw err });
    }

    delete(): Promise<any> {
        return new Promise((resolve, reject) => {
            RestaurantEntity.deleteMany({}).lean().exec((err, res) => {
                if (err) {
                    reject(err)
                }
                resolve(res)
            })
        })
    } 

    get(): Promise<object> {
        return new Promise((resolve, reject) => {
            RestaurantEntity.find().lean().exec((err, restaurants) => {
                if (err) {
                    reject(err)
                }
                resolve(restaurants[0])
            })
        })
    }

}