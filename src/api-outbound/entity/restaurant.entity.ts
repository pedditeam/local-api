import { Schema, Document, Model, model } from "mongoose";

const address = new Schema({
    zipcode: String,
    district: String,
    city: String,
    state: String
});

const ingredient = new Schema({
    mgmt_id: String,
    name: String,
    price: Number,
    type: Number,
    ingredient_cloud_id: Number
});

const product = new Schema({
    mgmt_id: String,
    name: String,
    image: String,
    description: String,
    featured: Number,
    price: Number,
    product_cloud_id: Number,
    ingredients: [ingredient]
})

const subcategory = new Schema({
    name: String,
    subcategory_cloud_id: Number,
    products: [product]
});

const category = new Schema({
    name: String,
    category_cloud_id: Number,
    image: String,
    subcategories: [subcategory],
    products: [product]
})

const manager = new Schema({
    manager_cloud_id: Number,
    name: String,
    username: String,
    email: String,
    status: Number
})

const waiter = new Schema({
    waiter_cloud_id: Number,
    QRcode: String,
    name: String,
    status: Number
})

const restaurantEntity: Schema = new Schema({
    restaurant_cloud_id: Number,
    token: String,
    name: String,
    cnpj: String,
    tables: Number,
    employees: Number,
    status: Number,
    logo_image: String,
    address: address,
    categories: [category],
    managers: [manager],
    waiters: [waiter],
    
});

export const RestaurantEntity: Model<any> = model("restaurant", restaurantEntity, "restaurant", true);
