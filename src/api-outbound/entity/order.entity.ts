import { Schema, Document, Model, model } from "mongoose";

const ingredient = {
    cloud_id: Number,
    action: Number,
    ingredient_price: Number
}

const item = {
    base_cloud_id: Number,
    item_price: Number,
    ingredients: [ingredient]
}

const consumer = {
    card: String,
    items: [item]
}

const order: Schema = new Schema({
    table: Number,
    guests: Number,
    order_price: Number,
    status: Number,
    restaurant_cloud_id: Number,
    waiter_cloud_id: Number,
    created_at: String,
    updated_at: String,
    consumers: [consumer]
});

export const OrderEntity: Model<any> = model("orders", order)//, "order", true);
