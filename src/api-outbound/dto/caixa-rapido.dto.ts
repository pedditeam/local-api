export class ProductDTO {

    public table: Number;
    public mgmtID: String;
    public quantity: Number;

    constructor(incoming: any) {    // DOES THIS OBJECT COME FROM PORT AT DOMAIN BOUNDARY?
        this.table = incoming.table;
        this.mgmtID = incoming.mgmtID;
        this.quantity = incoming.quantity;
    }
}