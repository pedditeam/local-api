export default interface PeddiCloudIntegrationPort {

    authenticate(): Promise<Object>;

    sendOrder(order: Object): Promise<Object>;

}