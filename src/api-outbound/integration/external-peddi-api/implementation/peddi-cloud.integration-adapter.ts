import fs from 'fs'
import JWT from 'jsonwebtoken'
import { injectable, inject } from "inversify";
import PeddiCloudIntegrationPort from "../peddi-cloud.integration-port";
import {types} from '../../../../config/types'
import RestaurantRepository from '../../../repository/restaurant.repository'
import request = require('request');
import { Request } from "express";


@injectable()
export default class PeddiCloudIntegrationAdapter implements PeddiCloudIntegrationPort {

    private privateKey
    private publicKey

    constructor(
        @inject(types.RestaurantRepository)
        private restaurantRepository: RestaurantRepository,
    ) { 
        this.privateKey = fs.readFileSync('./jwt/private.key', 'utf8')
        this.publicKey = fs.readFileSync('./jwt/public.key', 'utf8') 
    };

    private generateCredentialsToken = async (): Promise<any> => {
        if (!process.env.RESTAURANT_CLOUD_ID || !process.env.TOKEN) {
            await this.restaurantRepository.get()
                .then(info => {
                    process.env.RESTAURANT_CLOUD_ID = info.restaurant_cloud_id
                    process.env.TOKEN = info.token
                })
                .catch(err => { throw err })
        }

        return JWT.sign({
                restaurant_cloud_id: process.env.RESTAURANT_CLOUD_ID,
                token: process.env.TOKEN
            }, this.privateKey, {
                expiresIn: '10m',
                algorithm: 'RS256'
            })
    }

    authenticate = async (): Promise<Object> => {
        const apiToken = await this.generateCredentialsToken()
        return new Promise((resolve, reject) => {
            request.get({
                headers: {
                    'Authorization': 'Bearer ' + apiToken
                },
                url: 'http://peddi.sa-east-1.elasticbeanstalk.com/api/authenticate/local'
            }, async (err, resp, req: Request) => {
                if (!err) {
                    const headerValue = await resp.headers['authorization'];
                    return resolve(headerValue);
                }
                else {
                    return reject(err);
                }
            });
        });
    }

    sendOrder = async (order: Object): Promise<Object> => {
        const token = await this.authenticate()
        const orderString = JSON.stringify(order);
        return new Promise((resolve, reject) => {
            request.post({
                headers: {
                    'authorization' : token,
                    'Content-Type' : 'application/json'
                },
                url: 'http://peddi.sa-east-1.elasticbeanstalk.com/api/orders',
                body: orderString
            }, async (err, resp, body) => {
                if (!err && resp.statusCode < 400) {
                    return resolve(body);
                }
                else {
                    return reject(err || body);
                }
            });
        });
    }

}