import { ProductDTO } from '../../../dto/caixa-rapido.dto'

export default interface CaixaRapidoIntegrationPort {

   openTable(table: number) // : void

   addProductToTable(product: ProductDTO)// :void
   
   closeTable(table: number) //: void

   connect?()

   isConnected?(): boolean
   
}