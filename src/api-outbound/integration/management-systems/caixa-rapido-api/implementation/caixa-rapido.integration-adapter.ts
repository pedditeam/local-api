/**
 * Class containing the methods for integrating with the Caixa
 * Rapido API.
 */

const fs = require('fs')
const mssql = require('mssql')
import { injectable } from "inversify";
import ManagementSystemIntegrationPort from '../../mgmt-syst.integration-port'
import { ProductDTO } from '../../../../dto/caixa-rapido.dto'
import { UpdateModel } from '../../../../../api-core/commons/model/order.model'
import { IngredientDTO } from "../../../../../api-inbound/dto/order.dto";

@injectable()
export default class CaixaRapidoIntegration implements ManagementSystemIntegrationPort {
	private req
	private dbURL
	private pool

	// constructor(dbURL: string) {
	constructor() {
		this.dbURL = fs.readFileSync('./src/api-outbound/integration/management-systems/caixa-rapido-api/url.txt', 'utf8')
		this.pool = new mssql.ConnectionPool(this.dbURL)
	}

	private connect = async () => {
		await this.pool.connect().catch(err => { throw err })
		this.req = await this.pool.request()
	}

    /**
     * @summary: method for creating a new order assocaited with a given table.
     */
	private openTableProcedure = async (table: Number) =>  {
        /*
        Procedure para abrir mesa:
        Nome: sp_AbreMesa
        Parametros: @NroMesa		-> Numero da Mesa ou Comanda
                    @NroCartao		-> Fixo 0
                    @Nome			-> Fixo ''
                    @IPTerminal		-> IP ou nome do dispositivo que esta enviando o pedido
        ??          @VendorID		-> Nro de série do dispositivo
				    @Versao			-> Fixo ''
                    @Origem			-> Fixo 'iOS'
        Retorno: Codigo e Mensagem
        Codigo = 0 -> Sucesso
        Codigo <> 0 -> Ver Mensagem de erro

        Exemplo de Uso:
        sp_AbreMesa @NroMesa = 3, @NroCartao = 0, @Nome = '', @IPTerminal = 'chris', @VendorID = '1a280798a8c9989c', @Versao = '', @Origem = 'IOS'
        */
		this.req = await this.pool.request()
		const queryText = 'EXEC sp_AbreMesa @NroMesa = @nro_mesa, @NroCartao = 0, @Nome = \'\', @IPTerminal = @ip, @VendorID = @vendor_id, @Versao = \'\', @Origem = "IOS"'
		// define parameters
		this.req.input('nro_mesa', mssql.SmallInt, table)
		this.req.input('ip', mssql.NVarChar, 'Tablet - Mesa' + table)
		this.req.input('vendor_id', mssql.NVarChar, 'Tablet - Mesa' + table)
		// run query
		return this.req.query(queryText)
			.then(result => {
				if (result.recordset[0].Retorno !== 0) {
					throw new Error(result.recordset[0].Mensagem)
				}
				console.log('Bill open.')
			})
			.catch(err => { throw err })
	}

	public openTable = async (table: Number) => {
		if (!this.pool._connected) {
			await this.connect().catch(err => {throw err})
		}
		await this.openTableProcedure(table).catch(err => {throw err})
	}
	

    /**
     * @summary: method for adding a new product to the table's bill.
     */
	private addProductToTableProcedure = async (table: Number, mgmtID: String): Promise<void> => {
        /*
        Procedure para adicionar um produto na mesa:
        Nome: sp_AdicionaProduto
        Parametros: @NroMesa		-> Numero da Mesa ou Comanda
                    @NroCartao		-> Fixo 0
                    @Nome			-> Fixo ''
                    @IPTerminal		-> IP ou nome do dispositivo que esta enviando o pedido
                    @IDProduto		-> ID do produto(não é o código, é o campo ID Produto)
                    @Qtde			-> Informar a Qtde
                    @ObservacaoItem	-> No caso do FIT, informar ''
                    @VendedorID 	-> Fixo ''

        Retorno: Codigo e Mensagem
        Codigo = 0 -> Sucesso
        Codigo <> 0 -> Ver Mensagem de erro

        Exemplo de Uso:
        sp_AdicionaProduto @NroMesa = 3, @NroCartao = 0, @Nome = '', @IPTerminal = 'chris', @IDProduto = 14, @Qtde = 1.0, @ObservacaoItem = '', @VendedorID = ''
        */
		const queryText = 'EXEC sp_AdicionaProduto @NroMesa = @nro_mesa, @NroCartao = 0, @Nome = \'\', @IPTerminal = @ip, @IDProduto = @id_produto, @Qtde = 1.0, @ObservacaoItem = \'\', @VendedorID = \'\''
		// define parameters
		this.req.input('nro_mesa', mssql.TinyInt, table)
		this.req.input('ip', mssql.NVarChar, 'Tablet - Mesa' + table)
		this.req.input('id_produto', mssql.NVarChar, mgmtID)
		// run query
		return this.req.query(queryText)
			.then(result => {
				if (result.recordset[0].Retorno !== 0) {
					throw new Error(result.recordset[0].Mensagem)
				}
				console.log('Product added.')
			})
			.catch(err => {throw err})
	}

	public addProductsToTable = async (update: UpdateModel , table: Number): Promise<Array<any>> => {
		if (!this.pool._connected) {
			await this.connect().catch(err => { throw err })
		}
		let fails = []
		for (let consumer of update.consumers) {
			for (let item of consumer.items) {
				await this.addProductToTableProcedure(table, item.mgmt_id).catch(err => 
					fails.push({
						cloud_id: item.base_cloud_id,
						mgmt_id: item.mgmt_id,
						message: err.message
					}))
				if (item.ingredients) {
					for (let ingredient of item.ingredients) {
						await this.addProductToTableProcedure(table, ingredient.mgmt_id).catch(err => 
							fails.push({
								cloud_id: ingredient.cloud_id,
								mgmt_id: ingredient.mgmt_id,
								message: err.message
							}))
					}
				}			
			}
		}
		console.log('fails: ', fails)
		return fails
	}

    /**
     * @summary: method for finalizing the bill associated with a table.
     */
	private closeTableProcedure (table: Number) {
        /*
        Procedure para finalizar um pedido:
        Nome: sp_FinalizarPedido
        Parametros: @NroMesa		-> Numero da Mesa ou Comanda
                    @NroCartao		-> Fixo 0

        Retorno: Codigo e Mensagem
        Codigo = 0 -> Sucesso
        Codigo <> 0 -> Ver Mensagem de erro

        Exemplo de Uso:
        sp_FinalizarPedido @NroMesa = 3, @NroCartao = 0
        */
		const queryText = 'EXEC sp_FinalizarPedido @NroMesa = @nro_mesa, @NroCartao = 0'
		// define parameters
		this.req.input('nro_mesa', mssql.SmallInt, table)
		// run query
		return this.req.query(queryText)
			.then(result => {
				if (result.recordset[0].Retorno !== 0) {
					throw new Error('Invalid arguments.')
				}
				console.log('Bill closed.')
			})
			.catch(err => { throw err })
	}

	public closeTable = async (table: Number) => {
		if (!this.pool._connected) {
			await this.connect().catch(err => { throw err })
		}
		await this.closeTableProcedure(table)
		this.pool.close()
	}
	

    /**
     * @summary: method for checking the bill of a table that is still ordering.
     */
	public preBill(table: number, service: number, guests: number, print: number) {
        /*
        Procedure para visualizar a preconta
        Nome: sp_WSPreConta
        Parametros: @NroMesa		-> Numero da Mesa ou Comanda
                    @NroCartao		-> Fixo 0
                    @Servico		-> Indicador se vai ou não cobrar serviço(0 - não cobrar, ou 1 - cobrar)
                    @NroPessoas		-> Nro de pessoas na pesa
                    @Visualizar		-> Indicador se imprime (0) ou somente visualiza (1)

        Retorno: Codigo e Mensagem
        Codigo = 0 -> Sucesso
        Codigo <> 0 -> Ver Mensagem de erro

        Exemplo de Uso:
        sp_WSPreConta @NroMesa = 3, @NroCartao = 0, @Servico = 1, @NroPessoas = 1, @Visualizar = 1
        */
		const queryText = 'EXEC sp_WSPreConta @NroMesa = @nro_mesa, @NroCartao = 0, @Servico = @servico, @NroPessoas = @nro_pessoas, @Visualizar = @visualizar'
		// define parameters
		this.req.input('nro_mesa', mssql.SmallInt, table)
		this.req.input('servico', mssql.SmallInt, service)
		this.req.input('nro_pessoas', mssql.SmallInt, guests)
		this.req.input('visualizar', mssql.SmallInt, print)
		// run query
		return this.req.query(queryText)
			.then(result => {
				return result.recordset
			})
			.catch(err => { throw err })
	}
}