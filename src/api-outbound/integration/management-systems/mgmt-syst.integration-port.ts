import { UpdateModel } from '../../../api-core/commons/model/order.model'

export default interface CaixaRapidoIntegrationPort {

    openTable(table: Number): Promise<void>

    addProductsToTable(product: UpdateModel, table: Number): Promise<Array<any>>

    closeTable(table: Number) //: void

}