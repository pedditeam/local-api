import { injectable, inject } from "inversify";
import { types } from '../../config/types'
import OrderPortOutbound from "../../api-core/port/outbound/order.outbound-port";
import OrderModel, { ConsumerModel, UpdateModel } from "../../api-core/commons/model/order.model";
import { OrderEntity } from "../entity/order.entity";
import OrderRepository from "../repository/order.repository";
import PeddiCloudIntegrationPort from "../integration/external-peddi-api/peddi-cloud.integration-port";

@injectable()
export default class OrderAdapterOutbound implements OrderPortOutbound {

    constructor(
        @inject(types.OrderRepository)
        private orderRepository: OrderRepository,
        @inject(types.PeddiCloudIntegrationPort)
        private peddiCloudIntegration: PeddiCloudIntegrationPort,
        // @inject(types.EpocIntegrationPort)
        // private epocIntegration: EpocIntegrationPort
    ) { };

    getById = async (id: String): Promise<any> => {
        //it must makes a convert from model core to a dto;
        return this.orderRepository.getById(id)
            .then(order => order)
            .catch(err => {throw err})
    }

    getAll = async (): Promise<any> => {
        return this.orderRepository.getAll()
            .then(orders => orders)
            .catch(err => { throw err })
    }

    save = (order: OrderModel): Promise<String> => {
        // translate the core models into the particular DTOs and API calls required by the integration
        const mongoDTO = new OrderEntity(order);
        return this.orderRepository.save(mongoDTO)
            .then(id => id)
            .catch(err => {throw err})
    }

    update = async (id: String, update: Array<ConsumerModel>): Promise<any> => {
        try {
            return await this.orderRepository.update(id, update);
        } catch (err) {
            throw err
        }
    }

    updateDocument = async (doc: any, update: UpdateModel, fails: Array<any>): Promise<any> => {
        const failedIDs = fails.map(fail => fail.cloud_id)
        const oldCards = doc.consumers.map(consumer => consumer.card)

        for (let consumer of update.consumers) {
            // 1. remove items (and associated ingredients) that failed to integrate with management system from update
            for (let i = (consumer.items.length - 1); i >= 0; i--) {
                if (failedIDs.includes(consumer.items[i].base_cloud_id)) {
                    consumer.items.splice(i, 1) // iterate in reverse to keep index correct
                    continue // skip ingredient part if item is removed
                }
                // if only ingredients failed, remove them but keep associated item
                if (consumer.items[i].ingredients) {
                    for (let j = (consumer.items[i].ingredients.length - 1); j >= 0; j--) {                        
                        if (failedIDs.includes(consumer.items[i].ingredients[j].cloud_id)) {
                            consumer.items[i].ingredients.splice(j, 1) // iterate in reverse to keep index correct
                            // if all ingredients are removed, set array to null
                            if (consumer.items[i].ingredients.length === 0) {                                
                                consumer.items[i].ingredients = null
                            }
                        }
                    }
                }
            }
            // 2. update document
            let index = oldCards.findIndex(card => {
                if (typeof(consumer.card) === 'string') {
                    return card === consumer.card
                } else {
                    return card === consumer.card.toString()
                }
            })
            if (index === -1) {
                // if current card hasn't been used for any orders
                doc.consumers.push({
                    card: consumer.card,
                    items: consumer.items
                })
            } else {
                // if card has already been used, just add the new items to it
                doc.consumers[index].items = doc.consumers[index].items.concat(consumer.items)
            }
        }
        doc.updated_at = update.updated_at
        doc.order_price = update.order_price
        // 3. save document
        const mongoDTO = new OrderEntity(doc)
        return this.orderRepository.updateDocument(mongoDTO).catch(err => {throw err})
    }


    delete = async (id: String): Promise<object> => {
        return this.orderRepository.delete(id).catch(err => {throw err})
    }

    // async send(order: OrderModel): Promise<Object> {
    //     console.log("Chegou na camada outbound");
    //     // object to be authenticate
    //     let authenticateObject = {
    //         // restaurant_id_cloud: 1,
    //         // token: "P21v1I85uM"
    //         restaurant_id_cloud: order.restaurant_id_cloud,
    //         token: order
    //     }



    //     // console.log(authenticateObject);
    //     //make the hash to cloud api authentication 

    //     let hashToAuthenticate = this.tokenManager.generateToken(authenticateObject);
    //     // console.log(hashToAuthenticate);
    //     // const hashToAuthenticate: String ;

    //     //authenticate
    //     let tokenAuthenticated = await this.peddiCloudIntegration.authenticate(hashToAuthenticate);
    //     console.log("token retornado");
    //     console.log(tokenAuthenticated);

    //     //send the order
    //     console.log("Enviando pedido:")
    //     let sentOrder = await this.peddiCloudIntegration.sendOrder(order, tokenAuthenticated);
    //     return sentOrder;

        // this.orderRepository.save(order);



        // this.peddiCloudIntegration.authentication();
    // }


    // sellItem(order: OrderModel): Promise<ResponseEpoc> {
    //     let result;
    //     var setSys = () => {
    //         return this.epocIntegration.sistemaAuth();
    //     };

    //     result = this.epocIntegration.tokenGenerator(setSys).then((r) => {
    //         return r;
    //     });;

    //     console.log(result);
    //     return result;
    // }
}