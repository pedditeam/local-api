import RestaurantPortOutbound from "../../api-core/port/outbound/restaurant.outbound-port";
import { injectable, inject } from "inversify";
import RestaurantRepository from "../repository/restaurant.repository";
import RestaurantModel from '../../api-core/commons/model/restaurant.model'
import { RestaurantEntity } from "../entity/restaurant.entity";
import { types } from '../../config/types'


@injectable()
export default class RestaurantAdapterOutbound implements RestaurantPortOutbound {

    constructor(
        //@inject('RestaurantRepository') 
        @inject(types.RestaurantRepository)
        private restaurantRepository: RestaurantRepository
    ) { };

    get = async (): Promise<any> => {
        return await this.restaurantRepository.get()
            .then(restaurant => restaurant)
            .catch(err => { throw err })
    }

    save(restaurant: RestaurantModel): Promise<void> {
        const mongoDTO = new RestaurantEntity(restaurant)
        return this.restaurantRepository.save(mongoDTO)
            .then(info => {
                process.env.RESTAURANT_CLOUD_ID = info.restaurant_cloud_id
                process.env.TOKEN = info.token
            })
            .catch(err => { throw err })
    }

    delete = async (): Promise<any> => {
        return this.restaurantRepository.delete().catch(err => { throw err })
    }

}