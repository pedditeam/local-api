/* 
"The adapter handles incoming signals, such as an HTTP GET call, 
and translates the incoming DTO into a model before invoking a Port." 
https://dzone.com/articles/hexagonal-architecture-it-works > Inbound Gateaway

In our case, since we already have the controllers, the adapter will simply convert
the OrderDTO to order Model.
*/
import {OrderDTO, UpdateDTO} from '../dto/order.dto'
import {IngredientModel, ItemModel, ConsumerModel, UpdateModel} from '../../api-core/commons/model/order.model'
import OrderModel from '../../api-core/commons/model/order.model'

export const orderAdapter = (orderDTO: OrderDTO): OrderModel => {
    // iterate over OrderDTO object to create OrderModel object avoiding type constraints
    let consumers = []
    orderDTO.consumers.forEach(consumer => {
        let items = []
        consumer.items.forEach(item => {
            let ingredients = null
            if (item.ingredients !== null) {
                ingredients = []
                item.ingredients.forEach(ingredient => ingredients.push(new IngredientModel(ingredient)))
            }
            items.push(new ItemModel(Object.assign({}, item, { ingredients: ingredients })))
        })
        consumers.push(new ConsumerModel(Object.assign({}, consumer, { items: items })))
    })
    return new OrderModel(Object.assign({}, orderDTO, { consumers: consumers }))
}

export const updateAdapter = (updateDTO: UpdateDTO): UpdateModel => {
    // iterate over OrderDTO object to create OrderModel object avoiding type constraints
    let consumers = []
    updateDTO.consumers.forEach(consumer => {
        let items = []
        consumer.items.forEach(item => {
            let ingredients = null
            if (item.ingredients !== null) {
                ingredients = []
                item.ingredients.forEach(ingredient => ingredients.push(new IngredientModel(ingredient)))
            }
            items.push(new ItemModel(Object.assign({}, item, { ingredients: ingredients })))
        })
        consumers.push(new ConsumerModel(Object.assign({}, consumer, { items: items })))
    })
    return new UpdateModel(Object.assign({}, updateDTO, { consumers: consumers }))
}