/* 
"The adapter handles incoming signals, such as an HTTP GET call, 
and translates the incoming DTO into a model before invoking a Port." 
https://dzone.com/articles/hexagonal-architecture-it-works > Inbound Gateaway

In our case, since we already have the controllers, the adapter will simply convert
the OrderDTO to order Model.
*/
import {AddressModel, IngredientModel, ProductModel, SubCategoryModel, CategoryModel, ManagerModel, WaiterModel } from "../../api-core/commons/model/restaurant.model"
import RestaurantDTO from "../../api-inbound/dto/restaurant.dto"
import RestaurantModel from '../../api-core/commons/model/restaurant.model'

export default (restaurantDTO: RestaurantDTO): RestaurantModel => {
    // iterate over RestaurantDTO object to create RestaurantModel object avoiding type constraints
    let categories = []
    restaurantDTO.categories.forEach(category => {
        // conversion is different according to whether subcategories have been registered or not
        if (category.subcategories) {
            let subcategories = []
            category.subcategories.forEach(subcategory => {
                let products = []
                subcategory.products.forEach(product => {
                    let ingredients = null
                    if (product.ingredients !== null) {
                        ingredients = []
                        product.ingredients.forEach(ingredient => ingredients.push(new IngredientModel(ingredient)))
                    }
                    products.push(new ProductModel(Object.assign({}, product, { ingredients: ingredients })))
                })
                subcategories.push(new SubCategoryModel(Object.assign({}, subcategory, { products: products })))
            })
            categories.push(new CategoryModel(Object.assign({}, category, { subcategories: subcategories })))
        } else {
            let products = []
            category.products.forEach(product => {
                let ingredients = null
                if (product.ingredients !== null) {
                    ingredients = []
                    product.ingredients.forEach(ingredient => ingredients.push(new IngredientModel(ingredient)))
                }
                //console.log(new ProductModel(Object.assign({}, product, { ingredients: ingredients })))
                products.push(new ProductModel(Object.assign({}, product, { ingredients: ingredients })))
            })
            categories.push(new CategoryModel(Object.assign({}, category, { products: products })))
        }       
    })
    //console.log('\ncategories: ', categories)
    // convert from manager and waiter DTOs to models
    let managers = []
    restaurantDTO.managers.forEach(manager => managers.push(new ManagerModel(manager)))
    let waiters = []
    restaurantDTO.waiters.forEach(waiter => waiters.push(new WaiterModel(waiter)))
    const addressModel = new AddressModel(restaurantDTO.address)

    return new RestaurantModel(Object.assign({}, restaurantDTO, { 
        categories: categories,
        managers: managers,
        waiters: waiters,
        address: addressModel
    }))
}