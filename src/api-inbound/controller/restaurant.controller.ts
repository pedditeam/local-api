import {Request, Response} from "express";
import { interfaces, controller, httpGet, httpPost } from "inversify-express-utils";
import { inject } from "inversify";
import RestaurantDTO from "../dto/Restaurant.dto";
import RestaurantPortInbound from "../../api-core/port/inbound/restaurant.inbound-port";
import restaurantAdapter from "../adapter/restaurant.adapter"
import { types } from '../../config/types'

@controller("/api/sync")
export default class RestaurantController implements interfaces.Controller {

    constructor(
        //@inject("RestaurantPortInbound") 
        @inject(types.RestaurantPortInbound)
        private RestaurantPortInbound: RestaurantPortInbound) { }

    @httpPost("/")
    private save(req: Request, res: Response) {
        const restaurant = new RestaurantDTO(req.body);
        const restaurantModel = restaurantAdapter(restaurant)
        return this.RestaurantPortInbound.save(restaurantModel)
            .then(() => res.status(200).send())
            .catch(err => res.status(400).json({ "error": err.message }));
    }

    @httpGet("/")
    private get(req: Request, res: Response) {
        return this.RestaurantPortInbound.get()
            .then(info => res.status(200).json(info))
            .catch(err => res.status(400).json({ "error": err.message }))
    }
}