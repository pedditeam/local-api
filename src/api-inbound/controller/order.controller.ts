import { Request, Response, NextFunction } from "express";
import { interfaces, controller, httpGet, httpPost, httpDelete, requestParam, httpPut } from "inversify-express-utils";
import { inject } from "inversify";
import {OrderDTO, UpdateDTO} from "../dto/order.dto";
import OrderPortInbound from "../../api-core/port/inbound/order.inbound-port";
import {orderAdapter, updateAdapter} from "../adapter/order.adapter"
import { types } from '../../config/types'

@controller("/orders")
export default class OrderController implements interfaces.Controller {

    constructor(
        //@inject("OrderPortInbound") 
        @inject(types.OrderPortInbound)
        private orderPortInbound: OrderPortInbound) { }

    @httpGet("/")
    private getAll(req: Request, res: Response, next: NextFunction) {
        return this.orderPortInbound.getAll();
    }

    @httpGet("/:id")
    private get(@requestParam("id") id: String, req: Request, res: Response, next: NextFunction) {
        return this.orderPortInbound.getById(id)
                .then(order => res.status(200).json(order))
                .catch(err => res.status(400).json({ "error": err.message }))
    }

    @httpPost("/")
    private ask(req: Request, res: Response) {
        const order = new OrderDTO(req.body); // meant for validation
        const orderModel = orderAdapter(order); //convert DTO to core model
        return this.orderPortInbound.save(orderModel)
            .then(resp => res.status(200).json(resp))
            .catch(resp => res.status(400).json(resp)) 
    }

    @httpPut("/:id")
    private askAgain(@requestParam("id") id: String, req: Request, res: Response) {
        const update = new UpdateDTO(req.body);
        const updateModel = updateAdapter(update); //convert DTO to core model
        return this.orderPortInbound.update(id, updateModel)
            .then(fails => res.status(200).json(fails))
            .catch(err => res.status(400).json({ "error": err.message }))
    }

    @httpDelete("/:id")
    private send(@requestParam("id") id: String, req: Request, res: Response) {
        return this.orderPortInbound.close(id)
            .then(() => res.status(204).send())
            .catch(err => res.status(400).json({ "error": err.message }))
        //     console.log("Chegou na camada inbound");
        //     const order = new OrderDTO(req.body);
        //     try {
        //         this.orderPortInbound.send(order);
        //         res.status(200);
        //     } catch (error) {
        //         res.status(400).json({error: error});
        //     }


        //     // this.orderPortInbound.insert(order);

        //     // try {
        //     //     await this.orderPortInbound.send(order);
        //     //     res.sendStatus(201);
        //     // } catch (err) {
        //     //     res.status(400).json({ error: err.message });
        //     // }
    }



    // @httpPost("/")
    // private save(req: Request, res: Response) {

    //     const order = new OrderDTO(req.body);
    //     try {
    //         let id = this.orderPortInbound.save(order);
    //         res.status(200).json({
    //             status: res.statusCode,
    //             data: {
    //                 id: id
    //             }
    //         })
    //     } catch (error) {

    //     }


    //     // this.orderPortInbound.insert(order);

    //     // try {
    //     //     await this.orderPortInbound.send(order);
    //     //     res.sendStatus(201);
    //     // } catch (err) {
    //     //     res.status(400).json({ error: err.message });
    //     // }
    // }

    // @httpPost("/")
    // private send(req: Request, res: Response) {
    //     console.log("Chegou na camada inbound");
    //     const order = new OrderDTO(req.body);
    //     try {
    //         this.orderPortInbound.send(order);
    //         res.status(200);
    //     } catch (error) {
    //         res.status(400).json({error: error});
    //     }


    //     // this.orderPortInbound.insert(order);

    //     // try {
    //     //     await this.orderPortInbound.send(order);
    //     //     res.sendStatus(201);
    //     // } catch (err) {
    //     //     res.status(400).json({ error: err.message });
    //     // }
    // }



    // @httpPost("/:id")
    // private update(@requestParam("id") id : Number, req: Request, res: Response) {

    //     const order = new OrderDTO(req.body);
    //     try {
    //         const updatedOrder = this.orderPortInbound.update(id, order);
    //         res.status(200).json({
    //             status: res.statusCode,
    //             data: {
    //                 updatedOrder
    //             }
    //         })
    //     } catch (error) {

    //     }


    //     // this.orderPortInbound.insert(order);

    //     // try {
    //     //     await this.orderPortInbound.send(order);
    //     //     res.sendStatus(201);
    //     // } catch (err) {
    //     //     res.status(400).json({ error: err.message });
    //     // }
    // }

    // @httpGet("/")
    // private list(@queryParam("start") start: number, @queryParam("count") count: number): string {
    //     return this.fooService.get(start, count);
    // }



    // @httpDelete("/:id")
    // private delete(@requestParam("id") id: string, @response() res: Response): Promise<void> {
    //     return this.fooService.delete(id)
    //         .then(() => res.sendStatus(204))
    //         .catch((err: Error) => {
    //             res.status(400).json({ error: err.message });
    //         });
    // }
}