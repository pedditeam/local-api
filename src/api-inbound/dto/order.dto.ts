export class OrderDTO {

    public mongo_id?: String;
    public table: Number;
    public guests: Number;
    public amount_price: Number;
    public status: Number;
    public restaurant_cloud_id: Number;
    public waiter_cloud_id: Number;
    public created_at: String;
    public updated_at: String;
    public consumers: Array<ConsumerDTO>;

    constructor(incoming: OrderDTO) {
        this.mongo_id = incoming.mongo_id;
        this.table = incoming.table;
        this.guests = incoming.guests;
        this.amount_price = incoming.amount_price;
        this.status = incoming.status;
        this.restaurant_cloud_id = incoming.restaurant_cloud_id;
        this.waiter_cloud_id = incoming.waiter_cloud_id;
        this.created_at = incoming.created_at;
        this.updated_at = incoming.updated_at;
        this.consumers = incoming.consumers;

    }

}

export class UpdateDTO {

    public order_price: Number;
    public updated_at: String;
    public consumers: Array<ConsumerDTO>;

    constructor(incoming: UpdateDTO) {
        this.order_price = incoming.order_price
        this.updated_at = incoming.updated_at;
        this.consumers = incoming.consumers;
    }
}

export class ConsumerDTO {

    public card: String;
    public items: Array<ItemDTO>;

    constructor(incoming: ConsumerDTO) {
        this.card = incoming.card;
        this.items = incoming.items;
    }

}

export class ItemDTO {

    public base_cloud_id: Number;
    public mgmt_id: String;
    public item_price: Number;
    public ingredients: Array<IngredientDTO>;

    constructor(incoming: ItemDTO) {
        this.base_cloud_id = incoming.base_cloud_id;
        this.mgmt_id = incoming.mgmt_id
        this.item_price = incoming.item_price;
        this.ingredients = incoming.ingredients;
    }

}

export class IngredientDTO {

    public cloud_id: Number;
    public mgmt_id: String;
    public action: Number;
    public ingredient_price: Number;

    constructor(incoming: IngredientDTO) {
        this.cloud_id = incoming.cloud_id;
        this.mgmt_id = incoming.mgmt_id;
        this.action = incoming.action;
        this.ingredient_price = incoming.ingredient_price;
    }

}
