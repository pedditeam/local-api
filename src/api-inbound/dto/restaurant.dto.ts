export default class RestaurantDTO {
    public restaurant_cloud_id: Number
    public token: String
    public name: String
    public cnpj: String
    public tables: Number
    public employees: Number
    public status: Number
    public logo_image: String
    public address: AddressDTO
    public categories: Array<CategoryDTO>
    public managers: Array<ManagerDTO>
    public waiters: Array<WaiterDTO>


    constructor(incoming) {
        this.restaurant_cloud_id = incoming.restaurant_cloud_id
        this.token = incoming.token
        this.name = incoming.name
        this.cnpj = incoming.cnpj
        this.tables = incoming.tables
        this.employees = incoming.employees
        this.status = incoming.status
        this.logo_image = incoming.logo_image
        this.address = incoming.address
        this.categories = incoming.categories
        this.managers = incoming.managers
        this.waiters = incoming.waiters
    }

}

export class AddressDTO {
    public zipcode: String
    public district: String
    public city: String
    public state: String

    constructor (incoming) {
        this.zipcode = incoming.zipcode
        this.district = incoming.district
        this.city = incoming.city
        this.state = incoming.state
    }
}

export class IngredientDTO {
    public mgmt_id: String
    public name: String
    public price: Number
    public type: Number
    public ingredient_cloud_id: Number

    constructor(incoming) {
        this.mgmt_id = incoming.mgmt_id
        this.name = incoming.name
        this.price = incoming.price
        this.type = incoming.type
        this.ingredient_cloud_id = incoming.ingredient_cloud_id
    }
}

export class ProductDTO {
    public mgmt_id: String
    public name: String
    public image: String
    public description: String
    public featured: Number
    public price: Number
    public product_cloud_id: Number
    public ingredients: Array<IngredientDTO>

    constructor(incoming) {
        this.mgmt_id = incoming.mgmt_id
        this.name = incoming.name
        this.image = incoming.image
        this.description = incoming.description
        this.featured = incoming.featured
        this.price = incoming.price
        this.product_cloud_id = incoming.product_cloud_id
        this.ingredients = incoming.ingredients
    }
}

export class SubCategoryDTO {
    public name: String
    public subcategory_cloud_id: Number
    public products: Array<ProductDTO>

    constructor(incoming) {
        this.name = incoming.name
        this.subcategory_cloud_id = incoming.subcategory_cloud_id
        this.products = incoming.products
    }

}

export class CategoryDTO {
    public name: String
    public category_cloud_id: Number
    public image: String
    public subcategories?: Array<SubCategoryDTO>
    public products?: Array<ProductDTO>

    constructor(incoming) {
        this.name = incoming.name
        this.category_cloud_id = incoming.category_cloud_id
        this.image = incoming.image
        this.subcategories = incoming.subcategories
        this.products = incoming.products
    }
}

export class ManagerDTO {
    public manager_cloud_id: Number
    public name: String
    public username: String
    public email: String
    public status: Number

    constructor(incoming) {
        this.manager_cloud_id = incoming.manager_cloud_id
        this.name = incoming.name
        this.username = incoming.username
        this.email = incoming.email
        this.status = incoming.status
    }
}

export class WaiterDTO {
    public waiter_cloud_id: Number
    public QRcode: String
    public name: String
    public status: Number

    constructor(incoming) {
        this.waiter_cloud_id = incoming.waiter_cloud_id
        this.QRcode = incoming.QRcode
        this.name = incoming.name
        this.status = incoming.status
    }
}