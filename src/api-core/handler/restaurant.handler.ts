import { injectable, inject } from "inversify";
import { types } from '../../config/types'
import RestaurantPortInbound from "../port/inbound/restaurant.inbound-port";
import RestaurantPortOutbound from "../port/outbound/restaurant.outbound-port"
import RestaurantModel from "../commons/model/restaurant.model";

@injectable()
export default class RestaurantHandler implements RestaurantPortInbound {


    constructor(
        @inject(types.RestaurantPortOutbound) private restaurantPortOutbound: RestaurantPortOutbound
    ) { }

    save = async (restaurant: RestaurantModel): Promise<void> => {
        try {
            await this.restaurantPortOutbound.delete()
            await this.restaurantPortOutbound.save(restaurant)
        } catch(err) {
            throw err
        }
    }

    get(): Promise<Object> {
        try {
            return this.restaurantPortOutbound.get()
        } catch (err) {
            throw err
        }
    }


}