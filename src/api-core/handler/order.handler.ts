import { injectable, inject } from "inversify";
import { types } from '../../config/types'
import OrderPortInbound from "../port/inbound/order.inbound-port";
import OrderPortOutbound from "../port/outbound/order.outbound-port"
import ManagementSystemIntegrationPort from "../../api-outbound/integration/management-systems/mgmt-syst.integration-port"
import PeddiCloudIntegrationPort from "../../api-outbound/integration/external-peddi-api/peddi-cloud.integration-port"
import OrderModel, { UpdateModel } from "../commons/model/order.model";

@injectable()
export default class OrderHandler implements OrderPortInbound {

    constructor(
        @inject(types.OrderPortOutbound) 
        private orderPortOutbound: OrderPortOutbound,
        @inject(types.ManagementSystemIntegrationPort) 
        private managementSystemIntegrationPort: ManagementSystemIntegrationPort,
        @inject(types.PeddiCloudIntegrationPort)
        private peddiCloudIntegrationPort: PeddiCloudIntegrationPort
        ) { }

    getAll(): Promise<Array<any>> {
        try {
            return this.orderPortOutbound.getAll();
        } catch (err) {
            throw err
        }
    }

    getById(id: String): Promise<any> {
        try {
            return this.orderPortOutbound.getById(id);
        } catch (err) {
            throw err
        }
    }
    
    save = async (order: OrderModel): Promise<any> => {   
        let response = {
            id: undefined,
            msg: ''
        }
        try {
            await this.managementSystemIntegrationPort.openTable(order.table)
            response.id = await this.orderPortOutbound.save(order);
            return response
        } catch(err) {
           response.msg = err.message;
           throw response
        }
    }

    update = async (id: String, update: UpdateModel): Promise<Array<any>> => {
        try {
            const order = await this.getById(id)
            const fails = await this.managementSystemIntegrationPort.addProductsToTable(update, order.table)
            this.orderPortOutbound.updateDocument(order, update, fails);
            return fails
        } catch(err) {
            throw err
        }
    }

    close = async (id: String): Promise<void> => {
        try {
            const order = await this.getById(id)
            this.managementSystemIntegrationPort.closeTable(order.table)
            this.peddiCloudIntegrationPort.sendOrder(order)
                .then(() => this.orderPortOutbound.delete(order._id))
                .catch(err => {throw err})
        } catch(err) {
            throw err
        }   
    }
}