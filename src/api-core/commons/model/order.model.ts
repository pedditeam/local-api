import {OrderDTO} from "../../../api-inbound/dto/order.dto";
import {ConsumerDTO, ItemDTO, IngredientDTO, UpdateDTO} from "../../../api-inbound/dto/order.dto";

// const { BaseDTO, fields } = require('dtox')

// Define order mapping
// const ORDER_MAPPING = {
//     table: Number(),
//     guests: Number(),
//     amount_price: Number(),
//     status: Number(),
//     restaurant_id_cloud: Number(),
//     // waiter: fields.WaiterDTO()
// };

// const WAITER_MAPPING = {
//     cloud_id: fields.number(),
//     mgmt_id: fields.string(),
//     name: fields.string()
// };

// // Define a DTO which represents a single order
// export default class OrderDTO extends BaseDTO {
//     constructor(data: any) {
//         super(data, ORDER_MAPPING);
//     }
// }

// class WaiterDTO extends BaseDTO {
//     constructor(data: any) {
//       super(data, WAITER_MAPPING);
//     }
// }

export default class OrderModel {
    public mongo_id?: String;
    public table: Number;
    public guests: Number;
    public order_price: Number;
    public status: Number;
    public restaurant_cloud_id: Number;
    public waiter_cloud_id: Number;
    public created_at: String;
    public updated_at: String;
    public consumers: Array<ConsumerModel>;

    constructor(incoming: OrderDTO) {
        this.mongo_id = incoming.mongo_id;
        this.table = incoming.table;
        this.guests = incoming.guests;
        this.order_price = incoming.amount_price;
        this.status = incoming.status;
        this.restaurant_cloud_id = incoming.restaurant_cloud_id;
        this.waiter_cloud_id = incoming.waiter_cloud_id;
        this.created_at = incoming.created_at;
        this.updated_at = incoming.updated_at;
        this.consumers = incoming.consumers;

    }

}

export class UpdateModel {

    public order_price: Number;
    public updated_at: String;
    public consumers: Array<ConsumerModel>;

    constructor(incoming: UpdateDTO) {
        this.order_price = incoming.order_price
        this.updated_at = incoming.updated_at;
        this.consumers = incoming.consumers;
    }
}

export class ConsumerModel {

    public card: String;
    public items: Array<ItemModel>;

    constructor(incoming: ConsumerDTO) {
        this.card = incoming.card;
        this.items = incoming.items;
    }

}

export class ItemModel {

    public base_cloud_id: Number;
    public mgmt_id: String;
    public item_price: Number;
    public ingredients: Array<IngredientModel>;

    constructor(incoming: ItemDTO) {
        this.base_cloud_id = incoming.base_cloud_id;
        this.mgmt_id = incoming.mgmt_id;
        this.item_price = incoming.item_price;
        this.ingredients = incoming.ingredients;
    }

}

export class IngredientModel {

    public cloud_id: Number;
    public mgmt_id: String;
    public action: Number;
    public ingredient_price: Number;

    constructor(incoming: IngredientDTO) {
        this.cloud_id = incoming.cloud_id;
        this.mgmt_id = incoming.mgmt_id;
        this.action = incoming.action;
        this.ingredient_price = incoming.ingredient_price;
    }

}


//Contract
// {  
//     "table":3,
//     "guests":2,
//     "amount_price":66.30,
//     "status":1,
//     "restaurant_id_cloud":1,
//     "waiter_id_cloud":1,
//     "created_at":"12/02/2018 12:50",
//     "updated_at":"12/02/2018 12:51",
//     "consumers":[  
//         {  
//             "card":508,
//             "items":[  
//                 {  
//                     "base_cloud_id":33,
//                     "item_price": 19.4,
//                     "ingredients":null
//                 },
//                 {  
//                     "base_cloud_id":101,
//                     "item_price": 4.50,
//                     "ingredients":null
//                 }
//             ]
//         },
//         {  
//             "card":603,
//             "items":[  
//                 {  
//                     "base_cloud_id":10,
//                     "item_price": 16.00,
//                     "ingredients":[  
//                         {  
//             		        "id":1,
//             		        "action":1,
//             		        "ingredient_price":2.5
//             	        }
//                     ]
//                 },
//                 {  
//                     "base_cloud_id":71,
//                     "item_price": 25.00,
//                     "ingredients":[  
//                         {  
//             		        "id":5,
//             		        "action":1,
//             	    	    "ingredient_price":2.5
//             	        },
//                         {  
//                             "id":14,
//                             "action":1,
//                             "ingredient_price":2.5
//                         }
//                     ]
//                 }
//             ]
//         }
//     ]
// }