import OrderModel from "../../commons/model/order.model";
import {UpdateModel} from "../../commons/model/order.model";

export default interface OrderPortInbound {

    getAll(): Promise<Array<OrderModel>>;

    getById(id: String): Promise<OrderModel>;

    save(order: OrderModel): Promise<any>;

    // send(id: String): void;

    //Just keep the id value on Flutter
    update(id: String, update: UpdateModel): Promise<Array<any>>;

    close (id:String): Promise<void>

}