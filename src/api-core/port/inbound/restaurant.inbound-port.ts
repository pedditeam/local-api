import RestaurantModel from "../../commons/model/restaurant.model";

export default interface OrderPortInbound {

    save(restaurant: RestaurantModel): Promise<void>;

    // delete(): any

    get(): Promise<Object>;

}