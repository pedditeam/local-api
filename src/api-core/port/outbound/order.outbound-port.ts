import {Document} from 'mongoose'
import OrderModel from '../../commons/model/order.model';
import {ConsumerModel, UpdateModel} from '../../commons/model/order.model';

export default interface OrderPortOutbound {
    
    getById(id: String): Promise<any>;

    getAll(): Promise<Array<any>>
    
    save(order: OrderModel): Promise<String>;

    update(id: String, update: Array<ConsumerModel>): Promise<void>;

    updateDocument(doc: any, update: UpdateModel, fails: Array<any>): Promise<void>

    delete(id: String): void

    // send(order: OrderModel, restaurant_id_cloud: Number, restaurantToken: String) : Promise<Object>;

    // sellItem?(order: OrderModel) : Promise<Object>;
}