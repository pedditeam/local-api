import RestaurantModel from '../../commons/model/restaurant.model';

export default interface RestaurantPortOutbound {
    get(): Promise<any>

    save(restaurant: RestaurantModel):Promise<void>;

    delete(): Promise<void>;
}