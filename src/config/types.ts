// export const types = {
//     OrderPortInbound: Symbol.for('OrderPortInbound'),
//     OrderPortOutbound: Symbol.for('OrderPortOutbound'),
//     RestaurantPortInbound: Symbol.for('RestaurantPortInbound'),
//     RestaurantPortOutbound: Symbol.for('RestaurantPortOutbound'),
//     OrderRepository: Symbol.for('OrderRepository'),
//     RestaurantRepository: Symbol.for('RestaurantRepository'),
//     PeddiCloudIntegrationPort: Symbol.for('PeddiCloudIntegrationPort'),
//     ManagementSystemIntegrationPort: Symbol.for('ManagementSystemIntegrationPort')
// }

export const types = {
    OrderPortInbound: Symbol('OrderPortInbound'),
    OrderPortOutbound: Symbol('OrderPortOutbound'),
    RestaurantPortInbound: Symbol('RestaurantPortInbound'),
    RestaurantPortOutbound: Symbol('RestaurantPortOutbound'),
    OrderRepository: Symbol('OrderRepository'),
    RestaurantRepository: Symbol('RestaurantRepository'),
    PeddiCloudIntegrationPort: Symbol('PeddiCloudIntegrationPort'),
    ManagementSystemIntegrationPort: Symbol('ManagementSystemIntegrationPort')
}