"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const morgan_1 = __importDefault(require("morgan"));
const mongoose_1 = __importDefault(require("mongoose"));
const bodyParser = __importStar(require("body-parser"));
class App {
    constructor() {
        require('dotenv').config();
        this.express = express_1.default();
        this.middleware();
        this.database();
    }
    middleware() {
        this.express.use(morgan_1.default("dev"));
        this.express.use(bodyParser.json({ limit: '50mb' }));
        this.express.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
    }
    database() {
        const mongoUri = `mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`;
        const connectionOptions = {
            user: process.env.DB_USER,
            pass: process.env.DB_PWD,
            ssl: false
        };
        if (!process.env.DB_USER)
            delete connectionOptions.user;
        if (!process.env.DB_PWD)
            delete connectionOptions.pass;
        mongoose_1.default.Promise = global.Promise;
        // mongoose.connect(mongoUri, connectionOptions)
        mongoose_1.default.connect(mongoUri, { useNewUrlParser: true })
            .then(() => {
            console.log(`Connected to MongoDB database ${process.env.DB_NAME}@${process.env.DB_HOST}:${process.env.DB_PORT}`);
        })
            .catch(err => {
            console.log(err);
        });
    }
}
exports.default = new App().express;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9hcHAudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQUEsc0RBQThCO0FBQzlCLG9EQUE0QjtBQUM1Qix3REFBZ0M7QUFDaEMsd0RBQTBDO0FBRzFDLE1BQU0sR0FBRztJQUdMO1FBQ0ksT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQzNCLElBQUksQ0FBQyxPQUFPLEdBQUcsaUJBQU8sRUFBRSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUNsQixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDcEIsQ0FBQztJQUVNLFVBQVU7UUFDYixJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQkFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7UUFDaEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDckQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztJQUMvRSxDQUFDO0lBRU0sUUFBUTtRQUNYLE1BQU0sUUFBUSxHQUFHLGFBQWEsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUVsRyxNQUFNLGlCQUFpQixHQUFHO1lBQ3RCLElBQUksRUFBRSxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU87WUFDekIsSUFBSSxFQUFFLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTTtZQUN4QixHQUFHLEVBQUUsS0FBSztTQUNiLENBQUM7UUFFRixJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPO1lBQ3BCLE9BQU8saUJBQWlCLENBQUMsSUFBSSxDQUFDO1FBRWxDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU07WUFDbkIsT0FBTyxpQkFBaUIsQ0FBQyxJQUFJLENBQUM7UUFFNUIsa0JBQVEsQ0FBQyxPQUFRLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQztRQUV6QyxnREFBZ0Q7UUFDaEQsa0JBQVEsQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLEVBQUUsZUFBZSxFQUFFLElBQUksRUFBRSxDQUFDO2FBQ2hELElBQUksQ0FBQyxHQUFHLEVBQUU7WUFDUCxPQUFPLENBQUMsR0FBRyxDQUFDLGlDQUFpQyxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sSUFBSSxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sSUFBSSxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUM7UUFDdEgsQ0FBQyxDQUFDO2FBQ0QsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQ1QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNyQixDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7Q0FDSjtBQUVELGtCQUFlLElBQUksR0FBRyxFQUFFLENBQUMsT0FBTyxDQUFDIiwiZmlsZSI6ImFwcC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBleHByZXNzIGZyb20gXCJleHByZXNzXCI7XHJcbmltcG9ydCBsb2dnZXIgZnJvbSBcIm1vcmdhblwiO1xyXG5pbXBvcnQgbW9uZ29vc2UgZnJvbSBcIm1vbmdvb3NlXCI7XHJcbmltcG9ydCAqIGFzIGJvZHlQYXJzZXIgZnJvbSBcImJvZHktcGFyc2VyXCI7XHJcblxyXG5cclxuY2xhc3MgQXBwIHtcclxuICAgIHB1YmxpYyBleHByZXNzOiBleHByZXNzLkFwcGxpY2F0aW9uO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIHJlcXVpcmUoJ2RvdGVudicpLmNvbmZpZygpO1xyXG4gICAgICAgIHRoaXMuZXhwcmVzcyA9IGV4cHJlc3MoKTtcclxuICAgICAgICB0aGlzLm1pZGRsZXdhcmUoKTtcclxuICAgICAgICB0aGlzLmRhdGFiYXNlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIG1pZGRsZXdhcmUoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5leHByZXNzLnVzZShsb2dnZXIoXCJkZXZcIikpO1xyXG4gICAgICAgIHRoaXMuZXhwcmVzcy51c2UoYm9keVBhcnNlci5qc29uKHsgbGltaXQ6ICc1MG1iJyB9KSk7XHJcbiAgICAgICAgdGhpcy5leHByZXNzLnVzZShib2R5UGFyc2VyLnVybGVuY29kZWQoeyBsaW1pdDogJzUwbWInLCBleHRlbmRlZDogdHJ1ZSB9KSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGRhdGFiYXNlKCkge1xyXG4gICAgICAgIGNvbnN0IG1vbmdvVXJpID0gYG1vbmdvZGI6Ly8ke3Byb2Nlc3MuZW52LkRCX0hPU1R9OiR7cHJvY2Vzcy5lbnYuREJfUE9SVH0vJHtwcm9jZXNzLmVudi5EQl9OQU1FfWA7XHJcblxyXG4gICAgICAgIGNvbnN0IGNvbm5lY3Rpb25PcHRpb25zID0ge1xyXG4gICAgICAgICAgICB1c2VyOiBwcm9jZXNzLmVudi5EQl9VU0VSLFxyXG4gICAgICAgICAgICBwYXNzOiBwcm9jZXNzLmVudi5EQl9QV0QsXHJcbiAgICAgICAgICAgIHNzbDogZmFsc2VcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBpZiAoIXByb2Nlc3MuZW52LkRCX1VTRVIpXHJcbiAgICAgICAgICAgIGRlbGV0ZSBjb25uZWN0aW9uT3B0aW9ucy51c2VyO1xyXG5cclxuICAgICAgICBpZiAoIXByb2Nlc3MuZW52LkRCX1BXRClcclxuICAgICAgICAgICAgZGVsZXRlIGNvbm5lY3Rpb25PcHRpb25zLnBhc3M7XHJcblxyXG4gICAgICAgICg8YW55Pm1vbmdvb3NlLlByb21pc2UpID0gZ2xvYmFsLlByb21pc2U7XHJcblxyXG4gICAgICAgIC8vIG1vbmdvb3NlLmNvbm5lY3QobW9uZ29VcmksIGNvbm5lY3Rpb25PcHRpb25zKVxyXG4gICAgICAgIG1vbmdvb3NlLmNvbm5lY3QobW9uZ29VcmksIHsgdXNlTmV3VXJsUGFyc2VyOiB0cnVlIH0pXHJcbiAgICAgICAgICAgIC50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGBDb25uZWN0ZWQgdG8gTW9uZ29EQiBkYXRhYmFzZSAke3Byb2Nlc3MuZW52LkRCX05BTUV9QCR7cHJvY2Vzcy5lbnYuREJfSE9TVH06JHtwcm9jZXNzLmVudi5EQl9QT1JUfWApO1xyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAuY2F0Y2goZXJyID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycik7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBuZXcgQXBwKCkuZXhwcmVzczsiXX0=
