"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
const inversify_express_utils_1 = require("inversify-express-utils");
const app_1 = __importDefault(require("./app"));
// import "./api-inbound/controller/foo.controller";
require("./api-inbound/controller/order.controller");
require("./api-inbound/controller/restaurant.controller");
const ioc_config_1 = __importDefault(require("./config/ioc-config"));
const port = process.env.PORT || 3000;
app_1.default.set("port", port);
//set up a container of Invesify
// let container = new Container();
// container.bind<FooService>('FooService').to(FooImplementationService);
let server = new inversify_express_utils_1.InversifyExpressServer(ioc_config_1.default, app_1.default);
let app = server.build();
app.listen(port, () => {
    console.log(`App listening at ${port}`);
});
server.setErrorConfig((app) => {
    app.use((err, req, res, next) => {
        console.error(err.stack);
        res.status(500).send('Something broke!');
    });
});
// app.on("error", (error: NodeJS.ErrnoException) => {
//   switch (error.code) {
//       case "EACCES":
//           console.error(`${port} requires elevated privileges`);
//           process.exit(1);
//           break;
//       case "EADDRINUSE":
//           console.error(`${port} is already in use`);
//           process.exit(1);
//           break;
//       default:
//           throw error;
//   }
// });
// server.setConfig((app) => {
//   // add body parser
//   app.use(bodyParser.urlencoded({
//     extended: true
//   }));
//   app.use(bodyParser.json());
// });

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9pbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLDRCQUEwQjtBQUsxQixxRUFBaUU7QUFDakUsZ0RBQXdCO0FBQ3hCLG9EQUFvRDtBQUNwRCxxREFBbUQ7QUFDbkQsMERBQXdEO0FBQ3hELHFFQUE0QztBQUc1QyxNQUFNLElBQUksR0FBb0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDO0FBQ3ZELGFBQUcsQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDO0FBRXRCLGdDQUFnQztBQUNoQyxtQ0FBbUM7QUFFbkMseUVBQXlFO0FBRXpFLElBQUksTUFBTSxHQUFHLElBQUksZ0RBQXNCLENBQUMsb0JBQVMsRUFBRSxhQUFHLENBQUMsQ0FBQztBQUV4RCxJQUFJLEdBQUcsR0FBRyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUM7QUFDekIsR0FBRyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUMsR0FBRyxFQUFFO0lBQ25CLE9BQU8sQ0FBQyxHQUFHLENBQUMsb0JBQW9CLElBQUksRUFBRSxDQUFDLENBQUM7QUFDMUMsQ0FBQyxDQUFDLENBQUM7QUFFSCxNQUFNLENBQUMsY0FBYyxDQUFDLENBQUMsR0FBRyxFQUFFLEVBQUU7SUFDNUIsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQVUsRUFBRSxHQUFZLEVBQUUsR0FBYSxFQUFFLElBQWtCLEVBQUUsRUFBRTtRQUNwRSxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN6QixHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO0lBQzdDLENBQUMsQ0FBQyxDQUFDO0FBQ0wsQ0FBQyxDQUFDLENBQUM7QUFFSCxzREFBc0Q7QUFDdEQsMEJBQTBCO0FBQzFCLHVCQUF1QjtBQUN2QixtRUFBbUU7QUFDbkUsNkJBQTZCO0FBQzdCLG1CQUFtQjtBQUNuQiwyQkFBMkI7QUFDM0Isd0RBQXdEO0FBQ3hELDZCQUE2QjtBQUM3QixtQkFBbUI7QUFDbkIsaUJBQWlCO0FBQ2pCLHlCQUF5QjtBQUN6QixNQUFNO0FBQ04sTUFBTTtBQUVOLDhCQUE4QjtBQUM5Qix1QkFBdUI7QUFDdkIsb0NBQW9DO0FBQ3BDLHFCQUFxQjtBQUNyQixTQUFTO0FBQ1QsZ0NBQWdDO0FBQ2hDLE1BQU0iLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgXCJyZWZsZWN0LW1ldGFkYXRhXCI7XHJcbi8vIGltcG9ydCB7IENvbnRhaW5lciB9IGZyb20gJ2ludmVyc2lmeSc7XHJcbi8vIGltcG9ydCBGb29TZXJ2aWNlIGZyb20gXCIuL2FwaS1jb3JlL2hhbmRsZXIvZm9vLnNlcnZpY2VcIjtcclxuLy8gaW1wb3J0IEZvb0ltcGxlbWVudGF0aW9uU2VydmljZSBmcm9tIFwiLi9hcGktY29yZS9wb3J0L2luYm91bmQvZm9vLmlzZXJ2aWNlXCI7XHJcbmltcG9ydCB7IFJlcXVlc3QsIFJlc3BvbnNlLCBOZXh0RnVuY3Rpb24gfSBmcm9tIFwiZXhwcmVzc1wiO1xyXG5pbXBvcnQgeyBJbnZlcnNpZnlFeHByZXNzU2VydmVyIH0gZnJvbSAnaW52ZXJzaWZ5LWV4cHJlc3MtdXRpbHMnO1xyXG5pbXBvcnQgQXBwIGZyb20gXCIuL2FwcFwiO1xyXG4vLyBpbXBvcnQgXCIuL2FwaS1pbmJvdW5kL2NvbnRyb2xsZXIvZm9vLmNvbnRyb2xsZXJcIjtcclxuaW1wb3J0IFwiLi9hcGktaW5ib3VuZC9jb250cm9sbGVyL29yZGVyLmNvbnRyb2xsZXJcIjtcclxuaW1wb3J0IFwiLi9hcGktaW5ib3VuZC9jb250cm9sbGVyL3Jlc3RhdXJhbnQuY29udHJvbGxlclwiO1xyXG5pbXBvcnQgY29udGFpbmVyIGZyb20gXCIuL2NvbmZpZy9pb2MtY29uZmlnXCI7XHJcblxyXG5cclxuY29uc3QgcG9ydDogbnVtYmVyIHwgc3RyaW5nID0gcHJvY2Vzcy5lbnYuUE9SVCB8fCAzMDAwO1xyXG5BcHAuc2V0KFwicG9ydFwiLCBwb3J0KTtcclxuXHJcbi8vc2V0IHVwIGEgY29udGFpbmVyIG9mIEludmVzaWZ5XHJcbi8vIGxldCBjb250YWluZXIgPSBuZXcgQ29udGFpbmVyKCk7XHJcblxyXG4vLyBjb250YWluZXIuYmluZDxGb29TZXJ2aWNlPignRm9vU2VydmljZScpLnRvKEZvb0ltcGxlbWVudGF0aW9uU2VydmljZSk7XHJcblxyXG5sZXQgc2VydmVyID0gbmV3IEludmVyc2lmeUV4cHJlc3NTZXJ2ZXIoY29udGFpbmVyLCBBcHApO1xyXG5cclxubGV0IGFwcCA9IHNlcnZlci5idWlsZCgpO1xyXG5hcHAubGlzdGVuKHBvcnQsKCkgPT4ge1xyXG4gIGNvbnNvbGUubG9nKGBBcHAgbGlzdGVuaW5nIGF0ICR7cG9ydH1gKTtcclxufSk7XHJcblxyXG5zZXJ2ZXIuc2V0RXJyb3JDb25maWcoKGFwcCkgPT4ge1xyXG4gIGFwcC51c2UoKGVycjogRXJyb3IsIHJlcTogUmVxdWVzdCwgcmVzOiBSZXNwb25zZSwgbmV4dDogTmV4dEZ1bmN0aW9uKSA9PiB7XHJcbiAgICAgIGNvbnNvbGUuZXJyb3IoZXJyLnN0YWNrKTtcclxuICAgICAgcmVzLnN0YXR1cyg1MDApLnNlbmQoJ1NvbWV0aGluZyBicm9rZSEnKTtcclxuICB9KTtcclxufSk7XHJcblxyXG4vLyBhcHAub24oXCJlcnJvclwiLCAoZXJyb3I6IE5vZGVKUy5FcnJub0V4Y2VwdGlvbikgPT4ge1xyXG4vLyAgIHN3aXRjaCAoZXJyb3IuY29kZSkge1xyXG4vLyAgICAgICBjYXNlIFwiRUFDQ0VTXCI6XHJcbi8vICAgICAgICAgICBjb25zb2xlLmVycm9yKGAke3BvcnR9IHJlcXVpcmVzIGVsZXZhdGVkIHByaXZpbGVnZXNgKTtcclxuLy8gICAgICAgICAgIHByb2Nlc3MuZXhpdCgxKTtcclxuLy8gICAgICAgICAgIGJyZWFrO1xyXG4vLyAgICAgICBjYXNlIFwiRUFERFJJTlVTRVwiOlxyXG4vLyAgICAgICAgICAgY29uc29sZS5lcnJvcihgJHtwb3J0fSBpcyBhbHJlYWR5IGluIHVzZWApO1xyXG4vLyAgICAgICAgICAgcHJvY2Vzcy5leGl0KDEpO1xyXG4vLyAgICAgICAgICAgYnJlYWs7XHJcbi8vICAgICAgIGRlZmF1bHQ6XHJcbi8vICAgICAgICAgICB0aHJvdyBlcnJvcjtcclxuLy8gICB9XHJcbi8vIH0pO1xyXG5cclxuLy8gc2VydmVyLnNldENvbmZpZygoYXBwKSA9PiB7XHJcbi8vICAgLy8gYWRkIGJvZHkgcGFyc2VyXHJcbi8vICAgYXBwLnVzZShib2R5UGFyc2VyLnVybGVuY29kZWQoe1xyXG4vLyAgICAgZXh0ZW5kZWQ6IHRydWVcclxuLy8gICB9KSk7XHJcbi8vICAgYXBwLnVzZShib2R5UGFyc2VyLmpzb24oKSk7XHJcbi8vIH0pO1xyXG4iXX0=
