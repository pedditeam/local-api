"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const inversify_1 = require("inversify");
const restaurant_entity_1 = require("../entity/restaurant.entity");
const types_1 = require("../../config/types");
let RestaurantAdapterOutbound = class RestaurantAdapterOutbound {
    constructor(
    //@inject('RestaurantRepository') 
    restaurantRepository) {
        this.restaurantRepository = restaurantRepository;
        this.get = () => __awaiter(this, void 0, void 0, function* () {
            return yield this.restaurantRepository.get()
                .then(restaurant => restaurant)
                .catch(err => { throw err; });
        });
        this.delete = () => __awaiter(this, void 0, void 0, function* () {
            return this.restaurantRepository.delete().catch(err => { throw err; });
        });
    }
    ;
    save(restaurant) {
        const mongoDTO = new restaurant_entity_1.RestaurantEntity(restaurant);
        return this.restaurantRepository.save(mongoDTO)
            .then(info => {
            process.env.RESTAURANT_CLOUD_ID = info.restaurant_cloud_id;
            process.env.TOKEN = info.token;
        })
            .catch(err => { throw err; });
    }
};
RestaurantAdapterOutbound = __decorate([
    inversify_1.injectable(),
    __param(0, inversify_1.inject(types_1.types.RestaurantRepository)),
    __metadata("design:paramtypes", [Object])
], RestaurantAdapterOutbound);
exports.default = RestaurantAdapterOutbound;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9hcGktb3V0Ym91bmQvYWRhcHRlci9yZXN0YXVyYW50Lm91dGJvdW5kLWFkYXB0ZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNBLHlDQUErQztBQUcvQyxtRUFBK0Q7QUFDL0QsOENBQTBDO0FBSTFDLElBQXFCLHlCQUF5QixHQUE5QyxNQUFxQix5QkFBeUI7SUFFMUM7SUFDSSxrQ0FBa0M7SUFFMUIsb0JBQTBDO1FBQTFDLHlCQUFvQixHQUFwQixvQkFBb0IsQ0FBc0I7UUFHdEQsUUFBRyxHQUFHLEdBQXVCLEVBQUU7WUFDM0IsT0FBTyxNQUFNLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxHQUFHLEVBQUU7aUJBQ3ZDLElBQUksQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQztpQkFDOUIsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFLEdBQUcsTUFBTSxHQUFHLENBQUEsQ0FBQyxDQUFDLENBQUMsQ0FBQTtRQUNwQyxDQUFDLENBQUEsQ0FBQTtRQVlELFdBQU0sR0FBRyxHQUF1QixFQUFFO1lBQzlCLE9BQU8sSUFBSSxDQUFDLG9CQUFvQixDQUFDLE1BQU0sRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRSxHQUFHLE1BQU0sR0FBRyxDQUFBLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFDekUsQ0FBQyxDQUFBLENBQUE7SUFwQkcsQ0FBQztJQUFBLENBQUM7SUFRTixJQUFJLENBQUMsVUFBMkI7UUFDNUIsTUFBTSxRQUFRLEdBQUcsSUFBSSxvQ0FBZ0IsQ0FBQyxVQUFVLENBQUMsQ0FBQTtRQUNqRCxPQUFPLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO2FBQzFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUNULE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFBO1lBQzFELE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUE7UUFDbEMsQ0FBQyxDQUFDO2FBQ0QsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFLEdBQUcsTUFBTSxHQUFHLENBQUEsQ0FBQyxDQUFDLENBQUMsQ0FBQTtJQUNwQyxDQUFDO0NBTUosQ0FBQTtBQTVCb0IseUJBQXlCO0lBRDdDLHNCQUFVLEVBQUU7SUFLSixXQUFBLGtCQUFNLENBQUMsYUFBSyxDQUFDLG9CQUFvQixDQUFDLENBQUE7O0dBSnRCLHlCQUF5QixDQTRCN0M7a0JBNUJvQix5QkFBeUIiLCJmaWxlIjoiYXBpLW91dGJvdW5kL2FkYXB0ZXIvcmVzdGF1cmFudC5vdXRib3VuZC1hZGFwdGVyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlc3RhdXJhbnRQb3J0T3V0Ym91bmQgZnJvbSBcIi4uLy4uL2FwaS1jb3JlL3BvcnQvb3V0Ym91bmQvcmVzdGF1cmFudC5vdXRib3VuZC1wb3J0XCI7XHJcbmltcG9ydCB7IGluamVjdGFibGUsIGluamVjdCB9IGZyb20gXCJpbnZlcnNpZnlcIjtcclxuaW1wb3J0IFJlc3RhdXJhbnRSZXBvc2l0b3J5IGZyb20gXCIuLi9yZXBvc2l0b3J5L3Jlc3RhdXJhbnQucmVwb3NpdG9yeVwiO1xyXG5pbXBvcnQgUmVzdGF1cmFudE1vZGVsIGZyb20gJy4uLy4uL2FwaS1jb3JlL2NvbW1vbnMvbW9kZWwvcmVzdGF1cmFudC5tb2RlbCdcclxuaW1wb3J0IHsgUmVzdGF1cmFudEVudGl0eSB9IGZyb20gXCIuLi9lbnRpdHkvcmVzdGF1cmFudC5lbnRpdHlcIjtcclxuaW1wb3J0IHsgdHlwZXMgfSBmcm9tICcuLi8uLi9jb25maWcvdHlwZXMnXHJcblxyXG5cclxuQGluamVjdGFibGUoKVxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBSZXN0YXVyYW50QWRhcHRlck91dGJvdW5kIGltcGxlbWVudHMgUmVzdGF1cmFudFBvcnRPdXRib3VuZCB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgLy9AaW5qZWN0KCdSZXN0YXVyYW50UmVwb3NpdG9yeScpIFxyXG4gICAgICAgIEBpbmplY3QodHlwZXMuUmVzdGF1cmFudFJlcG9zaXRvcnkpXHJcbiAgICAgICAgcHJpdmF0ZSByZXN0YXVyYW50UmVwb3NpdG9yeTogUmVzdGF1cmFudFJlcG9zaXRvcnlcclxuICAgICkgeyB9O1xyXG5cclxuICAgIGdldCA9IGFzeW5jICgpOiBQcm9taXNlPGFueT4gPT4ge1xyXG4gICAgICAgIHJldHVybiBhd2FpdCB0aGlzLnJlc3RhdXJhbnRSZXBvc2l0b3J5LmdldCgpXHJcbiAgICAgICAgICAgIC50aGVuKHJlc3RhdXJhbnQgPT4gcmVzdGF1cmFudClcclxuICAgICAgICAgICAgLmNhdGNoKGVyciA9PiB7IHRocm93IGVyciB9KVxyXG4gICAgfVxyXG5cclxuICAgIHNhdmUocmVzdGF1cmFudDogUmVzdGF1cmFudE1vZGVsKTogUHJvbWlzZTx2b2lkPiB7XHJcbiAgICAgICAgY29uc3QgbW9uZ29EVE8gPSBuZXcgUmVzdGF1cmFudEVudGl0eShyZXN0YXVyYW50KVxyXG4gICAgICAgIHJldHVybiB0aGlzLnJlc3RhdXJhbnRSZXBvc2l0b3J5LnNhdmUobW9uZ29EVE8pXHJcbiAgICAgICAgICAgIC50aGVuKGluZm8gPT4ge1xyXG4gICAgICAgICAgICAgICAgcHJvY2Vzcy5lbnYuUkVTVEFVUkFOVF9DTE9VRF9JRCA9IGluZm8ucmVzdGF1cmFudF9jbG91ZF9pZFxyXG4gICAgICAgICAgICAgICAgcHJvY2Vzcy5lbnYuVE9LRU4gPSBpbmZvLnRva2VuXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIC5jYXRjaChlcnIgPT4geyB0aHJvdyBlcnIgfSlcclxuICAgIH1cclxuXHJcbiAgICBkZWxldGUgPSBhc3luYyAoKTogUHJvbWlzZTxhbnk+ID0+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5yZXN0YXVyYW50UmVwb3NpdG9yeS5kZWxldGUoKS5jYXRjaChlcnIgPT4geyB0aHJvdyBlcnIgfSlcclxuICAgIH1cclxuXHJcbn0iXX0=
