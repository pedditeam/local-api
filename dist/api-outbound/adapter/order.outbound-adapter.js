"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const inversify_1 = require("inversify");
const order_entity_1 = require("../entity/order.entity");
const types_1 = require("../../config/types");
let OrderAdapterOutbound = class OrderAdapterOutbound {
    constructor(orderRepository, peddiCloudIntegration) {
        this.orderRepository = orderRepository;
        this.peddiCloudIntegration = peddiCloudIntegration;
        this.getById = (id) => __awaiter(this, void 0, void 0, function* () {
            //it must makes a convert from model core to a dto;
            return this.orderRepository.getById(id)
                .then(order => order)
                .catch(err => { throw err; });
        });
        this.getAll = () => __awaiter(this, void 0, void 0, function* () {
            return this.orderRepository.getAll()
                .then(orders => orders)
                .catch(err => { throw err; });
        });
        this.save = (order) => {
            // translate the core models into the particular DTOs and API calls required by the integration
            const mongoDTO = new order_entity_1.OrderEntity(order);
            return this.orderRepository.save(mongoDTO)
                .then(id => id)
                .catch(err => { throw err; });
        };
        this.update = (id, update) => __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this.orderRepository.update(id, update);
            }
            catch (err) {
                throw err;
            }
        });
        this.updateDocument = (doc, update, fails) => __awaiter(this, void 0, void 0, function* () {
            const failedIDs = fails.map(fail => fail.cloud_id);
            const oldCards = doc.consumers.map(consumer => consumer.card);
            for (let consumer of update.consumers) {
                // 1. remove items (and associated ingredients) that failed to integrate with management system from update
                for (let i = (consumer.items.length - 1); i >= 0; i--) {
                    if (failedIDs.includes(consumer.items[i].base_cloud_id)) {
                        consumer.items.splice(i, 1); // iterate in reverse to keep index correct
                        continue; // skip ingredient part if item is removed
                    }
                    // if only ingredients failed, remove them but keep associated item
                    if (consumer.items[i].ingredients) {
                        for (let j = (consumer.items[i].ingredients.length - 1); j >= 0; j--) {
                            if (failedIDs.includes(consumer.items[i].ingredients[j].cloud_id)) {
                                consumer.items[i].ingredients.splice(j, 1); // iterate in reverse to keep index correct
                                // if all ingredients are removed, set array to null
                                if (consumer.items[i].ingredients.length === 0) {
                                    consumer.items[i].ingredients = null;
                                }
                            }
                        }
                    }
                }
                // 2. update document
                let index = oldCards.findIndex(card => {
                    if (typeof (consumer.card) === 'string') {
                        return card === consumer.card;
                    }
                    else {
                        return card === consumer.card.toString();
                    }
                });
                if (index === -1) {
                    // if current card hasn't been used for any orders
                    doc.consumers.push({
                        card: consumer.card,
                        items: consumer.items
                    });
                }
                else {
                    // if card has already been used, just add the new items to it
                    doc.consumers[index].items = doc.consumers[index].items.concat(consumer.items);
                }
            }
            doc.updated_at = update.updated_at;
            doc.order_price = update.order_price;
            // 3. save document
            const mongoDTO = new order_entity_1.OrderEntity(doc);
            return this.orderRepository.updateDocument(mongoDTO).catch(err => { throw err; });
        });
        this.delete = (id) => __awaiter(this, void 0, void 0, function* () {
            return this.orderRepository.delete(id).catch(err => { throw err; });
        });
    }
    ;
};
OrderAdapterOutbound = __decorate([
    inversify_1.injectable(),
    __param(0, inversify_1.inject(types_1.types.OrderRepository)),
    __param(1, inversify_1.inject(types_1.types.PeddiCloudIntegrationPort)),
    __metadata("design:paramtypes", [Object, Object])
], OrderAdapterOutbound);
exports.default = OrderAdapterOutbound;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9hcGktb3V0Ym91bmQvYWRhcHRlci9vcmRlci5vdXRib3VuZC1hZGFwdGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFQSx5Q0FBK0M7QUFFL0MseURBQW9EO0FBQ3BELDhDQUEwQztBQU0xQyxJQUFxQixvQkFBb0IsR0FBekMsTUFBcUIsb0JBQW9CO0lBRXJDLFlBRVksZUFBZ0MsRUFFaEMscUJBQWdEO1FBRmhELG9CQUFlLEdBQWYsZUFBZSxDQUFpQjtRQUVoQywwQkFBcUIsR0FBckIscUJBQXFCLENBQTJCO1FBSzVELFlBQU8sR0FBRyxDQUFPLEVBQVUsRUFBZ0IsRUFBRTtZQUN6QyxtREFBbUQ7WUFDbkQsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUM7aUJBQ2xDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztpQkFDcEIsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFLEdBQUUsTUFBTSxHQUFHLENBQUEsQ0FBQSxDQUFDLENBQUMsQ0FBQTtRQUNsQyxDQUFDLENBQUEsQ0FBQTtRQUVELFdBQU0sR0FBRyxHQUF1QixFQUFFO1lBQzlCLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEVBQUU7aUJBQy9CLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQztpQkFDdEIsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFLEdBQUcsTUFBTSxHQUFHLENBQUEsQ0FBQyxDQUFDLENBQUMsQ0FBQTtRQUNwQyxDQUFDLENBQUEsQ0FBQTtRQUVELFNBQUksR0FBRyxDQUFDLEtBQWlCLEVBQW1CLEVBQUU7WUFDMUMsK0ZBQStGO1lBQy9GLE1BQU0sUUFBUSxHQUFHLElBQUksMEJBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN4QyxPQUFPLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztpQkFDckMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDO2lCQUNkLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRSxHQUFFLE1BQU0sR0FBRyxDQUFBLENBQUEsQ0FBQyxDQUFDLENBQUE7UUFDbEMsQ0FBQyxDQUFBO1FBRUQsV0FBTSxHQUFHLENBQU8sRUFBVSxFQUFFLE1BQTRCLEVBQWdCLEVBQUU7WUFDdEUsSUFBSTtnQkFDQSxPQUFPLE1BQU0sSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLE1BQU0sQ0FBQyxDQUFDO2FBQ3hEO1lBQUMsT0FBTyxHQUFHLEVBQUU7Z0JBQ1YsTUFBTSxHQUFHLENBQUE7YUFDWjtRQUNMLENBQUMsQ0FBQSxDQUFBO1FBRUQsbUJBQWMsR0FBRyxDQUFPLEdBQVEsRUFBRSxNQUFtQixFQUFFLEtBQWlCLEVBQWdCLEVBQUU7WUFDdEYsTUFBTSxTQUFTLEdBQUcsS0FBSyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQTtZQUNsRCxNQUFNLFFBQVEsR0FBRyxHQUFHLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQTtZQUU3RCxLQUFLLElBQUksUUFBUSxJQUFJLE1BQU0sQ0FBQyxTQUFTLEVBQUU7Z0JBQ25DLDJHQUEyRztnQkFDM0csS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUU7b0JBQ25ELElBQUksU0FBUyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxFQUFFO3dCQUNyRCxRQUFRLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUEsQ0FBQywyQ0FBMkM7d0JBQ3ZFLFNBQVEsQ0FBQywwQ0FBMEM7cUJBQ3REO29CQUNELG1FQUFtRTtvQkFDbkUsSUFBSSxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsRUFBRTt3QkFDL0IsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFOzRCQUNsRSxJQUFJLFNBQVMsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0NBQy9ELFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUEsQ0FBQywyQ0FBMkM7Z0NBQ3RGLG9EQUFvRDtnQ0FDcEQsSUFBSSxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO29DQUM1QyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUE7aUNBQ3ZDOzZCQUNKO3lCQUNKO3FCQUNKO2lCQUNKO2dCQUNELHFCQUFxQjtnQkFDckIsSUFBSSxLQUFLLEdBQUcsUUFBUSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDbEMsSUFBSSxPQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLFFBQVEsRUFBRTt3QkFDcEMsT0FBTyxJQUFJLEtBQUssUUFBUSxDQUFDLElBQUksQ0FBQTtxQkFDaEM7eUJBQU07d0JBQ0gsT0FBTyxJQUFJLEtBQUssUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQTtxQkFDM0M7Z0JBQ0wsQ0FBQyxDQUFDLENBQUE7Z0JBQ0YsSUFBSSxLQUFLLEtBQUssQ0FBQyxDQUFDLEVBQUU7b0JBQ2Qsa0RBQWtEO29CQUNsRCxHQUFHLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQzt3QkFDZixJQUFJLEVBQUUsUUFBUSxDQUFDLElBQUk7d0JBQ25CLEtBQUssRUFBRSxRQUFRLENBQUMsS0FBSztxQkFDeEIsQ0FBQyxDQUFBO2lCQUNMO3FCQUFNO29CQUNILDhEQUE4RDtvQkFDOUQsR0FBRyxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQTtpQkFDakY7YUFDSjtZQUNELEdBQUcsQ0FBQyxVQUFVLEdBQUcsTUFBTSxDQUFDLFVBQVUsQ0FBQTtZQUNsQyxHQUFHLENBQUMsV0FBVyxHQUFHLE1BQU0sQ0FBQyxXQUFXLENBQUE7WUFDcEMsbUJBQW1CO1lBQ25CLE1BQU0sUUFBUSxHQUFHLElBQUksMEJBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQTtZQUNyQyxPQUFPLElBQUksQ0FBQyxlQUFlLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRSxHQUFFLE1BQU0sR0FBRyxDQUFBLENBQUEsQ0FBQyxDQUFDLENBQUE7UUFDbEYsQ0FBQyxDQUFBLENBQUE7UUFHRCxXQUFNLEdBQUcsQ0FBTyxFQUFVLEVBQW1CLEVBQUU7WUFDM0MsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsR0FBRSxNQUFNLEdBQUcsQ0FBQSxDQUFBLENBQUMsQ0FBQyxDQUFBO1FBQ3BFLENBQUMsQ0FBQSxDQUFBO0lBcEZHLENBQUM7SUFBQSxDQUFDO0NBd0lULENBQUE7QUFqSm9CLG9CQUFvQjtJQUR4QyxzQkFBVSxFQUFFO0lBSUosV0FBQSxrQkFBTSxDQUFDLGFBQUssQ0FBQyxlQUFlLENBQUMsQ0FBQTtJQUU3QixXQUFBLGtCQUFNLENBQUMsYUFBSyxDQUFDLHlCQUF5QixDQUFDLENBQUE7O0dBTDNCLG9CQUFvQixDQWlKeEM7a0JBakpvQixvQkFBb0IiLCJmaWxlIjoiYXBpLW91dGJvdW5kL2FkYXB0ZXIvb3JkZXIub3V0Ym91bmQtYWRhcHRlci5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERvY3VtZW50IH0gZnJvbSAnbW9uZ29vc2UnXHJcbmltcG9ydCBPcmRlclBvcnRPdXRib3VuZCBmcm9tIFwiLi4vLi4vYXBpLWNvcmUvcG9ydC9vdXRib3VuZC9vcmRlci5vdXRib3VuZC1wb3J0XCI7XHJcbmltcG9ydCB7IGluamVjdGFibGUsIGluamVjdCB9IGZyb20gXCJpbnZlcnNpZnlcIjtcclxuaW1wb3J0IE9yZGVyTW9kZWwsIHsgQ29uc3VtZXJNb2RlbCwgVXBkYXRlTW9kZWwgfSBmcm9tIFwiLi4vLi4vYXBpLWNvcmUvY29tbW9ucy9tb2RlbC9vcmRlci5tb2RlbFwiO1xyXG5pbXBvcnQgeyBPcmRlckVudGl0eX0gZnJvbSBcIi4uL2VudGl0eS9vcmRlci5lbnRpdHlcIjtcclxuaW1wb3J0IHsgdHlwZXMgfSBmcm9tICcuLi8uLi9jb25maWcvdHlwZXMnXHJcblxyXG5pbXBvcnQgT3JkZXJSZXBvc2l0b3J5IGZyb20gXCIuLi9yZXBvc2l0b3J5L29yZGVyLnJlcG9zaXRvcnlcIjtcclxuaW1wb3J0IFBlZGRpQ2xvdWRJbnRlZ3JhdGlvblBvcnQgZnJvbSBcIi4uL2ludGVncmF0aW9uL2V4dGVybmFsLXBlZGRpLWFwaS9wZWRkaS1jbG91ZC5pbnRlZ3JhdGlvbi1wb3J0XCI7XHJcblxyXG5AaW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIE9yZGVyQWRhcHRlck91dGJvdW5kIGltcGxlbWVudHMgT3JkZXJQb3J0T3V0Ym91bmQge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIEBpbmplY3QodHlwZXMuT3JkZXJSZXBvc2l0b3J5KVxyXG4gICAgICAgIHByaXZhdGUgb3JkZXJSZXBvc2l0b3J5OiBPcmRlclJlcG9zaXRvcnksXHJcbiAgICAgICAgQGluamVjdCh0eXBlcy5QZWRkaUNsb3VkSW50ZWdyYXRpb25Qb3J0KVxyXG4gICAgICAgIHByaXZhdGUgcGVkZGlDbG91ZEludGVncmF0aW9uOiBQZWRkaUNsb3VkSW50ZWdyYXRpb25Qb3J0LFxyXG4gICAgICAgIC8vIEBpbmplY3QodHlwZXMuRXBvY0ludGVncmF0aW9uUG9ydClcclxuICAgICAgICAvLyBwcml2YXRlIGVwb2NJbnRlZ3JhdGlvbjogRXBvY0ludGVncmF0aW9uUG9ydFxyXG4gICAgKSB7IH07XHJcblxyXG4gICAgZ2V0QnlJZCA9IGFzeW5jIChpZDogU3RyaW5nKTogUHJvbWlzZTxhbnk+ID0+IHtcclxuICAgICAgICAvL2l0IG11c3QgbWFrZXMgYSBjb252ZXJ0IGZyb20gbW9kZWwgY29yZSB0byBhIGR0bztcclxuICAgICAgICByZXR1cm4gdGhpcy5vcmRlclJlcG9zaXRvcnkuZ2V0QnlJZChpZClcclxuICAgICAgICAgICAgLnRoZW4ob3JkZXIgPT4gb3JkZXIpXHJcbiAgICAgICAgICAgIC5jYXRjaChlcnIgPT4ge3Rocm93IGVycn0pXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0QWxsID0gYXN5bmMgKCk6IFByb21pc2U8YW55PiA9PiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMub3JkZXJSZXBvc2l0b3J5LmdldEFsbCgpXHJcbiAgICAgICAgICAgIC50aGVuKG9yZGVycyA9PiBvcmRlcnMpXHJcbiAgICAgICAgICAgIC5jYXRjaChlcnIgPT4geyB0aHJvdyBlcnIgfSlcclxuICAgIH1cclxuXHJcbiAgICBzYXZlID0gKG9yZGVyOiBPcmRlck1vZGVsKTogUHJvbWlzZTxTdHJpbmc+ID0+IHtcclxuICAgICAgICAvLyB0cmFuc2xhdGUgdGhlIGNvcmUgbW9kZWxzIGludG8gdGhlIHBhcnRpY3VsYXIgRFRPcyBhbmQgQVBJIGNhbGxzIHJlcXVpcmVkIGJ5IHRoZSBpbnRlZ3JhdGlvblxyXG4gICAgICAgIGNvbnN0IG1vbmdvRFRPID0gbmV3IE9yZGVyRW50aXR5KG9yZGVyKTtcclxuICAgICAgICByZXR1cm4gdGhpcy5vcmRlclJlcG9zaXRvcnkuc2F2ZShtb25nb0RUTylcclxuICAgICAgICAgICAgLnRoZW4oaWQgPT4gaWQpXHJcbiAgICAgICAgICAgIC5jYXRjaChlcnIgPT4ge3Rocm93IGVycn0pXHJcbiAgICB9XHJcblxyXG4gICAgdXBkYXRlID0gYXN5bmMgKGlkOiBTdHJpbmcsIHVwZGF0ZTogQXJyYXk8Q29uc3VtZXJNb2RlbD4pOiBQcm9taXNlPGFueT4gPT4ge1xyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgIHJldHVybiBhd2FpdCB0aGlzLm9yZGVyUmVwb3NpdG9yeS51cGRhdGUoaWQsIHVwZGF0ZSk7XHJcbiAgICAgICAgfSBjYXRjaCAoZXJyKSB7XHJcbiAgICAgICAgICAgIHRocm93IGVyclxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICB1cGRhdGVEb2N1bWVudCA9IGFzeW5jIChkb2M6IGFueSwgdXBkYXRlOiBVcGRhdGVNb2RlbCwgZmFpbHM6IEFycmF5PGFueT4pOiBQcm9taXNlPGFueT4gPT4ge1xyXG4gICAgICAgIGNvbnN0IGZhaWxlZElEcyA9IGZhaWxzLm1hcChmYWlsID0+IGZhaWwuY2xvdWRfaWQpXHJcbiAgICAgICAgY29uc3Qgb2xkQ2FyZHMgPSBkb2MuY29uc3VtZXJzLm1hcChjb25zdW1lciA9PiBjb25zdW1lci5jYXJkKVxyXG5cclxuICAgICAgICBmb3IgKGxldCBjb25zdW1lciBvZiB1cGRhdGUuY29uc3VtZXJzKSB7XHJcbiAgICAgICAgICAgIC8vIDEuIHJlbW92ZSBpdGVtcyAoYW5kIGFzc29jaWF0ZWQgaW5ncmVkaWVudHMpIHRoYXQgZmFpbGVkIHRvIGludGVncmF0ZSB3aXRoIG1hbmFnZW1lbnQgc3lzdGVtIGZyb20gdXBkYXRlXHJcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAoY29uc3VtZXIuaXRlbXMubGVuZ3RoIC0gMSk7IGkgPj0gMDsgaS0tKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoZmFpbGVkSURzLmluY2x1ZGVzKGNvbnN1bWVyLml0ZW1zW2ldLmJhc2VfY2xvdWRfaWQpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3VtZXIuaXRlbXMuc3BsaWNlKGksIDEpIC8vIGl0ZXJhdGUgaW4gcmV2ZXJzZSB0byBrZWVwIGluZGV4IGNvcnJlY3RcclxuICAgICAgICAgICAgICAgICAgICBjb250aW51ZSAvLyBza2lwIGluZ3JlZGllbnQgcGFydCBpZiBpdGVtIGlzIHJlbW92ZWRcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIC8vIGlmIG9ubHkgaW5ncmVkaWVudHMgZmFpbGVkLCByZW1vdmUgdGhlbSBidXQga2VlcCBhc3NvY2lhdGVkIGl0ZW1cclxuICAgICAgICAgICAgICAgIGlmIChjb25zdW1lci5pdGVtc1tpXS5pbmdyZWRpZW50cykge1xyXG4gICAgICAgICAgICAgICAgICAgIGZvciAobGV0IGogPSAoY29uc3VtZXIuaXRlbXNbaV0uaW5ncmVkaWVudHMubGVuZ3RoIC0gMSk7IGogPj0gMDsgai0tKSB7ICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChmYWlsZWRJRHMuaW5jbHVkZXMoY29uc3VtZXIuaXRlbXNbaV0uaW5ncmVkaWVudHNbal0uY2xvdWRfaWQpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdW1lci5pdGVtc1tpXS5pbmdyZWRpZW50cy5zcGxpY2UoaiwgMSkgLy8gaXRlcmF0ZSBpbiByZXZlcnNlIHRvIGtlZXAgaW5kZXggY29ycmVjdFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gaWYgYWxsIGluZ3JlZGllbnRzIGFyZSByZW1vdmVkLCBzZXQgYXJyYXkgdG8gbnVsbFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGNvbnN1bWVyLml0ZW1zW2ldLmluZ3JlZGllbnRzLmxlbmd0aCA9PT0gMCkgeyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3VtZXIuaXRlbXNbaV0uaW5ncmVkaWVudHMgPSBudWxsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLy8gMi4gdXBkYXRlIGRvY3VtZW50XHJcbiAgICAgICAgICAgIGxldCBpbmRleCA9IG9sZENhcmRzLmZpbmRJbmRleChjYXJkID0+IHtcclxuICAgICAgICAgICAgICAgIGlmICh0eXBlb2YoY29uc3VtZXIuY2FyZCkgPT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGNhcmQgPT09IGNvbnN1bWVyLmNhcmRcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGNhcmQgPT09IGNvbnN1bWVyLmNhcmQudG9TdHJpbmcoKVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICBpZiAoaW5kZXggPT09IC0xKSB7XHJcbiAgICAgICAgICAgICAgICAvLyBpZiBjdXJyZW50IGNhcmQgaGFzbid0IGJlZW4gdXNlZCBmb3IgYW55IG9yZGVyc1xyXG4gICAgICAgICAgICAgICAgZG9jLmNvbnN1bWVycy5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICBjYXJkOiBjb25zdW1lci5jYXJkLFxyXG4gICAgICAgICAgICAgICAgICAgIGl0ZW1zOiBjb25zdW1lci5pdGVtc1xyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIC8vIGlmIGNhcmQgaGFzIGFscmVhZHkgYmVlbiB1c2VkLCBqdXN0IGFkZCB0aGUgbmV3IGl0ZW1zIHRvIGl0XHJcbiAgICAgICAgICAgICAgICBkb2MuY29uc3VtZXJzW2luZGV4XS5pdGVtcyA9IGRvYy5jb25zdW1lcnNbaW5kZXhdLml0ZW1zLmNvbmNhdChjb25zdW1lci5pdGVtcylcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBkb2MudXBkYXRlZF9hdCA9IHVwZGF0ZS51cGRhdGVkX2F0XHJcbiAgICAgICAgZG9jLm9yZGVyX3ByaWNlID0gdXBkYXRlLm9yZGVyX3ByaWNlXHJcbiAgICAgICAgLy8gMy4gc2F2ZSBkb2N1bWVudFxyXG4gICAgICAgIGNvbnN0IG1vbmdvRFRPID0gbmV3IE9yZGVyRW50aXR5KGRvYylcclxuICAgICAgICByZXR1cm4gdGhpcy5vcmRlclJlcG9zaXRvcnkudXBkYXRlRG9jdW1lbnQobW9uZ29EVE8pLmNhdGNoKGVyciA9PiB7dGhyb3cgZXJyfSlcclxuICAgIH1cclxuXHJcblxyXG4gICAgZGVsZXRlID0gYXN5bmMgKGlkOiBTdHJpbmcpOiBQcm9taXNlPG9iamVjdD4gPT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLm9yZGVyUmVwb3NpdG9yeS5kZWxldGUoaWQpLmNhdGNoKGVyciA9PiB7dGhyb3cgZXJyfSlcclxuICAgIH1cclxuXHJcbiAgICAvLyBhc3luYyBzZW5kKG9yZGVyOiBPcmRlck1vZGVsKTogUHJvbWlzZTxPYmplY3Q+IHtcclxuICAgIC8vICAgICBjb25zb2xlLmxvZyhcIkNoZWdvdSBuYSBjYW1hZGEgb3V0Ym91bmRcIik7XHJcbiAgICAvLyAgICAgLy8gb2JqZWN0IHRvIGJlIGF1dGhlbnRpY2F0ZVxyXG4gICAgLy8gICAgIGxldCBhdXRoZW50aWNhdGVPYmplY3QgPSB7XHJcbiAgICAvLyAgICAgICAgIC8vIHJlc3RhdXJhbnRfaWRfY2xvdWQ6IDEsXHJcbiAgICAvLyAgICAgICAgIC8vIHRva2VuOiBcIlAyMXYxSTg1dU1cIlxyXG4gICAgLy8gICAgICAgICByZXN0YXVyYW50X2lkX2Nsb3VkOiBvcmRlci5yZXN0YXVyYW50X2lkX2Nsb3VkLFxyXG4gICAgLy8gICAgICAgICB0b2tlbjogb3JkZXJcclxuICAgIC8vICAgICB9XHJcblxyXG5cclxuXHJcbiAgICAvLyAgICAgLy8gY29uc29sZS5sb2coYXV0aGVudGljYXRlT2JqZWN0KTtcclxuICAgIC8vICAgICAvL21ha2UgdGhlIGhhc2ggdG8gY2xvdWQgYXBpIGF1dGhlbnRpY2F0aW9uIFxyXG5cclxuICAgIC8vICAgICBsZXQgaGFzaFRvQXV0aGVudGljYXRlID0gdGhpcy50b2tlbk1hbmFnZXIuZ2VuZXJhdGVUb2tlbihhdXRoZW50aWNhdGVPYmplY3QpO1xyXG4gICAgLy8gICAgIC8vIGNvbnNvbGUubG9nKGhhc2hUb0F1dGhlbnRpY2F0ZSk7XHJcbiAgICAvLyAgICAgLy8gY29uc3QgaGFzaFRvQXV0aGVudGljYXRlOiBTdHJpbmcgO1xyXG5cclxuICAgIC8vICAgICAvL2F1dGhlbnRpY2F0ZVxyXG4gICAgLy8gICAgIGxldCB0b2tlbkF1dGhlbnRpY2F0ZWQgPSBhd2FpdCB0aGlzLnBlZGRpQ2xvdWRJbnRlZ3JhdGlvbi5hdXRoZW50aWNhdGUoaGFzaFRvQXV0aGVudGljYXRlKTtcclxuICAgIC8vICAgICBjb25zb2xlLmxvZyhcInRva2VuIHJldG9ybmFkb1wiKTtcclxuICAgIC8vICAgICBjb25zb2xlLmxvZyh0b2tlbkF1dGhlbnRpY2F0ZWQpO1xyXG5cclxuICAgIC8vICAgICAvL3NlbmQgdGhlIG9yZGVyXHJcbiAgICAvLyAgICAgY29uc29sZS5sb2coXCJFbnZpYW5kbyBwZWRpZG86XCIpXHJcbiAgICAvLyAgICAgbGV0IHNlbnRPcmRlciA9IGF3YWl0IHRoaXMucGVkZGlDbG91ZEludGVncmF0aW9uLnNlbmRPcmRlcihvcmRlciwgdG9rZW5BdXRoZW50aWNhdGVkKTtcclxuICAgIC8vICAgICByZXR1cm4gc2VudE9yZGVyO1xyXG5cclxuICAgICAgICAvLyB0aGlzLm9yZGVyUmVwb3NpdG9yeS5zYXZlKG9yZGVyKTtcclxuXHJcblxyXG5cclxuICAgICAgICAvLyB0aGlzLnBlZGRpQ2xvdWRJbnRlZ3JhdGlvbi5hdXRoZW50aWNhdGlvbigpO1xyXG4gICAgLy8gfVxyXG5cclxuXHJcbiAgICAvLyBzZWxsSXRlbShvcmRlcjogT3JkZXJNb2RlbCk6IFByb21pc2U8UmVzcG9uc2VFcG9jPiB7XHJcbiAgICAvLyAgICAgbGV0IHJlc3VsdDtcclxuICAgIC8vICAgICB2YXIgc2V0U3lzID0gKCkgPT4ge1xyXG4gICAgLy8gICAgICAgICByZXR1cm4gdGhpcy5lcG9jSW50ZWdyYXRpb24uc2lzdGVtYUF1dGgoKTtcclxuICAgIC8vICAgICB9O1xyXG5cclxuICAgIC8vICAgICByZXN1bHQgPSB0aGlzLmVwb2NJbnRlZ3JhdGlvbi50b2tlbkdlbmVyYXRvcihzZXRTeXMpLnRoZW4oKHIpID0+IHtcclxuICAgIC8vICAgICAgICAgcmV0dXJuIHI7XHJcbiAgICAvLyAgICAgfSk7O1xyXG5cclxuICAgIC8vICAgICBjb25zb2xlLmxvZyhyZXN1bHQpO1xyXG4gICAgLy8gICAgIHJldHVybiByZXN1bHQ7XHJcbiAgICAvLyB9XHJcbn0iXX0=
