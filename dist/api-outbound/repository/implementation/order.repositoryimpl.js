"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const order_entity_1 = require("../../entity/order.entity");
const inversify_1 = require("inversify");
let OrderRepositoryImpl = class OrderRepositoryImpl {
    constructor() {
        this.getById = (id) => __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                order_entity_1.OrderEntity.findById(id).lean().exec((err, order) => {
                    if (err) {
                        reject(err);
                    }
                    resolve(order);
                });
            });
        });
        this.getAll = () => __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                order_entity_1.OrderEntity.find().lean().exec((err, orders) => {
                    if (err) {
                        reject(err);
                    }
                    resolve(orders);
                });
            });
        });
        this.save = (order) => {
            return order.save()
                .then(order => order._id)
                .catch(err => { throw err; });
        };
        this.update = (id, update) => __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                // args: condition, update, options, callback
                order_entity_1.OrderEntity
                    .findByIdAndUpdate(id, { consumers: update }, { new: true })
                    .lean().exec((err, order) => {
                    if (err) {
                        reject(err);
                    }
                    resolve(order);
                });
            });
        });
        this.updateDocument = (doc) => __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                order_entity_1.OrderEntity.updateOne({ _id: doc._id }, doc)
                    .lean().exec((err, order) => {
                    if (err) {
                        reject(err);
                    }
                    resolve(order);
                });
            });
        });
        this.delete = (id) => __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                order_entity_1.OrderEntity.findByIdAndDelete(id).exec((err, order) => {
                    if (err) {
                        reject(err);
                    }
                    resolve(order);
                });
            });
        });
    }
};
OrderRepositoryImpl = __decorate([
    inversify_1.injectable()
], OrderRepositoryImpl);
exports.default = OrderRepositoryImpl;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9hcGktb3V0Ym91bmQvcmVwb3NpdG9yeS9pbXBsZW1lbnRhdGlvbi9vcmRlci5yZXBvc2l0b3J5aW1wbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQ0EsNERBQXdEO0FBRXhELHlDQUF1QztBQUl2QyxJQUFxQixtQkFBbUIsR0FBeEMsTUFBcUIsbUJBQW1CO0lBRHhDO1FBR0ksWUFBTyxHQUFHLENBQU8sRUFBVSxFQUFnQixFQUFFO1lBQ3pDLE9BQU8sSUFBSSxPQUFPLENBQUMsQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7Z0JBQ25DLDBCQUFXLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxLQUFLLEVBQUUsRUFBRTtvQkFDaEQsSUFBSSxHQUFHLEVBQUU7d0JBQ0wsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFBO3FCQUNkO29CQUNELE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQTtnQkFDbEIsQ0FBQyxDQUFDLENBQUE7WUFDTixDQUFDLENBQUMsQ0FBQTtRQUNOLENBQUMsQ0FBQSxDQUFBO1FBRUQsV0FBTSxHQUFHLEdBQXVCLEVBQUU7WUFDOUIsT0FBTyxJQUFJLE9BQU8sQ0FBQyxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtnQkFDbkMsMEJBQVcsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsTUFBTSxFQUFFLEVBQUU7b0JBQzNDLElBQUksR0FBRyxFQUFFO3dCQUNMLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQTtxQkFDZDtvQkFDRCxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUE7Z0JBQ25CLENBQUMsQ0FBQyxDQUFBO1lBQ04sQ0FBQyxDQUFDLENBQUE7UUFDTixDQUFDLENBQUEsQ0FBQTtRQUVELFNBQUksR0FBRyxDQUFDLEtBQWUsRUFBbUIsRUFBRTtZQUN4QyxPQUFPLEtBQUssQ0FBQyxJQUFJLEVBQUU7aUJBQ2QsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQztpQkFDeEIsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFLEdBQUUsTUFBTSxHQUFHLENBQUEsQ0FBQSxDQUFDLENBQUMsQ0FBQTtRQUNsQyxDQUFDLENBQUE7UUFFRCxXQUFNLEdBQUcsQ0FBTyxFQUFVLEVBQUUsTUFBNEIsRUFBZ0IsRUFBRTtZQUN0RSxPQUFPLElBQUksT0FBTyxDQUFDLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFO2dCQUNuQyw2Q0FBNkM7Z0JBQzdDLDBCQUFXO3FCQUNOLGlCQUFpQixDQUFDLEVBQUUsRUFBRSxFQUFDLFNBQVMsRUFBRSxNQUFNLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBRSxJQUFJLEVBQUMsQ0FBQztxQkFDdkQsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLEtBQUssRUFBRSxFQUFFO29CQUN4QixJQUFJLEdBQUcsRUFBRTt3QkFDTCxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUE7cUJBQ2Q7b0JBQ0QsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFBO2dCQUNsQixDQUFDLENBQUMsQ0FBQTtZQUNOLENBQUMsQ0FBQyxDQUFBO1FBQ1YsQ0FBQyxDQUFBLENBQUE7UUFFRCxtQkFBYyxHQUFHLENBQU8sR0FBUSxFQUFnQixFQUFFO1lBQzlDLE9BQU8sSUFBSSxPQUFPLENBQUMsQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7Z0JBQ25DLDBCQUFXLENBQUMsU0FBUyxDQUFDLEVBQUUsR0FBRyxFQUFFLEdBQUcsQ0FBQyxHQUFHLEVBQUUsRUFBRSxHQUFHLENBQUM7cUJBQ3ZDLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxLQUFLLEVBQUUsRUFBRTtvQkFDeEIsSUFBSSxHQUFHLEVBQUU7d0JBQ0wsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFBO3FCQUNkO29CQUNELE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQTtnQkFDbEIsQ0FBQyxDQUFDLENBQUE7WUFDVixDQUFDLENBQUMsQ0FBQTtRQUNOLENBQUMsQ0FBQSxDQUFBO1FBRUQsV0FBTSxHQUFHLENBQU8sRUFBVSxFQUFtQixFQUFFO1lBQzNDLE9BQU8sSUFBSSxPQUFPLENBQUMsQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7Z0JBQ25DLDBCQUFXLENBQUMsaUJBQWlCLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLEtBQUssRUFBRSxFQUFFO29CQUNsRCxJQUFJLEdBQUcsRUFBRTt3QkFDTCxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUE7cUJBQ2Q7b0JBQ0QsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFBO2dCQUNsQixDQUFDLENBQUMsQ0FBQTtZQUNOLENBQUMsQ0FBQyxDQUFBO1FBQ04sQ0FBQyxDQUFBLENBQUE7SUFDTCxDQUFDO0NBQUEsQ0FBQTtBQWxFb0IsbUJBQW1CO0lBRHZDLHNCQUFVLEVBQUU7R0FDUSxtQkFBbUIsQ0FrRXZDO2tCQWxFb0IsbUJBQW1CIiwiZmlsZSI6ImFwaS1vdXRib3VuZC9yZXBvc2l0b3J5L2ltcGxlbWVudGF0aW9uL29yZGVyLnJlcG9zaXRvcnlpbXBsLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IE9yZGVyUmVwb3NpdG9yeSBmcm9tIFwiLi4vb3JkZXIucmVwb3NpdG9yeVwiO1xyXG5pbXBvcnQgeyBPcmRlckVudGl0eSB9IGZyb20gXCIuLi8uLi9lbnRpdHkvb3JkZXIuZW50aXR5XCI7XHJcbmltcG9ydCB7IENvbnN1bWVyTW9kZWwgfSBmcm9tIFwiLi4vLi4vLi4vYXBpLWNvcmUvY29tbW9ucy9tb2RlbC9vcmRlci5tb2RlbFwiXHJcbmltcG9ydCB7IGluamVjdGFibGUgfSBmcm9tIFwiaW52ZXJzaWZ5XCI7XHJcbmltcG9ydCB7IERvY3VtZW50IH0gZnJvbSBcIm1vbmdvb3NlXCI7XHJcblxyXG5AaW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIE9yZGVyUmVwb3NpdG9yeUltcGwgaW1wbGVtZW50cyBPcmRlclJlcG9zaXRvcnkge1xyXG4gICAgXHJcbiAgICBnZXRCeUlkID0gYXN5bmMgKGlkOiBTdHJpbmcpOiBQcm9taXNlPGFueT4gPT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcbiAgICAgICAgICAgIE9yZGVyRW50aXR5LmZpbmRCeUlkKGlkKS5sZWFuKCkuZXhlYygoZXJyLCBvcmRlcikgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKGVycikge1xyXG4gICAgICAgICAgICAgICAgICAgIHJlamVjdChlcnIpXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICByZXNvbHZlKG9yZGVyKVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0QWxsID0gYXN5bmMgKCk6IFByb21pc2U8YW55PiA9PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcclxuICAgICAgICAgICAgT3JkZXJFbnRpdHkuZmluZCgpLmxlYW4oKS5leGVjKChlcnIsIG9yZGVycykgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKGVycikge1xyXG4gICAgICAgICAgICAgICAgICAgIHJlamVjdChlcnIpXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICByZXNvbHZlKG9yZGVycylcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICB9KVxyXG4gICAgfVxyXG4gICAgXHJcbiAgICBzYXZlID0gKG9yZGVyOiBEb2N1bWVudCk6IFByb21pc2U8U3RyaW5nPiA9PiB7XHJcbiAgICAgICAgcmV0dXJuIG9yZGVyLnNhdmUoKVxyXG4gICAgICAgICAgICAudGhlbihvcmRlciA9PiBvcmRlci5faWQpXHJcbiAgICAgICAgICAgIC5jYXRjaChlcnIgPT4ge3Rocm93IGVycn0pXHJcbiAgICB9XHJcblxyXG4gICAgdXBkYXRlID0gYXN5bmMgKGlkOiBTdHJpbmcsIHVwZGF0ZTogQXJyYXk8Q29uc3VtZXJNb2RlbD4pOiBQcm9taXNlPGFueT4gPT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcbiAgICAgICAgICAgIC8vIGFyZ3M6IGNvbmRpdGlvbiwgdXBkYXRlLCBvcHRpb25zLCBjYWxsYmFja1xyXG4gICAgICAgICAgICBPcmRlckVudGl0eVxyXG4gICAgICAgICAgICAgICAgLmZpbmRCeUlkQW5kVXBkYXRlKGlkLCB7Y29uc3VtZXJzOiB1cGRhdGV9LCB7bmV3OiB0cnVlfSlcclxuICAgICAgICAgICAgICAgIC5sZWFuKCkuZXhlYygoZXJyLCBvcmRlcikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChlcnIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVqZWN0KGVycilcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZShvcmRlcilcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgdXBkYXRlRG9jdW1lbnQgPSBhc3luYyAoZG9jOiBhbnkpOiBQcm9taXNlPGFueT4gPT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcbiAgICAgICAgICAgIE9yZGVyRW50aXR5LnVwZGF0ZU9uZSh7IF9pZDogZG9jLl9pZCB9LCBkb2MpXHJcbiAgICAgICAgICAgICAgICAubGVhbigpLmV4ZWMoKGVyciwgb3JkZXIpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoZXJyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlamVjdChlcnIpXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmUob3JkZXIpXHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgZGVsZXRlID0gYXN5bmMgKGlkOiBTdHJpbmcpOiBQcm9taXNlPG9iamVjdD4gPT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcbiAgICAgICAgICAgIE9yZGVyRW50aXR5LmZpbmRCeUlkQW5kRGVsZXRlKGlkKS5leGVjKChlcnIsIG9yZGVyKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoZXJyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVqZWN0KGVycilcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHJlc29sdmUob3JkZXIpXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgfSlcclxuICAgIH1cclxufSJdfQ==
