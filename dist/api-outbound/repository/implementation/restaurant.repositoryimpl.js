"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const inversify_1 = require("inversify");
const restaurant_entity_1 = require("../../entity/restaurant.entity");
let RestaurantRepositoryImpl = class RestaurantRepositoryImpl {
    save(restaurantDTO) {
        return restaurantDTO.save()
            .then(restaurant => {
            return {
                restaurant_cloud_id: restaurant.restaurant_cloud_id,
                token: restaurant.token
            };
        })
            .catch(err => { throw err; });
    }
    delete() {
        return new Promise((resolve, reject) => {
            restaurant_entity_1.RestaurantEntity.deleteMany({}).lean().exec((err, res) => {
                if (err) {
                    reject(err);
                }
                resolve(res);
            });
        });
    }
    get() {
        return new Promise((resolve, reject) => {
            restaurant_entity_1.RestaurantEntity.find().lean().exec((err, restaurants) => {
                if (err) {
                    reject(err);
                }
                resolve(restaurants[0]);
            });
        });
    }
};
RestaurantRepositoryImpl = __decorate([
    inversify_1.injectable()
], RestaurantRepositoryImpl);
exports.default = RestaurantRepositoryImpl;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9hcGktb3V0Ym91bmQvcmVwb3NpdG9yeS9pbXBsZW1lbnRhdGlvbi9yZXN0YXVyYW50LnJlcG9zaXRvcnlpbXBsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQ0EseUNBQXVDO0FBQ3ZDLHNFQUFrRTtBQUdsRSxJQUFxQix3QkFBd0IsR0FBN0MsTUFBcUIsd0JBQXdCO0lBRXpDLElBQUksQ0FBQyxhQUFrQjtRQUNuQixPQUFPLGFBQWEsQ0FBQyxJQUFJLEVBQUU7YUFDdEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFO1lBQUcsT0FBTztnQkFDekIsbUJBQW1CLEVBQUUsVUFBVSxDQUFDLG1CQUFtQjtnQkFDbkQsS0FBSyxFQUFFLFVBQVUsQ0FBQyxLQUFLO2FBQzFCLENBQUE7UUFBQSxDQUFDLENBQUM7YUFDRixLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsR0FBRyxNQUFNLEdBQUcsQ0FBQSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3JDLENBQUM7SUFFRCxNQUFNO1FBQ0YsT0FBTyxJQUFJLE9BQU8sQ0FBQyxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUNuQyxvQ0FBZ0IsQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxFQUFFO2dCQUNyRCxJQUFJLEdBQUcsRUFBRTtvQkFDTCxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUE7aUJBQ2Q7Z0JBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFBO1lBQ2hCLENBQUMsQ0FBQyxDQUFBO1FBQ04sQ0FBQyxDQUFDLENBQUE7SUFDTixDQUFDO0lBRUQsR0FBRztRQUNDLE9BQU8sSUFBSSxPQUFPLENBQUMsQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFDbkMsb0NBQWdCLENBQUMsSUFBSSxFQUFFLENBQUMsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLFdBQVcsRUFBRSxFQUFFO2dCQUNyRCxJQUFJLEdBQUcsRUFBRTtvQkFDTCxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUE7aUJBQ2Q7Z0JBQ0QsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBO1lBQzNCLENBQUMsQ0FBQyxDQUFBO1FBQ04sQ0FBQyxDQUFDLENBQUE7SUFDTixDQUFDO0NBRUosQ0FBQTtBQWpDb0Isd0JBQXdCO0lBRDVDLHNCQUFVLEVBQUU7R0FDUSx3QkFBd0IsQ0FpQzVDO2tCQWpDb0Isd0JBQXdCIiwiZmlsZSI6ImFwaS1vdXRib3VuZC9yZXBvc2l0b3J5L2ltcGxlbWVudGF0aW9uL3Jlc3RhdXJhbnQucmVwb3NpdG9yeWltcGwuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVzdGF1cmFudFJlcG9zaXRvcnkgZnJvbSBcIi4uL3Jlc3RhdXJhbnQucmVwb3NpdG9yeVwiO1xyXG5pbXBvcnQgeyBpbmplY3RhYmxlIH0gZnJvbSBcImludmVyc2lmeVwiO1xyXG5pbXBvcnQgeyBSZXN0YXVyYW50RW50aXR5IH0gZnJvbSBcIi4uLy4uL2VudGl0eS9yZXN0YXVyYW50LmVudGl0eVwiO1xyXG5cclxuQGluamVjdGFibGUoKVxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBSZXN0YXVyYW50UmVwb3NpdG9yeUltcGwgaW1wbGVtZW50cyBSZXN0YXVyYW50UmVwb3NpdG9yeSB7XHJcblxyXG4gICAgc2F2ZShyZXN0YXVyYW50RFRPOiBhbnkpOiBQcm9taXNlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiByZXN0YXVyYW50RFRPLnNhdmUoKVxyXG4gICAgICAgICAgICAudGhlbihyZXN0YXVyYW50ID0+IHsgcmV0dXJuIHsgXHJcbiAgICAgICAgICAgICAgICByZXN0YXVyYW50X2Nsb3VkX2lkOiByZXN0YXVyYW50LnJlc3RhdXJhbnRfY2xvdWRfaWQsXHJcbiAgICAgICAgICAgICAgICB0b2tlbjogcmVzdGF1cmFudC50b2tlblxyXG4gICAgICAgICAgICB9fSlcclxuICAgICAgICAgICAgLmNhdGNoKGVyciA9PiB7IHRocm93IGVyciB9KTtcclxuICAgIH1cclxuXHJcbiAgICBkZWxldGUoKTogUHJvbWlzZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG4gICAgICAgICAgICBSZXN0YXVyYW50RW50aXR5LmRlbGV0ZU1hbnkoe30pLmxlYW4oKS5leGVjKChlcnIsIHJlcykgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKGVycikge1xyXG4gICAgICAgICAgICAgICAgICAgIHJlamVjdChlcnIpXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICByZXNvbHZlKHJlcylcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICB9KVxyXG4gICAgfSBcclxuXHJcbiAgICBnZXQoKTogUHJvbWlzZTxvYmplY3Q+IHtcclxuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG4gICAgICAgICAgICBSZXN0YXVyYW50RW50aXR5LmZpbmQoKS5sZWFuKCkuZXhlYygoZXJyLCByZXN0YXVyYW50cykgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKGVycikge1xyXG4gICAgICAgICAgICAgICAgICAgIHJlamVjdChlcnIpXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICByZXNvbHZlKHJlc3RhdXJhbnRzWzBdKVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG59Il19
