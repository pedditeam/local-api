"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const jwt = __importStar(require("jsonwebtoken"));
const morgan_1 = require("morgan");
const fs = require("fs");
class GeradorToken {
    constructor() {
        this.generateToken = (payload) => {
            const signOptions = {
                issuer: process.env.TOKEN_ISSUER,
                subject: process.env.TOKEN_SUBJECT,
                audience: process.env.TOKEN_AUDIENCE,
                expiresIn: '10m',
                algorithm: 'RS256'
            };
            try {
                return 'Bearer ' + jwt.sign(payload, this.privateKey, signOptions);
            }
            catch (error) {
                console.log(error);
            }
            return morgan_1.token;
        };
        this.privateKey = fs.readFileSync('./jwt/private.key', 'utf8');
        this.publicKey = fs.readFileSync('./jwt/public.key', 'utf8');
    }
    /**
     * @param payload dados que deseja codificar para base64.
     */
    gerarToken(payload) {
        // let token = jwt.sign(payload, process.env.SECRET, {expiresIn: 8640});
        // return token;
    }
    /**
     *
     * @param token a ser decodificado.
     */
    decodificaToken(token) {
        let decoded = jwt.decode(token, { complete: true });
        return decoded;
    }
    verificarToken(token) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve) => {
                // if(!token) throw new Error('Token Inválido!');
                // jwt.verify(token, process.env.EPOC_KEY, (err, decoded) => {
                // if(err) throw err;
                // return resolve(decoded);
                // });
            });
        });
    }
}
exports.GeradorToken = GeradorToken;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9hcGktb3V0Ym91bmQvdXRpbC90b2tlbi1jb25maWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxrREFBb0M7QUFDcEMsbUNBQStCO0FBQy9CLHlCQUEwQjtBQUMxQixNQUFhLFlBQVk7SUFLeEI7UUFzQk8sa0JBQWEsR0FBRyxDQUFDLE9BQVksRUFBRSxFQUFFO1lBQ2pDLE1BQU0sV0FBVyxHQUFHO2dCQUNoQixNQUFNLEVBQUUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZO2dCQUNoQyxPQUFPLEVBQUUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhO2dCQUNsQyxRQUFRLEVBQUUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjO2dCQUNwQyxTQUFTLEVBQUUsS0FBSztnQkFDaEIsU0FBUyxFQUFFLE9BQU87YUFDM0IsQ0FBQTtZQUNELElBQUk7Z0JBQ0gsT0FBTyxTQUFTLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLFVBQVUsRUFBRSxXQUFXLENBQUMsQ0FBQzthQUNuRTtZQUFDLE9BQU8sS0FBSyxFQUFFO2dCQUNmLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDbkI7WUFDSyxPQUFPLGNBQUssQ0FBQztRQUNqQixDQUFDLENBQUE7UUFuQ0gsSUFBSSxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUMsWUFBWSxDQUFDLG1CQUFtQixFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQy9ELElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDLFlBQVksQ0FBQyxrQkFBa0IsRUFBRSxNQUFNLENBQUMsQ0FBQztJQUM5RCxDQUFDO0lBRUU7O09BRUc7SUFDSSxVQUFVLENBQUMsT0FBWTtRQUMxQix3RUFBd0U7UUFDeEUsZ0JBQWdCO0lBQ3BCLENBQUM7SUFFSjs7O09BR0c7SUFDSSxlQUFlLENBQUMsS0FBVTtRQUNoQyxJQUFJLE9BQU8sR0FBRyxHQUFHLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxFQUFDLFFBQVEsRUFBQyxJQUFJLEVBQUMsQ0FBQyxDQUFDO1FBQ2pELE9BQU8sT0FBTyxDQUFDO0lBQ2hCLENBQUM7SUFtQlksY0FBYyxDQUFDLEtBQWE7O1lBQ3hDLE9BQU8sSUFBSSxPQUFPLENBQUMsQ0FBQyxPQUFPLEVBQUUsRUFBRTtnQkFDOUIsaURBQWlEO2dCQUNqRCw4REFBOEQ7Z0JBQzdELHFCQUFxQjtnQkFDckIsMkJBQTJCO2dCQUM1QixNQUFNO1lBQ1AsQ0FBQyxDQUFDLENBQUM7UUFDSixDQUFDO0tBQUE7Q0FDRDtBQXJERCxvQ0FxREMiLCJmaWxlIjoiYXBpLW91dGJvdW5kL3V0aWwvdG9rZW4tY29uZmlnLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgand0IGZyb20gJ2pzb253ZWJ0b2tlbic7XHJcbmltcG9ydCB7IHRva2VuIH0gZnJvbSAnbW9yZ2FuJztcclxuaW1wb3J0IGZzID0gcmVxdWlyZSgnZnMnKTtcclxuZXhwb3J0IGNsYXNzIEdlcmFkb3JUb2tlbiB7XHJcblxyXG5cdHByaXZhdGUgcHJpdmF0ZUtleTogYW55O1xyXG5cdHByaXZhdGUgcHVibGljS2V5OiBhbnk7XHJcblxyXG5cdGNvbnN0cnVjdG9yKCl7XHJcblx0XHR0aGlzLnByaXZhdGVLZXkgPSBmcy5yZWFkRmlsZVN5bmMoJy4vand0L3ByaXZhdGUua2V5JywgJ3V0ZjgnKTtcclxuXHRcdHRoaXMucHVibGljS2V5ID0gZnMucmVhZEZpbGVTeW5jKCcuL2p3dC9wdWJsaWMua2V5JywgJ3V0ZjgnKTtcclxuXHR9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBAcGFyYW0gcGF5bG9hZCBkYWRvcyBxdWUgZGVzZWphIGNvZGlmaWNhciBwYXJhIGJhc2U2NC5cclxuICAgICAqL1xyXG4gICAgcHVibGljIGdlcmFyVG9rZW4ocGF5bG9hZDogYW55KSB7XHJcbiAgICAgICAgLy8gbGV0IHRva2VuID0gand0LnNpZ24ocGF5bG9hZCwgcHJvY2Vzcy5lbnYuU0VDUkVULCB7ZXhwaXJlc0luOiA4NjQwfSk7XHJcbiAgICAgICAgLy8gcmV0dXJuIHRva2VuO1xyXG4gICAgfVxyXG4gICAgXHJcblx0LyoqXHJcblx0ICpcclxuXHQgKiBAcGFyYW0gdG9rZW4gYSBzZXIgZGVjb2RpZmljYWRvLlxyXG5cdCAqL1xyXG5cdHB1YmxpYyBkZWNvZGlmaWNhVG9rZW4odG9rZW46IGFueSkge1xyXG5cdFx0bGV0IGRlY29kZWQgPSBqd3QuZGVjb2RlKHRva2VuLCB7Y29tcGxldGU6dHJ1ZX0pO1xyXG5cdFx0cmV0dXJuIGRlY29kZWQ7XHJcblx0fVxyXG5cclxuXHRwdWJsaWMgZ2VuZXJhdGVUb2tlbiA9IChwYXlsb2FkOiBhbnkpID0+IHtcclxuICAgICAgICBjb25zdCBzaWduT3B0aW9ucyA9IHtcclxuICAgICAgICAgICAgaXNzdWVyOiBwcm9jZXNzLmVudi5UT0tFTl9JU1NVRVIsXHJcbiAgICAgICAgICAgIHN1YmplY3Q6IHByb2Nlc3MuZW52LlRPS0VOX1NVQkpFQ1QsXHJcbiAgICAgICAgICAgIGF1ZGllbmNlOiBwcm9jZXNzLmVudi5UT0tFTl9BVURJRU5DRSxcclxuICAgICAgICAgICAgZXhwaXJlc0luOiAnMTBtJyxcclxuICAgICAgICAgICAgYWxnb3JpdGhtOiAnUlMyNTYnXHJcblx0XHR9XHJcblx0XHR0cnkge1xyXG5cdFx0XHRyZXR1cm4gJ0JlYXJlciAnICsgand0LnNpZ24ocGF5bG9hZCwgdGhpcy5wcml2YXRlS2V5LCBzaWduT3B0aW9ucyk7XHJcblx0XHR9IGNhdGNoIChlcnJvcikge1xyXG5cdFx0XHRjb25zb2xlLmxvZyhlcnJvcik7XHJcblx0XHR9XHJcbiAgICAgICAgcmV0dXJuIHRva2VuO1xyXG4gICAgfVxyXG5cclxuXHJcblx0cHVibGljIGFzeW5jIHZlcmlmaWNhclRva2VuKHRva2VuOiBzdHJpbmcpIHtcclxuXHRcdHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSkgPT4ge1xyXG5cdFx0XHQvLyBpZighdG9rZW4pIHRocm93IG5ldyBFcnJvcignVG9rZW4gSW52w6FsaWRvIScpO1xyXG5cdFx0XHQvLyBqd3QudmVyaWZ5KHRva2VuLCBwcm9jZXNzLmVudi5FUE9DX0tFWSwgKGVyciwgZGVjb2RlZCkgPT4ge1xyXG5cdFx0XHRcdC8vIGlmKGVycikgdGhyb3cgZXJyO1xyXG5cdFx0XHRcdC8vIHJldHVybiByZXNvbHZlKGRlY29kZWQpO1xyXG5cdFx0XHQvLyB9KTtcclxuXHRcdH0pO1xyXG5cdH1cclxufVxyXG4iXX0=
