"use strict";
/**
 * Class containing the methods for integrating with the Caixa
 * Rapido API.
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const mssql = require('mssql');
const inversify_1 = require("inversify");
let CaixaRapidoIntegration = class CaixaRapidoIntegration {
    constructor(dbURL) {
        /**
         * @summary: method for connecting to Caixa Rapido's DB for the restaurant.
         */
        this.connect = () => __awaiter(this, void 0, void 0, function* () {
            // retrieve dbURL and ingredients category name from restaurant info
            this.pool = new mssql.ConnectionPool(this.dbURL);
            yield this.pool.connect();
        });
        /**
         * @summary: tests whether object has been connected to DB for integration.
         */
        this.isConnected = () => {
            return this.pool !== undefined;
        };
        this.dbURL = dbURL;
        this.connect();
        this.req = this.pool.request();
    }
    /**
     * @summary: method for creating a new order assocaited with a given table.
     */
    openTable(table) {
        /*
        Procedure para abrir mesa:
        Nome: sp_AbreMesa
        Parametros: @NroMesa		-> Numero da Mesa ou Comanda
                    @NroCartao		-> Fixo 0
                    @Nome			-> Fixo ''
                    @IPTerminal		-> IP ou nome do dispositivo que esta enviando o pedido
        ??          @VendorID		-> Nro de série do dispositivo
                    @Versao			-> Fixo ''
                    @Origem			-> Fixo 'iOS'
        Retorno: Codigo e Mensagem
        Codigo = 0 -> Sucesso
        Codigo <> 0 -> Ver Mensagem de erro

        Exemplo de Uso:
        sp_AbreMesa @NroMesa = 3, @NroCartao = 0, @Nome = '', @IPTerminal = 'chris', @VendorID = '1a280798a8c9989c', @Versao = '', @Origem = 'IOS'
        */
        const queryText = 'EXEC sp_AbreMesa @NroMesa = @nro_mesa, @NroCartao = 0, @Nome = \'\', @IPTerminal = @ip, @VendorID = @vendor_id, @Versao = \'\', @Origem = "IOS"';
        // define parameters
        this.req.input('nro_mesa', mssql.SmallInt, table);
        this.req.input('ip', mssql.NVarChar, 'Tablet - Mesa' + table);
        this.req.input('vendor_id', mssql.NVarChar, 'Tablet - Mesa' + table);
        // run query
        return this.req.query(queryText)
            .then(result => {
            if (result.recordset[0].Retorno !== 0) {
                throw new Error(result.recordset[0].Mensagem);
            }
            console.log('Bill open.');
        })
            .catch(err => { throw err; });
    }
    /**
     * @summary: method for adding a new product to the table's bill.
     */
    addProductToTable(product) {
        /*
        Procedure para adicionar um produto na mesa:
        Nome: sp_AdicionaProduto
        Parametros: @NroMesa		-> Numero da Mesa ou Comanda
                    @NroCartao		-> Fixo 0
                    @Nome			-> Fixo ''
                    @IPTerminal		-> IP ou nome do dispositivo que esta enviando o pedido
                    @IDProduto		-> ID do produto(não é o código, é o campo ID Produto)
                    @Qtde			-> Informar a Qtde
                    @ObservacaoItem	-> No caso do FIT, informar ''
                    @VendedorID 	-> Fixo ''

        Retorno: Codigo e Mensagem
        Codigo = 0 -> Sucesso
        Codigo <> 0 -> Ver Mensagem de erro

        Exemplo de Uso:
        sp_AdicionaProduto @NroMesa = 3, @NroCartao = 0, @Nome = '', @IPTerminal = 'chris', @IDProduto = 14, @Qtde = 1.0, @ObservacaoItem = '', @VendedorID = ''
        */
        const queryText = 'EXEC sp_AdicionaProduto @NroMesa = @nro_mesa, @NroCartao = 0, @Nome = \'\', @IPTerminal = @ip, @IDProduto = 14, @Qtde = @qtde, @ObservacaoItem = \'\', @VendedorID = \'\'';
        // define parameters
        this.req.input('nro_mesa', mssql.SmallInt, product.table);
        this.req.input('ip', mssql.NVarChar, 'Tablet - Mesa' + product.table);
        this.req.input('id_produto', mssql.NVarChar, product.mgmtID);
        this.req.input('qtde', mssql.SmallInt, product.quantity);
        // run query
        return this.req.query(queryText)
            .then(result => {
            if (result.recordset[0].Retorno === 2) {
                // only this error is not associated with a message
                throw new Error('Product ID not found in Caixa Rapido');
            }
            else if (result.recordset[0].Retorno !== 0) {
                throw new Error(result.recordset[0].Mensagem);
            }
            console.log('Product added.');
        })
            .catch(err => { throw err; });
    }
    /**
     * @summary: method for finalizing the bill associated with a table.
     */
    closeTable(table) {
        /*
        Procedure para finalizar um pedido:
        Nome: sp_FinalizarPedido
        Parametros: @NroMesa		-> Numero da Mesa ou Comanda
                    @NroCartao		-> Fixo 0

        Retorno: Codigo e Mensagem
        Codigo = 0 -> Sucesso
        Codigo <> 0 -> Ver Mensagem de erro

        Exemplo de Uso:
        sp_FinalizarPedido @NroMesa = 3, @NroCartao = 0
        */
        const queryText = 'EXEC sp_FinalizarPedido @NroMesa = @nro_mesa, @NroCartao = 0';
        // define parameters
        this.req.input('nro_mesa', mssql.SmallInt, table);
        // run query
        return this.req.query(queryText)
            .then(result => {
            if (result.recordset[0].Retorno !== 0) {
                throw new Error('Invalid arguments.');
            }
            console.log('Bill closed.');
        })
            .catch(err => { throw err; });
    }
    /**
     * @summary: method for checking the bill of a table that is still ordering.
     */
    preBill(table, service, guests, print) {
        /*
        Procedure para visualizar a preconta
        Nome: sp_WSPreConta
        Parametros: @NroMesa		-> Numero da Mesa ou Comanda
                    @NroCartao		-> Fixo 0
                    @Servico		-> Indicador se vai ou não cobrar serviço(0 - não cobrar, ou 1 - cobrar)
                    @NroPessoas		-> Nro de pessoas na pesa
                    @Visualizar		-> Indicador se imprime (0) ou somente visualiza (1)

        Retorno: Codigo e Mensagem
        Codigo = 0 -> Sucesso
        Codigo <> 0 -> Ver Mensagem de erro

        Exemplo de Uso:
        sp_WSPreConta @NroMesa = 3, @NroCartao = 0, @Servico = 1, @NroPessoas = 1, @Visualizar = 1
        */
        const queryText = 'EXEC sp_WSPreConta @NroMesa = @nro_mesa, @NroCartao = 0, @Servico = @servico, @NroPessoas = @nro_pessoas, @Visualizar = @visualizar';
        // define parameters
        this.req.input('nro_mesa', mssql.SmallInt, table);
        this.req.input('servico', mssql.SmallInt, service);
        this.req.input('nro_pessoas', mssql.SmallInt, guests);
        this.req.input('visualizar', mssql.SmallInt, print);
        // run query
        return this.req.query(queryText)
            .then(result => {
            return result.recordset;
        })
            .catch(err => { throw err; });
    }
};
CaixaRapidoIntegration = __decorate([
    inversify_1.injectable(),
    __metadata("design:paramtypes", [String])
], CaixaRapidoIntegration);
exports.default = CaixaRapidoIntegration;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9hcGktb3V0Ym91bmQvaW50ZWdyYXRpb24vY2FpeGEtcmFwaWRvLWFwaS9pbXBsZW1lbnRhdGlvbi9jYWl4YS1yYXBpZG8uaW50ZWdyYXRpb24tYWRhcHRlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUE7OztHQUdHOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRUgsTUFBTSxLQUFLLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFBO0FBQzlCLHlDQUF1QztBQUt2QyxJQUFxQixzQkFBc0IsR0FBM0MsTUFBcUIsc0JBQXNCO0lBSzFDLFlBQVksS0FBYTtRQU16Qjs7V0FFRztRQUNJLFlBQU8sR0FBRyxHQUFTLEVBQUU7WUFDM0Isb0VBQW9FO1lBQ3BFLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxLQUFLLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQTtZQUNoRCxNQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUE7UUFDMUIsQ0FBQyxDQUFBLENBQUE7UUFFRDs7V0FFRztRQUNJLGdCQUFXLEdBQUcsR0FBRyxFQUFFO1lBQ3pCLE9BQU8sSUFBSSxDQUFDLElBQUksS0FBSyxTQUFTLENBQUE7UUFDL0IsQ0FBQyxDQUFBO1FBbkJBLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFBO1FBQ2xCLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQTtRQUNkLElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQTtJQUMvQixDQUFDO0lBa0JFOztPQUVHO0lBQ0MsU0FBUyxDQUFDLEtBQWE7UUFDdkI7Ozs7Ozs7Ozs7Ozs7Ozs7VUFnQkU7UUFDUixNQUFNLFNBQVMsR0FBRyxpSkFBaUosQ0FBQTtRQUNuSyxvQkFBb0I7UUFDcEIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsVUFBVSxFQUFFLEtBQUssQ0FBQyxRQUFRLEVBQUUsS0FBSyxDQUFDLENBQUE7UUFDakQsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxRQUFRLEVBQUUsZUFBZSxHQUFHLEtBQUssQ0FBQyxDQUFBO1FBQzdELElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLFdBQVcsRUFBRSxLQUFLLENBQUMsUUFBUSxFQUFFLGVBQWUsR0FBRyxLQUFLLENBQUMsQ0FBQTtRQUNwRSxZQUFZO1FBQ1osT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUM7YUFDOUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFO1lBQ2QsSUFBSSxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sS0FBSyxDQUFDLEVBQUU7Z0JBQ3RDLE1BQU0sSUFBSSxLQUFLLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQTthQUM3QztZQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUE7UUFDMUIsQ0FBQyxDQUFDO2FBQ0QsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFLEdBQUcsTUFBTSxHQUFHLENBQUEsQ0FBQyxDQUFDLENBQUMsQ0FBQTtJQUM5QixDQUFDO0lBRUU7O09BRUc7SUFDQyxpQkFBaUIsQ0FBQyxPQUFtQjtRQUNyQzs7Ozs7Ozs7Ozs7Ozs7Ozs7O1VBa0JFO1FBQ1IsTUFBTSxTQUFTLEdBQUcsMktBQTJLLENBQUE7UUFDN0wsb0JBQW9CO1FBQ3BCLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUMsUUFBUSxFQUFFLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUN6RCxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLFFBQVEsRUFBRSxlQUFlLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFBO1FBQ3JFLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLFlBQVksRUFBRSxLQUFLLENBQUMsUUFBUSxFQUFFLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQTtRQUM1RCxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLFFBQVEsRUFBRSxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUE7UUFDeEQsWUFBWTtRQUNaLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDO2FBQzlCLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRTtZQUNkLElBQUksTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEtBQUssQ0FBQyxFQUFFO2dCQUN0QyxtREFBbUQ7Z0JBQ25ELE1BQU0sSUFBSSxLQUFLLENBQUMsc0NBQXNDLENBQUMsQ0FBQTthQUN2RDtpQkFDSSxJQUFJLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxLQUFLLENBQUMsRUFBRTtnQkFDM0MsTUFBTSxJQUFJLEtBQUssQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFBO2FBQzdDO1lBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFBO1FBQzlCLENBQUMsQ0FBQzthQUNELEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRSxHQUFHLE1BQU0sR0FBRyxDQUFBLENBQUMsQ0FBQyxDQUFDLENBQUE7SUFDOUIsQ0FBQztJQUVFOztPQUVHO0lBQ0MsVUFBVSxDQUFDLEtBQWE7UUFDeEI7Ozs7Ozs7Ozs7OztVQVlFO1FBQ1IsTUFBTSxTQUFTLEdBQUcsOERBQThELENBQUE7UUFDaEYsb0JBQW9CO1FBQ3BCLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUMsUUFBUSxFQUFFLEtBQUssQ0FBQyxDQUFBO1FBQ2pELFlBQVk7UUFDWixPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQzthQUM5QixJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUU7WUFDZCxJQUFJLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxLQUFLLENBQUMsRUFBRTtnQkFDdEMsTUFBTSxJQUFJLEtBQUssQ0FBQyxvQkFBb0IsQ0FBQyxDQUFBO2FBQ3JDO1lBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQTtRQUM1QixDQUFDLENBQUM7YUFDRCxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsR0FBRyxNQUFNLEdBQUcsQ0FBQSxDQUFDLENBQUMsQ0FBQyxDQUFBO0lBQzlCLENBQUM7SUFFRTs7T0FFRztJQUNDLE9BQU8sQ0FBQyxLQUFhLEVBQUUsT0FBZSxFQUFFLE1BQWMsRUFBRSxLQUFhO1FBQ3JFOzs7Ozs7Ozs7Ozs7Ozs7VUFlRTtRQUNSLE1BQU0sU0FBUyxHQUFHLHFJQUFxSSxDQUFBO1FBQ3ZKLG9CQUFvQjtRQUNwQixJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxVQUFVLEVBQUUsS0FBSyxDQUFDLFFBQVEsRUFBRSxLQUFLLENBQUMsQ0FBQTtRQUNqRCxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxTQUFTLEVBQUUsS0FBSyxDQUFDLFFBQVEsRUFBRSxPQUFPLENBQUMsQ0FBQTtRQUNsRCxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxhQUFhLEVBQUUsS0FBSyxDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQTtRQUNyRCxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxZQUFZLEVBQUUsS0FBSyxDQUFDLFFBQVEsRUFBRSxLQUFLLENBQUMsQ0FBQTtRQUNuRCxZQUFZO1FBQ1osT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUM7YUFDOUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFO1lBQ2QsT0FBTyxNQUFNLENBQUMsU0FBUyxDQUFBO1FBQ3hCLENBQUMsQ0FBQzthQUNELEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRSxHQUFHLE1BQU0sR0FBRyxDQUFBLENBQUMsQ0FBQyxDQUFDLENBQUE7SUFDOUIsQ0FBQztDQUNELENBQUE7QUE1S29CLHNCQUFzQjtJQUQxQyxzQkFBVSxFQUFFOztHQUNRLHNCQUFzQixDQTRLMUM7a0JBNUtvQixzQkFBc0IiLCJmaWxlIjoiYXBpLW91dGJvdW5kL2ludGVncmF0aW9uL2NhaXhhLXJhcGlkby1hcGkvaW1wbGVtZW50YXRpb24vY2FpeGEtcmFwaWRvLmludGVncmF0aW9uLWFkYXB0ZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcclxuICogQ2xhc3MgY29udGFpbmluZyB0aGUgbWV0aG9kcyBmb3IgaW50ZWdyYXRpbmcgd2l0aCB0aGUgQ2FpeGFcclxuICogUmFwaWRvIEFQSS5cclxuICovXHJcblxyXG5jb25zdCBtc3NxbCA9IHJlcXVpcmUoJ21zc3FsJylcclxuaW1wb3J0IHsgaW5qZWN0YWJsZSB9IGZyb20gXCJpbnZlcnNpZnlcIjtcclxuaW1wb3J0IENhaXhhUmFwaWRvSW50ZWdyYXRpb25Qb3J0IGZyb20gJy4uL2NhaXhhLXJhcGlkby5pbnRlZ3JhdGlvbi1wb3J0J1xyXG5pbXBvcnQgeyBQcm9kdWN0RFRPIH0gZnJvbSAnLi4vLi4vLi4vZHRvL2NhaXhhLXJhcGlkby5kdG8nXHJcblxyXG5AaW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIENhaXhhUmFwaWRvSW50ZWdyYXRpb24gaW1wbGVtZW50cyBDYWl4YVJhcGlkb0ludGVncmF0aW9uUG9ydCB7XHJcblx0cHJpdmF0ZSByZXFcclxuXHRwcml2YXRlIGRiVVJMXHJcblx0cHJpdmF0ZSBwb29sXHJcblxyXG5cdGNvbnN0cnVjdG9yKGRiVVJMOiBzdHJpbmcpIHtcclxuXHRcdHRoaXMuZGJVUkwgPSBkYlVSTFxyXG5cdFx0dGhpcy5jb25uZWN0KClcclxuXHRcdHRoaXMucmVxID0gdGhpcy5wb29sLnJlcXVlc3QoKVxyXG5cdH1cclxuXHJcblx0LyoqXHJcblx0ICogQHN1bW1hcnk6IG1ldGhvZCBmb3IgY29ubmVjdGluZyB0byBDYWl4YSBSYXBpZG8ncyBEQiBmb3IgdGhlIHJlc3RhdXJhbnQuXHJcblx0ICovXHJcblx0cHVibGljIGNvbm5lY3QgPSBhc3luYyAoKSA9PiB7XHJcblx0XHQvLyByZXRyaWV2ZSBkYlVSTCBhbmQgaW5ncmVkaWVudHMgY2F0ZWdvcnkgbmFtZSBmcm9tIHJlc3RhdXJhbnQgaW5mb1xyXG5cdFx0dGhpcy5wb29sID0gbmV3IG1zc3FsLkNvbm5lY3Rpb25Qb29sKHRoaXMuZGJVUkwpXHJcblx0XHRhd2FpdCB0aGlzLnBvb2wuY29ubmVjdCgpXHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiBAc3VtbWFyeTogdGVzdHMgd2hldGhlciBvYmplY3QgaGFzIGJlZW4gY29ubmVjdGVkIHRvIERCIGZvciBpbnRlZ3JhdGlvbi5cclxuXHQgKi9cclxuXHRwdWJsaWMgaXNDb25uZWN0ZWQgPSAoKSA9PiB7XHJcblx0XHRyZXR1cm4gdGhpcy5wb29sICE9PSB1bmRlZmluZWRcclxuXHR9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBAc3VtbWFyeTogbWV0aG9kIGZvciBjcmVhdGluZyBhIG5ldyBvcmRlciBhc3NvY2FpdGVkIHdpdGggYSBnaXZlbiB0YWJsZS5cclxuICAgICAqL1xyXG5cdHB1YmxpYyBvcGVuVGFibGUodGFibGU6IE51bWJlcikge1xyXG4gICAgICAgIC8qXHJcbiAgICAgICAgUHJvY2VkdXJlIHBhcmEgYWJyaXIgbWVzYTpcclxuICAgICAgICBOb21lOiBzcF9BYnJlTWVzYVxyXG4gICAgICAgIFBhcmFtZXRyb3M6IEBOcm9NZXNhXHRcdC0+IE51bWVybyBkYSBNZXNhIG91IENvbWFuZGFcclxuICAgICAgICAgICAgICAgICAgICBATnJvQ2FydGFvXHRcdC0+IEZpeG8gMFxyXG4gICAgICAgICAgICAgICAgICAgIEBOb21lXHRcdFx0LT4gRml4byAnJ1xyXG4gICAgICAgICAgICAgICAgICAgIEBJUFRlcm1pbmFsXHRcdC0+IElQIG91IG5vbWUgZG8gZGlzcG9zaXRpdm8gcXVlIGVzdGEgZW52aWFuZG8gbyBwZWRpZG9cclxuICAgICAgICA/PyAgICAgICAgICBAVmVuZG9ySURcdFx0LT4gTnJvIGRlIHPDqXJpZSBkbyBkaXNwb3NpdGl2b1xyXG5cdFx0XHRcdCAgICBAVmVyc2FvXHRcdFx0LT4gRml4byAnJ1xyXG4gICAgICAgICAgICAgICAgICAgIEBPcmlnZW1cdFx0XHQtPiBGaXhvICdpT1MnXHJcbiAgICAgICAgUmV0b3JubzogQ29kaWdvIGUgTWVuc2FnZW1cclxuICAgICAgICBDb2RpZ28gPSAwIC0+IFN1Y2Vzc29cclxuICAgICAgICBDb2RpZ28gPD4gMCAtPiBWZXIgTWVuc2FnZW0gZGUgZXJyb1xyXG5cclxuICAgICAgICBFeGVtcGxvIGRlIFVzbzpcclxuICAgICAgICBzcF9BYnJlTWVzYSBATnJvTWVzYSA9IDMsIEBOcm9DYXJ0YW8gPSAwLCBATm9tZSA9ICcnLCBASVBUZXJtaW5hbCA9ICdjaHJpcycsIEBWZW5kb3JJRCA9ICcxYTI4MDc5OGE4Yzk5ODljJywgQFZlcnNhbyA9ICcnLCBAT3JpZ2VtID0gJ0lPUydcclxuICAgICAgICAqL1xyXG5cdFx0Y29uc3QgcXVlcnlUZXh0ID0gJ0VYRUMgc3BfQWJyZU1lc2EgQE5yb01lc2EgPSBAbnJvX21lc2EsIEBOcm9DYXJ0YW8gPSAwLCBATm9tZSA9IFxcJ1xcJywgQElQVGVybWluYWwgPSBAaXAsIEBWZW5kb3JJRCA9IEB2ZW5kb3JfaWQsIEBWZXJzYW8gPSBcXCdcXCcsIEBPcmlnZW0gPSBcIklPU1wiJ1xyXG5cdFx0Ly8gZGVmaW5lIHBhcmFtZXRlcnNcclxuXHRcdHRoaXMucmVxLmlucHV0KCducm9fbWVzYScsIG1zc3FsLlNtYWxsSW50LCB0YWJsZSlcclxuXHRcdHRoaXMucmVxLmlucHV0KCdpcCcsIG1zc3FsLk5WYXJDaGFyLCAnVGFibGV0IC0gTWVzYScgKyB0YWJsZSlcclxuXHRcdHRoaXMucmVxLmlucHV0KCd2ZW5kb3JfaWQnLCBtc3NxbC5OVmFyQ2hhciwgJ1RhYmxldCAtIE1lc2EnICsgdGFibGUpXHJcblx0XHQvLyBydW4gcXVlcnlcclxuXHRcdHJldHVybiB0aGlzLnJlcS5xdWVyeShxdWVyeVRleHQpXHJcblx0XHRcdC50aGVuKHJlc3VsdCA9PiB7XHJcblx0XHRcdFx0aWYgKHJlc3VsdC5yZWNvcmRzZXRbMF0uUmV0b3JubyAhPT0gMCkge1xyXG5cdFx0XHRcdFx0dGhyb3cgbmV3IEVycm9yKHJlc3VsdC5yZWNvcmRzZXRbMF0uTWVuc2FnZW0pXHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdGNvbnNvbGUubG9nKCdCaWxsIG9wZW4uJylcclxuXHRcdFx0fSlcclxuXHRcdFx0LmNhdGNoKGVyciA9PiB7IHRocm93IGVyciB9KVxyXG5cdH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEBzdW1tYXJ5OiBtZXRob2QgZm9yIGFkZGluZyBhIG5ldyBwcm9kdWN0IHRvIHRoZSB0YWJsZSdzIGJpbGwuXHJcbiAgICAgKi9cclxuXHRwdWJsaWMgYWRkUHJvZHVjdFRvVGFibGUocHJvZHVjdDogUHJvZHVjdERUTykge1xyXG4gICAgICAgIC8qXHJcbiAgICAgICAgUHJvY2VkdXJlIHBhcmEgYWRpY2lvbmFyIHVtIHByb2R1dG8gbmEgbWVzYTpcclxuICAgICAgICBOb21lOiBzcF9BZGljaW9uYVByb2R1dG9cclxuICAgICAgICBQYXJhbWV0cm9zOiBATnJvTWVzYVx0XHQtPiBOdW1lcm8gZGEgTWVzYSBvdSBDb21hbmRhXHJcbiAgICAgICAgICAgICAgICAgICAgQE5yb0NhcnRhb1x0XHQtPiBGaXhvIDBcclxuICAgICAgICAgICAgICAgICAgICBATm9tZVx0XHRcdC0+IEZpeG8gJydcclxuICAgICAgICAgICAgICAgICAgICBASVBUZXJtaW5hbFx0XHQtPiBJUCBvdSBub21lIGRvIGRpc3Bvc2l0aXZvIHF1ZSBlc3RhIGVudmlhbmRvIG8gcGVkaWRvXHJcbiAgICAgICAgICAgICAgICAgICAgQElEUHJvZHV0b1x0XHQtPiBJRCBkbyBwcm9kdXRvKG7Do28gw6kgbyBjw7NkaWdvLCDDqSBvIGNhbXBvIElEIFByb2R1dG8pXHJcbiAgICAgICAgICAgICAgICAgICAgQFF0ZGVcdFx0XHQtPiBJbmZvcm1hciBhIFF0ZGVcclxuICAgICAgICAgICAgICAgICAgICBAT2JzZXJ2YWNhb0l0ZW1cdC0+IE5vIGNhc28gZG8gRklULCBpbmZvcm1hciAnJ1xyXG4gICAgICAgICAgICAgICAgICAgIEBWZW5kZWRvcklEIFx0LT4gRml4byAnJ1xyXG5cclxuICAgICAgICBSZXRvcm5vOiBDb2RpZ28gZSBNZW5zYWdlbVxyXG4gICAgICAgIENvZGlnbyA9IDAgLT4gU3VjZXNzb1xyXG4gICAgICAgIENvZGlnbyA8PiAwIC0+IFZlciBNZW5zYWdlbSBkZSBlcnJvXHJcblxyXG4gICAgICAgIEV4ZW1wbG8gZGUgVXNvOlxyXG4gICAgICAgIHNwX0FkaWNpb25hUHJvZHV0byBATnJvTWVzYSA9IDMsIEBOcm9DYXJ0YW8gPSAwLCBATm9tZSA9ICcnLCBASVBUZXJtaW5hbCA9ICdjaHJpcycsIEBJRFByb2R1dG8gPSAxNCwgQFF0ZGUgPSAxLjAsIEBPYnNlcnZhY2FvSXRlbSA9ICcnLCBAVmVuZGVkb3JJRCA9ICcnXHJcbiAgICAgICAgKi9cclxuXHRcdGNvbnN0IHF1ZXJ5VGV4dCA9ICdFWEVDIHNwX0FkaWNpb25hUHJvZHV0byBATnJvTWVzYSA9IEBucm9fbWVzYSwgQE5yb0NhcnRhbyA9IDAsIEBOb21lID0gXFwnXFwnLCBASVBUZXJtaW5hbCA9IEBpcCwgQElEUHJvZHV0byA9IDE0LCBAUXRkZSA9IEBxdGRlLCBAT2JzZXJ2YWNhb0l0ZW0gPSBcXCdcXCcsIEBWZW5kZWRvcklEID0gXFwnXFwnJ1xyXG5cdFx0Ly8gZGVmaW5lIHBhcmFtZXRlcnNcclxuXHRcdHRoaXMucmVxLmlucHV0KCducm9fbWVzYScsIG1zc3FsLlNtYWxsSW50LCBwcm9kdWN0LnRhYmxlKVxyXG5cdFx0dGhpcy5yZXEuaW5wdXQoJ2lwJywgbXNzcWwuTlZhckNoYXIsICdUYWJsZXQgLSBNZXNhJyArIHByb2R1Y3QudGFibGUpXHJcblx0XHR0aGlzLnJlcS5pbnB1dCgnaWRfcHJvZHV0bycsIG1zc3FsLk5WYXJDaGFyLCBwcm9kdWN0Lm1nbXRJRClcclxuXHRcdHRoaXMucmVxLmlucHV0KCdxdGRlJywgbXNzcWwuU21hbGxJbnQsIHByb2R1Y3QucXVhbnRpdHkpXHJcblx0XHQvLyBydW4gcXVlcnlcclxuXHRcdHJldHVybiB0aGlzLnJlcS5xdWVyeShxdWVyeVRleHQpXHJcblx0XHRcdC50aGVuKHJlc3VsdCA9PiB7XHJcblx0XHRcdFx0aWYgKHJlc3VsdC5yZWNvcmRzZXRbMF0uUmV0b3JubyA9PT0gMikge1xyXG5cdFx0XHRcdFx0Ly8gb25seSB0aGlzIGVycm9yIGlzIG5vdCBhc3NvY2lhdGVkIHdpdGggYSBtZXNzYWdlXHJcblx0XHRcdFx0XHR0aHJvdyBuZXcgRXJyb3IoJ1Byb2R1Y3QgSUQgbm90IGZvdW5kIGluIENhaXhhIFJhcGlkbycpXHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdGVsc2UgaWYgKHJlc3VsdC5yZWNvcmRzZXRbMF0uUmV0b3JubyAhPT0gMCkge1xyXG5cdFx0XHRcdFx0dGhyb3cgbmV3IEVycm9yKHJlc3VsdC5yZWNvcmRzZXRbMF0uTWVuc2FnZW0pXHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdGNvbnNvbGUubG9nKCdQcm9kdWN0IGFkZGVkLicpXHJcblx0XHRcdH0pXHJcblx0XHRcdC5jYXRjaChlcnIgPT4geyB0aHJvdyBlcnIgfSlcclxuXHR9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBAc3VtbWFyeTogbWV0aG9kIGZvciBmaW5hbGl6aW5nIHRoZSBiaWxsIGFzc29jaWF0ZWQgd2l0aCBhIHRhYmxlLlxyXG4gICAgICovXHJcblx0cHVibGljIGNsb3NlVGFibGUodGFibGU6IE51bWJlcikge1xyXG4gICAgICAgIC8qXHJcbiAgICAgICAgUHJvY2VkdXJlIHBhcmEgZmluYWxpemFyIHVtIHBlZGlkbzpcclxuICAgICAgICBOb21lOiBzcF9GaW5hbGl6YXJQZWRpZG9cclxuICAgICAgICBQYXJhbWV0cm9zOiBATnJvTWVzYVx0XHQtPiBOdW1lcm8gZGEgTWVzYSBvdSBDb21hbmRhXHJcbiAgICAgICAgICAgICAgICAgICAgQE5yb0NhcnRhb1x0XHQtPiBGaXhvIDBcclxuXHJcbiAgICAgICAgUmV0b3JubzogQ29kaWdvIGUgTWVuc2FnZW1cclxuICAgICAgICBDb2RpZ28gPSAwIC0+IFN1Y2Vzc29cclxuICAgICAgICBDb2RpZ28gPD4gMCAtPiBWZXIgTWVuc2FnZW0gZGUgZXJyb1xyXG5cclxuICAgICAgICBFeGVtcGxvIGRlIFVzbzpcclxuICAgICAgICBzcF9GaW5hbGl6YXJQZWRpZG8gQE5yb01lc2EgPSAzLCBATnJvQ2FydGFvID0gMFxyXG4gICAgICAgICovXHJcblx0XHRjb25zdCBxdWVyeVRleHQgPSAnRVhFQyBzcF9GaW5hbGl6YXJQZWRpZG8gQE5yb01lc2EgPSBAbnJvX21lc2EsIEBOcm9DYXJ0YW8gPSAwJ1xyXG5cdFx0Ly8gZGVmaW5lIHBhcmFtZXRlcnNcclxuXHRcdHRoaXMucmVxLmlucHV0KCducm9fbWVzYScsIG1zc3FsLlNtYWxsSW50LCB0YWJsZSlcclxuXHRcdC8vIHJ1biBxdWVyeVxyXG5cdFx0cmV0dXJuIHRoaXMucmVxLnF1ZXJ5KHF1ZXJ5VGV4dClcclxuXHRcdFx0LnRoZW4ocmVzdWx0ID0+IHtcclxuXHRcdFx0XHRpZiAocmVzdWx0LnJlY29yZHNldFswXS5SZXRvcm5vICE9PSAwKSB7XHJcblx0XHRcdFx0XHR0aHJvdyBuZXcgRXJyb3IoJ0ludmFsaWQgYXJndW1lbnRzLicpXHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdGNvbnNvbGUubG9nKCdCaWxsIGNsb3NlZC4nKVxyXG5cdFx0XHR9KVxyXG5cdFx0XHQuY2F0Y2goZXJyID0+IHsgdGhyb3cgZXJyIH0pXHJcblx0fVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQHN1bW1hcnk6IG1ldGhvZCBmb3IgY2hlY2tpbmcgdGhlIGJpbGwgb2YgYSB0YWJsZSB0aGF0IGlzIHN0aWxsIG9yZGVyaW5nLlxyXG4gICAgICovXHJcblx0cHVibGljIHByZUJpbGwodGFibGU6IG51bWJlciwgc2VydmljZTogbnVtYmVyLCBndWVzdHM6IG51bWJlciwgcHJpbnQ6IG51bWJlcikge1xyXG4gICAgICAgIC8qXHJcbiAgICAgICAgUHJvY2VkdXJlIHBhcmEgdmlzdWFsaXphciBhIHByZWNvbnRhXHJcbiAgICAgICAgTm9tZTogc3BfV1NQcmVDb250YVxyXG4gICAgICAgIFBhcmFtZXRyb3M6IEBOcm9NZXNhXHRcdC0+IE51bWVybyBkYSBNZXNhIG91IENvbWFuZGFcclxuICAgICAgICAgICAgICAgICAgICBATnJvQ2FydGFvXHRcdC0+IEZpeG8gMFxyXG4gICAgICAgICAgICAgICAgICAgIEBTZXJ2aWNvXHRcdC0+IEluZGljYWRvciBzZSB2YWkgb3UgbsOjbyBjb2JyYXIgc2VydmnDp28oMCAtIG7Do28gY29icmFyLCBvdSAxIC0gY29icmFyKVxyXG4gICAgICAgICAgICAgICAgICAgIEBOcm9QZXNzb2FzXHRcdC0+IE5ybyBkZSBwZXNzb2FzIG5hIHBlc2FcclxuICAgICAgICAgICAgICAgICAgICBAVmlzdWFsaXphclx0XHQtPiBJbmRpY2Fkb3Igc2UgaW1wcmltZSAoMCkgb3Ugc29tZW50ZSB2aXN1YWxpemEgKDEpXHJcblxyXG4gICAgICAgIFJldG9ybm86IENvZGlnbyBlIE1lbnNhZ2VtXHJcbiAgICAgICAgQ29kaWdvID0gMCAtPiBTdWNlc3NvXHJcbiAgICAgICAgQ29kaWdvIDw+IDAgLT4gVmVyIE1lbnNhZ2VtIGRlIGVycm9cclxuXHJcbiAgICAgICAgRXhlbXBsbyBkZSBVc286XHJcbiAgICAgICAgc3BfV1NQcmVDb250YSBATnJvTWVzYSA9IDMsIEBOcm9DYXJ0YW8gPSAwLCBAU2VydmljbyA9IDEsIEBOcm9QZXNzb2FzID0gMSwgQFZpc3VhbGl6YXIgPSAxXHJcbiAgICAgICAgKi9cclxuXHRcdGNvbnN0IHF1ZXJ5VGV4dCA9ICdFWEVDIHNwX1dTUHJlQ29udGEgQE5yb01lc2EgPSBAbnJvX21lc2EsIEBOcm9DYXJ0YW8gPSAwLCBAU2VydmljbyA9IEBzZXJ2aWNvLCBATnJvUGVzc29hcyA9IEBucm9fcGVzc29hcywgQFZpc3VhbGl6YXIgPSBAdmlzdWFsaXphcidcclxuXHRcdC8vIGRlZmluZSBwYXJhbWV0ZXJzXHJcblx0XHR0aGlzLnJlcS5pbnB1dCgnbnJvX21lc2EnLCBtc3NxbC5TbWFsbEludCwgdGFibGUpXHJcblx0XHR0aGlzLnJlcS5pbnB1dCgnc2VydmljbycsIG1zc3FsLlNtYWxsSW50LCBzZXJ2aWNlKVxyXG5cdFx0dGhpcy5yZXEuaW5wdXQoJ25yb19wZXNzb2FzJywgbXNzcWwuU21hbGxJbnQsIGd1ZXN0cylcclxuXHRcdHRoaXMucmVxLmlucHV0KCd2aXN1YWxpemFyJywgbXNzcWwuU21hbGxJbnQsIHByaW50KVxyXG5cdFx0Ly8gcnVuIHF1ZXJ5XHJcblx0XHRyZXR1cm4gdGhpcy5yZXEucXVlcnkocXVlcnlUZXh0KVxyXG5cdFx0XHQudGhlbihyZXN1bHQgPT4ge1xyXG5cdFx0XHRcdHJldHVybiByZXN1bHQucmVjb3Jkc2V0XHJcblx0XHRcdH0pXHJcblx0XHRcdC5jYXRjaChlcnIgPT4geyB0aHJvdyBlcnIgfSlcclxuXHR9XHJcbn0iXX0=
