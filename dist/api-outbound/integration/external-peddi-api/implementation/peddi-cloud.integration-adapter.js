"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const inversify_1 = require("inversify");
const types_1 = require("../../../../config/types");
const request = require("request");
let PeddiCloudIntegrationAdapter = class PeddiCloudIntegrationAdapter {
    constructor(restaurantRepository) {
        this.restaurantRepository = restaurantRepository;
        this.generateCredentialsToken = () => __awaiter(this, void 0, void 0, function* () {
            if (!process.env.RESTAURANT_CLOUD_ID || !process.env.TOKEN) {
                yield this.restaurantRepository.get()
                    .then(info => {
                    process.env.RESTAURANT_CLOUD_ID = info.restaurant_cloud_id;
                    process.env.TOKEN = info.token;
                })
                    .catch(err => { throw err; });
            }
            return jsonwebtoken_1.default.sign({
                restaurant_cloud_id: process.env.RESTAURANT_CLOUD_ID,
                token: process.env.TOKEN
            }, this.privateKey, {
                expiresIn: '10m',
                algorithm: 'RS256'
            });
        });
        this.authenticate = () => __awaiter(this, void 0, void 0, function* () {
            const apiToken = yield this.generateCredentialsToken();
            return new Promise((resolve, reject) => {
                request.get({
                    headers: {
                        'Authorization': 'Bearer ' + apiToken
                    },
                    url: 'http://localhost:3000/api/authenticate/local'
                }, (err, resp, req) => __awaiter(this, void 0, void 0, function* () {
                    if (!err) {
                        const headerValue = yield resp.headers['authorization'];
                        return resolve(headerValue);
                    }
                    else {
                        return reject(err);
                    }
                }));
            });
        });
        this.privateKey = fs_1.default.readFileSync('./jwt/private.key', 'utf8');
        this.publicKey = fs_1.default.readFileSync('./jwt/public.key', 'utf8');
    }
    ;
    sendOrder(order, token) {
        const orderString = JSON.stringify(order);
        return new Promise((resolve, reject) => {
            request.post({
                headers: {
                    'authorization': token,
                    'Content-Type': 'application/json'
                },
                url: 'http://localhost:3000/api/orders',
                body: orderString
            }, (err, resp, body) => __awaiter(this, void 0, void 0, function* () {
                if (!err && resp.statusCode < 400) {
                    return resolve(body);
                }
                else {
                    return reject(err || body);
                }
            }));
        });
    }
};
PeddiCloudIntegrationAdapter = __decorate([
    inversify_1.injectable(),
    __param(0, inversify_1.inject(types_1.types.RestaurantRepository)),
    __metadata("design:paramtypes", [Object])
], PeddiCloudIntegrationAdapter);
exports.default = PeddiCloudIntegrationAdapter;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9hcGktb3V0Ym91bmQvaW50ZWdyYXRpb24vZXh0ZXJuYWwtcGVkZGktYXBpL2ltcGxlbWVudGF0aW9uL3BlZGRpLWNsb3VkLmludGVncmF0aW9uLWFkYXB0ZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLDRDQUFtQjtBQUNuQixnRUFBOEI7QUFDOUIseUNBQStDO0FBRS9DLG9EQUE4QztBQUU5QyxtQ0FBb0M7QUFLcEMsSUFBcUIsNEJBQTRCLEdBQWpELE1BQXFCLDRCQUE0QjtJQUs3QyxZQUVZLG9CQUEwQztRQUExQyx5QkFBb0IsR0FBcEIsb0JBQW9CLENBQXNCO1FBTTlDLDZCQUF3QixHQUFHLEdBQXVCLEVBQUU7WUFDeEQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRTtnQkFDeEQsTUFBTSxJQUFJLENBQUMsb0JBQW9CLENBQUMsR0FBRyxFQUFFO3FCQUNoQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUU7b0JBQ1QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUE7b0JBQzFELE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUE7Z0JBQ2xDLENBQUMsQ0FBQztxQkFDRCxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsR0FBRyxNQUFNLEdBQUcsQ0FBQSxDQUFDLENBQUMsQ0FBQyxDQUFBO2FBQ25DO1lBRUQsT0FBTyxzQkFBRyxDQUFDLElBQUksQ0FBQztnQkFDUixtQkFBbUIsRUFBRSxPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQjtnQkFDcEQsS0FBSyxFQUFFLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSzthQUMzQixFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUU7Z0JBQ2hCLFNBQVMsRUFBRSxLQUFLO2dCQUNoQixTQUFTLEVBQUUsT0FBTzthQUNyQixDQUFDLENBQUE7UUFDVixDQUFDLENBQUEsQ0FBQTtRQUVELGlCQUFZLEdBQUcsR0FBMEIsRUFBRTtZQUN2QyxNQUFNLFFBQVEsR0FBRyxNQUFNLElBQUksQ0FBQyx3QkFBd0IsRUFBRSxDQUFBO1lBQ3RELE9BQU8sSUFBSSxPQUFPLENBQUMsQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7Z0JBQ25DLE9BQU8sQ0FBQyxHQUFHLENBQUM7b0JBQ1IsT0FBTyxFQUFFO3dCQUNMLGVBQWUsRUFBRSxTQUFTLEdBQUcsUUFBUTtxQkFDeEM7b0JBQ0QsR0FBRyxFQUFFLDhDQUE4QztpQkFDdEQsRUFBRSxDQUFPLEdBQUcsRUFBRSxJQUFJLEVBQUUsR0FBWSxFQUFFLEVBQUU7b0JBQ2pDLElBQUksQ0FBQyxHQUFHLEVBQUU7d0JBQ04sTUFBTSxXQUFXLEdBQUcsTUFBTSxJQUFJLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxDQUFDO3dCQUN4RCxPQUFPLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBQztxQkFDL0I7eUJBQ0k7d0JBQ0QsT0FBTyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7cUJBQ3RCO2dCQUNMLENBQUMsQ0FBQSxDQUFDLENBQUM7WUFDUCxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQSxDQUFBO1FBekNHLElBQUksQ0FBQyxVQUFVLEdBQUcsWUFBRSxDQUFDLFlBQVksQ0FBQyxtQkFBbUIsRUFBRSxNQUFNLENBQUMsQ0FBQTtRQUM5RCxJQUFJLENBQUMsU0FBUyxHQUFHLFlBQUUsQ0FBQyxZQUFZLENBQUMsa0JBQWtCLEVBQUUsTUFBTSxDQUFDLENBQUE7SUFDaEUsQ0FBQztJQUFBLENBQUM7SUF5Q0YsU0FBUyxDQUFDLEtBQWEsRUFBRSxLQUFVO1FBQy9CLE1BQU0sV0FBVyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDMUMsT0FBTyxJQUFJLE9BQU8sQ0FBQyxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUNuQyxPQUFPLENBQUMsSUFBSSxDQUFDO2dCQUNULE9BQU8sRUFBRTtvQkFDTCxlQUFlLEVBQUcsS0FBSztvQkFDdkIsY0FBYyxFQUFHLGtCQUFrQjtpQkFDdEM7Z0JBQ0QsR0FBRyxFQUFFLGtDQUFrQztnQkFDdkMsSUFBSSxFQUFFLFdBQVc7YUFDcEIsRUFBRSxDQUFPLEdBQUcsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLEVBQUU7Z0JBQ3pCLElBQUksQ0FBQyxHQUFHLElBQUksSUFBSSxDQUFDLFVBQVUsR0FBRyxHQUFHLEVBQUU7b0JBQy9CLE9BQU8sT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUN4QjtxQkFDSTtvQkFDRCxPQUFPLE1BQU0sQ0FBQyxHQUFHLElBQUksSUFBSSxDQUFDLENBQUM7aUJBQzlCO1lBQ0wsQ0FBQyxDQUFBLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztDQUVKLENBQUE7QUF6RW9CLDRCQUE0QjtJQURoRCxzQkFBVSxFQUFFO0lBT0osV0FBQSxrQkFBTSxDQUFDLGFBQUssQ0FBQyxvQkFBb0IsQ0FBQyxDQUFBOztHQU50Qiw0QkFBNEIsQ0F5RWhEO2tCQXpFb0IsNEJBQTRCIiwiZmlsZSI6ImFwaS1vdXRib3VuZC9pbnRlZ3JhdGlvbi9leHRlcm5hbC1wZWRkaS1hcGkvaW1wbGVtZW50YXRpb24vcGVkZGktY2xvdWQuaW50ZWdyYXRpb24tYWRhcHRlci5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBmcyBmcm9tICdmcydcclxuaW1wb3J0IEpXVCBmcm9tICdqc29ud2VidG9rZW4nXHJcbmltcG9ydCB7IGluamVjdGFibGUsIGluamVjdCB9IGZyb20gXCJpbnZlcnNpZnlcIjtcclxuaW1wb3J0IFBlZGRpQ2xvdWRJbnRlZ3JhdGlvblBvcnQgZnJvbSBcIi4uL3BlZGRpLWNsb3VkLmludGVncmF0aW9uLXBvcnRcIjtcclxuaW1wb3J0IHt0eXBlc30gZnJvbSAnLi4vLi4vLi4vLi4vY29uZmlnL3R5cGVzJ1xyXG5pbXBvcnQgUmVzdGF1cmFudFJlcG9zaXRvcnkgZnJvbSAnLi4vLi4vLi4vcmVwb3NpdG9yeS9yZXN0YXVyYW50LnJlcG9zaXRvcnknXHJcbmltcG9ydCByZXF1ZXN0ID0gcmVxdWlyZSgncmVxdWVzdCcpO1xyXG5pbXBvcnQgeyBSZXF1ZXN0IH0gZnJvbSBcImV4cHJlc3NcIjtcclxuXHJcblxyXG5AaW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFBlZGRpQ2xvdWRJbnRlZ3JhdGlvbkFkYXB0ZXIgaW1wbGVtZW50cyBQZWRkaUNsb3VkSW50ZWdyYXRpb25Qb3J0IHtcclxuXHJcbiAgICBwcml2YXRlIHByaXZhdGVLZXlcclxuICAgIHByaXZhdGUgcHVibGljS2V5XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgQGluamVjdCh0eXBlcy5SZXN0YXVyYW50UmVwb3NpdG9yeSlcclxuICAgICAgICBwcml2YXRlIHJlc3RhdXJhbnRSZXBvc2l0b3J5OiBSZXN0YXVyYW50UmVwb3NpdG9yeSxcclxuICAgICkgeyBcclxuICAgICAgICB0aGlzLnByaXZhdGVLZXkgPSBmcy5yZWFkRmlsZVN5bmMoJy4vand0L3ByaXZhdGUua2V5JywgJ3V0ZjgnKVxyXG4gICAgICAgIHRoaXMucHVibGljS2V5ID0gZnMucmVhZEZpbGVTeW5jKCcuL2p3dC9wdWJsaWMua2V5JywgJ3V0ZjgnKSBcclxuICAgIH07XHJcblxyXG4gICAgcHJpdmF0ZSBnZW5lcmF0ZUNyZWRlbnRpYWxzVG9rZW4gPSBhc3luYyAoKTogUHJvbWlzZTxhbnk+ID0+IHtcclxuICAgICAgICBpZiAoIXByb2Nlc3MuZW52LlJFU1RBVVJBTlRfQ0xPVURfSUQgfHwgIXByb2Nlc3MuZW52LlRPS0VOKSB7XHJcbiAgICAgICAgICAgIGF3YWl0IHRoaXMucmVzdGF1cmFudFJlcG9zaXRvcnkuZ2V0KClcclxuICAgICAgICAgICAgICAgIC50aGVuKGluZm8gPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHByb2Nlc3MuZW52LlJFU1RBVVJBTlRfQ0xPVURfSUQgPSBpbmZvLnJlc3RhdXJhbnRfY2xvdWRfaWRcclxuICAgICAgICAgICAgICAgICAgICBwcm9jZXNzLmVudi5UT0tFTiA9IGluZm8udG9rZW5cclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAuY2F0Y2goZXJyID0+IHsgdGhyb3cgZXJyIH0pXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gSldULnNpZ24oe1xyXG4gICAgICAgICAgICAgICAgcmVzdGF1cmFudF9jbG91ZF9pZDogcHJvY2Vzcy5lbnYuUkVTVEFVUkFOVF9DTE9VRF9JRCxcclxuICAgICAgICAgICAgICAgIHRva2VuOiBwcm9jZXNzLmVudi5UT0tFTlxyXG4gICAgICAgICAgICB9LCB0aGlzLnByaXZhdGVLZXksIHtcclxuICAgICAgICAgICAgICAgIGV4cGlyZXNJbjogJzEwbScsXHJcbiAgICAgICAgICAgICAgICBhbGdvcml0aG06ICdSUzI1NidcclxuICAgICAgICAgICAgfSlcclxuICAgIH1cclxuXHJcbiAgICBhdXRoZW50aWNhdGUgPSBhc3luYyAoKTogUHJvbWlzZTxPYmplY3Q+ID0+IHtcclxuICAgICAgICBjb25zdCBhcGlUb2tlbiA9IGF3YWl0IHRoaXMuZ2VuZXJhdGVDcmVkZW50aWFsc1Rva2VuKClcclxuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG4gICAgICAgICAgICByZXF1ZXN0LmdldCh7XHJcbiAgICAgICAgICAgICAgICBoZWFkZXJzOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgJ0F1dGhvcml6YXRpb24nOiAnQmVhcmVyICcgKyBhcGlUb2tlblxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIHVybDogJ2h0dHA6Ly9sb2NhbGhvc3Q6MzAwMC9hcGkvYXV0aGVudGljYXRlL2xvY2FsJ1xyXG4gICAgICAgICAgICB9LCBhc3luYyAoZXJyLCByZXNwLCByZXE6IFJlcXVlc3QpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmICghZXJyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgaGVhZGVyVmFsdWUgPSBhd2FpdCByZXNwLmhlYWRlcnNbJ2F1dGhvcml6YXRpb24nXTtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gcmVzb2x2ZShoZWFkZXJWYWx1ZSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gcmVqZWN0KGVycik7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHNlbmRPcmRlcihvcmRlcjogT2JqZWN0LCB0b2tlbjogYW55KTogUHJvbWlzZTxPYmplY3Q+IHtcclxuICAgICAgICBjb25zdCBvcmRlclN0cmluZyA9IEpTT04uc3RyaW5naWZ5KG9yZGVyKTtcclxuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG4gICAgICAgICAgICByZXF1ZXN0LnBvc3Qoe1xyXG4gICAgICAgICAgICAgICAgaGVhZGVyczoge1xyXG4gICAgICAgICAgICAgICAgICAgICdhdXRob3JpemF0aW9uJyA6IHRva2VuLFxyXG4gICAgICAgICAgICAgICAgICAgICdDb250ZW50LVR5cGUnIDogJ2FwcGxpY2F0aW9uL2pzb24nXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgdXJsOiAnaHR0cDovL2xvY2FsaG9zdDozMDAwL2FwaS9vcmRlcnMnLFxyXG4gICAgICAgICAgICAgICAgYm9keTogb3JkZXJTdHJpbmdcclxuICAgICAgICAgICAgfSwgYXN5bmMgKGVyciwgcmVzcCwgYm9keSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKCFlcnIgJiYgcmVzcC5zdGF0dXNDb2RlIDwgNDAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJlc29sdmUoYm9keSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gcmVqZWN0KGVyciB8fCBib2R5KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG59Il19
