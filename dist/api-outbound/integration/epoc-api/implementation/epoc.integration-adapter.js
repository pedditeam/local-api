"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const request = require("request");
const crypto = require("crypto");
const querystring = require("querystring");
const epoc_dto_1 = require("../../../dto/epoc.dto");
const inversify_1 = require("inversify");
const response_epoc_1 = __importDefault(require("../../../envelopes/response.epoc"));
let EpocIntegrationAdapter = class EpocIntegrationAdapter {
    tokenGenerator(systemAuth) {
        return __awaiter(this, void 0, void 0, function* () {
            let sistemaAuth = new epoc_dto_1.SistemaAuth();
            let sistemaAuthString;
            sistemaAuth.hashemp = systemAuth.hashemp;
            sistemaAuth.mac = systemAuth.mac;
            sistemaAuth.service = 'geraToken';
            sistemaAuth.tokenParceiro = systemAuth.tokenParceiro;
            sistemaAuth.saltParceiro = systemAuth.saltParceiro;
            sistemaAuthString = querystring.stringify(sistemaAuth);
            return new Promise((resolve, reject) => {
                request.post({
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'Accept': 'application/json'
                    },
                    url: process.env.EPOC_URL + `/API/mod_valida_parceiro/index.php`,
                    body: sistemaAuthString
                }, (err, resp, body) => {
                    if (!err) {
                        body = this.tokenDecoder(body);
                        console.log(body);
                        return resolve(body);
                    }
                    else {
                        return reject(err);
                    }
                });
            });
        });
    }
    venderItem(order) {
        return __awaiter(this, void 0, void 0, function* () {
            // let payload: any;
            let consumerCard;
            let token;
            let sistemaGestaoPayload;
            let venderItem = new epoc_dto_1.VendaItem();
            let orderIt = new epoc_dto_1.OrderItem();
            // let produtos: Array<Produtos> = new Array<Produtos>();
            // produtos = order.products;
            // payload = await this.tokenManager.decodificaToken(JSON.parse(body.tokenPeddi));
            // payload = payload;
            sistemaGestaoPayload = this.sistemaAuth();
            token = yield this.tokenGenerator(sistemaGestaoPayload);
            consumerCard = order.ticket;
            venderItem.mac = sistemaGestaoPayload.mac;
            venderItem.hashemp = sistemaGestaoPayload.hashemp;
            venderItem.ult_mesa = order.table;
            venderItem.service = 'GravarVendaItem';
            let tokenData = yield this.tokenDecoder(token);
            venderItem.token = tokenData.data;
            orderIt.CODEVENTO = "";
            orderIt.HOST = venderItem.mac;
            let orderItem = {
                [consumerCard]: orderIt
                // {
                // 	// HOST: venderItem.mac,
                // 	// CODEVENTO: "",
                // 	itens: Array<Item>()
                // }
            };
            for (let i = 0; i < order.products.length; i++) {
                let productQuantity = order.products[i].quantity;
                // let item = {
                // 	quantidade: 1,
                // 	codigo: order.products[i].epoc_id,
                // 	cod_func_autoriz: "",
                // 	motivo_autorizacao: "1",
                // 	cod_func: order.responsibleEmployee.epoc_id,
                // 	marchar: 0,
                // 	valor: order.products[i].price,
                // 	tipoItem: "P",
                // 	lanceItem: "S",
                // 	acrescimo: 0,
                // 	desconto: 0,
                // 	obs: [
                // 		{}
                // 	]
                // }
                let item = new epoc_dto_1.Item();
                item.quantidade = 1;
                item.codigo = order.products[i].epoc_id;
                item.cod_func_autoriz = "";
                item.motivo_autorizacao = "";
                item.cod_func = order.responsibleEmployee.epoc_id;
                item.marchar = 0;
                item.valor = order.products[i].price;
                item.tipoItem = "P";
                item.lanceItem = "S";
                item.acrescimo = 0;
                item.desconto = 0;
                item.obs = new Array();
                if (order.products[i].ingredients !== null && order.products[i].ingredients.length > 0) {
                    for (let j = 0; j < order.products[i].ingredients.length; j++) {
                        item.obs[j] = new epoc_dto_1.ItemObs();
                        item.obs[j].desc_obs = order.products[i].ingredients[j].name;
                        item.obs[j].cod_obs = order.products[i].ingredients[j].epoc_id;
                        item.obs[j].cod_modificador = 0;
                    }
                }
                for (let q = 0; q < productQuantity; q++) {
                    orderItem[consumerCard].ITENS.push(item);
                }
                // orderItem[comanda].ITENS[i] = {
                // 	quantidade: order.products[i].quantity,
                // 	codigo: order.products[i].epoc_id,
                // 	cod_func_autoriz: "",
                // 	motivo_autorizacao: "1",
                // 	cod_func: order.responsibleEmployee.epoc_id,
                // 	marchar: 0,
                // 	valor: order.products[i].price,
                // 	tipoItem: "P",
                // 	lanceItem: "S",
                // 	acrescimo: 0,
                // 	desconto: 0,
                // 	obs: []
                // }
                // if (order.products[i].ingredients !== null && order.products[i].ingredients.length > 0) {
                // 	for (let j = 0; j < order.products[i].ingredients.length; j++) {
                // 		orderItem[comanda].ITENS[i].obs[j] = {
                // 			desc_obs: order.products[i].ingredients[j].name,
                // 			cod_obs: order.products[i].ingredients[j].epoc_id,
                // 			cod_modificador: 0
                // 		}
                // 	}
                // }
            }
            let stringSalt = "";
            for (let i = 0; i < orderItem[consumerCard].ITENS.length; i++) {
                stringSalt += orderItem[consumerCard].ITENS[i].codigo + '_';
            }
            stringSalt = stringSalt.slice(0, -1);
            let itemConvert;
            let venderItemToString;
            itemConvert = JSON.stringify(orderItem);
            venderItem.dataVenda = new Buffer(itemConvert).toString('base64');
            venderItem.salt = crypto.createHash('md5').update(stringSalt).digest("hex");
            venderItemToString = querystring.stringify(venderItem);
            // return new Promise<ResponseEpoc>((resolve, reject) => {
            // })
            return new Promise((resolve, reject) => {
                request.post({
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'Accept': 'application/json'
                    },
                    url: `http://llm09.epoc2.cdmon.org:56478/API/mod_venda_item/index.php`,
                    body: venderItemToString
                }, (err, resp, body) => __awaiter(this, void 0, void 0, function* () {
                    if (!err) {
                        body = yield this.tokenDecoder(body);
                        console.log(body);
                        return new response_epoc_1.default(body);
                    }
                    else {
                        return reject(err);
                    }
                }));
            });
        });
    }
    getExtrato(objExtrato) {
        throw new Error("Method not implemented.");
    }
    getProducts() {
        throw new Error("Method not implemented.");
    }
    tokenDecoder(token) {
        return __awaiter(this, void 0, void 0, function* () {
            let decodeBuffer = new Buffer(token, 'base64');
            let tokenDecoded;
            decodeBuffer = decodeBuffer.toString('ascii');
            tokenDecoded = JSON.parse(decodeBuffer);
            return tokenDecoded;
        });
    }
    sistemaAuth() {
        let epocAuth = new epoc_dto_1.SistemaAuth();
        epocAuth.hashemp = 'da2g39jbcdja8601';
        epocAuth.mac = 'e4:c1:dd:c0:9a:4d';
        epocAuth.tokenParceiro = '353c1ea3-682b-4754-91ef-f55d05aec758';
        epocAuth.saltParceiro = crypto.createHash('md5').update(epocAuth.tokenParceiro + '__TOKEN__').digest("hex");
        return epocAuth;
    }
    ;
};
EpocIntegrationAdapter = __decorate([
    inversify_1.injectable()
], EpocIntegrationAdapter);
exports.default = EpocIntegrationAdapter;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9hcGktb3V0Ym91bmQvaW50ZWdyYXRpb24vZXBvYy1hcGkvaW1wbGVtZW50YXRpb24vZXBvYy5pbnRlZ3JhdGlvbi1hZGFwdGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQSxtQ0FBb0M7QUFDcEMsaUNBQWtDO0FBQ2xDLDJDQUE0QztBQUU1QyxvREFTK0I7QUFDL0IseUNBQXVDO0FBQ3ZDLHFGQUE0RDtBQUs1RCxJQUFxQixzQkFBc0IsR0FBM0MsTUFBcUIsc0JBQXNCO0lBR3BDLGNBQWMsQ0FBQyxVQUFlOztZQUVuQyxJQUFJLFdBQVcsR0FBZ0IsSUFBSSxzQkFBVyxFQUFFLENBQUM7WUFDakQsSUFBSSxpQkFBeUIsQ0FBQztZQUM5QixXQUFXLENBQUMsT0FBTyxHQUFHLFVBQVUsQ0FBQyxPQUFPLENBQUM7WUFDekMsV0FBVyxDQUFDLEdBQUcsR0FBRyxVQUFVLENBQUMsR0FBRyxDQUFDO1lBQ2pDLFdBQVcsQ0FBQyxPQUFPLEdBQUcsV0FBVyxDQUFDO1lBQ2xDLFdBQVcsQ0FBQyxhQUFhLEdBQUcsVUFBVSxDQUFDLGFBQWEsQ0FBQztZQUNyRCxXQUFXLENBQUMsWUFBWSxHQUFHLFVBQVUsQ0FBQyxZQUFZLENBQUM7WUFDbkQsaUJBQWlCLEdBQUcsV0FBVyxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUV2RCxPQUFPLElBQUksT0FBTyxDQUFlLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFO2dCQUNwRCxPQUFPLENBQUMsSUFBSSxDQUFDO29CQUNaLE9BQU8sRUFBRTt3QkFDUixjQUFjLEVBQUUsbUNBQW1DO3dCQUNuRCxRQUFRLEVBQUUsa0JBQWtCO3FCQUM1QjtvQkFDRCxHQUFHLEVBQUUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEdBQUcsb0NBQW9DO29CQUNoRSxJQUFJLEVBQUUsaUJBQWlCO2lCQUN2QixFQUFFLENBQUMsR0FBVSxFQUFFLElBQVMsRUFBRSxJQUFTLEVBQUUsRUFBRTtvQkFDdkMsSUFBSSxDQUFDLEdBQUcsRUFBRTt3QkFDVCxJQUFJLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDL0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDbEIsT0FBTyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7cUJBQ3JCO3lCQUNJO3dCQUNKLE9BQU8sTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO3FCQUNuQjtnQkFDRixDQUFDLENBQUMsQ0FBQztZQUNKLENBQUMsQ0FBQyxDQUFBO1FBQ0gsQ0FBQztLQUFBO0lBRUssVUFBVSxDQUFDLEtBQVU7O1lBQzFCLG9CQUFvQjtZQUNwQixJQUFJLFlBQW9CLENBQUM7WUFDekIsSUFBSSxLQUFVLENBQUM7WUFDZixJQUFJLG9CQUF5QixDQUFDO1lBQzlCLElBQUksVUFBVSxHQUFjLElBQUksb0JBQVMsRUFBRSxDQUFDO1lBQzVDLElBQUksT0FBTyxHQUFjLElBQUksb0JBQVMsRUFBRSxDQUFDO1lBR3pDLHlEQUF5RDtZQUN6RCw2QkFBNkI7WUFDN0Isa0ZBQWtGO1lBQ2xGLHFCQUFxQjtZQUNyQixvQkFBb0IsR0FBRyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDMUMsS0FBSyxHQUFHLE1BQU0sSUFBSSxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1lBQ3hELFlBQVksR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDO1lBRTVCLFVBQVUsQ0FBQyxHQUFHLEdBQUcsb0JBQW9CLENBQUMsR0FBRyxDQUFDO1lBQzFDLFVBQVUsQ0FBQyxPQUFPLEdBQUcsb0JBQW9CLENBQUMsT0FBTyxDQUFDO1lBQ2xELFVBQVUsQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQztZQUNsQyxVQUFVLENBQUMsT0FBTyxHQUFHLGlCQUFpQixDQUFDO1lBQ3ZDLElBQUksU0FBUyxHQUFHLE1BQU0sSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMvQyxVQUFVLENBQUMsS0FBSyxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUM7WUFFbEMsT0FBTyxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7WUFDdkIsT0FBTyxDQUFDLElBQUksR0FBRyxVQUFVLENBQUMsR0FBRyxDQUFDO1lBQzlCLElBQUksU0FBUyxHQUFHO2dCQUNmLENBQUMsWUFBWSxDQUFDLEVBQUUsT0FBTztnQkFFdkIsSUFBSTtnQkFFSiw0QkFBNEI7Z0JBQzVCLHFCQUFxQjtnQkFDckIsd0JBQXdCO2dCQUN4QixJQUFJO2FBQ0osQ0FBQTtZQUNELEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDL0MsSUFBSSxlQUFlLEdBQUcsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUM7Z0JBRWpELGVBQWU7Z0JBQ2Ysa0JBQWtCO2dCQUNsQixzQ0FBc0M7Z0JBQ3RDLHlCQUF5QjtnQkFDekIsNEJBQTRCO2dCQUM1QixnREFBZ0Q7Z0JBQ2hELGVBQWU7Z0JBQ2YsbUNBQW1DO2dCQUNuQyxrQkFBa0I7Z0JBQ2xCLG1CQUFtQjtnQkFDbkIsaUJBQWlCO2dCQUNqQixnQkFBZ0I7Z0JBQ2hCLFVBQVU7Z0JBQ1YsT0FBTztnQkFDUCxLQUFLO2dCQUNMLElBQUk7Z0JBQ0osSUFBSSxJQUFJLEdBQVMsSUFBSSxlQUFJLEVBQUUsQ0FBQztnQkFDNUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxDQUFDLENBQUM7Z0JBQ3BCLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUM7Z0JBQ3hDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxFQUFFLENBQUM7Z0JBQzNCLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxFQUFFLENBQUM7Z0JBQzdCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDLG1CQUFtQixDQUFDLE9BQU8sQ0FBQztnQkFDbEQsSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUM7Z0JBQ2pCLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7Z0JBQ3JDLElBQUksQ0FBQyxRQUFRLEdBQUcsR0FBRyxDQUFDO2dCQUNwQixJQUFJLENBQUMsU0FBUyxHQUFHLEdBQUcsQ0FBQztnQkFDckIsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUM7Z0JBQ25CLElBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyxDQUFDO2dCQUNsQixJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksS0FBSyxFQUFXLENBQUM7Z0JBRWhDLElBQUksS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLEtBQUssSUFBSSxJQUFJLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7b0JBQ3ZGLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7d0JBQzlELElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxrQkFBTyxFQUFFLENBQUM7d0JBQzVCLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQzt3QkFDN0QsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDO3dCQUMvRCxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLGVBQWUsR0FBRyxDQUFDLENBQUM7cUJBQ2hDO2lCQUNEO2dCQUVELEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxlQUFlLEVBQUUsQ0FBQyxFQUFFLEVBQUU7b0JBQ3pDLFNBQVMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUN6QztnQkFHRCxrQ0FBa0M7Z0JBQ2xDLDJDQUEyQztnQkFDM0Msc0NBQXNDO2dCQUN0Qyx5QkFBeUI7Z0JBQ3pCLDRCQUE0QjtnQkFDNUIsZ0RBQWdEO2dCQUNoRCxlQUFlO2dCQUNmLG1DQUFtQztnQkFDbkMsa0JBQWtCO2dCQUNsQixtQkFBbUI7Z0JBQ25CLGlCQUFpQjtnQkFDakIsZ0JBQWdCO2dCQUNoQixXQUFXO2dCQUNYLElBQUk7Z0JBQ0osNEZBQTRGO2dCQUM1RixvRUFBb0U7Z0JBQ3BFLDJDQUEyQztnQkFDM0Msc0RBQXNEO2dCQUN0RCx3REFBd0Q7Z0JBQ3hELHdCQUF3QjtnQkFDeEIsTUFBTTtnQkFDTixLQUFLO2dCQUNMLElBQUk7YUFFSjtZQUVELElBQUksVUFBVSxHQUFXLEVBQUUsQ0FBQztZQUM1QixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsU0FBUyxDQUFDLFlBQVksQ0FBQyxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQzlELFVBQVUsSUFBSSxTQUFTLENBQUMsWUFBWSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUM7YUFDNUQ7WUFDRCxVQUFVLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNyQyxJQUFJLFdBQW1CLENBQUM7WUFDeEIsSUFBSSxrQkFBMEIsQ0FBQztZQUMvQixXQUFXLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUN4QyxVQUFVLENBQUMsU0FBUyxHQUFHLElBQUksTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUNsRSxVQUFVLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUM1RSxrQkFBa0IsR0FBRyxXQUFXLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBR3ZELDBEQUEwRDtZQUUxRCxLQUFLO1lBR0wsT0FBTyxJQUFJLE9BQU8sQ0FBZSxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtnQkFDcEQsT0FBTyxDQUFDLElBQUksQ0FBQztvQkFDWixPQUFPLEVBQUU7d0JBQ1IsY0FBYyxFQUFFLG1DQUFtQzt3QkFDbkQsUUFBUSxFQUFFLGtCQUFrQjtxQkFDNUI7b0JBQ0QsR0FBRyxFQUFFLGlFQUFpRTtvQkFDdEUsSUFBSSxFQUFFLGtCQUFrQjtpQkFDeEIsRUFBRSxDQUFPLEdBQUcsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLEVBQUU7b0JBQzVCLElBQUksQ0FBQyxHQUFHLEVBQUU7d0JBQ1QsSUFBSSxHQUFHLE1BQU0sSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDckMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDbEIsT0FBTyxJQUFJLHVCQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7cUJBQzlCO3lCQUNJO3dCQUNKLE9BQU8sTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO3FCQUNuQjtnQkFDRixDQUFDLENBQUEsQ0FBQyxDQUFDO1lBQ0osQ0FBQyxDQUFDLENBQUM7UUFJSixDQUFDO0tBQUE7SUFDRCxVQUFVLENBQUMsVUFBZTtRQUN6QixNQUFNLElBQUksS0FBSyxDQUFDLHlCQUF5QixDQUFDLENBQUM7SUFDNUMsQ0FBQztJQUNELFdBQVc7UUFDVixNQUFNLElBQUksS0FBSyxDQUFDLHlCQUF5QixDQUFDLENBQUM7SUFDNUMsQ0FBQztJQUVhLFlBQVksQ0FBQyxLQUFVOztZQUNwQyxJQUFJLFlBQVksR0FBRyxJQUFJLE1BQU0sQ0FBQyxLQUFLLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFDL0MsSUFBSSxZQUFpQixDQUFDO1lBQ3RCLFlBQVksR0FBRyxZQUFZLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzlDLFlBQVksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ3hDLE9BQU8sWUFBWSxDQUFDO1FBQ3JCLENBQUM7S0FBQTtJQUVELFdBQVc7UUFFVixJQUFJLFFBQVEsR0FBRyxJQUFJLHNCQUFXLEVBQUUsQ0FBQztRQUNqQyxRQUFRLENBQUMsT0FBTyxHQUFHLGtCQUFrQixDQUFDO1FBQ3RDLFFBQVEsQ0FBQyxHQUFHLEdBQUcsbUJBQW1CLENBQUM7UUFDbkMsUUFBUSxDQUFDLGFBQWEsR0FBRyxzQ0FBc0MsQ0FBQztRQUNoRSxRQUFRLENBQUMsWUFBWSxHQUFHLE1BQU0sQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxhQUFhLEdBQUcsV0FBVyxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBRTVHLE9BQU8sUUFBUSxDQUFDO0lBQ2pCLENBQUM7SUFBQSxDQUFDO0NBRUYsQ0FBQTtBQW5Ob0Isc0JBQXNCO0lBRDFDLHNCQUFVLEVBQUU7R0FDUSxzQkFBc0IsQ0FtTjFDO2tCQW5Ob0Isc0JBQXNCIiwiZmlsZSI6ImFwaS1vdXRib3VuZC9pbnRlZ3JhdGlvbi9lcG9jLWFwaS9pbXBsZW1lbnRhdGlvbi9lcG9jLmludGVncmF0aW9uLWFkYXB0ZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgRXBvY0ludGVncmF0aW9uUG9ydCBmcm9tIFwiLi4vZXBvYy5pbnRlZ3JhdGlvbi1wb3J0XCI7XHJcbmltcG9ydCByZXF1ZXN0ID0gcmVxdWlyZSgncmVxdWVzdCcpO1xyXG5pbXBvcnQgY3J5cHRvID0gcmVxdWlyZSgnY3J5cHRvJyk7XHJcbmltcG9ydCBxdWVyeXN0cmluZyA9IHJlcXVpcmUoJ3F1ZXJ5c3RyaW5nJyk7XHJcblxyXG5pbXBvcnQge1xyXG5cdENvbWFuZGEsXHJcblx0VmVuZGFJdGVtLFxyXG5cdE9yZGVySXRlbSxcclxuXHRJdGVtLFxyXG5cdEl0ZW1PYnMsXHJcblx0Q2FyZ2FQcm9kdXRvLFxyXG5cdEV4dHJhdG8sXHJcblx0U2lzdGVtYUF1dGhcclxufSBmcm9tICcuLi8uLi8uLi9kdG8vZXBvYy5kdG8nO1xyXG5pbXBvcnQgeyBpbmplY3RhYmxlIH0gZnJvbSBcImludmVyc2lmeVwiO1xyXG5pbXBvcnQgUmVzcG9uc2VFcG9jIGZyb20gXCIuLi8uLi8uLi9lbnZlbG9wZXMvcmVzcG9uc2UuZXBvY1wiO1xyXG5pbXBvcnQgeyByZWplY3RzIH0gZnJvbSBcImFzc2VydFwiO1xyXG5kZWNsYXJlIGNvbnN0IEJ1ZmZlcjogYW55O1xyXG5cclxuQGluamVjdGFibGUoKVxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBFcG9jSW50ZWdyYXRpb25BZGFwdGVyIGltcGxlbWVudHMgRXBvY0ludGVncmF0aW9uUG9ydCB7XHJcblxyXG5cclxuXHRhc3luYyB0b2tlbkdlbmVyYXRvcihzeXN0ZW1BdXRoOiBhbnkpOiBQcm9taXNlPFJlc3BvbnNlRXBvYz4ge1xyXG5cclxuXHRcdGxldCBzaXN0ZW1hQXV0aDogU2lzdGVtYUF1dGggPSBuZXcgU2lzdGVtYUF1dGgoKTtcclxuXHRcdGxldCBzaXN0ZW1hQXV0aFN0cmluZzogc3RyaW5nO1xyXG5cdFx0c2lzdGVtYUF1dGguaGFzaGVtcCA9IHN5c3RlbUF1dGguaGFzaGVtcDtcclxuXHRcdHNpc3RlbWFBdXRoLm1hYyA9IHN5c3RlbUF1dGgubWFjO1xyXG5cdFx0c2lzdGVtYUF1dGguc2VydmljZSA9ICdnZXJhVG9rZW4nO1xyXG5cdFx0c2lzdGVtYUF1dGgudG9rZW5QYXJjZWlybyA9IHN5c3RlbUF1dGgudG9rZW5QYXJjZWlybztcclxuXHRcdHNpc3RlbWFBdXRoLnNhbHRQYXJjZWlybyA9IHN5c3RlbUF1dGguc2FsdFBhcmNlaXJvO1xyXG5cdFx0c2lzdGVtYUF1dGhTdHJpbmcgPSBxdWVyeXN0cmluZy5zdHJpbmdpZnkoc2lzdGVtYUF1dGgpO1xyXG5cclxuXHRcdHJldHVybiBuZXcgUHJvbWlzZTxSZXNwb25zZUVwb2M+KChyZXNvbHZlLCByZWplY3QpID0+IHtcclxuXHRcdFx0cmVxdWVzdC5wb3N0KHtcclxuXHRcdFx0XHRoZWFkZXJzOiB7XHJcblx0XHRcdFx0XHQnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL3gtd3d3LWZvcm0tdXJsZW5jb2RlZCcsXHJcblx0XHRcdFx0XHQnQWNjZXB0JzogJ2FwcGxpY2F0aW9uL2pzb24nXHJcblx0XHRcdFx0fSxcclxuXHRcdFx0XHR1cmw6IHByb2Nlc3MuZW52LkVQT0NfVVJMICsgYC9BUEkvbW9kX3ZhbGlkYV9wYXJjZWlyby9pbmRleC5waHBgLFxyXG5cdFx0XHRcdGJvZHk6IHNpc3RlbWFBdXRoU3RyaW5nXHJcblx0XHRcdH0sIChlcnI6IEVycm9yLCByZXNwOiBhbnksIGJvZHk6IGFueSkgPT4ge1xyXG5cdFx0XHRcdGlmICghZXJyKSB7XHJcblx0XHRcdFx0XHRib2R5ID0gdGhpcy50b2tlbkRlY29kZXIoYm9keSk7XHJcblx0XHRcdFx0XHRjb25zb2xlLmxvZyhib2R5KTtcclxuXHRcdFx0XHRcdHJldHVybiByZXNvbHZlKGJvZHkpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRlbHNlIHtcclxuXHRcdFx0XHRcdHJldHVybiByZWplY3QoZXJyKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0pO1xyXG5cdFx0fSlcclxuXHR9XHJcblxyXG5cdGFzeW5jIHZlbmRlckl0ZW0ob3JkZXI6IGFueSk6IFByb21pc2U8UmVzcG9uc2VFcG9jPiB7XHJcblx0XHQvLyBsZXQgcGF5bG9hZDogYW55O1xyXG5cdFx0bGV0IGNvbnN1bWVyQ2FyZDogc3RyaW5nO1xyXG5cdFx0bGV0IHRva2VuOiBhbnk7XHJcblx0XHRsZXQgc2lzdGVtYUdlc3Rhb1BheWxvYWQ6IGFueTtcclxuXHRcdGxldCB2ZW5kZXJJdGVtOiBWZW5kYUl0ZW0gPSBuZXcgVmVuZGFJdGVtKCk7XHJcblx0XHRsZXQgb3JkZXJJdDogT3JkZXJJdGVtID0gbmV3IE9yZGVySXRlbSgpO1xyXG5cclxuXHJcblx0XHQvLyBsZXQgcHJvZHV0b3M6IEFycmF5PFByb2R1dG9zPiA9IG5ldyBBcnJheTxQcm9kdXRvcz4oKTtcclxuXHRcdC8vIHByb2R1dG9zID0gb3JkZXIucHJvZHVjdHM7XHJcblx0XHQvLyBwYXlsb2FkID0gYXdhaXQgdGhpcy50b2tlbk1hbmFnZXIuZGVjb2RpZmljYVRva2VuKEpTT04ucGFyc2UoYm9keS50b2tlblBlZGRpKSk7XHJcblx0XHQvLyBwYXlsb2FkID0gcGF5bG9hZDtcclxuXHRcdHNpc3RlbWFHZXN0YW9QYXlsb2FkID0gdGhpcy5zaXN0ZW1hQXV0aCgpO1xyXG5cdFx0dG9rZW4gPSBhd2FpdCB0aGlzLnRva2VuR2VuZXJhdG9yKHNpc3RlbWFHZXN0YW9QYXlsb2FkKTtcclxuXHRcdGNvbnN1bWVyQ2FyZCA9IG9yZGVyLnRpY2tldDtcclxuXHJcblx0XHR2ZW5kZXJJdGVtLm1hYyA9IHNpc3RlbWFHZXN0YW9QYXlsb2FkLm1hYztcclxuXHRcdHZlbmRlckl0ZW0uaGFzaGVtcCA9IHNpc3RlbWFHZXN0YW9QYXlsb2FkLmhhc2hlbXA7XHJcblx0XHR2ZW5kZXJJdGVtLnVsdF9tZXNhID0gb3JkZXIudGFibGU7XHJcblx0XHR2ZW5kZXJJdGVtLnNlcnZpY2UgPSAnR3JhdmFyVmVuZGFJdGVtJztcclxuXHRcdGxldCB0b2tlbkRhdGEgPSBhd2FpdCB0aGlzLnRva2VuRGVjb2Rlcih0b2tlbik7XHJcblx0XHR2ZW5kZXJJdGVtLnRva2VuID0gdG9rZW5EYXRhLmRhdGE7XHJcblxyXG5cdFx0b3JkZXJJdC5DT0RFVkVOVE8gPSBcIlwiO1xyXG5cdFx0b3JkZXJJdC5IT1NUID0gdmVuZGVySXRlbS5tYWM7XHJcblx0XHRsZXQgb3JkZXJJdGVtID0ge1xyXG5cdFx0XHRbY29uc3VtZXJDYXJkXTogb3JkZXJJdFxyXG5cclxuXHRcdFx0Ly8ge1xyXG5cclxuXHRcdFx0Ly8gXHQvLyBIT1NUOiB2ZW5kZXJJdGVtLm1hYyxcclxuXHRcdFx0Ly8gXHQvLyBDT0RFVkVOVE86IFwiXCIsXHJcblx0XHRcdC8vIFx0aXRlbnM6IEFycmF5PEl0ZW0+KClcclxuXHRcdFx0Ly8gfVxyXG5cdFx0fVxyXG5cdFx0Zm9yIChsZXQgaSA9IDA7IGkgPCBvcmRlci5wcm9kdWN0cy5sZW5ndGg7IGkrKykge1xyXG5cdFx0XHRsZXQgcHJvZHVjdFF1YW50aXR5ID0gb3JkZXIucHJvZHVjdHNbaV0ucXVhbnRpdHk7XHJcblxyXG5cdFx0XHQvLyBsZXQgaXRlbSA9IHtcclxuXHRcdFx0Ly8gXHRxdWFudGlkYWRlOiAxLFxyXG5cdFx0XHQvLyBcdGNvZGlnbzogb3JkZXIucHJvZHVjdHNbaV0uZXBvY19pZCxcclxuXHRcdFx0Ly8gXHRjb2RfZnVuY19hdXRvcml6OiBcIlwiLFxyXG5cdFx0XHQvLyBcdG1vdGl2b19hdXRvcml6YWNhbzogXCIxXCIsXHJcblx0XHRcdC8vIFx0Y29kX2Z1bmM6IG9yZGVyLnJlc3BvbnNpYmxlRW1wbG95ZWUuZXBvY19pZCxcclxuXHRcdFx0Ly8gXHRtYXJjaGFyOiAwLFxyXG5cdFx0XHQvLyBcdHZhbG9yOiBvcmRlci5wcm9kdWN0c1tpXS5wcmljZSxcclxuXHRcdFx0Ly8gXHR0aXBvSXRlbTogXCJQXCIsXHJcblx0XHRcdC8vIFx0bGFuY2VJdGVtOiBcIlNcIixcclxuXHRcdFx0Ly8gXHRhY3Jlc2NpbW86IDAsXHJcblx0XHRcdC8vIFx0ZGVzY29udG86IDAsXHJcblx0XHRcdC8vIFx0b2JzOiBbXHJcblx0XHRcdC8vIFx0XHR7fVxyXG5cdFx0XHQvLyBcdF1cclxuXHRcdFx0Ly8gfVxyXG5cdFx0XHRsZXQgaXRlbTogSXRlbSA9IG5ldyBJdGVtKCk7XHJcblx0XHRcdGl0ZW0ucXVhbnRpZGFkZSA9IDE7XHJcblx0XHRcdGl0ZW0uY29kaWdvID0gb3JkZXIucHJvZHVjdHNbaV0uZXBvY19pZDtcclxuXHRcdFx0aXRlbS5jb2RfZnVuY19hdXRvcml6ID0gXCJcIjtcclxuXHRcdFx0aXRlbS5tb3Rpdm9fYXV0b3JpemFjYW8gPSBcIlwiO1xyXG5cdFx0XHRpdGVtLmNvZF9mdW5jID0gb3JkZXIucmVzcG9uc2libGVFbXBsb3llZS5lcG9jX2lkO1xyXG5cdFx0XHRpdGVtLm1hcmNoYXIgPSAwO1xyXG5cdFx0XHRpdGVtLnZhbG9yID0gb3JkZXIucHJvZHVjdHNbaV0ucHJpY2U7XHJcblx0XHRcdGl0ZW0udGlwb0l0ZW0gPSBcIlBcIjtcclxuXHRcdFx0aXRlbS5sYW5jZUl0ZW0gPSBcIlNcIjtcclxuXHRcdFx0aXRlbS5hY3Jlc2NpbW8gPSAwO1xyXG5cdFx0XHRpdGVtLmRlc2NvbnRvID0gMDtcclxuXHRcdFx0aXRlbS5vYnMgPSBuZXcgQXJyYXk8SXRlbU9icz4oKTtcclxuXHJcblx0XHRcdGlmIChvcmRlci5wcm9kdWN0c1tpXS5pbmdyZWRpZW50cyAhPT0gbnVsbCAmJiBvcmRlci5wcm9kdWN0c1tpXS5pbmdyZWRpZW50cy5sZW5ndGggPiAwKSB7XHJcblx0XHRcdFx0Zm9yIChsZXQgaiA9IDA7IGogPCBvcmRlci5wcm9kdWN0c1tpXS5pbmdyZWRpZW50cy5sZW5ndGg7IGorKykge1xyXG5cdFx0XHRcdFx0aXRlbS5vYnNbal0gPSBuZXcgSXRlbU9icygpO1xyXG5cdFx0XHRcdFx0aXRlbS5vYnNbal0uZGVzY19vYnMgPSBvcmRlci5wcm9kdWN0c1tpXS5pbmdyZWRpZW50c1tqXS5uYW1lO1xyXG5cdFx0XHRcdFx0aXRlbS5vYnNbal0uY29kX29icyA9IG9yZGVyLnByb2R1Y3RzW2ldLmluZ3JlZGllbnRzW2pdLmVwb2NfaWQ7XHJcblx0XHRcdFx0XHRpdGVtLm9ic1tqXS5jb2RfbW9kaWZpY2Fkb3IgPSAwO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0Zm9yIChsZXQgcSA9IDA7IHEgPCBwcm9kdWN0UXVhbnRpdHk7IHErKykge1xyXG5cdFx0XHRcdG9yZGVySXRlbVtjb25zdW1lckNhcmRdLklURU5TLnB1c2goaXRlbSk7XHJcblx0XHRcdH1cclxuXHJcblxyXG5cdFx0XHQvLyBvcmRlckl0ZW1bY29tYW5kYV0uSVRFTlNbaV0gPSB7XHJcblx0XHRcdC8vIFx0cXVhbnRpZGFkZTogb3JkZXIucHJvZHVjdHNbaV0ucXVhbnRpdHksXHJcblx0XHRcdC8vIFx0Y29kaWdvOiBvcmRlci5wcm9kdWN0c1tpXS5lcG9jX2lkLFxyXG5cdFx0XHQvLyBcdGNvZF9mdW5jX2F1dG9yaXo6IFwiXCIsXHJcblx0XHRcdC8vIFx0bW90aXZvX2F1dG9yaXphY2FvOiBcIjFcIixcclxuXHRcdFx0Ly8gXHRjb2RfZnVuYzogb3JkZXIucmVzcG9uc2libGVFbXBsb3llZS5lcG9jX2lkLFxyXG5cdFx0XHQvLyBcdG1hcmNoYXI6IDAsXHJcblx0XHRcdC8vIFx0dmFsb3I6IG9yZGVyLnByb2R1Y3RzW2ldLnByaWNlLFxyXG5cdFx0XHQvLyBcdHRpcG9JdGVtOiBcIlBcIixcclxuXHRcdFx0Ly8gXHRsYW5jZUl0ZW06IFwiU1wiLFxyXG5cdFx0XHQvLyBcdGFjcmVzY2ltbzogMCxcclxuXHRcdFx0Ly8gXHRkZXNjb250bzogMCxcclxuXHRcdFx0Ly8gXHRvYnM6IFtdXHJcblx0XHRcdC8vIH1cclxuXHRcdFx0Ly8gaWYgKG9yZGVyLnByb2R1Y3RzW2ldLmluZ3JlZGllbnRzICE9PSBudWxsICYmIG9yZGVyLnByb2R1Y3RzW2ldLmluZ3JlZGllbnRzLmxlbmd0aCA+IDApIHtcclxuXHRcdFx0Ly8gXHRmb3IgKGxldCBqID0gMDsgaiA8IG9yZGVyLnByb2R1Y3RzW2ldLmluZ3JlZGllbnRzLmxlbmd0aDsgaisrKSB7XHJcblx0XHRcdC8vIFx0XHRvcmRlckl0ZW1bY29tYW5kYV0uSVRFTlNbaV0ub2JzW2pdID0ge1xyXG5cdFx0XHQvLyBcdFx0XHRkZXNjX29iczogb3JkZXIucHJvZHVjdHNbaV0uaW5ncmVkaWVudHNbal0ubmFtZSxcclxuXHRcdFx0Ly8gXHRcdFx0Y29kX29iczogb3JkZXIucHJvZHVjdHNbaV0uaW5ncmVkaWVudHNbal0uZXBvY19pZCxcclxuXHRcdFx0Ly8gXHRcdFx0Y29kX21vZGlmaWNhZG9yOiAwXHJcblx0XHRcdC8vIFx0XHR9XHJcblx0XHRcdC8vIFx0fVxyXG5cdFx0XHQvLyB9XHJcblxyXG5cdFx0fVxyXG5cclxuXHRcdGxldCBzdHJpbmdTYWx0OiBzdHJpbmcgPSBcIlwiO1xyXG5cdFx0Zm9yIChsZXQgaSA9IDA7IGkgPCBvcmRlckl0ZW1bY29uc3VtZXJDYXJkXS5JVEVOUy5sZW5ndGg7IGkrKykge1xyXG5cdFx0XHRzdHJpbmdTYWx0ICs9IG9yZGVySXRlbVtjb25zdW1lckNhcmRdLklURU5TW2ldLmNvZGlnbyArICdfJztcclxuXHRcdH1cclxuXHRcdHN0cmluZ1NhbHQgPSBzdHJpbmdTYWx0LnNsaWNlKDAsIC0xKTtcclxuXHRcdGxldCBpdGVtQ29udmVydDogc3RyaW5nO1xyXG5cdFx0bGV0IHZlbmRlckl0ZW1Ub1N0cmluZzogc3RyaW5nO1xyXG5cdFx0aXRlbUNvbnZlcnQgPSBKU09OLnN0cmluZ2lmeShvcmRlckl0ZW0pO1xyXG5cdFx0dmVuZGVySXRlbS5kYXRhVmVuZGEgPSBuZXcgQnVmZmVyKGl0ZW1Db252ZXJ0KS50b1N0cmluZygnYmFzZTY0Jyk7XHJcblx0XHR2ZW5kZXJJdGVtLnNhbHQgPSBjcnlwdG8uY3JlYXRlSGFzaCgnbWQ1JykudXBkYXRlKHN0cmluZ1NhbHQpLmRpZ2VzdChcImhleFwiKTtcclxuXHRcdHZlbmRlckl0ZW1Ub1N0cmluZyA9IHF1ZXJ5c3RyaW5nLnN0cmluZ2lmeSh2ZW5kZXJJdGVtKTtcclxuXHJcblxyXG5cdFx0Ly8gcmV0dXJuIG5ldyBQcm9taXNlPFJlc3BvbnNlRXBvYz4oKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG5cclxuXHRcdC8vIH0pXHJcblxyXG5cclxuXHRcdHJldHVybiBuZXcgUHJvbWlzZTxSZXNwb25zZUVwb2M+KChyZXNvbHZlLCByZWplY3QpID0+IHtcclxuXHRcdFx0cmVxdWVzdC5wb3N0KHtcclxuXHRcdFx0XHRoZWFkZXJzOiB7XHJcblx0XHRcdFx0XHQnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL3gtd3d3LWZvcm0tdXJsZW5jb2RlZCcsXHJcblx0XHRcdFx0XHQnQWNjZXB0JzogJ2FwcGxpY2F0aW9uL2pzb24nXHJcblx0XHRcdFx0fSxcclxuXHRcdFx0XHR1cmw6IGBodHRwOi8vbGxtMDkuZXBvYzIuY2Rtb24ub3JnOjU2NDc4L0FQSS9tb2RfdmVuZGFfaXRlbS9pbmRleC5waHBgLFxyXG5cdFx0XHRcdGJvZHk6IHZlbmRlckl0ZW1Ub1N0cmluZ1xyXG5cdFx0XHR9LCBhc3luYyAoZXJyLCByZXNwLCBib2R5KSA9PiB7XHJcblx0XHRcdFx0aWYgKCFlcnIpIHtcclxuXHRcdFx0XHRcdGJvZHkgPSBhd2FpdCB0aGlzLnRva2VuRGVjb2Rlcihib2R5KTtcclxuXHRcdFx0XHRcdGNvbnNvbGUubG9nKGJvZHkpO1xyXG5cdFx0XHRcdFx0cmV0dXJuIG5ldyBSZXNwb25zZUVwb2MoYm9keSk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdGVsc2UgeyBcclxuXHRcdFx0XHRcdHJldHVybiByZWplY3QoZXJyKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0pO1xyXG5cdFx0fSk7XHJcblxyXG5cclxuXHJcblx0fVxyXG5cdGdldEV4dHJhdG8ob2JqRXh0cmF0bzogYW55KTogU3RyaW5nIHtcclxuXHRcdHRocm93IG5ldyBFcnJvcihcIk1ldGhvZCBub3QgaW1wbGVtZW50ZWQuXCIpO1xyXG5cdH1cclxuXHRnZXRQcm9kdWN0cygpOiBTdHJpbmcge1xyXG5cdFx0dGhyb3cgbmV3IEVycm9yKFwiTWV0aG9kIG5vdCBpbXBsZW1lbnRlZC5cIik7XHJcblx0fVxyXG5cclxuXHRwcml2YXRlIGFzeW5jIHRva2VuRGVjb2Rlcih0b2tlbjogYW55KSB7XHJcblx0XHRsZXQgZGVjb2RlQnVmZmVyID0gbmV3IEJ1ZmZlcih0b2tlbiwgJ2Jhc2U2NCcpO1xyXG5cdFx0bGV0IHRva2VuRGVjb2RlZDogYW55O1xyXG5cdFx0ZGVjb2RlQnVmZmVyID0gZGVjb2RlQnVmZmVyLnRvU3RyaW5nKCdhc2NpaScpO1xyXG5cdFx0dG9rZW5EZWNvZGVkID0gSlNPTi5wYXJzZShkZWNvZGVCdWZmZXIpO1xyXG5cdFx0cmV0dXJuIHRva2VuRGVjb2RlZDtcclxuXHR9XHJcblxyXG5cdHNpc3RlbWFBdXRoKCk6IFNpc3RlbWFBdXRoIHtcclxuXHJcblx0XHRsZXQgZXBvY0F1dGggPSBuZXcgU2lzdGVtYUF1dGgoKTtcclxuXHRcdGVwb2NBdXRoLmhhc2hlbXAgPSAnZGEyZzM5amJjZGphODYwMSc7XHJcblx0XHRlcG9jQXV0aC5tYWMgPSAnZTQ6YzE6ZGQ6YzA6OWE6NGQnO1xyXG5cdFx0ZXBvY0F1dGgudG9rZW5QYXJjZWlybyA9ICczNTNjMWVhMy02ODJiLTQ3NTQtOTFlZi1mNTVkMDVhZWM3NTgnO1xyXG5cdFx0ZXBvY0F1dGguc2FsdFBhcmNlaXJvID0gY3J5cHRvLmNyZWF0ZUhhc2goJ21kNScpLnVwZGF0ZShlcG9jQXV0aC50b2tlblBhcmNlaXJvICsgJ19fVE9LRU5fXycpLmRpZ2VzdChcImhleFwiKTtcclxuXHJcblx0XHRyZXR1cm4gZXBvY0F1dGg7XHJcblx0fTtcclxuXHJcbn0iXX0=
