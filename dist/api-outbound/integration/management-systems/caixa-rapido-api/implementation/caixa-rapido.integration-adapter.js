"use strict";
/**
 * Class containing the methods for integrating with the Caixa
 * Rapido API.
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require('fs');
const mssql = require('mssql');
const inversify_1 = require("inversify");
let CaixaRapidoIntegration = class CaixaRapidoIntegration {
    // constructor(dbURL: string) {
    constructor() {
        this.connect = () => __awaiter(this, void 0, void 0, function* () {
            yield this.pool.connect().catch(err => { throw err; });
            this.req = yield this.pool.request();
        });
        /**
         * @summary: method for creating a new order assocaited with a given table.
         */
        this.openTableProcedure = (table) => __awaiter(this, void 0, void 0, function* () {
            /*
            Procedure para abrir mesa:
            Nome: sp_AbreMesa
            Parametros: @NroMesa		-> Numero da Mesa ou Comanda
                        @NroCartao		-> Fixo 0
                        @Nome			-> Fixo ''
                        @IPTerminal		-> IP ou nome do dispositivo que esta enviando o pedido
            ??          @VendorID		-> Nro de série do dispositivo
                        @Versao			-> Fixo ''
                        @Origem			-> Fixo 'iOS'
            Retorno: Codigo e Mensagem
            Codigo = 0 -> Sucesso
            Codigo <> 0 -> Ver Mensagem de erro
    
            Exemplo de Uso:
            sp_AbreMesa @NroMesa = 3, @NroCartao = 0, @Nome = '', @IPTerminal = 'chris', @VendorID = '1a280798a8c9989c', @Versao = '', @Origem = 'IOS'
            */
            this.req = yield this.pool.request();
            const queryText = 'EXEC sp_AbreMesa @NroMesa = @nro_mesa, @NroCartao = 0, @Nome = \'\', @IPTerminal = @ip, @VendorID = @vendor_id, @Versao = \'\', @Origem = "IOS"';
            // define parameters
            this.req.input('nro_mesa', mssql.SmallInt, table);
            this.req.input('ip', mssql.NVarChar, 'Tablet - Mesa' + table);
            this.req.input('vendor_id', mssql.NVarChar, 'Tablet - Mesa' + table);
            // run query
            return this.req.query(queryText)
                .then(result => {
                if (result.recordset[0].Retorno !== 0) {
                    throw new Error(result.recordset[0].Mensagem);
                }
                console.log('Bill open.');
            })
                .catch(err => { throw err; });
        });
        this.openTable = (table) => __awaiter(this, void 0, void 0, function* () {
            if (!this.pool._connected) {
                yield this.connect().catch(err => { throw err; });
            }
            yield this.openTableProcedure(table).catch(err => { throw err; });
        });
        /**
         * @summary: method for adding a new product to the table's bill.
         */
        this.addProductToTableProcedure = (table, mgmtID) => __awaiter(this, void 0, void 0, function* () {
            /*
            Procedure para adicionar um produto na mesa:
            Nome: sp_AdicionaProduto
            Parametros: @NroMesa		-> Numero da Mesa ou Comanda
                        @NroCartao		-> Fixo 0
                        @Nome			-> Fixo ''
                        @IPTerminal		-> IP ou nome do dispositivo que esta enviando o pedido
                        @IDProduto		-> ID do produto(não é o código, é o campo ID Produto)
                        @Qtde			-> Informar a Qtde
                        @ObservacaoItem	-> No caso do FIT, informar ''
                        @VendedorID 	-> Fixo ''
    
            Retorno: Codigo e Mensagem
            Codigo = 0 -> Sucesso
            Codigo <> 0 -> Ver Mensagem de erro
    
            Exemplo de Uso:
            sp_AdicionaProduto @NroMesa = 3, @NroCartao = 0, @Nome = '', @IPTerminal = 'chris', @IDProduto = 14, @Qtde = 1.0, @ObservacaoItem = '', @VendedorID = ''
            */
            const queryText = 'EXEC sp_AdicionaProduto @NroMesa = @nro_mesa, @NroCartao = 0, @Nome = \'\', @IPTerminal = @ip, @IDProduto = @id_produto, @Qtde = 1.0, @ObservacaoItem = \'\', @VendedorID = \'\'';
            // define parameters
            this.req.input('nro_mesa', mssql.TinyInt, table);
            this.req.input('ip', mssql.NVarChar, 'Tablet - Mesa' + table);
            this.req.input('id_produto', mssql.NVarChar, mgmtID);
            // run query
            return this.req.query(queryText)
                .then(result => {
                if (result.recordset[0].Retorno !== 0) {
                    throw new Error(result.recordset[0].Mensagem);
                }
                console.log('Product added.');
            })
                .catch(err => { throw err; });
        });
        this.addProductsToTable = (update, table) => __awaiter(this, void 0, void 0, function* () {
            if (!this.pool._connected) {
                yield this.connect().catch(err => { throw err; });
            }
            let fails = [];
            for (let consumer of update.consumers) {
                for (let item of consumer.items) {
                    yield this.addProductToTableProcedure(table, item.mgmt_id).catch(err => fails.push({
                        cloud_id: item.base_cloud_id,
                        mgmt_id: item.mgmt_id,
                        message: err.message
                    }));
                    if (item.ingredients) {
                        for (let ingredient of item.ingredients) {
                            yield this.addProductToTableProcedure(table, ingredient.mgmt_id).catch(err => fails.push({
                                cloud_id: ingredient.cloud_id,
                                mgmt_id: ingredient.mgmt_id,
                                message: err.message
                            }));
                        }
                    }
                }
            }
            console.log('fails: ', fails);
            return fails;
        });
        this.closeTable = (table) => __awaiter(this, void 0, void 0, function* () {
            if (!this.pool._connected) {
                yield this.connect().catch(err => { throw err; });
            }
            yield this.closeTableProcedure(table);
            this.pool.close();
        });
        this.dbURL = fs.readFileSync('./src/api-outbound/integration/management-systems/caixa-rapido-api/url.txt', 'utf8');
        this.pool = new mssql.ConnectionPool(this.dbURL);
    }
    /**
     * @summary: method for finalizing the bill associated with a table.
     */
    closeTableProcedure(table) {
        /*
        Procedure para finalizar um pedido:
        Nome: sp_FinalizarPedido
        Parametros: @NroMesa		-> Numero da Mesa ou Comanda
                    @NroCartao		-> Fixo 0

        Retorno: Codigo e Mensagem
        Codigo = 0 -> Sucesso
        Codigo <> 0 -> Ver Mensagem de erro

        Exemplo de Uso:
        sp_FinalizarPedido @NroMesa = 3, @NroCartao = 0
        */
        const queryText = 'EXEC sp_FinalizarPedido @NroMesa = @nro_mesa, @NroCartao = 0';
        // define parameters
        this.req.input('nro_mesa', mssql.SmallInt, table);
        // run query
        return this.req.query(queryText)
            .then(result => {
            if (result.recordset[0].Retorno !== 0) {
                throw new Error('Invalid arguments.');
            }
            console.log('Bill closed.');
        })
            .catch(err => { throw err; });
    }
    /**
     * @summary: method for checking the bill of a table that is still ordering.
     */
    preBill(table, service, guests, print) {
        /*
        Procedure para visualizar a preconta
        Nome: sp_WSPreConta
        Parametros: @NroMesa		-> Numero da Mesa ou Comanda
                    @NroCartao		-> Fixo 0
                    @Servico		-> Indicador se vai ou não cobrar serviço(0 - não cobrar, ou 1 - cobrar)
                    @NroPessoas		-> Nro de pessoas na pesa
                    @Visualizar		-> Indicador se imprime (0) ou somente visualiza (1)

        Retorno: Codigo e Mensagem
        Codigo = 0 -> Sucesso
        Codigo <> 0 -> Ver Mensagem de erro

        Exemplo de Uso:
        sp_WSPreConta @NroMesa = 3, @NroCartao = 0, @Servico = 1, @NroPessoas = 1, @Visualizar = 1
        */
        const queryText = 'EXEC sp_WSPreConta @NroMesa = @nro_mesa, @NroCartao = 0, @Servico = @servico, @NroPessoas = @nro_pessoas, @Visualizar = @visualizar';
        // define parameters
        this.req.input('nro_mesa', mssql.SmallInt, table);
        this.req.input('servico', mssql.SmallInt, service);
        this.req.input('nro_pessoas', mssql.SmallInt, guests);
        this.req.input('visualizar', mssql.SmallInt, print);
        // run query
        return this.req.query(queryText)
            .then(result => {
            return result.recordset;
        })
            .catch(err => { throw err; });
    }
};
CaixaRapidoIntegration = __decorate([
    inversify_1.injectable(),
    __metadata("design:paramtypes", [])
], CaixaRapidoIntegration);
exports.default = CaixaRapidoIntegration;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9hcGktb3V0Ym91bmQvaW50ZWdyYXRpb24vbWFuYWdlbWVudC1zeXN0ZW1zL2NhaXhhLXJhcGlkby1hcGkvaW1wbGVtZW50YXRpb24vY2FpeGEtcmFwaWRvLmludGVncmF0aW9uLWFkYXB0ZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBOzs7R0FHRzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVILE1BQU0sRUFBRSxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQTtBQUN4QixNQUFNLEtBQUssR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUE7QUFDOUIseUNBQXVDO0FBT3ZDLElBQXFCLHNCQUFzQixHQUEzQyxNQUFxQixzQkFBc0I7SUFLMUMsK0JBQStCO0lBQy9CO1FBS1EsWUFBTyxHQUFHLEdBQVMsRUFBRTtZQUM1QixNQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFLEdBQUcsTUFBTSxHQUFHLENBQUEsQ0FBQyxDQUFDLENBQUMsQ0FBQTtZQUNyRCxJQUFJLENBQUMsR0FBRyxHQUFHLE1BQU0sSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQTtRQUNyQyxDQUFDLENBQUEsQ0FBQTtRQUVFOztXQUVHO1FBQ0UsdUJBQWtCLEdBQUcsQ0FBTyxLQUFhLEVBQUUsRUFBRTtZQUM5Qzs7Ozs7Ozs7Ozs7Ozs7OztjQWdCRTtZQUNSLElBQUksQ0FBQyxHQUFHLEdBQUcsTUFBTSxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFBO1lBQ3BDLE1BQU0sU0FBUyxHQUFHLGlKQUFpSixDQUFBO1lBQ25LLG9CQUFvQjtZQUNwQixJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxVQUFVLEVBQUUsS0FBSyxDQUFDLFFBQVEsRUFBRSxLQUFLLENBQUMsQ0FBQTtZQUNqRCxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLFFBQVEsRUFBRSxlQUFlLEdBQUcsS0FBSyxDQUFDLENBQUE7WUFDN0QsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQyxRQUFRLEVBQUUsZUFBZSxHQUFHLEtBQUssQ0FBQyxDQUFBO1lBQ3BFLFlBQVk7WUFDWixPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQztpQkFDOUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFO2dCQUNkLElBQUksTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEtBQUssQ0FBQyxFQUFFO29CQUN0QyxNQUFNLElBQUksS0FBSyxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUE7aUJBQzdDO2dCQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUE7WUFDMUIsQ0FBQyxDQUFDO2lCQUNELEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRSxHQUFHLE1BQU0sR0FBRyxDQUFBLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFDOUIsQ0FBQyxDQUFBLENBQUE7UUFFTSxjQUFTLEdBQUcsQ0FBTyxLQUFhLEVBQUUsRUFBRTtZQUMxQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUU7Z0JBQzFCLE1BQU0sSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRSxHQUFFLE1BQU0sR0FBRyxDQUFBLENBQUEsQ0FBQyxDQUFDLENBQUE7YUFDOUM7WUFDRCxNQUFNLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsR0FBRSxNQUFNLEdBQUcsQ0FBQSxDQUFBLENBQUMsQ0FBQyxDQUFBO1FBQy9ELENBQUMsQ0FBQSxDQUFBO1FBR0U7O1dBRUc7UUFDRSwrQkFBMEIsR0FBRyxDQUFPLEtBQWEsRUFBRSxNQUFjLEVBQWlCLEVBQUU7WUFDckY7Ozs7Ozs7Ozs7Ozs7Ozs7OztjQWtCRTtZQUNSLE1BQU0sU0FBUyxHQUFHLGtMQUFrTCxDQUFBO1lBQ3BNLG9CQUFvQjtZQUNwQixJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxVQUFVLEVBQUUsS0FBSyxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQTtZQUNoRCxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLFFBQVEsRUFBRSxlQUFlLEdBQUcsS0FBSyxDQUFDLENBQUE7WUFDN0QsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsWUFBWSxFQUFFLEtBQUssQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUE7WUFDcEQsWUFBWTtZQUNaLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDO2lCQUM5QixJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUU7Z0JBQ2QsSUFBSSxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sS0FBSyxDQUFDLEVBQUU7b0JBQ3RDLE1BQU0sSUFBSSxLQUFLLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQTtpQkFDN0M7Z0JBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFBO1lBQzlCLENBQUMsQ0FBQztpQkFDRCxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsR0FBRSxNQUFNLEdBQUcsQ0FBQSxDQUFBLENBQUMsQ0FBQyxDQUFBO1FBQzVCLENBQUMsQ0FBQSxDQUFBO1FBRU0sdUJBQWtCLEdBQUcsQ0FBTyxNQUFtQixFQUFHLEtBQWEsRUFBdUIsRUFBRTtZQUM5RixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUU7Z0JBQzFCLE1BQU0sSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRSxHQUFHLE1BQU0sR0FBRyxDQUFBLENBQUMsQ0FBQyxDQUFDLENBQUE7YUFDaEQ7WUFDRCxJQUFJLEtBQUssR0FBRyxFQUFFLENBQUE7WUFDZCxLQUFLLElBQUksUUFBUSxJQUFJLE1BQU0sQ0FBQyxTQUFTLEVBQUU7Z0JBQ3RDLEtBQUssSUFBSSxJQUFJLElBQUksUUFBUSxDQUFDLEtBQUssRUFBRTtvQkFDaEMsTUFBTSxJQUFJLENBQUMsMEJBQTBCLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FDdEUsS0FBSyxDQUFDLElBQUksQ0FBQzt3QkFDVixRQUFRLEVBQUUsSUFBSSxDQUFDLGFBQWE7d0JBQzVCLE9BQU8sRUFBRSxJQUFJLENBQUMsT0FBTzt3QkFDckIsT0FBTyxFQUFFLEdBQUcsQ0FBQyxPQUFPO3FCQUNwQixDQUFDLENBQUMsQ0FBQTtvQkFDSixJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7d0JBQ3JCLEtBQUssSUFBSSxVQUFVLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTs0QkFDeEMsTUFBTSxJQUFJLENBQUMsMEJBQTBCLENBQUMsS0FBSyxFQUFFLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FDNUUsS0FBSyxDQUFDLElBQUksQ0FBQztnQ0FDVixRQUFRLEVBQUUsVUFBVSxDQUFDLFFBQVE7Z0NBQzdCLE9BQU8sRUFBRSxVQUFVLENBQUMsT0FBTztnQ0FDM0IsT0FBTyxFQUFFLEdBQUcsQ0FBQyxPQUFPOzZCQUNwQixDQUFDLENBQUMsQ0FBQTt5QkFDSjtxQkFDRDtpQkFDRDthQUNEO1lBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsS0FBSyxDQUFDLENBQUE7WUFDN0IsT0FBTyxLQUFLLENBQUE7UUFDYixDQUFDLENBQUEsQ0FBQTtRQWlDTSxlQUFVLEdBQUcsQ0FBTyxLQUFhLEVBQUUsRUFBRTtZQUMzQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUU7Z0JBQzFCLE1BQU0sSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRSxHQUFHLE1BQU0sR0FBRyxDQUFBLENBQUMsQ0FBQyxDQUFDLENBQUE7YUFDaEQ7WUFDRCxNQUFNLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsQ0FBQTtZQUNyQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFBO1FBQ2xCLENBQUMsQ0FBQSxDQUFBO1FBaEtBLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDLFlBQVksQ0FBQyw0RUFBNEUsRUFBRSxNQUFNLENBQUMsQ0FBQTtRQUNsSCxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksS0FBSyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUE7SUFDakQsQ0FBQztJQXlIRTs7T0FFRztJQUNFLG1CQUFtQixDQUFFLEtBQWE7UUFDbkM7Ozs7Ozs7Ozs7OztVQVlFO1FBQ1IsTUFBTSxTQUFTLEdBQUcsOERBQThELENBQUE7UUFDaEYsb0JBQW9CO1FBQ3BCLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUMsUUFBUSxFQUFFLEtBQUssQ0FBQyxDQUFBO1FBQ2pELFlBQVk7UUFDWixPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQzthQUM5QixJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUU7WUFDZCxJQUFJLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxLQUFLLENBQUMsRUFBRTtnQkFDdEMsTUFBTSxJQUFJLEtBQUssQ0FBQyxvQkFBb0IsQ0FBQyxDQUFBO2FBQ3JDO1lBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQTtRQUM1QixDQUFDLENBQUM7YUFDRCxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsR0FBRyxNQUFNLEdBQUcsQ0FBQSxDQUFDLENBQUMsQ0FBQyxDQUFBO0lBQzlCLENBQUM7SUFXRTs7T0FFRztJQUNDLE9BQU8sQ0FBQyxLQUFhLEVBQUUsT0FBZSxFQUFFLE1BQWMsRUFBRSxLQUFhO1FBQ3JFOzs7Ozs7Ozs7Ozs7Ozs7VUFlRTtRQUNSLE1BQU0sU0FBUyxHQUFHLHFJQUFxSSxDQUFBO1FBQ3ZKLG9CQUFvQjtRQUNwQixJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxVQUFVLEVBQUUsS0FBSyxDQUFDLFFBQVEsRUFBRSxLQUFLLENBQUMsQ0FBQTtRQUNqRCxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxTQUFTLEVBQUUsS0FBSyxDQUFDLFFBQVEsRUFBRSxPQUFPLENBQUMsQ0FBQTtRQUNsRCxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxhQUFhLEVBQUUsS0FBSyxDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQTtRQUNyRCxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxZQUFZLEVBQUUsS0FBSyxDQUFDLFFBQVEsRUFBRSxLQUFLLENBQUMsQ0FBQTtRQUNuRCxZQUFZO1FBQ1osT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUM7YUFDOUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFO1lBQ2QsT0FBTyxNQUFNLENBQUMsU0FBUyxDQUFBO1FBQ3hCLENBQUMsQ0FBQzthQUNELEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRSxHQUFHLE1BQU0sR0FBRyxDQUFBLENBQUMsQ0FBQyxDQUFDLENBQUE7SUFDOUIsQ0FBQztDQUNELENBQUE7QUEzTW9CLHNCQUFzQjtJQUQxQyxzQkFBVSxFQUFFOztHQUNRLHNCQUFzQixDQTJNMUM7a0JBM01vQixzQkFBc0IiLCJmaWxlIjoiYXBpLW91dGJvdW5kL2ludGVncmF0aW9uL21hbmFnZW1lbnQtc3lzdGVtcy9jYWl4YS1yYXBpZG8tYXBpL2ltcGxlbWVudGF0aW9uL2NhaXhhLXJhcGlkby5pbnRlZ3JhdGlvbi1hZGFwdGVyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXHJcbiAqIENsYXNzIGNvbnRhaW5pbmcgdGhlIG1ldGhvZHMgZm9yIGludGVncmF0aW5nIHdpdGggdGhlIENhaXhhXHJcbiAqIFJhcGlkbyBBUEkuXHJcbiAqL1xyXG5cclxuY29uc3QgZnMgPSByZXF1aXJlKCdmcycpXHJcbmNvbnN0IG1zc3FsID0gcmVxdWlyZSgnbXNzcWwnKVxyXG5pbXBvcnQgeyBpbmplY3RhYmxlIH0gZnJvbSBcImludmVyc2lmeVwiO1xyXG5pbXBvcnQgTWFuYWdlbWVudFN5c3RlbUludGVncmF0aW9uUG9ydCBmcm9tICcuLi8uLi9tZ210LXN5c3QuaW50ZWdyYXRpb24tcG9ydCdcclxuaW1wb3J0IHsgUHJvZHVjdERUTyB9IGZyb20gJy4uLy4uLy4uLy4uL2R0by9jYWl4YS1yYXBpZG8uZHRvJ1xyXG5pbXBvcnQgeyBVcGRhdGVNb2RlbCB9IGZyb20gJy4uLy4uLy4uLy4uLy4uL2FwaS1jb3JlL2NvbW1vbnMvbW9kZWwvb3JkZXIubW9kZWwnXHJcbmltcG9ydCB7IEluZ3JlZGllbnREVE8gfSBmcm9tIFwiLi4vLi4vLi4vLi4vLi4vYXBpLWluYm91bmQvZHRvL29yZGVyLmR0b1wiO1xyXG5cclxuQGluamVjdGFibGUoKVxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBDYWl4YVJhcGlkb0ludGVncmF0aW9uIGltcGxlbWVudHMgTWFuYWdlbWVudFN5c3RlbUludGVncmF0aW9uUG9ydCB7XHJcblx0cHJpdmF0ZSByZXFcclxuXHRwcml2YXRlIGRiVVJMXHJcblx0cHJpdmF0ZSBwb29sXHJcblxyXG5cdC8vIGNvbnN0cnVjdG9yKGRiVVJMOiBzdHJpbmcpIHtcclxuXHRjb25zdHJ1Y3RvcigpIHtcclxuXHRcdHRoaXMuZGJVUkwgPSBmcy5yZWFkRmlsZVN5bmMoJy4vc3JjL2FwaS1vdXRib3VuZC9pbnRlZ3JhdGlvbi9tYW5hZ2VtZW50LXN5c3RlbXMvY2FpeGEtcmFwaWRvLWFwaS91cmwudHh0JywgJ3V0ZjgnKVxyXG5cdFx0dGhpcy5wb29sID0gbmV3IG1zc3FsLkNvbm5lY3Rpb25Qb29sKHRoaXMuZGJVUkwpXHJcblx0fVxyXG5cclxuXHRwcml2YXRlIGNvbm5lY3QgPSBhc3luYyAoKSA9PiB7XHJcblx0XHRhd2FpdCB0aGlzLnBvb2wuY29ubmVjdCgpLmNhdGNoKGVyciA9PiB7IHRocm93IGVyciB9KVxyXG5cdFx0dGhpcy5yZXEgPSBhd2FpdCB0aGlzLnBvb2wucmVxdWVzdCgpXHJcblx0fVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQHN1bW1hcnk6IG1ldGhvZCBmb3IgY3JlYXRpbmcgYSBuZXcgb3JkZXIgYXNzb2NhaXRlZCB3aXRoIGEgZ2l2ZW4gdGFibGUuXHJcbiAgICAgKi9cclxuXHRwcml2YXRlIG9wZW5UYWJsZVByb2NlZHVyZSA9IGFzeW5jICh0YWJsZTogTnVtYmVyKSA9PiAge1xyXG4gICAgICAgIC8qXHJcbiAgICAgICAgUHJvY2VkdXJlIHBhcmEgYWJyaXIgbWVzYTpcclxuICAgICAgICBOb21lOiBzcF9BYnJlTWVzYVxyXG4gICAgICAgIFBhcmFtZXRyb3M6IEBOcm9NZXNhXHRcdC0+IE51bWVybyBkYSBNZXNhIG91IENvbWFuZGFcclxuICAgICAgICAgICAgICAgICAgICBATnJvQ2FydGFvXHRcdC0+IEZpeG8gMFxyXG4gICAgICAgICAgICAgICAgICAgIEBOb21lXHRcdFx0LT4gRml4byAnJ1xyXG4gICAgICAgICAgICAgICAgICAgIEBJUFRlcm1pbmFsXHRcdC0+IElQIG91IG5vbWUgZG8gZGlzcG9zaXRpdm8gcXVlIGVzdGEgZW52aWFuZG8gbyBwZWRpZG9cclxuICAgICAgICA/PyAgICAgICAgICBAVmVuZG9ySURcdFx0LT4gTnJvIGRlIHPDqXJpZSBkbyBkaXNwb3NpdGl2b1xyXG5cdFx0XHRcdCAgICBAVmVyc2FvXHRcdFx0LT4gRml4byAnJ1xyXG4gICAgICAgICAgICAgICAgICAgIEBPcmlnZW1cdFx0XHQtPiBGaXhvICdpT1MnXHJcbiAgICAgICAgUmV0b3JubzogQ29kaWdvIGUgTWVuc2FnZW1cclxuICAgICAgICBDb2RpZ28gPSAwIC0+IFN1Y2Vzc29cclxuICAgICAgICBDb2RpZ28gPD4gMCAtPiBWZXIgTWVuc2FnZW0gZGUgZXJyb1xyXG5cclxuICAgICAgICBFeGVtcGxvIGRlIFVzbzpcclxuICAgICAgICBzcF9BYnJlTWVzYSBATnJvTWVzYSA9IDMsIEBOcm9DYXJ0YW8gPSAwLCBATm9tZSA9ICcnLCBASVBUZXJtaW5hbCA9ICdjaHJpcycsIEBWZW5kb3JJRCA9ICcxYTI4MDc5OGE4Yzk5ODljJywgQFZlcnNhbyA9ICcnLCBAT3JpZ2VtID0gJ0lPUydcclxuICAgICAgICAqL1xyXG5cdFx0dGhpcy5yZXEgPSBhd2FpdCB0aGlzLnBvb2wucmVxdWVzdCgpXHJcblx0XHRjb25zdCBxdWVyeVRleHQgPSAnRVhFQyBzcF9BYnJlTWVzYSBATnJvTWVzYSA9IEBucm9fbWVzYSwgQE5yb0NhcnRhbyA9IDAsIEBOb21lID0gXFwnXFwnLCBASVBUZXJtaW5hbCA9IEBpcCwgQFZlbmRvcklEID0gQHZlbmRvcl9pZCwgQFZlcnNhbyA9IFxcJ1xcJywgQE9yaWdlbSA9IFwiSU9TXCInXHJcblx0XHQvLyBkZWZpbmUgcGFyYW1ldGVyc1xyXG5cdFx0dGhpcy5yZXEuaW5wdXQoJ25yb19tZXNhJywgbXNzcWwuU21hbGxJbnQsIHRhYmxlKVxyXG5cdFx0dGhpcy5yZXEuaW5wdXQoJ2lwJywgbXNzcWwuTlZhckNoYXIsICdUYWJsZXQgLSBNZXNhJyArIHRhYmxlKVxyXG5cdFx0dGhpcy5yZXEuaW5wdXQoJ3ZlbmRvcl9pZCcsIG1zc3FsLk5WYXJDaGFyLCAnVGFibGV0IC0gTWVzYScgKyB0YWJsZSlcclxuXHRcdC8vIHJ1biBxdWVyeVxyXG5cdFx0cmV0dXJuIHRoaXMucmVxLnF1ZXJ5KHF1ZXJ5VGV4dClcclxuXHRcdFx0LnRoZW4ocmVzdWx0ID0+IHtcclxuXHRcdFx0XHRpZiAocmVzdWx0LnJlY29yZHNldFswXS5SZXRvcm5vICE9PSAwKSB7XHJcblx0XHRcdFx0XHR0aHJvdyBuZXcgRXJyb3IocmVzdWx0LnJlY29yZHNldFswXS5NZW5zYWdlbSlcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0Y29uc29sZS5sb2coJ0JpbGwgb3Blbi4nKVxyXG5cdFx0XHR9KVxyXG5cdFx0XHQuY2F0Y2goZXJyID0+IHsgdGhyb3cgZXJyIH0pXHJcblx0fVxyXG5cclxuXHRwdWJsaWMgb3BlblRhYmxlID0gYXN5bmMgKHRhYmxlOiBOdW1iZXIpID0+IHtcclxuXHRcdGlmICghdGhpcy5wb29sLl9jb25uZWN0ZWQpIHtcclxuXHRcdFx0YXdhaXQgdGhpcy5jb25uZWN0KCkuY2F0Y2goZXJyID0+IHt0aHJvdyBlcnJ9KVxyXG5cdFx0fVxyXG5cdFx0YXdhaXQgdGhpcy5vcGVuVGFibGVQcm9jZWR1cmUodGFibGUpLmNhdGNoKGVyciA9PiB7dGhyb3cgZXJyfSlcclxuXHR9XHJcblx0XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBAc3VtbWFyeTogbWV0aG9kIGZvciBhZGRpbmcgYSBuZXcgcHJvZHVjdCB0byB0aGUgdGFibGUncyBiaWxsLlxyXG4gICAgICovXHJcblx0cHJpdmF0ZSBhZGRQcm9kdWN0VG9UYWJsZVByb2NlZHVyZSA9IGFzeW5jICh0YWJsZTogTnVtYmVyLCBtZ210SUQ6IFN0cmluZyk6IFByb21pc2U8dm9pZD4gPT4ge1xyXG4gICAgICAgIC8qXHJcbiAgICAgICAgUHJvY2VkdXJlIHBhcmEgYWRpY2lvbmFyIHVtIHByb2R1dG8gbmEgbWVzYTpcclxuICAgICAgICBOb21lOiBzcF9BZGljaW9uYVByb2R1dG9cclxuICAgICAgICBQYXJhbWV0cm9zOiBATnJvTWVzYVx0XHQtPiBOdW1lcm8gZGEgTWVzYSBvdSBDb21hbmRhXHJcbiAgICAgICAgICAgICAgICAgICAgQE5yb0NhcnRhb1x0XHQtPiBGaXhvIDBcclxuICAgICAgICAgICAgICAgICAgICBATm9tZVx0XHRcdC0+IEZpeG8gJydcclxuICAgICAgICAgICAgICAgICAgICBASVBUZXJtaW5hbFx0XHQtPiBJUCBvdSBub21lIGRvIGRpc3Bvc2l0aXZvIHF1ZSBlc3RhIGVudmlhbmRvIG8gcGVkaWRvXHJcbiAgICAgICAgICAgICAgICAgICAgQElEUHJvZHV0b1x0XHQtPiBJRCBkbyBwcm9kdXRvKG7Do28gw6kgbyBjw7NkaWdvLCDDqSBvIGNhbXBvIElEIFByb2R1dG8pXHJcbiAgICAgICAgICAgICAgICAgICAgQFF0ZGVcdFx0XHQtPiBJbmZvcm1hciBhIFF0ZGVcclxuICAgICAgICAgICAgICAgICAgICBAT2JzZXJ2YWNhb0l0ZW1cdC0+IE5vIGNhc28gZG8gRklULCBpbmZvcm1hciAnJ1xyXG4gICAgICAgICAgICAgICAgICAgIEBWZW5kZWRvcklEIFx0LT4gRml4byAnJ1xyXG5cclxuICAgICAgICBSZXRvcm5vOiBDb2RpZ28gZSBNZW5zYWdlbVxyXG4gICAgICAgIENvZGlnbyA9IDAgLT4gU3VjZXNzb1xyXG4gICAgICAgIENvZGlnbyA8PiAwIC0+IFZlciBNZW5zYWdlbSBkZSBlcnJvXHJcblxyXG4gICAgICAgIEV4ZW1wbG8gZGUgVXNvOlxyXG4gICAgICAgIHNwX0FkaWNpb25hUHJvZHV0byBATnJvTWVzYSA9IDMsIEBOcm9DYXJ0YW8gPSAwLCBATm9tZSA9ICcnLCBASVBUZXJtaW5hbCA9ICdjaHJpcycsIEBJRFByb2R1dG8gPSAxNCwgQFF0ZGUgPSAxLjAsIEBPYnNlcnZhY2FvSXRlbSA9ICcnLCBAVmVuZGVkb3JJRCA9ICcnXHJcbiAgICAgICAgKi9cclxuXHRcdGNvbnN0IHF1ZXJ5VGV4dCA9ICdFWEVDIHNwX0FkaWNpb25hUHJvZHV0byBATnJvTWVzYSA9IEBucm9fbWVzYSwgQE5yb0NhcnRhbyA9IDAsIEBOb21lID0gXFwnXFwnLCBASVBUZXJtaW5hbCA9IEBpcCwgQElEUHJvZHV0byA9IEBpZF9wcm9kdXRvLCBAUXRkZSA9IDEuMCwgQE9ic2VydmFjYW9JdGVtID0gXFwnXFwnLCBAVmVuZGVkb3JJRCA9IFxcJ1xcJydcclxuXHRcdC8vIGRlZmluZSBwYXJhbWV0ZXJzXHJcblx0XHR0aGlzLnJlcS5pbnB1dCgnbnJvX21lc2EnLCBtc3NxbC5UaW55SW50LCB0YWJsZSlcclxuXHRcdHRoaXMucmVxLmlucHV0KCdpcCcsIG1zc3FsLk5WYXJDaGFyLCAnVGFibGV0IC0gTWVzYScgKyB0YWJsZSlcclxuXHRcdHRoaXMucmVxLmlucHV0KCdpZF9wcm9kdXRvJywgbXNzcWwuTlZhckNoYXIsIG1nbXRJRClcclxuXHRcdC8vIHJ1biBxdWVyeVxyXG5cdFx0cmV0dXJuIHRoaXMucmVxLnF1ZXJ5KHF1ZXJ5VGV4dClcclxuXHRcdFx0LnRoZW4ocmVzdWx0ID0+IHtcclxuXHRcdFx0XHRpZiAocmVzdWx0LnJlY29yZHNldFswXS5SZXRvcm5vICE9PSAwKSB7XHJcblx0XHRcdFx0XHR0aHJvdyBuZXcgRXJyb3IocmVzdWx0LnJlY29yZHNldFswXS5NZW5zYWdlbSlcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0Y29uc29sZS5sb2coJ1Byb2R1Y3QgYWRkZWQuJylcclxuXHRcdFx0fSlcclxuXHRcdFx0LmNhdGNoKGVyciA9PiB7dGhyb3cgZXJyfSlcclxuXHR9XHJcblxyXG5cdHB1YmxpYyBhZGRQcm9kdWN0c1RvVGFibGUgPSBhc3luYyAodXBkYXRlOiBVcGRhdGVNb2RlbCAsIHRhYmxlOiBOdW1iZXIpOiBQcm9taXNlPEFycmF5PGFueT4+ID0+IHtcclxuXHRcdGlmICghdGhpcy5wb29sLl9jb25uZWN0ZWQpIHtcclxuXHRcdFx0YXdhaXQgdGhpcy5jb25uZWN0KCkuY2F0Y2goZXJyID0+IHsgdGhyb3cgZXJyIH0pXHJcblx0XHR9XHJcblx0XHRsZXQgZmFpbHMgPSBbXVxyXG5cdFx0Zm9yIChsZXQgY29uc3VtZXIgb2YgdXBkYXRlLmNvbnN1bWVycykge1xyXG5cdFx0XHRmb3IgKGxldCBpdGVtIG9mIGNvbnN1bWVyLml0ZW1zKSB7XHJcblx0XHRcdFx0YXdhaXQgdGhpcy5hZGRQcm9kdWN0VG9UYWJsZVByb2NlZHVyZSh0YWJsZSwgaXRlbS5tZ210X2lkKS5jYXRjaChlcnIgPT4gXHJcblx0XHRcdFx0XHRmYWlscy5wdXNoKHtcclxuXHRcdFx0XHRcdFx0Y2xvdWRfaWQ6IGl0ZW0uYmFzZV9jbG91ZF9pZCxcclxuXHRcdFx0XHRcdFx0bWdtdF9pZDogaXRlbS5tZ210X2lkLFxyXG5cdFx0XHRcdFx0XHRtZXNzYWdlOiBlcnIubWVzc2FnZVxyXG5cdFx0XHRcdFx0fSkpXHJcblx0XHRcdFx0aWYgKGl0ZW0uaW5ncmVkaWVudHMpIHtcclxuXHRcdFx0XHRcdGZvciAobGV0IGluZ3JlZGllbnQgb2YgaXRlbS5pbmdyZWRpZW50cykge1xyXG5cdFx0XHRcdFx0XHRhd2FpdCB0aGlzLmFkZFByb2R1Y3RUb1RhYmxlUHJvY2VkdXJlKHRhYmxlLCBpbmdyZWRpZW50Lm1nbXRfaWQpLmNhdGNoKGVyciA9PiBcclxuXHRcdFx0XHRcdFx0XHRmYWlscy5wdXNoKHtcclxuXHRcdFx0XHRcdFx0XHRcdGNsb3VkX2lkOiBpbmdyZWRpZW50LmNsb3VkX2lkLFxyXG5cdFx0XHRcdFx0XHRcdFx0bWdtdF9pZDogaW5ncmVkaWVudC5tZ210X2lkLFxyXG5cdFx0XHRcdFx0XHRcdFx0bWVzc2FnZTogZXJyLm1lc3NhZ2VcclxuXHRcdFx0XHRcdFx0XHR9KSlcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9XHRcdFx0XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdGNvbnNvbGUubG9nKCdmYWlsczogJywgZmFpbHMpXHJcblx0XHRyZXR1cm4gZmFpbHNcclxuXHR9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBAc3VtbWFyeTogbWV0aG9kIGZvciBmaW5hbGl6aW5nIHRoZSBiaWxsIGFzc29jaWF0ZWQgd2l0aCBhIHRhYmxlLlxyXG4gICAgICovXHJcblx0cHJpdmF0ZSBjbG9zZVRhYmxlUHJvY2VkdXJlICh0YWJsZTogTnVtYmVyKSB7XHJcbiAgICAgICAgLypcclxuICAgICAgICBQcm9jZWR1cmUgcGFyYSBmaW5hbGl6YXIgdW0gcGVkaWRvOlxyXG4gICAgICAgIE5vbWU6IHNwX0ZpbmFsaXphclBlZGlkb1xyXG4gICAgICAgIFBhcmFtZXRyb3M6IEBOcm9NZXNhXHRcdC0+IE51bWVybyBkYSBNZXNhIG91IENvbWFuZGFcclxuICAgICAgICAgICAgICAgICAgICBATnJvQ2FydGFvXHRcdC0+IEZpeG8gMFxyXG5cclxuICAgICAgICBSZXRvcm5vOiBDb2RpZ28gZSBNZW5zYWdlbVxyXG4gICAgICAgIENvZGlnbyA9IDAgLT4gU3VjZXNzb1xyXG4gICAgICAgIENvZGlnbyA8PiAwIC0+IFZlciBNZW5zYWdlbSBkZSBlcnJvXHJcblxyXG4gICAgICAgIEV4ZW1wbG8gZGUgVXNvOlxyXG4gICAgICAgIHNwX0ZpbmFsaXphclBlZGlkbyBATnJvTWVzYSA9IDMsIEBOcm9DYXJ0YW8gPSAwXHJcbiAgICAgICAgKi9cclxuXHRcdGNvbnN0IHF1ZXJ5VGV4dCA9ICdFWEVDIHNwX0ZpbmFsaXphclBlZGlkbyBATnJvTWVzYSA9IEBucm9fbWVzYSwgQE5yb0NhcnRhbyA9IDAnXHJcblx0XHQvLyBkZWZpbmUgcGFyYW1ldGVyc1xyXG5cdFx0dGhpcy5yZXEuaW5wdXQoJ25yb19tZXNhJywgbXNzcWwuU21hbGxJbnQsIHRhYmxlKVxyXG5cdFx0Ly8gcnVuIHF1ZXJ5XHJcblx0XHRyZXR1cm4gdGhpcy5yZXEucXVlcnkocXVlcnlUZXh0KVxyXG5cdFx0XHQudGhlbihyZXN1bHQgPT4ge1xyXG5cdFx0XHRcdGlmIChyZXN1bHQucmVjb3Jkc2V0WzBdLlJldG9ybm8gIT09IDApIHtcclxuXHRcdFx0XHRcdHRocm93IG5ldyBFcnJvcignSW52YWxpZCBhcmd1bWVudHMuJylcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0Y29uc29sZS5sb2coJ0JpbGwgY2xvc2VkLicpXHJcblx0XHRcdH0pXHJcblx0XHRcdC5jYXRjaChlcnIgPT4geyB0aHJvdyBlcnIgfSlcclxuXHR9XHJcblxyXG5cdHB1YmxpYyBjbG9zZVRhYmxlID0gYXN5bmMgKHRhYmxlOiBOdW1iZXIpID0+IHtcclxuXHRcdGlmICghdGhpcy5wb29sLl9jb25uZWN0ZWQpIHtcclxuXHRcdFx0YXdhaXQgdGhpcy5jb25uZWN0KCkuY2F0Y2goZXJyID0+IHsgdGhyb3cgZXJyIH0pXHJcblx0XHR9XHJcblx0XHRhd2FpdCB0aGlzLmNsb3NlVGFibGVQcm9jZWR1cmUodGFibGUpXHJcblx0XHR0aGlzLnBvb2wuY2xvc2UoKVxyXG5cdH1cclxuXHRcclxuXHJcbiAgICAvKipcclxuICAgICAqIEBzdW1tYXJ5OiBtZXRob2QgZm9yIGNoZWNraW5nIHRoZSBiaWxsIG9mIGEgdGFibGUgdGhhdCBpcyBzdGlsbCBvcmRlcmluZy5cclxuICAgICAqL1xyXG5cdHB1YmxpYyBwcmVCaWxsKHRhYmxlOiBudW1iZXIsIHNlcnZpY2U6IG51bWJlciwgZ3Vlc3RzOiBudW1iZXIsIHByaW50OiBudW1iZXIpIHtcclxuICAgICAgICAvKlxyXG4gICAgICAgIFByb2NlZHVyZSBwYXJhIHZpc3VhbGl6YXIgYSBwcmVjb250YVxyXG4gICAgICAgIE5vbWU6IHNwX1dTUHJlQ29udGFcclxuICAgICAgICBQYXJhbWV0cm9zOiBATnJvTWVzYVx0XHQtPiBOdW1lcm8gZGEgTWVzYSBvdSBDb21hbmRhXHJcbiAgICAgICAgICAgICAgICAgICAgQE5yb0NhcnRhb1x0XHQtPiBGaXhvIDBcclxuICAgICAgICAgICAgICAgICAgICBAU2Vydmljb1x0XHQtPiBJbmRpY2Fkb3Igc2UgdmFpIG91IG7Do28gY29icmFyIHNlcnZpw6dvKDAgLSBuw6NvIGNvYnJhciwgb3UgMSAtIGNvYnJhcilcclxuICAgICAgICAgICAgICAgICAgICBATnJvUGVzc29hc1x0XHQtPiBOcm8gZGUgcGVzc29hcyBuYSBwZXNhXHJcbiAgICAgICAgICAgICAgICAgICAgQFZpc3VhbGl6YXJcdFx0LT4gSW5kaWNhZG9yIHNlIGltcHJpbWUgKDApIG91IHNvbWVudGUgdmlzdWFsaXphICgxKVxyXG5cclxuICAgICAgICBSZXRvcm5vOiBDb2RpZ28gZSBNZW5zYWdlbVxyXG4gICAgICAgIENvZGlnbyA9IDAgLT4gU3VjZXNzb1xyXG4gICAgICAgIENvZGlnbyA8PiAwIC0+IFZlciBNZW5zYWdlbSBkZSBlcnJvXHJcblxyXG4gICAgICAgIEV4ZW1wbG8gZGUgVXNvOlxyXG4gICAgICAgIHNwX1dTUHJlQ29udGEgQE5yb01lc2EgPSAzLCBATnJvQ2FydGFvID0gMCwgQFNlcnZpY28gPSAxLCBATnJvUGVzc29hcyA9IDEsIEBWaXN1YWxpemFyID0gMVxyXG4gICAgICAgICovXHJcblx0XHRjb25zdCBxdWVyeVRleHQgPSAnRVhFQyBzcF9XU1ByZUNvbnRhIEBOcm9NZXNhID0gQG5yb19tZXNhLCBATnJvQ2FydGFvID0gMCwgQFNlcnZpY28gPSBAc2VydmljbywgQE5yb1Blc3NvYXMgPSBAbnJvX3Blc3NvYXMsIEBWaXN1YWxpemFyID0gQHZpc3VhbGl6YXInXHJcblx0XHQvLyBkZWZpbmUgcGFyYW1ldGVyc1xyXG5cdFx0dGhpcy5yZXEuaW5wdXQoJ25yb19tZXNhJywgbXNzcWwuU21hbGxJbnQsIHRhYmxlKVxyXG5cdFx0dGhpcy5yZXEuaW5wdXQoJ3NlcnZpY28nLCBtc3NxbC5TbWFsbEludCwgc2VydmljZSlcclxuXHRcdHRoaXMucmVxLmlucHV0KCducm9fcGVzc29hcycsIG1zc3FsLlNtYWxsSW50LCBndWVzdHMpXHJcblx0XHR0aGlzLnJlcS5pbnB1dCgndmlzdWFsaXphcicsIG1zc3FsLlNtYWxsSW50LCBwcmludClcclxuXHRcdC8vIHJ1biBxdWVyeVxyXG5cdFx0cmV0dXJuIHRoaXMucmVxLnF1ZXJ5KHF1ZXJ5VGV4dClcclxuXHRcdFx0LnRoZW4ocmVzdWx0ID0+IHtcclxuXHRcdFx0XHRyZXR1cm4gcmVzdWx0LnJlY29yZHNldFxyXG5cdFx0XHR9KVxyXG5cdFx0XHQuY2F0Y2goZXJyID0+IHsgdGhyb3cgZXJyIH0pXHJcblx0fVxyXG59Il19
