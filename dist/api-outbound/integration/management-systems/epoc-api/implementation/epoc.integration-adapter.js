"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const request = require("request");
const crypto = require("crypto");
const querystring = require("querystring");
const epoc_dto_1 = require("../../../../dto/epoc.dto");
const inversify_1 = require("inversify");
const response_epoc_1 = __importDefault(require("../../../../envelopes/response.epoc"));
let EpocIntegrationAdapter = class EpocIntegrationAdapter {
    tokenGenerator(systemAuth) {
        return __awaiter(this, void 0, void 0, function* () {
            let sistemaAuth = new epoc_dto_1.SistemaAuth();
            let sistemaAuthString;
            sistemaAuth.hashemp = systemAuth.hashemp;
            sistemaAuth.mac = systemAuth.mac;
            sistemaAuth.service = 'geraToken';
            sistemaAuth.tokenParceiro = systemAuth.tokenParceiro;
            sistemaAuth.saltParceiro = systemAuth.saltParceiro;
            sistemaAuthString = querystring.stringify(sistemaAuth);
            return new Promise((resolve, reject) => {
                request.post({
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'Accept': 'application/json'
                    },
                    url: process.env.EPOC_URL + `/API/mod_valida_parceiro/index.php`,
                    body: sistemaAuthString
                }, (err, resp, body) => {
                    if (!err) {
                        body = this.tokenDecoder(body);
                        console.log(body);
                        return resolve(body);
                    }
                    else {
                        return reject(err);
                    }
                });
            });
        });
    }
    venderItem(order) {
        return __awaiter(this, void 0, void 0, function* () {
            // let payload: any;
            let consumerCard;
            let token;
            let sistemaGestaoPayload;
            let venderItem = new epoc_dto_1.VendaItem();
            let orderIt = new epoc_dto_1.OrderItem();
            // let produtos: Array<Produtos> = new Array<Produtos>();
            // produtos = order.products;
            // payload = await this.tokenManager.decodificaToken(JSON.parse(body.tokenPeddi));
            // payload = payload;
            sistemaGestaoPayload = this.sistemaAuth();
            token = yield this.tokenGenerator(sistemaGestaoPayload);
            consumerCard = order.ticket;
            venderItem.mac = sistemaGestaoPayload.mac;
            venderItem.hashemp = sistemaGestaoPayload.hashemp;
            venderItem.ult_mesa = order.table;
            venderItem.service = 'GravarVendaItem';
            let tokenData = yield this.tokenDecoder(token);
            venderItem.token = tokenData.data;
            orderIt.CODEVENTO = "";
            orderIt.HOST = venderItem.mac;
            let orderItem = {
                [consumerCard]: orderIt
                // {
                // 	// HOST: venderItem.mac,
                // 	// CODEVENTO: "",
                // 	itens: Array<Item>()
                // }
            };
            for (let i = 0; i < order.products.length; i++) {
                let productQuantity = order.products[i].quantity;
                // let item = {
                // 	quantidade: 1,
                // 	codigo: order.products[i].epoc_id,
                // 	cod_func_autoriz: "",
                // 	motivo_autorizacao: "1",
                // 	cod_func: order.responsibleEmployee.epoc_id,
                // 	marchar: 0,
                // 	valor: order.products[i].price,
                // 	tipoItem: "P",
                // 	lanceItem: "S",
                // 	acrescimo: 0,
                // 	desconto: 0,
                // 	obs: [
                // 		{}
                // 	]
                // }
                let item = new epoc_dto_1.Item();
                item.quantidade = 1;
                item.codigo = order.products[i].epoc_id;
                item.cod_func_autoriz = "";
                item.motivo_autorizacao = "";
                item.cod_func = order.responsibleEmployee.epoc_id;
                item.marchar = 0;
                item.valor = order.products[i].price;
                item.tipoItem = "P";
                item.lanceItem = "S";
                item.acrescimo = 0;
                item.desconto = 0;
                item.obs = new Array();
                if (order.products[i].ingredients !== null && order.products[i].ingredients.length > 0) {
                    for (let j = 0; j < order.products[i].ingredients.length; j++) {
                        item.obs[j] = new epoc_dto_1.ItemObs();
                        item.obs[j].desc_obs = order.products[i].ingredients[j].name;
                        item.obs[j].cod_obs = order.products[i].ingredients[j].epoc_id;
                        item.obs[j].cod_modificador = 0;
                    }
                }
                for (let q = 0; q < productQuantity; q++) {
                    orderItem[consumerCard].ITENS.push(item);
                }
                // orderItem[comanda].ITENS[i] = {
                // 	quantidade: order.products[i].quantity,
                // 	codigo: order.products[i].epoc_id,
                // 	cod_func_autoriz: "",
                // 	motivo_autorizacao: "1",
                // 	cod_func: order.responsibleEmployee.epoc_id,
                // 	marchar: 0,
                // 	valor: order.products[i].price,
                // 	tipoItem: "P",
                // 	lanceItem: "S",
                // 	acrescimo: 0,
                // 	desconto: 0,
                // 	obs: []
                // }
                // if (order.products[i].ingredients !== null && order.products[i].ingredients.length > 0) {
                // 	for (let j = 0; j < order.products[i].ingredients.length; j++) {
                // 		orderItem[comanda].ITENS[i].obs[j] = {
                // 			desc_obs: order.products[i].ingredients[j].name,
                // 			cod_obs: order.products[i].ingredients[j].epoc_id,
                // 			cod_modificador: 0
                // 		}
                // 	}
                // }
            }
            let stringSalt = "";
            for (let i = 0; i < orderItem[consumerCard].ITENS.length; i++) {
                stringSalt += orderItem[consumerCard].ITENS[i].codigo + '_';
            }
            stringSalt = stringSalt.slice(0, -1);
            let itemConvert;
            let venderItemToString;
            itemConvert = JSON.stringify(orderItem);
            venderItem.dataVenda = new Buffer(itemConvert).toString('base64');
            venderItem.salt = crypto.createHash('md5').update(stringSalt).digest("hex");
            venderItemToString = querystring.stringify(venderItem);
            // return new Promise<ResponseEpoc>((resolve, reject) => {
            // })
            return new Promise((resolve, reject) => {
                request.post({
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'Accept': 'application/json'
                    },
                    url: `http://llm09.epoc2.cdmon.org:56478/API/mod_venda_item/index.php`,
                    body: venderItemToString
                }, (err, resp, body) => __awaiter(this, void 0, void 0, function* () {
                    if (!err) {
                        body = yield this.tokenDecoder(body);
                        console.log(body);
                        return new response_epoc_1.default(body);
                    }
                    else {
                        return reject(err);
                    }
                }));
            });
        });
    }
    getExtrato(objExtrato) {
        throw new Error("Method not implemented.");
    }
    getProducts() {
        throw new Error("Method not implemented.");
    }
    tokenDecoder(token) {
        return __awaiter(this, void 0, void 0, function* () {
            let decodeBuffer = new Buffer(token, 'base64');
            let tokenDecoded;
            decodeBuffer = decodeBuffer.toString('ascii');
            tokenDecoded = JSON.parse(decodeBuffer);
            return tokenDecoded;
        });
    }
    sistemaAuth() {
        let epocAuth = new epoc_dto_1.SistemaAuth();
        epocAuth.hashemp = 'da2g39jbcdja8601';
        epocAuth.mac = 'e4:c1:dd:c0:9a:4d';
        epocAuth.tokenParceiro = '353c1ea3-682b-4754-91ef-f55d05aec758';
        epocAuth.saltParceiro = crypto.createHash('md5').update(epocAuth.tokenParceiro + '__TOKEN__').digest("hex");
        return epocAuth;
    }
    ;
};
EpocIntegrationAdapter = __decorate([
    inversify_1.injectable()
], EpocIntegrationAdapter);
exports.default = EpocIntegrationAdapter;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9hcGktb3V0Ym91bmQvaW50ZWdyYXRpb24vbWFuYWdlbWVudC1zeXN0ZW1zL2Vwb2MtYXBpL2ltcGxlbWVudGF0aW9uL2Vwb2MuaW50ZWdyYXRpb24tYWRhcHRlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0EsbUNBQW9DO0FBQ3BDLGlDQUFrQztBQUNsQywyQ0FBNEM7QUFFNUMsdURBU2tDO0FBQ2xDLHlDQUF1QztBQUN2Qyx3RkFBK0Q7QUFLL0QsSUFBcUIsc0JBQXNCLEdBQTNDLE1BQXFCLHNCQUFzQjtJQUdwQyxjQUFjLENBQUMsVUFBZTs7WUFFbkMsSUFBSSxXQUFXLEdBQWdCLElBQUksc0JBQVcsRUFBRSxDQUFDO1lBQ2pELElBQUksaUJBQXlCLENBQUM7WUFDOUIsV0FBVyxDQUFDLE9BQU8sR0FBRyxVQUFVLENBQUMsT0FBTyxDQUFDO1lBQ3pDLFdBQVcsQ0FBQyxHQUFHLEdBQUcsVUFBVSxDQUFDLEdBQUcsQ0FBQztZQUNqQyxXQUFXLENBQUMsT0FBTyxHQUFHLFdBQVcsQ0FBQztZQUNsQyxXQUFXLENBQUMsYUFBYSxHQUFHLFVBQVUsQ0FBQyxhQUFhLENBQUM7WUFDckQsV0FBVyxDQUFDLFlBQVksR0FBRyxVQUFVLENBQUMsWUFBWSxDQUFDO1lBQ25ELGlCQUFpQixHQUFHLFdBQVcsQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLENBQUM7WUFFdkQsT0FBTyxJQUFJLE9BQU8sQ0FBZSxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtnQkFDcEQsT0FBTyxDQUFDLElBQUksQ0FBQztvQkFDWixPQUFPLEVBQUU7d0JBQ1IsY0FBYyxFQUFFLG1DQUFtQzt3QkFDbkQsUUFBUSxFQUFFLGtCQUFrQjtxQkFDNUI7b0JBQ0QsR0FBRyxFQUFFLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxHQUFHLG9DQUFvQztvQkFDaEUsSUFBSSxFQUFFLGlCQUFpQjtpQkFDdkIsRUFBRSxDQUFDLEdBQVUsRUFBRSxJQUFTLEVBQUUsSUFBUyxFQUFFLEVBQUU7b0JBQ3ZDLElBQUksQ0FBQyxHQUFHLEVBQUU7d0JBQ1QsSUFBSSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7d0JBQy9CLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ2xCLE9BQU8sT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO3FCQUNyQjt5QkFDSTt3QkFDSixPQUFPLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztxQkFDbkI7Z0JBQ0YsQ0FBQyxDQUFDLENBQUM7WUFDSixDQUFDLENBQUMsQ0FBQTtRQUNILENBQUM7S0FBQTtJQUVLLFVBQVUsQ0FBQyxLQUFVOztZQUMxQixvQkFBb0I7WUFDcEIsSUFBSSxZQUFvQixDQUFDO1lBQ3pCLElBQUksS0FBVSxDQUFDO1lBQ2YsSUFBSSxvQkFBeUIsQ0FBQztZQUM5QixJQUFJLFVBQVUsR0FBYyxJQUFJLG9CQUFTLEVBQUUsQ0FBQztZQUM1QyxJQUFJLE9BQU8sR0FBYyxJQUFJLG9CQUFTLEVBQUUsQ0FBQztZQUd6Qyx5REFBeUQ7WUFDekQsNkJBQTZCO1lBQzdCLGtGQUFrRjtZQUNsRixxQkFBcUI7WUFDckIsb0JBQW9CLEdBQUcsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQzFDLEtBQUssR0FBRyxNQUFNLElBQUksQ0FBQyxjQUFjLENBQUMsb0JBQW9CLENBQUMsQ0FBQztZQUN4RCxZQUFZLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQztZQUU1QixVQUFVLENBQUMsR0FBRyxHQUFHLG9CQUFvQixDQUFDLEdBQUcsQ0FBQztZQUMxQyxVQUFVLENBQUMsT0FBTyxHQUFHLG9CQUFvQixDQUFDLE9BQU8sQ0FBQztZQUNsRCxVQUFVLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUM7WUFDbEMsVUFBVSxDQUFDLE9BQU8sR0FBRyxpQkFBaUIsQ0FBQztZQUN2QyxJQUFJLFNBQVMsR0FBRyxNQUFNLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDL0MsVUFBVSxDQUFDLEtBQUssR0FBRyxTQUFTLENBQUMsSUFBSSxDQUFDO1lBRWxDLE9BQU8sQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO1lBQ3ZCLE9BQU8sQ0FBQyxJQUFJLEdBQUcsVUFBVSxDQUFDLEdBQUcsQ0FBQztZQUM5QixJQUFJLFNBQVMsR0FBRztnQkFDZixDQUFDLFlBQVksQ0FBQyxFQUFFLE9BQU87Z0JBRXZCLElBQUk7Z0JBRUosNEJBQTRCO2dCQUM1QixxQkFBcUI7Z0JBQ3JCLHdCQUF3QjtnQkFDeEIsSUFBSTthQUNKLENBQUE7WUFDRCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQy9DLElBQUksZUFBZSxHQUFHLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDO2dCQUVqRCxlQUFlO2dCQUNmLGtCQUFrQjtnQkFDbEIsc0NBQXNDO2dCQUN0Qyx5QkFBeUI7Z0JBQ3pCLDRCQUE0QjtnQkFDNUIsZ0RBQWdEO2dCQUNoRCxlQUFlO2dCQUNmLG1DQUFtQztnQkFDbkMsa0JBQWtCO2dCQUNsQixtQkFBbUI7Z0JBQ25CLGlCQUFpQjtnQkFDakIsZ0JBQWdCO2dCQUNoQixVQUFVO2dCQUNWLE9BQU87Z0JBQ1AsS0FBSztnQkFDTCxJQUFJO2dCQUNKLElBQUksSUFBSSxHQUFTLElBQUksZUFBSSxFQUFFLENBQUM7Z0JBQzVCLElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDO2dCQUNwQixJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDO2dCQUN4QyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO2dCQUMzQixJQUFJLENBQUMsa0JBQWtCLEdBQUcsRUFBRSxDQUFDO2dCQUM3QixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLENBQUM7Z0JBQ2xELElBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDO2dCQUNqQixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO2dCQUNyQyxJQUFJLENBQUMsUUFBUSxHQUFHLEdBQUcsQ0FBQztnQkFDcEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxHQUFHLENBQUM7Z0JBQ3JCLElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDO2dCQUNuQixJQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQztnQkFDbEIsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLEtBQUssRUFBVyxDQUFDO2dCQUVoQyxJQUFJLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxLQUFLLElBQUksSUFBSSxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO29CQUN2RixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO3dCQUM5RCxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksa0JBQU8sRUFBRSxDQUFDO3dCQUM1QixJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7d0JBQzdELElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQzt3QkFDL0QsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxlQUFlLEdBQUcsQ0FBQyxDQUFDO3FCQUNoQztpQkFDRDtnQkFFRCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsZUFBZSxFQUFFLENBQUMsRUFBRSxFQUFFO29CQUN6QyxTQUFTLENBQUMsWUFBWSxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDekM7Z0JBR0Qsa0NBQWtDO2dCQUNsQywyQ0FBMkM7Z0JBQzNDLHNDQUFzQztnQkFDdEMseUJBQXlCO2dCQUN6Qiw0QkFBNEI7Z0JBQzVCLGdEQUFnRDtnQkFDaEQsZUFBZTtnQkFDZixtQ0FBbUM7Z0JBQ25DLGtCQUFrQjtnQkFDbEIsbUJBQW1CO2dCQUNuQixpQkFBaUI7Z0JBQ2pCLGdCQUFnQjtnQkFDaEIsV0FBVztnQkFDWCxJQUFJO2dCQUNKLDRGQUE0RjtnQkFDNUYsb0VBQW9FO2dCQUNwRSwyQ0FBMkM7Z0JBQzNDLHNEQUFzRDtnQkFDdEQsd0RBQXdEO2dCQUN4RCx3QkFBd0I7Z0JBQ3hCLE1BQU07Z0JBQ04sS0FBSztnQkFDTCxJQUFJO2FBRUo7WUFFRCxJQUFJLFVBQVUsR0FBVyxFQUFFLENBQUM7WUFDNUIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFNBQVMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUM5RCxVQUFVLElBQUksU0FBUyxDQUFDLFlBQVksQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDO2FBQzVEO1lBQ0QsVUFBVSxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDckMsSUFBSSxXQUFtQixDQUFDO1lBQ3hCLElBQUksa0JBQTBCLENBQUM7WUFDL0IsV0FBVyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDeEMsVUFBVSxDQUFDLFNBQVMsR0FBRyxJQUFJLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDbEUsVUFBVSxDQUFDLElBQUksR0FBRyxNQUFNLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDNUUsa0JBQWtCLEdBQUcsV0FBVyxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUd2RCwwREFBMEQ7WUFFMUQsS0FBSztZQUdMLE9BQU8sSUFBSSxPQUFPLENBQWUsQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7Z0JBQ3BELE9BQU8sQ0FBQyxJQUFJLENBQUM7b0JBQ1osT0FBTyxFQUFFO3dCQUNSLGNBQWMsRUFBRSxtQ0FBbUM7d0JBQ25ELFFBQVEsRUFBRSxrQkFBa0I7cUJBQzVCO29CQUNELEdBQUcsRUFBRSxpRUFBaUU7b0JBQ3RFLElBQUksRUFBRSxrQkFBa0I7aUJBQ3hCLEVBQUUsQ0FBTyxHQUFHLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxFQUFFO29CQUM1QixJQUFJLENBQUMsR0FBRyxFQUFFO3dCQUNULElBQUksR0FBRyxNQUFNLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ3JDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ2xCLE9BQU8sSUFBSSx1QkFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO3FCQUM5Qjt5QkFDSTt3QkFDSixPQUFPLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztxQkFDbkI7Z0JBQ0YsQ0FBQyxDQUFBLENBQUMsQ0FBQztZQUNKLENBQUMsQ0FBQyxDQUFDO1FBSUosQ0FBQztLQUFBO0lBQ0QsVUFBVSxDQUFDLFVBQWU7UUFDekIsTUFBTSxJQUFJLEtBQUssQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO0lBQzVDLENBQUM7SUFDRCxXQUFXO1FBQ1YsTUFBTSxJQUFJLEtBQUssQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO0lBQzVDLENBQUM7SUFFYSxZQUFZLENBQUMsS0FBVTs7WUFDcEMsSUFBSSxZQUFZLEdBQUcsSUFBSSxNQUFNLENBQUMsS0FBSyxFQUFFLFFBQVEsQ0FBQyxDQUFDO1lBQy9DLElBQUksWUFBaUIsQ0FBQztZQUN0QixZQUFZLEdBQUcsWUFBWSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUM5QyxZQUFZLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUN4QyxPQUFPLFlBQVksQ0FBQztRQUNyQixDQUFDO0tBQUE7SUFFRCxXQUFXO1FBRVYsSUFBSSxRQUFRLEdBQUcsSUFBSSxzQkFBVyxFQUFFLENBQUM7UUFDakMsUUFBUSxDQUFDLE9BQU8sR0FBRyxrQkFBa0IsQ0FBQztRQUN0QyxRQUFRLENBQUMsR0FBRyxHQUFHLG1CQUFtQixDQUFDO1FBQ25DLFFBQVEsQ0FBQyxhQUFhLEdBQUcsc0NBQXNDLENBQUM7UUFDaEUsUUFBUSxDQUFDLFlBQVksR0FBRyxNQUFNLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsYUFBYSxHQUFHLFdBQVcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUU1RyxPQUFPLFFBQVEsQ0FBQztJQUNqQixDQUFDO0lBQUEsQ0FBQztDQUVGLENBQUE7QUFuTm9CLHNCQUFzQjtJQUQxQyxzQkFBVSxFQUFFO0dBQ1Esc0JBQXNCLENBbU4xQztrQkFuTm9CLHNCQUFzQiIsImZpbGUiOiJhcGktb3V0Ym91bmQvaW50ZWdyYXRpb24vbWFuYWdlbWVudC1zeXN0ZW1zL2Vwb2MtYXBpL2ltcGxlbWVudGF0aW9uL2Vwb2MuaW50ZWdyYXRpb24tYWRhcHRlci5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBFcG9jSW50ZWdyYXRpb25Qb3J0IGZyb20gXCIuLi9lcG9jLmludGVncmF0aW9uLXBvcnRcIjtcclxuaW1wb3J0IHJlcXVlc3QgPSByZXF1aXJlKCdyZXF1ZXN0Jyk7XHJcbmltcG9ydCBjcnlwdG8gPSByZXF1aXJlKCdjcnlwdG8nKTtcclxuaW1wb3J0IHF1ZXJ5c3RyaW5nID0gcmVxdWlyZSgncXVlcnlzdHJpbmcnKTtcclxuXHJcbmltcG9ydCB7XHJcblx0Q29tYW5kYSxcclxuXHRWZW5kYUl0ZW0sXHJcblx0T3JkZXJJdGVtLFxyXG5cdEl0ZW0sXHJcblx0SXRlbU9icyxcclxuXHRDYXJnYVByb2R1dG8sXHJcblx0RXh0cmF0byxcclxuXHRTaXN0ZW1hQXV0aFxyXG59IGZyb20gJy4uLy4uLy4uLy4uL2R0by9lcG9jLmR0byc7XHJcbmltcG9ydCB7IGluamVjdGFibGUgfSBmcm9tIFwiaW52ZXJzaWZ5XCI7XHJcbmltcG9ydCBSZXNwb25zZUVwb2MgZnJvbSBcIi4uLy4uLy4uLy4uL2VudmVsb3Blcy9yZXNwb25zZS5lcG9jXCI7XHJcbmltcG9ydCB7IHJlamVjdHMgfSBmcm9tIFwiYXNzZXJ0XCI7XHJcbmRlY2xhcmUgY29uc3QgQnVmZmVyOiBhbnk7XHJcblxyXG5AaW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEVwb2NJbnRlZ3JhdGlvbkFkYXB0ZXIgaW1wbGVtZW50cyBFcG9jSW50ZWdyYXRpb25Qb3J0IHtcclxuXHJcblxyXG5cdGFzeW5jIHRva2VuR2VuZXJhdG9yKHN5c3RlbUF1dGg6IGFueSk6IFByb21pc2U8UmVzcG9uc2VFcG9jPiB7XHJcblxyXG5cdFx0bGV0IHNpc3RlbWFBdXRoOiBTaXN0ZW1hQXV0aCA9IG5ldyBTaXN0ZW1hQXV0aCgpO1xyXG5cdFx0bGV0IHNpc3RlbWFBdXRoU3RyaW5nOiBzdHJpbmc7XHJcblx0XHRzaXN0ZW1hQXV0aC5oYXNoZW1wID0gc3lzdGVtQXV0aC5oYXNoZW1wO1xyXG5cdFx0c2lzdGVtYUF1dGgubWFjID0gc3lzdGVtQXV0aC5tYWM7XHJcblx0XHRzaXN0ZW1hQXV0aC5zZXJ2aWNlID0gJ2dlcmFUb2tlbic7XHJcblx0XHRzaXN0ZW1hQXV0aC50b2tlblBhcmNlaXJvID0gc3lzdGVtQXV0aC50b2tlblBhcmNlaXJvO1xyXG5cdFx0c2lzdGVtYUF1dGguc2FsdFBhcmNlaXJvID0gc3lzdGVtQXV0aC5zYWx0UGFyY2Vpcm87XHJcblx0XHRzaXN0ZW1hQXV0aFN0cmluZyA9IHF1ZXJ5c3RyaW5nLnN0cmluZ2lmeShzaXN0ZW1hQXV0aCk7XHJcblxyXG5cdFx0cmV0dXJuIG5ldyBQcm9taXNlPFJlc3BvbnNlRXBvYz4oKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG5cdFx0XHRyZXF1ZXN0LnBvc3Qoe1xyXG5cdFx0XHRcdGhlYWRlcnM6IHtcclxuXHRcdFx0XHRcdCdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkJyxcclxuXHRcdFx0XHRcdCdBY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbidcclxuXHRcdFx0XHR9LFxyXG5cdFx0XHRcdHVybDogcHJvY2Vzcy5lbnYuRVBPQ19VUkwgKyBgL0FQSS9tb2RfdmFsaWRhX3BhcmNlaXJvL2luZGV4LnBocGAsXHJcblx0XHRcdFx0Ym9keTogc2lzdGVtYUF1dGhTdHJpbmdcclxuXHRcdFx0fSwgKGVycjogRXJyb3IsIHJlc3A6IGFueSwgYm9keTogYW55KSA9PiB7XHJcblx0XHRcdFx0aWYgKCFlcnIpIHtcclxuXHRcdFx0XHRcdGJvZHkgPSB0aGlzLnRva2VuRGVjb2Rlcihib2R5KTtcclxuXHRcdFx0XHRcdGNvbnNvbGUubG9nKGJvZHkpO1xyXG5cdFx0XHRcdFx0cmV0dXJuIHJlc29sdmUoYm9keSk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdGVsc2Uge1xyXG5cdFx0XHRcdFx0cmV0dXJuIHJlamVjdChlcnIpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSk7XHJcblx0XHR9KVxyXG5cdH1cclxuXHJcblx0YXN5bmMgdmVuZGVySXRlbShvcmRlcjogYW55KTogUHJvbWlzZTxSZXNwb25zZUVwb2M+IHtcclxuXHRcdC8vIGxldCBwYXlsb2FkOiBhbnk7XHJcblx0XHRsZXQgY29uc3VtZXJDYXJkOiBzdHJpbmc7XHJcblx0XHRsZXQgdG9rZW46IGFueTtcclxuXHRcdGxldCBzaXN0ZW1hR2VzdGFvUGF5bG9hZDogYW55O1xyXG5cdFx0bGV0IHZlbmRlckl0ZW06IFZlbmRhSXRlbSA9IG5ldyBWZW5kYUl0ZW0oKTtcclxuXHRcdGxldCBvcmRlckl0OiBPcmRlckl0ZW0gPSBuZXcgT3JkZXJJdGVtKCk7XHJcblxyXG5cclxuXHRcdC8vIGxldCBwcm9kdXRvczogQXJyYXk8UHJvZHV0b3M+ID0gbmV3IEFycmF5PFByb2R1dG9zPigpO1xyXG5cdFx0Ly8gcHJvZHV0b3MgPSBvcmRlci5wcm9kdWN0cztcclxuXHRcdC8vIHBheWxvYWQgPSBhd2FpdCB0aGlzLnRva2VuTWFuYWdlci5kZWNvZGlmaWNhVG9rZW4oSlNPTi5wYXJzZShib2R5LnRva2VuUGVkZGkpKTtcclxuXHRcdC8vIHBheWxvYWQgPSBwYXlsb2FkO1xyXG5cdFx0c2lzdGVtYUdlc3Rhb1BheWxvYWQgPSB0aGlzLnNpc3RlbWFBdXRoKCk7XHJcblx0XHR0b2tlbiA9IGF3YWl0IHRoaXMudG9rZW5HZW5lcmF0b3Ioc2lzdGVtYUdlc3Rhb1BheWxvYWQpO1xyXG5cdFx0Y29uc3VtZXJDYXJkID0gb3JkZXIudGlja2V0O1xyXG5cclxuXHRcdHZlbmRlckl0ZW0ubWFjID0gc2lzdGVtYUdlc3Rhb1BheWxvYWQubWFjO1xyXG5cdFx0dmVuZGVySXRlbS5oYXNoZW1wID0gc2lzdGVtYUdlc3Rhb1BheWxvYWQuaGFzaGVtcDtcclxuXHRcdHZlbmRlckl0ZW0udWx0X21lc2EgPSBvcmRlci50YWJsZTtcclxuXHRcdHZlbmRlckl0ZW0uc2VydmljZSA9ICdHcmF2YXJWZW5kYUl0ZW0nO1xyXG5cdFx0bGV0IHRva2VuRGF0YSA9IGF3YWl0IHRoaXMudG9rZW5EZWNvZGVyKHRva2VuKTtcclxuXHRcdHZlbmRlckl0ZW0udG9rZW4gPSB0b2tlbkRhdGEuZGF0YTtcclxuXHJcblx0XHRvcmRlckl0LkNPREVWRU5UTyA9IFwiXCI7XHJcblx0XHRvcmRlckl0LkhPU1QgPSB2ZW5kZXJJdGVtLm1hYztcclxuXHRcdGxldCBvcmRlckl0ZW0gPSB7XHJcblx0XHRcdFtjb25zdW1lckNhcmRdOiBvcmRlckl0XHJcblxyXG5cdFx0XHQvLyB7XHJcblxyXG5cdFx0XHQvLyBcdC8vIEhPU1Q6IHZlbmRlckl0ZW0ubWFjLFxyXG5cdFx0XHQvLyBcdC8vIENPREVWRU5UTzogXCJcIixcclxuXHRcdFx0Ly8gXHRpdGVuczogQXJyYXk8SXRlbT4oKVxyXG5cdFx0XHQvLyB9XHJcblx0XHR9XHJcblx0XHRmb3IgKGxldCBpID0gMDsgaSA8IG9yZGVyLnByb2R1Y3RzLmxlbmd0aDsgaSsrKSB7XHJcblx0XHRcdGxldCBwcm9kdWN0UXVhbnRpdHkgPSBvcmRlci5wcm9kdWN0c1tpXS5xdWFudGl0eTtcclxuXHJcblx0XHRcdC8vIGxldCBpdGVtID0ge1xyXG5cdFx0XHQvLyBcdHF1YW50aWRhZGU6IDEsXHJcblx0XHRcdC8vIFx0Y29kaWdvOiBvcmRlci5wcm9kdWN0c1tpXS5lcG9jX2lkLFxyXG5cdFx0XHQvLyBcdGNvZF9mdW5jX2F1dG9yaXo6IFwiXCIsXHJcblx0XHRcdC8vIFx0bW90aXZvX2F1dG9yaXphY2FvOiBcIjFcIixcclxuXHRcdFx0Ly8gXHRjb2RfZnVuYzogb3JkZXIucmVzcG9uc2libGVFbXBsb3llZS5lcG9jX2lkLFxyXG5cdFx0XHQvLyBcdG1hcmNoYXI6IDAsXHJcblx0XHRcdC8vIFx0dmFsb3I6IG9yZGVyLnByb2R1Y3RzW2ldLnByaWNlLFxyXG5cdFx0XHQvLyBcdHRpcG9JdGVtOiBcIlBcIixcclxuXHRcdFx0Ly8gXHRsYW5jZUl0ZW06IFwiU1wiLFxyXG5cdFx0XHQvLyBcdGFjcmVzY2ltbzogMCxcclxuXHRcdFx0Ly8gXHRkZXNjb250bzogMCxcclxuXHRcdFx0Ly8gXHRvYnM6IFtcclxuXHRcdFx0Ly8gXHRcdHt9XHJcblx0XHRcdC8vIFx0XVxyXG5cdFx0XHQvLyB9XHJcblx0XHRcdGxldCBpdGVtOiBJdGVtID0gbmV3IEl0ZW0oKTtcclxuXHRcdFx0aXRlbS5xdWFudGlkYWRlID0gMTtcclxuXHRcdFx0aXRlbS5jb2RpZ28gPSBvcmRlci5wcm9kdWN0c1tpXS5lcG9jX2lkO1xyXG5cdFx0XHRpdGVtLmNvZF9mdW5jX2F1dG9yaXogPSBcIlwiO1xyXG5cdFx0XHRpdGVtLm1vdGl2b19hdXRvcml6YWNhbyA9IFwiXCI7XHJcblx0XHRcdGl0ZW0uY29kX2Z1bmMgPSBvcmRlci5yZXNwb25zaWJsZUVtcGxveWVlLmVwb2NfaWQ7XHJcblx0XHRcdGl0ZW0ubWFyY2hhciA9IDA7XHJcblx0XHRcdGl0ZW0udmFsb3IgPSBvcmRlci5wcm9kdWN0c1tpXS5wcmljZTtcclxuXHRcdFx0aXRlbS50aXBvSXRlbSA9IFwiUFwiO1xyXG5cdFx0XHRpdGVtLmxhbmNlSXRlbSA9IFwiU1wiO1xyXG5cdFx0XHRpdGVtLmFjcmVzY2ltbyA9IDA7XHJcblx0XHRcdGl0ZW0uZGVzY29udG8gPSAwO1xyXG5cdFx0XHRpdGVtLm9icyA9IG5ldyBBcnJheTxJdGVtT2JzPigpO1xyXG5cclxuXHRcdFx0aWYgKG9yZGVyLnByb2R1Y3RzW2ldLmluZ3JlZGllbnRzICE9PSBudWxsICYmIG9yZGVyLnByb2R1Y3RzW2ldLmluZ3JlZGllbnRzLmxlbmd0aCA+IDApIHtcclxuXHRcdFx0XHRmb3IgKGxldCBqID0gMDsgaiA8IG9yZGVyLnByb2R1Y3RzW2ldLmluZ3JlZGllbnRzLmxlbmd0aDsgaisrKSB7XHJcblx0XHRcdFx0XHRpdGVtLm9ic1tqXSA9IG5ldyBJdGVtT2JzKCk7XHJcblx0XHRcdFx0XHRpdGVtLm9ic1tqXS5kZXNjX29icyA9IG9yZGVyLnByb2R1Y3RzW2ldLmluZ3JlZGllbnRzW2pdLm5hbWU7XHJcblx0XHRcdFx0XHRpdGVtLm9ic1tqXS5jb2Rfb2JzID0gb3JkZXIucHJvZHVjdHNbaV0uaW5ncmVkaWVudHNbal0uZXBvY19pZDtcclxuXHRcdFx0XHRcdGl0ZW0ub2JzW2pdLmNvZF9tb2RpZmljYWRvciA9IDA7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRmb3IgKGxldCBxID0gMDsgcSA8IHByb2R1Y3RRdWFudGl0eTsgcSsrKSB7XHJcblx0XHRcdFx0b3JkZXJJdGVtW2NvbnN1bWVyQ2FyZF0uSVRFTlMucHVzaChpdGVtKTtcclxuXHRcdFx0fVxyXG5cclxuXHJcblx0XHRcdC8vIG9yZGVySXRlbVtjb21hbmRhXS5JVEVOU1tpXSA9IHtcclxuXHRcdFx0Ly8gXHRxdWFudGlkYWRlOiBvcmRlci5wcm9kdWN0c1tpXS5xdWFudGl0eSxcclxuXHRcdFx0Ly8gXHRjb2RpZ286IG9yZGVyLnByb2R1Y3RzW2ldLmVwb2NfaWQsXHJcblx0XHRcdC8vIFx0Y29kX2Z1bmNfYXV0b3JpejogXCJcIixcclxuXHRcdFx0Ly8gXHRtb3Rpdm9fYXV0b3JpemFjYW86IFwiMVwiLFxyXG5cdFx0XHQvLyBcdGNvZF9mdW5jOiBvcmRlci5yZXNwb25zaWJsZUVtcGxveWVlLmVwb2NfaWQsXHJcblx0XHRcdC8vIFx0bWFyY2hhcjogMCxcclxuXHRcdFx0Ly8gXHR2YWxvcjogb3JkZXIucHJvZHVjdHNbaV0ucHJpY2UsXHJcblx0XHRcdC8vIFx0dGlwb0l0ZW06IFwiUFwiLFxyXG5cdFx0XHQvLyBcdGxhbmNlSXRlbTogXCJTXCIsXHJcblx0XHRcdC8vIFx0YWNyZXNjaW1vOiAwLFxyXG5cdFx0XHQvLyBcdGRlc2NvbnRvOiAwLFxyXG5cdFx0XHQvLyBcdG9iczogW11cclxuXHRcdFx0Ly8gfVxyXG5cdFx0XHQvLyBpZiAob3JkZXIucHJvZHVjdHNbaV0uaW5ncmVkaWVudHMgIT09IG51bGwgJiYgb3JkZXIucHJvZHVjdHNbaV0uaW5ncmVkaWVudHMubGVuZ3RoID4gMCkge1xyXG5cdFx0XHQvLyBcdGZvciAobGV0IGogPSAwOyBqIDwgb3JkZXIucHJvZHVjdHNbaV0uaW5ncmVkaWVudHMubGVuZ3RoOyBqKyspIHtcclxuXHRcdFx0Ly8gXHRcdG9yZGVySXRlbVtjb21hbmRhXS5JVEVOU1tpXS5vYnNbal0gPSB7XHJcblx0XHRcdC8vIFx0XHRcdGRlc2Nfb2JzOiBvcmRlci5wcm9kdWN0c1tpXS5pbmdyZWRpZW50c1tqXS5uYW1lLFxyXG5cdFx0XHQvLyBcdFx0XHRjb2Rfb2JzOiBvcmRlci5wcm9kdWN0c1tpXS5pbmdyZWRpZW50c1tqXS5lcG9jX2lkLFxyXG5cdFx0XHQvLyBcdFx0XHRjb2RfbW9kaWZpY2Fkb3I6IDBcclxuXHRcdFx0Ly8gXHRcdH1cclxuXHRcdFx0Ly8gXHR9XHJcblx0XHRcdC8vIH1cclxuXHJcblx0XHR9XHJcblxyXG5cdFx0bGV0IHN0cmluZ1NhbHQ6IHN0cmluZyA9IFwiXCI7XHJcblx0XHRmb3IgKGxldCBpID0gMDsgaSA8IG9yZGVySXRlbVtjb25zdW1lckNhcmRdLklURU5TLmxlbmd0aDsgaSsrKSB7XHJcblx0XHRcdHN0cmluZ1NhbHQgKz0gb3JkZXJJdGVtW2NvbnN1bWVyQ2FyZF0uSVRFTlNbaV0uY29kaWdvICsgJ18nO1xyXG5cdFx0fVxyXG5cdFx0c3RyaW5nU2FsdCA9IHN0cmluZ1NhbHQuc2xpY2UoMCwgLTEpO1xyXG5cdFx0bGV0IGl0ZW1Db252ZXJ0OiBzdHJpbmc7XHJcblx0XHRsZXQgdmVuZGVySXRlbVRvU3RyaW5nOiBzdHJpbmc7XHJcblx0XHRpdGVtQ29udmVydCA9IEpTT04uc3RyaW5naWZ5KG9yZGVySXRlbSk7XHJcblx0XHR2ZW5kZXJJdGVtLmRhdGFWZW5kYSA9IG5ldyBCdWZmZXIoaXRlbUNvbnZlcnQpLnRvU3RyaW5nKCdiYXNlNjQnKTtcclxuXHRcdHZlbmRlckl0ZW0uc2FsdCA9IGNyeXB0by5jcmVhdGVIYXNoKCdtZDUnKS51cGRhdGUoc3RyaW5nU2FsdCkuZGlnZXN0KFwiaGV4XCIpO1xyXG5cdFx0dmVuZGVySXRlbVRvU3RyaW5nID0gcXVlcnlzdHJpbmcuc3RyaW5naWZ5KHZlbmRlckl0ZW0pO1xyXG5cclxuXHJcblx0XHQvLyByZXR1cm4gbmV3IFByb21pc2U8UmVzcG9uc2VFcG9jPigocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcblxyXG5cdFx0Ly8gfSlcclxuXHJcblxyXG5cdFx0cmV0dXJuIG5ldyBQcm9taXNlPFJlc3BvbnNlRXBvYz4oKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG5cdFx0XHRyZXF1ZXN0LnBvc3Qoe1xyXG5cdFx0XHRcdGhlYWRlcnM6IHtcclxuXHRcdFx0XHRcdCdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkJyxcclxuXHRcdFx0XHRcdCdBY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbidcclxuXHRcdFx0XHR9LFxyXG5cdFx0XHRcdHVybDogYGh0dHA6Ly9sbG0wOS5lcG9jMi5jZG1vbi5vcmc6NTY0NzgvQVBJL21vZF92ZW5kYV9pdGVtL2luZGV4LnBocGAsXHJcblx0XHRcdFx0Ym9keTogdmVuZGVySXRlbVRvU3RyaW5nXHJcblx0XHRcdH0sIGFzeW5jIChlcnIsIHJlc3AsIGJvZHkpID0+IHtcclxuXHRcdFx0XHRpZiAoIWVycikge1xyXG5cdFx0XHRcdFx0Ym9keSA9IGF3YWl0IHRoaXMudG9rZW5EZWNvZGVyKGJvZHkpO1xyXG5cdFx0XHRcdFx0Y29uc29sZS5sb2coYm9keSk7XHJcblx0XHRcdFx0XHRyZXR1cm4gbmV3IFJlc3BvbnNlRXBvYyhib2R5KTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0ZWxzZSB7IFxyXG5cdFx0XHRcdFx0cmV0dXJuIHJlamVjdChlcnIpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSk7XHJcblx0XHR9KTtcclxuXHJcblxyXG5cclxuXHR9XHJcblx0Z2V0RXh0cmF0byhvYmpFeHRyYXRvOiBhbnkpOiBTdHJpbmcge1xyXG5cdFx0dGhyb3cgbmV3IEVycm9yKFwiTWV0aG9kIG5vdCBpbXBsZW1lbnRlZC5cIik7XHJcblx0fVxyXG5cdGdldFByb2R1Y3RzKCk6IFN0cmluZyB7XHJcblx0XHR0aHJvdyBuZXcgRXJyb3IoXCJNZXRob2Qgbm90IGltcGxlbWVudGVkLlwiKTtcclxuXHR9XHJcblxyXG5cdHByaXZhdGUgYXN5bmMgdG9rZW5EZWNvZGVyKHRva2VuOiBhbnkpIHtcclxuXHRcdGxldCBkZWNvZGVCdWZmZXIgPSBuZXcgQnVmZmVyKHRva2VuLCAnYmFzZTY0Jyk7XHJcblx0XHRsZXQgdG9rZW5EZWNvZGVkOiBhbnk7XHJcblx0XHRkZWNvZGVCdWZmZXIgPSBkZWNvZGVCdWZmZXIudG9TdHJpbmcoJ2FzY2lpJyk7XHJcblx0XHR0b2tlbkRlY29kZWQgPSBKU09OLnBhcnNlKGRlY29kZUJ1ZmZlcik7XHJcblx0XHRyZXR1cm4gdG9rZW5EZWNvZGVkO1xyXG5cdH1cclxuXHJcblx0c2lzdGVtYUF1dGgoKTogU2lzdGVtYUF1dGgge1xyXG5cclxuXHRcdGxldCBlcG9jQXV0aCA9IG5ldyBTaXN0ZW1hQXV0aCgpO1xyXG5cdFx0ZXBvY0F1dGguaGFzaGVtcCA9ICdkYTJnMzlqYmNkamE4NjAxJztcclxuXHRcdGVwb2NBdXRoLm1hYyA9ICdlNDpjMTpkZDpjMDo5YTo0ZCc7XHJcblx0XHRlcG9jQXV0aC50b2tlblBhcmNlaXJvID0gJzM1M2MxZWEzLTY4MmItNDc1NC05MWVmLWY1NWQwNWFlYzc1OCc7XHJcblx0XHRlcG9jQXV0aC5zYWx0UGFyY2Vpcm8gPSBjcnlwdG8uY3JlYXRlSGFzaCgnbWQ1JykudXBkYXRlKGVwb2NBdXRoLnRva2VuUGFyY2Vpcm8gKyAnX19UT0tFTl9fJykuZGlnZXN0KFwiaGV4XCIpO1xyXG5cclxuXHRcdHJldHVybiBlcG9jQXV0aDtcclxuXHR9O1xyXG5cclxufSJdfQ==
