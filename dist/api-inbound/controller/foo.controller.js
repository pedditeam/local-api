"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = __importStar(require("express"));
const inversify_express_utils_1 = require("inversify-express-utils");
let FooController = class FooController {
    constructor(// @inject("FooService") 
    fooService) {
        this.fooService = fooService;
    }
    get(req, res, next) {
        return this.fooService.get();
    }
    send(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.fooService.send(req.body);
                res.sendStatus(201);
            }
            catch (err) {
                res.status(400).json({ error: err.message });
            }
        });
    }
};
__decorate([
    inversify_express_utils_1.httpGet("/"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Function]),
    __metadata("design:returntype", String)
], FooController.prototype, "get", null);
__decorate([
    inversify_express_utils_1.httpPost("/"),
    __param(0, inversify_express_utils_1.request()), __param(1, inversify_express_utils_1.response()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], FooController.prototype, "send", null);
FooController = __decorate([
    inversify_express_utils_1.controller("/foo"),
    __metadata("design:paramtypes", [Object])
], FooController);
exports.FooController = FooController;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9hcGktaW5ib3VuZC9jb250cm9sbGVyL2Zvby5jb250cm9sbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsaURBQW1DO0FBQ25DLHFFQUE2STtBQUs3SSxJQUFhLGFBQWEsR0FBMUIsTUFBYSxhQUFhO0lBRXRCLFlBQWEseUJBQXlCO0lBQzlCLFVBQXNCO1FBQXRCLGVBQVUsR0FBVixVQUFVLENBQVk7SUFBSSxDQUFDO0lBRzNCLEdBQUcsQ0FBQyxHQUFvQixFQUFFLEdBQXFCLEVBQUUsSUFBMEI7UUFDL0UsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFDO0lBQ2pDLENBQUM7SUFHYSxJQUFJLENBQVksR0FBb0IsRUFBYyxHQUFxQjs7WUFDakYsSUFBSTtnQkFDQSxNQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDckMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQzthQUN2QjtZQUFDLE9BQU8sR0FBRyxFQUFFO2dCQUNWLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUUsS0FBSyxFQUFFLEdBQUcsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDO2FBQ2hEO1FBQ0wsQ0FBQztLQUFBO0NBaUJKLENBQUE7QUE3Qkc7SUFEQyxpQ0FBTyxDQUFDLEdBQUcsQ0FBQzs7Ozt3Q0FHWjtBQUdEO0lBREMsa0NBQVEsQ0FBQyxHQUFHLENBQUM7SUFDTSxXQUFBLGlDQUFPLEVBQUUsQ0FBQSxFQUF3QixXQUFBLGtDQUFRLEVBQUUsQ0FBQTs7Ozt5Q0FPOUQ7QUFsQlEsYUFBYTtJQUR6QixvQ0FBVSxDQUFDLE1BQU0sQ0FBQzs7R0FDTixhQUFhLENBbUN6QjtBQW5DWSxzQ0FBYSIsImZpbGUiOiJhcGktaW5ib3VuZC9jb250cm9sbGVyL2Zvby5jb250cm9sbGVyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgZXhwcmVzcyBmcm9tIFwiZXhwcmVzc1wiO1xyXG5pbXBvcnQgeyBpbnRlcmZhY2VzLCBjb250cm9sbGVyLCBodHRwR2V0LCBodHRwUG9zdCwgaHR0cERlbGV0ZSwgcmVxdWVzdCwgcXVlcnlQYXJhbSwgcmVzcG9uc2UsIHJlcXVlc3RQYXJhbSB9IGZyb20gXCJpbnZlcnNpZnktZXhwcmVzcy11dGlsc1wiO1xyXG5pbXBvcnQgeyBpbmplY3RhYmxlLCBpbmplY3QgfSBmcm9tIFwiaW52ZXJzaWZ5XCI7XHJcbmltcG9ydCBGb29TZXJ2aWNlIGZyb20gXCIuLi8uLi9hcGktY29yZS9oYW5kbGVyL2Zvby5zZXJ2aWNlXCI7XHJcblxyXG5AY29udHJvbGxlcihcIi9mb29cIilcclxuZXhwb3J0IGNsYXNzIEZvb0NvbnRyb2xsZXIgaW1wbGVtZW50cyBpbnRlcmZhY2VzLkNvbnRyb2xsZXIge1xyXG4gXHJcbiAgICBjb25zdHJ1Y3RvciggLy8gQGluamVjdChcIkZvb1NlcnZpY2VcIikgXHJcbiAgICBwcml2YXRlIGZvb1NlcnZpY2U6IEZvb1NlcnZpY2UgKSB7fVxyXG4gXHJcbiAgICBAaHR0cEdldChcIi9cIilcclxuICAgIHByaXZhdGUgZ2V0KHJlcTogZXhwcmVzcy5SZXF1ZXN0LCByZXM6IGV4cHJlc3MuUmVzcG9uc2UsIG5leHQ6IGV4cHJlc3MuTmV4dEZ1bmN0aW9uKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5mb29TZXJ2aWNlLmdldCgpO1xyXG4gICAgfVxyXG5cclxuICAgIEBodHRwUG9zdChcIi9cIilcclxuICAgIHByaXZhdGUgYXN5bmMgc2VuZChAcmVxdWVzdCgpIHJlcTogZXhwcmVzcy5SZXF1ZXN0LCBAcmVzcG9uc2UoKSByZXM6IGV4cHJlc3MuUmVzcG9uc2UpIHtcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICBhd2FpdCB0aGlzLmZvb1NlcnZpY2Uuc2VuZChyZXEuYm9keSk7XHJcbiAgICAgICAgICAgIHJlcy5zZW5kU3RhdHVzKDIwMSk7XHJcbiAgICAgICAgfSBjYXRjaCAoZXJyKSB7XHJcbiAgICAgICAgICAgIHJlcy5zdGF0dXMoNDAwKS5qc29uKHsgZXJyb3I6IGVyci5tZXNzYWdlIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuIFxyXG4gICAgLy8gQGh0dHBHZXQoXCIvXCIpXHJcbiAgICAvLyBwcml2YXRlIGxpc3QoQHF1ZXJ5UGFyYW0oXCJzdGFydFwiKSBzdGFydDogbnVtYmVyLCBAcXVlcnlQYXJhbShcImNvdW50XCIpIGNvdW50OiBudW1iZXIpOiBzdHJpbmcge1xyXG4gICAgLy8gICAgIHJldHVybiB0aGlzLmZvb1NlcnZpY2UuZ2V0KHN0YXJ0LCBjb3VudCk7XHJcbiAgICAvLyB9XHJcbiBcclxuICAgXHJcbiBcclxuICAgIC8vIEBodHRwRGVsZXRlKFwiLzppZFwiKVxyXG4gICAgLy8gcHJpdmF0ZSBkZWxldGUoQHJlcXVlc3RQYXJhbShcImlkXCIpIGlkOiBzdHJpbmcsIEByZXNwb25zZSgpIHJlczogZXhwcmVzcy5SZXNwb25zZSk6IFByb21pc2U8dm9pZD4ge1xyXG4gICAgLy8gICAgIHJldHVybiB0aGlzLmZvb1NlcnZpY2UuZGVsZXRlKGlkKVxyXG4gICAgLy8gICAgICAgICAudGhlbigoKSA9PiByZXMuc2VuZFN0YXR1cygyMDQpKVxyXG4gICAgLy8gICAgICAgICAuY2F0Y2goKGVycjogRXJyb3IpID0+IHtcclxuICAgIC8vICAgICAgICAgICAgIHJlcy5zdGF0dXMoNDAwKS5qc29uKHsgZXJyb3I6IGVyci5tZXNzYWdlIH0pO1xyXG4gICAgLy8gICAgICAgICB9KTtcclxuICAgIC8vIH1cclxufSJdfQ==
