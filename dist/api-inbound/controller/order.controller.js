"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const inversify_express_utils_1 = require("inversify-express-utils");
const inversify_1 = require("inversify");
const order_dto_1 = require("../dto/order.dto");
const order_adapter_1 = require("../adapter/order.adapter");
const types_1 = require("../../config/types");
let OrderController = class OrderController {
    constructor(
    //@inject("OrderPortInbound") 
    orderPortInbound) {
        this.orderPortInbound = orderPortInbound;
    }
    getAll(req, res, next) {
        return this.orderPortInbound.getAll();
    }
    get(id, req, res, next) {
        return this.orderPortInbound.getById(id)
            .then(order => res.status(200).json(order))
            .catch(err => res.status(400).json({ "error": err.message }));
    }
    ask(req, res) {
        const order = new order_dto_1.OrderDTO(req.body); // meant for validation
        const orderModel = order_adapter_1.orderAdapter(order); //convert DTO to core model
        return this.orderPortInbound.save(orderModel)
            .then(resp => res.status(200).json(resp))
            .catch(resp => res.status(400).json(resp));
    }
    askAgain(id, req, res) {
        const update = new order_dto_1.UpdateDTO(req.body);
        const updateModel = order_adapter_1.updateAdapter(update); //convert DTO to core model
        return this.orderPortInbound.update(id, updateModel)
            .then(fails => res.status(200).json(fails))
            .catch(err => res.status(400).json({ "error": err.message }));
    }
    send(id, req, res) {
        return this.orderPortInbound.close(id)
            .then(() => res.status(204).send())
            .catch(err => res.status(400).json({ "error": err.message }));
        //     console.log("Chegou na camada inbound");
        //     const order = new OrderDTO(req.body);
        //     try {
        //         this.orderPortInbound.send(order);
        //         res.status(200);
        //     } catch (error) {
        //         res.status(400).json({error: error});
        //     }
        //     // this.orderPortInbound.insert(order);
        //     // try {
        //     //     await this.orderPortInbound.send(order);
        //     //     res.sendStatus(201);
        //     // } catch (err) {
        //     //     res.status(400).json({ error: err.message });
        //     // }
    }
};
__decorate([
    inversify_express_utils_1.httpGet("/"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Function]),
    __metadata("design:returntype", void 0)
], OrderController.prototype, "getAll", null);
__decorate([
    inversify_express_utils_1.httpGet("/:id"),
    __param(0, inversify_express_utils_1.requestParam("id")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object, Object, Function]),
    __metadata("design:returntype", void 0)
], OrderController.prototype, "get", null);
__decorate([
    inversify_express_utils_1.httpPost("/"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], OrderController.prototype, "ask", null);
__decorate([
    inversify_express_utils_1.httpPut("/:id"),
    __param(0, inversify_express_utils_1.requestParam("id")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object, Object]),
    __metadata("design:returntype", void 0)
], OrderController.prototype, "askAgain", null);
__decorate([
    inversify_express_utils_1.httpDelete("/:id"),
    __param(0, inversify_express_utils_1.requestParam("id")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object, Object]),
    __metadata("design:returntype", void 0)
], OrderController.prototype, "send", null);
OrderController = __decorate([
    inversify_express_utils_1.controller("/orders"),
    __param(0, inversify_1.inject(types_1.types.OrderPortInbound)),
    __metadata("design:paramtypes", [Object])
], OrderController);
exports.default = OrderController;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9hcGktaW5ib3VuZC9jb250cm9sbGVyL29yZGVyLmNvbnRyb2xsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7QUFDQSxxRUFBc0o7QUFDdEoseUNBQStDO0FBQy9DLGdEQUFxRDtBQUVyRCw0REFBb0U7QUFFcEUsOENBQTBDO0FBRzFDLElBQXFCLGVBQWUsR0FBcEMsTUFBcUIsZUFBZTtJQUVoQztJQUNJLDhCQUE4QjtJQUV0QixnQkFBa0M7UUFBbEMscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtJQUFJLENBQUM7SUFHM0MsTUFBTSxDQUFDLEdBQVksRUFBRSxHQUFhLEVBQUUsSUFBa0I7UUFDMUQsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxFQUFFLENBQUM7SUFDMUMsQ0FBQztJQUdPLEdBQUcsQ0FBcUIsRUFBVSxFQUFFLEdBQVksRUFBRSxHQUFhLEVBQUUsSUFBa0I7UUFDdkYsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQzthQUMvQixJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUMxQyxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFFLE9BQU8sRUFBRSxHQUFHLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxDQUFBO0lBQ3pFLENBQUM7SUFHTyxHQUFHLENBQUMsR0FBWSxFQUFFLEdBQWE7UUFDbkMsTUFBTSxLQUFLLEdBQUcsSUFBSSxvQkFBUSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLHVCQUF1QjtRQUM3RCxNQUFNLFVBQVUsR0FBRyw0QkFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsMkJBQTJCO1FBQ25FLE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7YUFDeEMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDeEMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQTtJQUNsRCxDQUFDO0lBR08sUUFBUSxDQUFxQixFQUFVLEVBQUUsR0FBWSxFQUFFLEdBQWE7UUFDeEUsTUFBTSxNQUFNLEdBQUcsSUFBSSxxQkFBUyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN2QyxNQUFNLFdBQVcsR0FBRyw2QkFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsMkJBQTJCO1FBQ3RFLE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsV0FBVyxDQUFDO2FBQy9DLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQzFDLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUUsT0FBTyxFQUFFLEdBQUcsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLENBQUE7SUFDckUsQ0FBQztJQUdPLElBQUksQ0FBcUIsRUFBVSxFQUFFLEdBQVksRUFBRSxHQUFhO1FBQ3BFLE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUM7YUFDakMsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7YUFDbEMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsRUFBRSxPQUFPLEVBQUUsR0FBRyxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsQ0FBQTtRQUNqRSwrQ0FBK0M7UUFDL0MsNENBQTRDO1FBQzVDLFlBQVk7UUFDWiw2Q0FBNkM7UUFDN0MsMkJBQTJCO1FBQzNCLHdCQUF3QjtRQUN4QixnREFBZ0Q7UUFDaEQsUUFBUTtRQUdSLDhDQUE4QztRQUU5QyxlQUFlO1FBQ2Ysc0RBQXNEO1FBQ3RELGtDQUFrQztRQUNsQyx5QkFBeUI7UUFDekIsMkRBQTJEO1FBQzNELFdBQVc7SUFDZixDQUFDO0NBaUdKLENBQUE7QUFySkc7SUFEQyxpQ0FBTyxDQUFDLEdBQUcsQ0FBQzs7Ozs2Q0FHWjtBQUdEO0lBREMsaUNBQU8sQ0FBQyxNQUFNLENBQUM7SUFDSCxXQUFBLHNDQUFZLENBQUMsSUFBSSxDQUFDLENBQUE7O3FDQUFLLE1BQU07OzBDQUl6QztBQUdEO0lBREMsa0NBQVEsQ0FBQyxHQUFHLENBQUM7Ozs7MENBT2I7QUFHRDtJQURDLGlDQUFPLENBQUMsTUFBTSxDQUFDO0lBQ0UsV0FBQSxzQ0FBWSxDQUFDLElBQUksQ0FBQyxDQUFBOztxQ0FBSyxNQUFNOzsrQ0FNOUM7QUFHRDtJQURDLG9DQUFVLENBQUMsTUFBTSxDQUFDO0lBQ0wsV0FBQSxzQ0FBWSxDQUFDLElBQUksQ0FBQyxDQUFBOztxQ0FBSyxNQUFNOzsyQ0FzQjFDO0FBNURnQixlQUFlO0lBRG5DLG9DQUFVLENBQUMsU0FBUyxDQUFDO0lBS2IsV0FBQSxrQkFBTSxDQUFDLGFBQUssQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFBOztHQUpsQixlQUFlLENBNkpuQztrQkE3Sm9CLGVBQWUiLCJmaWxlIjoiYXBpLWluYm91bmQvY29udHJvbGxlci9vcmRlci5jb250cm9sbGVyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUmVxdWVzdCwgUmVzcG9uc2UsIE5leHRGdW5jdGlvbiB9IGZyb20gXCJleHByZXNzXCI7XHJcbmltcG9ydCB7IGludGVyZmFjZXMsIGNvbnRyb2xsZXIsIGh0dHBHZXQsIGh0dHBQb3N0LCBodHRwRGVsZXRlLCByZXF1ZXN0LCBxdWVyeVBhcmFtLCByZXNwb25zZSwgcmVxdWVzdFBhcmFtLCBodHRwUHV0IH0gZnJvbSBcImludmVyc2lmeS1leHByZXNzLXV0aWxzXCI7XHJcbmltcG9ydCB7IGluamVjdGFibGUsIGluamVjdCB9IGZyb20gXCJpbnZlcnNpZnlcIjtcclxuaW1wb3J0IHtPcmRlckRUTywgVXBkYXRlRFRPfSBmcm9tIFwiLi4vZHRvL29yZGVyLmR0b1wiO1xyXG5pbXBvcnQgT3JkZXJQb3J0SW5ib3VuZCBmcm9tIFwiLi4vLi4vYXBpLWNvcmUvcG9ydC9pbmJvdW5kL29yZGVyLmluYm91bmQtcG9ydFwiO1xyXG5pbXBvcnQge29yZGVyQWRhcHRlciwgdXBkYXRlQWRhcHRlcn0gZnJvbSBcIi4uL2FkYXB0ZXIvb3JkZXIuYWRhcHRlclwiXHJcbmltcG9ydCB7IFNUQVRVU19DT0RFUyB9IGZyb20gXCJodHRwXCI7XHJcbmltcG9ydCB7IHR5cGVzIH0gZnJvbSAnLi4vLi4vY29uZmlnL3R5cGVzJ1xyXG5cclxuQGNvbnRyb2xsZXIoXCIvb3JkZXJzXCIpXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIE9yZGVyQ29udHJvbGxlciBpbXBsZW1lbnRzIGludGVyZmFjZXMuQ29udHJvbGxlciB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgLy9AaW5qZWN0KFwiT3JkZXJQb3J0SW5ib3VuZFwiKSBcclxuICAgICAgICBAaW5qZWN0KHR5cGVzLk9yZGVyUG9ydEluYm91bmQpXHJcbiAgICAgICAgcHJpdmF0ZSBvcmRlclBvcnRJbmJvdW5kOiBPcmRlclBvcnRJbmJvdW5kKSB7IH1cclxuXHJcbiAgICBAaHR0cEdldChcIi9cIilcclxuICAgIHByaXZhdGUgZ2V0QWxsKHJlcTogUmVxdWVzdCwgcmVzOiBSZXNwb25zZSwgbmV4dDogTmV4dEZ1bmN0aW9uKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMub3JkZXJQb3J0SW5ib3VuZC5nZXRBbGwoKTtcclxuICAgIH1cclxuXHJcbiAgICBAaHR0cEdldChcIi86aWRcIilcclxuICAgIHByaXZhdGUgZ2V0KEByZXF1ZXN0UGFyYW0oXCJpZFwiKSBpZDogU3RyaW5nLCByZXE6IFJlcXVlc3QsIHJlczogUmVzcG9uc2UsIG5leHQ6IE5leHRGdW5jdGlvbikge1xyXG4gICAgICAgIHJldHVybiB0aGlzLm9yZGVyUG9ydEluYm91bmQuZ2V0QnlJZChpZClcclxuICAgICAgICAgICAgICAgIC50aGVuKG9yZGVyID0+IHJlcy5zdGF0dXMoMjAwKS5qc29uKG9yZGVyKSlcclxuICAgICAgICAgICAgICAgIC5jYXRjaChlcnIgPT4gcmVzLnN0YXR1cyg0MDApLmpzb24oeyBcImVycm9yXCI6IGVyci5tZXNzYWdlIH0pKVxyXG4gICAgfVxyXG5cclxuICAgIEBodHRwUG9zdChcIi9cIilcclxuICAgIHByaXZhdGUgYXNrKHJlcTogUmVxdWVzdCwgcmVzOiBSZXNwb25zZSkge1xyXG4gICAgICAgIGNvbnN0IG9yZGVyID0gbmV3IE9yZGVyRFRPKHJlcS5ib2R5KTsgLy8gbWVhbnQgZm9yIHZhbGlkYXRpb25cclxuICAgICAgICBjb25zdCBvcmRlck1vZGVsID0gb3JkZXJBZGFwdGVyKG9yZGVyKTsgLy9jb252ZXJ0IERUTyB0byBjb3JlIG1vZGVsXHJcbiAgICAgICAgcmV0dXJuIHRoaXMub3JkZXJQb3J0SW5ib3VuZC5zYXZlKG9yZGVyTW9kZWwpXHJcbiAgICAgICAgICAgIC50aGVuKHJlc3AgPT4gcmVzLnN0YXR1cygyMDApLmpzb24ocmVzcCkpXHJcbiAgICAgICAgICAgIC5jYXRjaChyZXNwID0+IHJlcy5zdGF0dXMoNDAwKS5qc29uKHJlc3ApKSBcclxuICAgIH1cclxuXHJcbiAgICBAaHR0cFB1dChcIi86aWRcIilcclxuICAgIHByaXZhdGUgYXNrQWdhaW4oQHJlcXVlc3RQYXJhbShcImlkXCIpIGlkOiBTdHJpbmcsIHJlcTogUmVxdWVzdCwgcmVzOiBSZXNwb25zZSkge1xyXG4gICAgICAgIGNvbnN0IHVwZGF0ZSA9IG5ldyBVcGRhdGVEVE8ocmVxLmJvZHkpO1xyXG4gICAgICAgIGNvbnN0IHVwZGF0ZU1vZGVsID0gdXBkYXRlQWRhcHRlcih1cGRhdGUpOyAvL2NvbnZlcnQgRFRPIHRvIGNvcmUgbW9kZWxcclxuICAgICAgICByZXR1cm4gdGhpcy5vcmRlclBvcnRJbmJvdW5kLnVwZGF0ZShpZCwgdXBkYXRlTW9kZWwpXHJcbiAgICAgICAgICAgIC50aGVuKGZhaWxzID0+IHJlcy5zdGF0dXMoMjAwKS5qc29uKGZhaWxzKSlcclxuICAgICAgICAgICAgLmNhdGNoKGVyciA9PiByZXMuc3RhdHVzKDQwMCkuanNvbih7IFwiZXJyb3JcIjogZXJyLm1lc3NhZ2UgfSkpXHJcbiAgICB9XHJcblxyXG4gICAgQGh0dHBEZWxldGUoXCIvOmlkXCIpXHJcbiAgICBwcml2YXRlIHNlbmQoQHJlcXVlc3RQYXJhbShcImlkXCIpIGlkOiBTdHJpbmcsIHJlcTogUmVxdWVzdCwgcmVzOiBSZXNwb25zZSkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLm9yZGVyUG9ydEluYm91bmQuY2xvc2UoaWQpXHJcbiAgICAgICAgICAgIC50aGVuKCgpID0+IHJlcy5zdGF0dXMoMjA0KS5zZW5kKCkpXHJcbiAgICAgICAgICAgIC5jYXRjaChlcnIgPT4gcmVzLnN0YXR1cyg0MDApLmpzb24oeyBcImVycm9yXCI6IGVyci5tZXNzYWdlIH0pKVxyXG4gICAgICAgIC8vICAgICBjb25zb2xlLmxvZyhcIkNoZWdvdSBuYSBjYW1hZGEgaW5ib3VuZFwiKTtcclxuICAgICAgICAvLyAgICAgY29uc3Qgb3JkZXIgPSBuZXcgT3JkZXJEVE8ocmVxLmJvZHkpO1xyXG4gICAgICAgIC8vICAgICB0cnkge1xyXG4gICAgICAgIC8vICAgICAgICAgdGhpcy5vcmRlclBvcnRJbmJvdW5kLnNlbmQob3JkZXIpO1xyXG4gICAgICAgIC8vICAgICAgICAgcmVzLnN0YXR1cygyMDApO1xyXG4gICAgICAgIC8vICAgICB9IGNhdGNoIChlcnJvcikge1xyXG4gICAgICAgIC8vICAgICAgICAgcmVzLnN0YXR1cyg0MDApLmpzb24oe2Vycm9yOiBlcnJvcn0pO1xyXG4gICAgICAgIC8vICAgICB9XHJcblxyXG5cclxuICAgICAgICAvLyAgICAgLy8gdGhpcy5vcmRlclBvcnRJbmJvdW5kLmluc2VydChvcmRlcik7XHJcblxyXG4gICAgICAgIC8vICAgICAvLyB0cnkge1xyXG4gICAgICAgIC8vICAgICAvLyAgICAgYXdhaXQgdGhpcy5vcmRlclBvcnRJbmJvdW5kLnNlbmQob3JkZXIpO1xyXG4gICAgICAgIC8vICAgICAvLyAgICAgcmVzLnNlbmRTdGF0dXMoMjAxKTtcclxuICAgICAgICAvLyAgICAgLy8gfSBjYXRjaCAoZXJyKSB7XHJcbiAgICAgICAgLy8gICAgIC8vICAgICByZXMuc3RhdHVzKDQwMCkuanNvbih7IGVycm9yOiBlcnIubWVzc2FnZSB9KTtcclxuICAgICAgICAvLyAgICAgLy8gfVxyXG4gICAgfVxyXG5cclxuXHJcblxyXG4gICAgLy8gQGh0dHBQb3N0KFwiL1wiKVxyXG4gICAgLy8gcHJpdmF0ZSBzYXZlKHJlcTogUmVxdWVzdCwgcmVzOiBSZXNwb25zZSkge1xyXG5cclxuICAgIC8vICAgICBjb25zdCBvcmRlciA9IG5ldyBPcmRlckRUTyhyZXEuYm9keSk7XHJcbiAgICAvLyAgICAgdHJ5IHtcclxuICAgIC8vICAgICAgICAgbGV0IGlkID0gdGhpcy5vcmRlclBvcnRJbmJvdW5kLnNhdmUob3JkZXIpO1xyXG4gICAgLy8gICAgICAgICByZXMuc3RhdHVzKDIwMCkuanNvbih7XHJcbiAgICAvLyAgICAgICAgICAgICBzdGF0dXM6IHJlcy5zdGF0dXNDb2RlLFxyXG4gICAgLy8gICAgICAgICAgICAgZGF0YToge1xyXG4gICAgLy8gICAgICAgICAgICAgICAgIGlkOiBpZFxyXG4gICAgLy8gICAgICAgICAgICAgfVxyXG4gICAgLy8gICAgICAgICB9KVxyXG4gICAgLy8gICAgIH0gY2F0Y2ggKGVycm9yKSB7XHJcblxyXG4gICAgLy8gICAgIH1cclxuXHJcblxyXG4gICAgLy8gICAgIC8vIHRoaXMub3JkZXJQb3J0SW5ib3VuZC5pbnNlcnQob3JkZXIpO1xyXG5cclxuICAgIC8vICAgICAvLyB0cnkge1xyXG4gICAgLy8gICAgIC8vICAgICBhd2FpdCB0aGlzLm9yZGVyUG9ydEluYm91bmQuc2VuZChvcmRlcik7XHJcbiAgICAvLyAgICAgLy8gICAgIHJlcy5zZW5kU3RhdHVzKDIwMSk7XHJcbiAgICAvLyAgICAgLy8gfSBjYXRjaCAoZXJyKSB7XHJcbiAgICAvLyAgICAgLy8gICAgIHJlcy5zdGF0dXMoNDAwKS5qc29uKHsgZXJyb3I6IGVyci5tZXNzYWdlIH0pO1xyXG4gICAgLy8gICAgIC8vIH1cclxuICAgIC8vIH1cclxuXHJcbiAgICAvLyBAaHR0cFBvc3QoXCIvXCIpXHJcbiAgICAvLyBwcml2YXRlIHNlbmQocmVxOiBSZXF1ZXN0LCByZXM6IFJlc3BvbnNlKSB7XHJcbiAgICAvLyAgICAgY29uc29sZS5sb2coXCJDaGVnb3UgbmEgY2FtYWRhIGluYm91bmRcIik7XHJcbiAgICAvLyAgICAgY29uc3Qgb3JkZXIgPSBuZXcgT3JkZXJEVE8ocmVxLmJvZHkpO1xyXG4gICAgLy8gICAgIHRyeSB7XHJcbiAgICAvLyAgICAgICAgIHRoaXMub3JkZXJQb3J0SW5ib3VuZC5zZW5kKG9yZGVyKTtcclxuICAgIC8vICAgICAgICAgcmVzLnN0YXR1cygyMDApO1xyXG4gICAgLy8gICAgIH0gY2F0Y2ggKGVycm9yKSB7XHJcbiAgICAvLyAgICAgICAgIHJlcy5zdGF0dXMoNDAwKS5qc29uKHtlcnJvcjogZXJyb3J9KTtcclxuICAgIC8vICAgICB9XHJcblxyXG5cclxuICAgIC8vICAgICAvLyB0aGlzLm9yZGVyUG9ydEluYm91bmQuaW5zZXJ0KG9yZGVyKTtcclxuXHJcbiAgICAvLyAgICAgLy8gdHJ5IHtcclxuICAgIC8vICAgICAvLyAgICAgYXdhaXQgdGhpcy5vcmRlclBvcnRJbmJvdW5kLnNlbmQob3JkZXIpO1xyXG4gICAgLy8gICAgIC8vICAgICByZXMuc2VuZFN0YXR1cygyMDEpO1xyXG4gICAgLy8gICAgIC8vIH0gY2F0Y2ggKGVycikge1xyXG4gICAgLy8gICAgIC8vICAgICByZXMuc3RhdHVzKDQwMCkuanNvbih7IGVycm9yOiBlcnIubWVzc2FnZSB9KTtcclxuICAgIC8vICAgICAvLyB9XHJcbiAgICAvLyB9XHJcblxyXG5cclxuXHJcbiAgICAvLyBAaHR0cFBvc3QoXCIvOmlkXCIpXHJcbiAgICAvLyBwcml2YXRlIHVwZGF0ZShAcmVxdWVzdFBhcmFtKFwiaWRcIikgaWQgOiBOdW1iZXIsIHJlcTogUmVxdWVzdCwgcmVzOiBSZXNwb25zZSkge1xyXG5cclxuICAgIC8vICAgICBjb25zdCBvcmRlciA9IG5ldyBPcmRlckRUTyhyZXEuYm9keSk7XHJcbiAgICAvLyAgICAgdHJ5IHtcclxuICAgIC8vICAgICAgICAgY29uc3QgdXBkYXRlZE9yZGVyID0gdGhpcy5vcmRlclBvcnRJbmJvdW5kLnVwZGF0ZShpZCwgb3JkZXIpO1xyXG4gICAgLy8gICAgICAgICByZXMuc3RhdHVzKDIwMCkuanNvbih7XHJcbiAgICAvLyAgICAgICAgICAgICBzdGF0dXM6IHJlcy5zdGF0dXNDb2RlLFxyXG4gICAgLy8gICAgICAgICAgICAgZGF0YToge1xyXG4gICAgLy8gICAgICAgICAgICAgICAgIHVwZGF0ZWRPcmRlclxyXG4gICAgLy8gICAgICAgICAgICAgfVxyXG4gICAgLy8gICAgICAgICB9KVxyXG4gICAgLy8gICAgIH0gY2F0Y2ggKGVycm9yKSB7XHJcblxyXG4gICAgLy8gICAgIH1cclxuXHJcblxyXG4gICAgLy8gICAgIC8vIHRoaXMub3JkZXJQb3J0SW5ib3VuZC5pbnNlcnQob3JkZXIpO1xyXG5cclxuICAgIC8vICAgICAvLyB0cnkge1xyXG4gICAgLy8gICAgIC8vICAgICBhd2FpdCB0aGlzLm9yZGVyUG9ydEluYm91bmQuc2VuZChvcmRlcik7XHJcbiAgICAvLyAgICAgLy8gICAgIHJlcy5zZW5kU3RhdHVzKDIwMSk7XHJcbiAgICAvLyAgICAgLy8gfSBjYXRjaCAoZXJyKSB7XHJcbiAgICAvLyAgICAgLy8gICAgIHJlcy5zdGF0dXMoNDAwKS5qc29uKHsgZXJyb3I6IGVyci5tZXNzYWdlIH0pO1xyXG4gICAgLy8gICAgIC8vIH1cclxuICAgIC8vIH1cclxuXHJcbiAgICAvLyBAaHR0cEdldChcIi9cIilcclxuICAgIC8vIHByaXZhdGUgbGlzdChAcXVlcnlQYXJhbShcInN0YXJ0XCIpIHN0YXJ0OiBudW1iZXIsIEBxdWVyeVBhcmFtKFwiY291bnRcIikgY291bnQ6IG51bWJlcik6IHN0cmluZyB7XHJcbiAgICAvLyAgICAgcmV0dXJuIHRoaXMuZm9vU2VydmljZS5nZXQoc3RhcnQsIGNvdW50KTtcclxuICAgIC8vIH1cclxuXHJcblxyXG5cclxuICAgIC8vIEBodHRwRGVsZXRlKFwiLzppZFwiKVxyXG4gICAgLy8gcHJpdmF0ZSBkZWxldGUoQHJlcXVlc3RQYXJhbShcImlkXCIpIGlkOiBzdHJpbmcsIEByZXNwb25zZSgpIHJlczogUmVzcG9uc2UpOiBQcm9taXNlPHZvaWQ+IHtcclxuICAgIC8vICAgICByZXR1cm4gdGhpcy5mb29TZXJ2aWNlLmRlbGV0ZShpZClcclxuICAgIC8vICAgICAgICAgLnRoZW4oKCkgPT4gcmVzLnNlbmRTdGF0dXMoMjA0KSlcclxuICAgIC8vICAgICAgICAgLmNhdGNoKChlcnI6IEVycm9yKSA9PiB7XHJcbiAgICAvLyAgICAgICAgICAgICByZXMuc3RhdHVzKDQwMCkuanNvbih7IGVycm9yOiBlcnIubWVzc2FnZSB9KTtcclxuICAgIC8vICAgICAgICAgfSk7XHJcbiAgICAvLyB9XHJcbn0iXX0=
