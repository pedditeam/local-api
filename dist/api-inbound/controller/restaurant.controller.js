"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const inversify_express_utils_1 = require("inversify-express-utils");
const inversify_1 = require("inversify");
const Restaurant_dto_1 = __importDefault(require("../dto/Restaurant.dto"));
const restaurant_adapter_1 = __importDefault(require("../adapter/restaurant.adapter"));
const types_1 = require("../../config/types");
let RestaurantController = class RestaurantController {
    constructor(
    //@inject("RestaurantPortInbound") 
    RestaurantPortInbound) {
        this.RestaurantPortInbound = RestaurantPortInbound;
    }
    save(req, res) {
        const restaurant = new Restaurant_dto_1.default(req.body);
        const restaurantModel = restaurant_adapter_1.default(restaurant);
        return this.RestaurantPortInbound.save(restaurantModel)
            .then(() => res.status(200).send())
            .catch(err => res.status(400).json({ "error": err.message }));
    }
    get(req, res) {
        return this.RestaurantPortInbound.get()
            .then(info => res.status(200).json(info))
            .catch(err => res.status(400).json({ "error": err.message }));
    }
};
__decorate([
    inversify_express_utils_1.httpPost("/"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], RestaurantController.prototype, "save", null);
__decorate([
    inversify_express_utils_1.httpGet("/"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], RestaurantController.prototype, "get", null);
RestaurantController = __decorate([
    inversify_express_utils_1.controller("/api/sync"),
    __param(0, inversify_1.inject(types_1.types.RestaurantPortInbound)),
    __metadata("design:paramtypes", [Object])
], RestaurantController);
exports.default = RestaurantController;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9hcGktaW5ib3VuZC9jb250cm9sbGVyL3Jlc3RhdXJhbnQuY29udHJvbGxlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUNBLHFFQUFzSjtBQUN0Six5Q0FBK0M7QUFDL0MsMkVBQWtEO0FBRWxELHVGQUE2RDtBQUM3RCw4Q0FBMEM7QUFHMUMsSUFBcUIsb0JBQW9CLEdBQXpDLE1BQXFCLG9CQUFvQjtJQUVyQztJQUNJLG1DQUFtQztJQUUzQixxQkFBNEM7UUFBNUMsMEJBQXFCLEdBQXJCLHFCQUFxQixDQUF1QjtJQUFJLENBQUM7SUFHckQsSUFBSSxDQUFDLEdBQVksRUFBRSxHQUFhO1FBQ3BDLE1BQU0sVUFBVSxHQUFHLElBQUksd0JBQWEsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDL0MsTUFBTSxlQUFlLEdBQUcsNEJBQWlCLENBQUMsVUFBVSxDQUFDLENBQUE7UUFDckQsT0FBTyxJQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQzthQUNsRCxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQzthQUNsQyxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFFLE9BQU8sRUFBRSxHQUFHLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQ3RFLENBQUM7SUFHTyxHQUFHLENBQUMsR0FBWSxFQUFFLEdBQWE7UUFDbkMsT0FBTyxJQUFJLENBQUMscUJBQXFCLENBQUMsR0FBRyxFQUFFO2FBQ2xDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ3hDLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUUsT0FBTyxFQUFFLEdBQUcsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLENBQUE7SUFDckUsQ0FBQztDQUNKLENBQUE7QUFkRztJQURDLGtDQUFRLENBQUMsR0FBRyxDQUFDOzs7O2dEQU9iO0FBR0Q7SUFEQyxpQ0FBTyxDQUFDLEdBQUcsQ0FBQzs7OzsrQ0FLWjtBQXJCZ0Isb0JBQW9CO0lBRHhDLG9DQUFVLENBQUMsV0FBVyxDQUFDO0lBS2YsV0FBQSxrQkFBTSxDQUFDLGFBQUssQ0FBQyxxQkFBcUIsQ0FBQyxDQUFBOztHQUp2QixvQkFBb0IsQ0FzQnhDO2tCQXRCb0Isb0JBQW9CIiwiZmlsZSI6ImFwaS1pbmJvdW5kL2NvbnRyb2xsZXIvcmVzdGF1cmFudC5jb250cm9sbGVyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtSZXF1ZXN0LCBSZXNwb25zZX0gZnJvbSBcImV4cHJlc3NcIjtcclxuaW1wb3J0IHsgaW50ZXJmYWNlcywgY29udHJvbGxlciwgaHR0cEdldCwgaHR0cFBvc3QsIGh0dHBEZWxldGUsIHJlcXVlc3QsIHF1ZXJ5UGFyYW0sIHJlc3BvbnNlLCByZXF1ZXN0UGFyYW0sIGh0dHBQdXQgfSBmcm9tIFwiaW52ZXJzaWZ5LWV4cHJlc3MtdXRpbHNcIjtcclxuaW1wb3J0IHsgaW5qZWN0YWJsZSwgaW5qZWN0IH0gZnJvbSBcImludmVyc2lmeVwiO1xyXG5pbXBvcnQgUmVzdGF1cmFudERUTyBmcm9tIFwiLi4vZHRvL1Jlc3RhdXJhbnQuZHRvXCI7XHJcbmltcG9ydCBSZXN0YXVyYW50UG9ydEluYm91bmQgZnJvbSBcIi4uLy4uL2FwaS1jb3JlL3BvcnQvaW5ib3VuZC9yZXN0YXVyYW50LmluYm91bmQtcG9ydFwiO1xyXG5pbXBvcnQgcmVzdGF1cmFudEFkYXB0ZXIgZnJvbSBcIi4uL2FkYXB0ZXIvcmVzdGF1cmFudC5hZGFwdGVyXCJcclxuaW1wb3J0IHsgdHlwZXMgfSBmcm9tICcuLi8uLi9jb25maWcvdHlwZXMnXHJcblxyXG5AY29udHJvbGxlcihcIi9hcGkvc3luY1wiKVxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBSZXN0YXVyYW50Q29udHJvbGxlciBpbXBsZW1lbnRzIGludGVyZmFjZXMuQ29udHJvbGxlciB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgLy9AaW5qZWN0KFwiUmVzdGF1cmFudFBvcnRJbmJvdW5kXCIpIFxyXG4gICAgICAgIEBpbmplY3QodHlwZXMuUmVzdGF1cmFudFBvcnRJbmJvdW5kKVxyXG4gICAgICAgIHByaXZhdGUgUmVzdGF1cmFudFBvcnRJbmJvdW5kOiBSZXN0YXVyYW50UG9ydEluYm91bmQpIHsgfVxyXG5cclxuICAgIEBodHRwUG9zdChcIi9cIilcclxuICAgIHByaXZhdGUgc2F2ZShyZXE6IFJlcXVlc3QsIHJlczogUmVzcG9uc2UpIHtcclxuICAgICAgICBjb25zdCByZXN0YXVyYW50ID0gbmV3IFJlc3RhdXJhbnREVE8ocmVxLmJvZHkpO1xyXG4gICAgICAgIGNvbnN0IHJlc3RhdXJhbnRNb2RlbCA9IHJlc3RhdXJhbnRBZGFwdGVyKHJlc3RhdXJhbnQpXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuUmVzdGF1cmFudFBvcnRJbmJvdW5kLnNhdmUocmVzdGF1cmFudE1vZGVsKVxyXG4gICAgICAgICAgICAudGhlbigoKSA9PiByZXMuc3RhdHVzKDIwMCkuc2VuZCgpKVxyXG4gICAgICAgICAgICAuY2F0Y2goZXJyID0+IHJlcy5zdGF0dXMoNDAwKS5qc29uKHsgXCJlcnJvclwiOiBlcnIubWVzc2FnZSB9KSk7XHJcbiAgICB9XHJcblxyXG4gICAgQGh0dHBHZXQoXCIvXCIpXHJcbiAgICBwcml2YXRlIGdldChyZXE6IFJlcXVlc3QsIHJlczogUmVzcG9uc2UpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5SZXN0YXVyYW50UG9ydEluYm91bmQuZ2V0KClcclxuICAgICAgICAgICAgLnRoZW4oaW5mbyA9PiByZXMuc3RhdHVzKDIwMCkuanNvbihpbmZvKSlcclxuICAgICAgICAgICAgLmNhdGNoKGVyciA9PiByZXMuc3RhdHVzKDQwMCkuanNvbih7IFwiZXJyb3JcIjogZXJyLm1lc3NhZ2UgfSkpXHJcbiAgICB9XHJcbn0iXX0=
