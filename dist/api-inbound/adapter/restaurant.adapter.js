"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/*
"The adapter handles incoming signals, such as an HTTP GET call,
and translates the incoming DTO into a model before invoking a Port."
https://dzone.com/articles/hexagonal-architecture-it-works > Inbound Gateaway

In our case, since we already have the controllers, the adapter will simply convert
the OrderDTO to order Model.
*/
const restaurant_model_1 = require("../../api-core/commons/model/restaurant.model");
const restaurant_model_2 = __importDefault(require("../../api-core/commons/model/restaurant.model"));
exports.default = (restaurantDTO) => {
    // iterate over RestaurantDTO object to create RestaurantModel object avoiding type constraints
    let categories = [];
    restaurantDTO.categories.forEach(category => {
        // conversion is different according to whether subcategories have been registered or not
        if (category.subcategories) {
            let subcategories = [];
            category.subcategories.forEach(subcategory => {
                let products = [];
                subcategory.products.forEach(product => {
                    let ingredients = null;
                    if (product.ingredients !== null) {
                        ingredients = [];
                        product.ingredients.forEach(ingredient => ingredients.push(new restaurant_model_1.IngredientModel(ingredient)));
                    }
                    products.push(new restaurant_model_1.ProductModel(Object.assign({}, product, { ingredients: ingredients })));
                });
                subcategories.push(new restaurant_model_1.SubCategoryModel(Object.assign({}, subcategory, { products: products })));
            });
            categories.push(new restaurant_model_1.CategoryModel(Object.assign({}, category, { subcategories: subcategories })));
        }
        else {
            let products = [];
            category.products.forEach(product => {
                let ingredients = null;
                if (product.ingredients !== null) {
                    ingredients = [];
                    product.ingredients.forEach(ingredient => ingredients.push(new restaurant_model_1.IngredientModel(ingredient)));
                }
                //console.log(new ProductModel(Object.assign({}, product, { ingredients: ingredients })))
                products.push(new restaurant_model_1.ProductModel(Object.assign({}, product, { ingredients: ingredients })));
            });
            categories.push(new restaurant_model_1.CategoryModel(Object.assign({}, category, { products: products })));
        }
    });
    //console.log('\ncategories: ', categories)
    // convert from manager and waiter DTOs to models
    let managers = [];
    restaurantDTO.managers.forEach(manager => managers.push(new restaurant_model_1.ManagerModel(manager)));
    let waiters = [];
    restaurantDTO.waiters.forEach(waiter => waiters.push(new restaurant_model_1.WaiterModel(waiter)));
    const addressModel = new restaurant_model_1.AddressModel(restaurantDTO.address);
    return new restaurant_model_2.default(Object.assign({}, restaurantDTO, {
        categories: categories,
        managers: managers,
        waiters: waiters,
        address: addressModel
    }));
};

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9hcGktaW5ib3VuZC9hZGFwdGVyL3Jlc3RhdXJhbnQuYWRhcHRlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBOzs7Ozs7O0VBT0U7QUFDRixvRkFBc0s7QUFFdEsscUdBQTJFO0FBRTNFLGtCQUFlLENBQUMsYUFBNEIsRUFBbUIsRUFBRTtJQUM3RCwrRkFBK0Y7SUFDL0YsSUFBSSxVQUFVLEdBQUcsRUFBRSxDQUFBO0lBQ25CLGFBQWEsQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxFQUFFO1FBQ3hDLHlGQUF5RjtRQUN6RixJQUFJLFFBQVEsQ0FBQyxhQUFhLEVBQUU7WUFDeEIsSUFBSSxhQUFhLEdBQUcsRUFBRSxDQUFBO1lBQ3RCLFFBQVEsQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxFQUFFO2dCQUN6QyxJQUFJLFFBQVEsR0FBRyxFQUFFLENBQUE7Z0JBQ2pCLFdBQVcsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxFQUFFO29CQUNuQyxJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUE7b0JBQ3RCLElBQUksT0FBTyxDQUFDLFdBQVcsS0FBSyxJQUFJLEVBQUU7d0JBQzlCLFdBQVcsR0FBRyxFQUFFLENBQUE7d0JBQ2hCLE9BQU8sQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLGtDQUFlLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFBO3FCQUMvRjtvQkFDRCxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksK0JBQVksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxPQUFPLEVBQUUsRUFBRSxXQUFXLEVBQUUsV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUE7Z0JBQzdGLENBQUMsQ0FBQyxDQUFBO2dCQUNGLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxtQ0FBZ0IsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxXQUFXLEVBQUUsRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUE7WUFDcEcsQ0FBQyxDQUFDLENBQUE7WUFDRixVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksZ0NBQWEsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxRQUFRLEVBQUUsRUFBRSxhQUFhLEVBQUUsYUFBYSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUE7U0FDcEc7YUFBTTtZQUNILElBQUksUUFBUSxHQUFHLEVBQUUsQ0FBQTtZQUNqQixRQUFRLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsRUFBRTtnQkFDaEMsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFBO2dCQUN0QixJQUFJLE9BQU8sQ0FBQyxXQUFXLEtBQUssSUFBSSxFQUFFO29CQUM5QixXQUFXLEdBQUcsRUFBRSxDQUFBO29CQUNoQixPQUFPLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxrQ0FBZSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQTtpQkFDL0Y7Z0JBQ0QseUZBQXlGO2dCQUN6RixRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksK0JBQVksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxPQUFPLEVBQUUsRUFBRSxXQUFXLEVBQUUsV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUE7WUFDN0YsQ0FBQyxDQUFDLENBQUE7WUFDRixVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksZ0NBQWEsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxRQUFRLEVBQUUsRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUE7U0FDMUY7SUFDTCxDQUFDLENBQUMsQ0FBQTtJQUNGLDJDQUEyQztJQUMzQyxpREFBaUQ7SUFDakQsSUFBSSxRQUFRLEdBQUcsRUFBRSxDQUFBO0lBQ2pCLGFBQWEsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLCtCQUFZLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFBO0lBQ25GLElBQUksT0FBTyxHQUFHLEVBQUUsQ0FBQTtJQUNoQixhQUFhLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSw4QkFBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQTtJQUM5RSxNQUFNLFlBQVksR0FBRyxJQUFJLCtCQUFZLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxDQUFBO0lBRTVELE9BQU8sSUFBSSwwQkFBZSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLGFBQWEsRUFBRTtRQUN4RCxVQUFVLEVBQUUsVUFBVTtRQUN0QixRQUFRLEVBQUUsUUFBUTtRQUNsQixPQUFPLEVBQUUsT0FBTztRQUNoQixPQUFPLEVBQUUsWUFBWTtLQUN4QixDQUFDLENBQUMsQ0FBQTtBQUNQLENBQUMsQ0FBQSIsImZpbGUiOiJhcGktaW5ib3VuZC9hZGFwdGVyL3Jlc3RhdXJhbnQuYWRhcHRlci5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIFxyXG5cIlRoZSBhZGFwdGVyIGhhbmRsZXMgaW5jb21pbmcgc2lnbmFscywgc3VjaCBhcyBhbiBIVFRQIEdFVCBjYWxsLCBcclxuYW5kIHRyYW5zbGF0ZXMgdGhlIGluY29taW5nIERUTyBpbnRvIGEgbW9kZWwgYmVmb3JlIGludm9raW5nIGEgUG9ydC5cIiBcclxuaHR0cHM6Ly9kem9uZS5jb20vYXJ0aWNsZXMvaGV4YWdvbmFsLWFyY2hpdGVjdHVyZS1pdC13b3JrcyA+IEluYm91bmQgR2F0ZWF3YXlcclxuXHJcbkluIG91ciBjYXNlLCBzaW5jZSB3ZSBhbHJlYWR5IGhhdmUgdGhlIGNvbnRyb2xsZXJzLCB0aGUgYWRhcHRlciB3aWxsIHNpbXBseSBjb252ZXJ0XHJcbnRoZSBPcmRlckRUTyB0byBvcmRlciBNb2RlbC5cclxuKi9cclxuaW1wb3J0IHtBZGRyZXNzTW9kZWwsIEluZ3JlZGllbnRNb2RlbCwgUHJvZHVjdE1vZGVsLCBTdWJDYXRlZ29yeU1vZGVsLCBDYXRlZ29yeU1vZGVsLCBNYW5hZ2VyTW9kZWwsIFdhaXRlck1vZGVsIH0gZnJvbSBcIi4uLy4uL2FwaS1jb3JlL2NvbW1vbnMvbW9kZWwvcmVzdGF1cmFudC5tb2RlbFwiXHJcbmltcG9ydCBSZXN0YXVyYW50RFRPIGZyb20gXCIuLi8uLi9hcGktaW5ib3VuZC9kdG8vcmVzdGF1cmFudC5kdG9cIlxyXG5pbXBvcnQgUmVzdGF1cmFudE1vZGVsIGZyb20gJy4uLy4uL2FwaS1jb3JlL2NvbW1vbnMvbW9kZWwvcmVzdGF1cmFudC5tb2RlbCdcclxuXHJcbmV4cG9ydCBkZWZhdWx0IChyZXN0YXVyYW50RFRPOiBSZXN0YXVyYW50RFRPKTogUmVzdGF1cmFudE1vZGVsID0+IHtcclxuICAgIC8vIGl0ZXJhdGUgb3ZlciBSZXN0YXVyYW50RFRPIG9iamVjdCB0byBjcmVhdGUgUmVzdGF1cmFudE1vZGVsIG9iamVjdCBhdm9pZGluZyB0eXBlIGNvbnN0cmFpbnRzXHJcbiAgICBsZXQgY2F0ZWdvcmllcyA9IFtdXHJcbiAgICByZXN0YXVyYW50RFRPLmNhdGVnb3JpZXMuZm9yRWFjaChjYXRlZ29yeSA9PiB7XHJcbiAgICAgICAgLy8gY29udmVyc2lvbiBpcyBkaWZmZXJlbnQgYWNjb3JkaW5nIHRvIHdoZXRoZXIgc3ViY2F0ZWdvcmllcyBoYXZlIGJlZW4gcmVnaXN0ZXJlZCBvciBub3RcclxuICAgICAgICBpZiAoY2F0ZWdvcnkuc3ViY2F0ZWdvcmllcykge1xyXG4gICAgICAgICAgICBsZXQgc3ViY2F0ZWdvcmllcyA9IFtdXHJcbiAgICAgICAgICAgIGNhdGVnb3J5LnN1YmNhdGVnb3JpZXMuZm9yRWFjaChzdWJjYXRlZ29yeSA9PiB7XHJcbiAgICAgICAgICAgICAgICBsZXQgcHJvZHVjdHMgPSBbXVxyXG4gICAgICAgICAgICAgICAgc3ViY2F0ZWdvcnkucHJvZHVjdHMuZm9yRWFjaChwcm9kdWN0ID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgaW5ncmVkaWVudHMgPSBudWxsXHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHByb2R1Y3QuaW5ncmVkaWVudHMgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaW5ncmVkaWVudHMgPSBbXVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwcm9kdWN0LmluZ3JlZGllbnRzLmZvckVhY2goaW5ncmVkaWVudCA9PiBpbmdyZWRpZW50cy5wdXNoKG5ldyBJbmdyZWRpZW50TW9kZWwoaW5ncmVkaWVudCkpKVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBwcm9kdWN0cy5wdXNoKG5ldyBQcm9kdWN0TW9kZWwoT2JqZWN0LmFzc2lnbih7fSwgcHJvZHVjdCwgeyBpbmdyZWRpZW50czogaW5ncmVkaWVudHMgfSkpKVxyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgIHN1YmNhdGVnb3JpZXMucHVzaChuZXcgU3ViQ2F0ZWdvcnlNb2RlbChPYmplY3QuYXNzaWduKHt9LCBzdWJjYXRlZ29yeSwgeyBwcm9kdWN0czogcHJvZHVjdHMgfSkpKVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICBjYXRlZ29yaWVzLnB1c2gobmV3IENhdGVnb3J5TW9kZWwoT2JqZWN0LmFzc2lnbih7fSwgY2F0ZWdvcnksIHsgc3ViY2F0ZWdvcmllczogc3ViY2F0ZWdvcmllcyB9KSkpXHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgbGV0IHByb2R1Y3RzID0gW11cclxuICAgICAgICAgICAgY2F0ZWdvcnkucHJvZHVjdHMuZm9yRWFjaChwcm9kdWN0ID0+IHtcclxuICAgICAgICAgICAgICAgIGxldCBpbmdyZWRpZW50cyA9IG51bGxcclxuICAgICAgICAgICAgICAgIGlmIChwcm9kdWN0LmluZ3JlZGllbnRzICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaW5ncmVkaWVudHMgPSBbXVxyXG4gICAgICAgICAgICAgICAgICAgIHByb2R1Y3QuaW5ncmVkaWVudHMuZm9yRWFjaChpbmdyZWRpZW50ID0+IGluZ3JlZGllbnRzLnB1c2gobmV3IEluZ3JlZGllbnRNb2RlbChpbmdyZWRpZW50KSkpXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAvL2NvbnNvbGUubG9nKG5ldyBQcm9kdWN0TW9kZWwoT2JqZWN0LmFzc2lnbih7fSwgcHJvZHVjdCwgeyBpbmdyZWRpZW50czogaW5ncmVkaWVudHMgfSkpKVxyXG4gICAgICAgICAgICAgICAgcHJvZHVjdHMucHVzaChuZXcgUHJvZHVjdE1vZGVsKE9iamVjdC5hc3NpZ24oe30sIHByb2R1Y3QsIHsgaW5ncmVkaWVudHM6IGluZ3JlZGllbnRzIH0pKSlcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgY2F0ZWdvcmllcy5wdXNoKG5ldyBDYXRlZ29yeU1vZGVsKE9iamVjdC5hc3NpZ24oe30sIGNhdGVnb3J5LCB7IHByb2R1Y3RzOiBwcm9kdWN0cyB9KSkpXHJcbiAgICAgICAgfSAgICAgICBcclxuICAgIH0pXHJcbiAgICAvL2NvbnNvbGUubG9nKCdcXG5jYXRlZ29yaWVzOiAnLCBjYXRlZ29yaWVzKVxyXG4gICAgLy8gY29udmVydCBmcm9tIG1hbmFnZXIgYW5kIHdhaXRlciBEVE9zIHRvIG1vZGVsc1xyXG4gICAgbGV0IG1hbmFnZXJzID0gW11cclxuICAgIHJlc3RhdXJhbnREVE8ubWFuYWdlcnMuZm9yRWFjaChtYW5hZ2VyID0+IG1hbmFnZXJzLnB1c2gobmV3IE1hbmFnZXJNb2RlbChtYW5hZ2VyKSkpXHJcbiAgICBsZXQgd2FpdGVycyA9IFtdXHJcbiAgICByZXN0YXVyYW50RFRPLndhaXRlcnMuZm9yRWFjaCh3YWl0ZXIgPT4gd2FpdGVycy5wdXNoKG5ldyBXYWl0ZXJNb2RlbCh3YWl0ZXIpKSlcclxuICAgIGNvbnN0IGFkZHJlc3NNb2RlbCA9IG5ldyBBZGRyZXNzTW9kZWwocmVzdGF1cmFudERUTy5hZGRyZXNzKVxyXG5cclxuICAgIHJldHVybiBuZXcgUmVzdGF1cmFudE1vZGVsKE9iamVjdC5hc3NpZ24oe30sIHJlc3RhdXJhbnREVE8sIHsgXHJcbiAgICAgICAgY2F0ZWdvcmllczogY2F0ZWdvcmllcyxcclxuICAgICAgICBtYW5hZ2VyczogbWFuYWdlcnMsXHJcbiAgICAgICAgd2FpdGVyczogd2FpdGVycyxcclxuICAgICAgICBhZGRyZXNzOiBhZGRyZXNzTW9kZWxcclxuICAgIH0pKVxyXG59Il19
