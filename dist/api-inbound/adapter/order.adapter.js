"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const order_model_1 = require("../../api-core/commons/model/order.model");
const order_model_2 = __importDefault(require("../../api-core/commons/model/order.model"));
exports.orderAdapter = (orderDTO) => {
    // iterate over OrderDTO object to create OrderModel object avoiding type constraints
    let consumers = [];
    orderDTO.consumers.forEach(consumer => {
        let items = [];
        consumer.items.forEach(item => {
            let ingredients = null;
            if (item.ingredients !== null) {
                ingredients = [];
                item.ingredients.forEach(ingredient => ingredients.push(new order_model_1.IngredientModel(ingredient)));
            }
            items.push(new order_model_1.ItemModel(Object.assign({}, item, { ingredients: ingredients })));
        });
        consumers.push(new order_model_1.ConsumerModel(Object.assign({}, consumer, { items: items })));
    });
    return new order_model_2.default(Object.assign({}, orderDTO, { consumers: consumers }));
};
exports.updateAdapter = (updateDTO) => {
    // iterate over OrderDTO object to create OrderModel object avoiding type constraints
    let consumers = [];
    updateDTO.consumers.forEach(consumer => {
        let items = [];
        consumer.items.forEach(item => {
            let ingredients = null;
            if (item.ingredients !== null) {
                ingredients = [];
                item.ingredients.forEach(ingredient => ingredients.push(new order_model_1.IngredientModel(ingredient)));
            }
            items.push(new order_model_1.ItemModel(Object.assign({}, item, { ingredients: ingredients })));
        });
        consumers.push(new order_model_1.ConsumerModel(Object.assign({}, consumer, { items: items })));
    });
    return new order_model_1.UpdateModel(Object.assign({}, updateDTO, { consumers: consumers }));
};

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9hcGktaW5ib3VuZC9hZGFwdGVyL29yZGVyLmFkYXB0ZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFTQSwwRUFBK0c7QUFDL0csMkZBQWlFO0FBRXBELFFBQUEsWUFBWSxHQUFHLENBQUMsUUFBa0IsRUFBYyxFQUFFO0lBQzNELHFGQUFxRjtJQUNyRixJQUFJLFNBQVMsR0FBRyxFQUFFLENBQUE7SUFDbEIsUUFBUSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEVBQUU7UUFDbEMsSUFBSSxLQUFLLEdBQUcsRUFBRSxDQUFBO1FBQ2QsUUFBUSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDMUIsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFBO1lBQ3RCLElBQUksSUFBSSxDQUFDLFdBQVcsS0FBSyxJQUFJLEVBQUU7Z0JBQzNCLFdBQVcsR0FBRyxFQUFFLENBQUE7Z0JBQ2hCLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLDZCQUFlLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFBO2FBQzVGO1lBQ0QsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLHVCQUFTLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsSUFBSSxFQUFFLEVBQUUsV0FBVyxFQUFFLFdBQVcsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFBO1FBQ3BGLENBQUMsQ0FBQyxDQUFBO1FBQ0YsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLDJCQUFhLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsUUFBUSxFQUFFLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFBO0lBQ3BGLENBQUMsQ0FBQyxDQUFBO0lBQ0YsT0FBTyxJQUFJLHFCQUFVLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsUUFBUSxFQUFFLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQTtBQUNoRixDQUFDLENBQUE7QUFFWSxRQUFBLGFBQWEsR0FBRyxDQUFDLFNBQW9CLEVBQWUsRUFBRTtJQUMvRCxxRkFBcUY7SUFDckYsSUFBSSxTQUFTLEdBQUcsRUFBRSxDQUFBO0lBQ2xCLFNBQVMsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxFQUFFO1FBQ25DLElBQUksS0FBSyxHQUFHLEVBQUUsQ0FBQTtRQUNkLFFBQVEsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQzFCLElBQUksV0FBVyxHQUFHLElBQUksQ0FBQTtZQUN0QixJQUFJLElBQUksQ0FBQyxXQUFXLEtBQUssSUFBSSxFQUFFO2dCQUMzQixXQUFXLEdBQUcsRUFBRSxDQUFBO2dCQUNoQixJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSw2QkFBZSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQTthQUM1RjtZQUNELEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSx1QkFBUyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLElBQUksRUFBRSxFQUFFLFdBQVcsRUFBRSxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQTtRQUNwRixDQUFDLENBQUMsQ0FBQTtRQUNGLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSwyQkFBYSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLFFBQVEsRUFBRSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQTtJQUNwRixDQUFDLENBQUMsQ0FBQTtJQUNGLE9BQU8sSUFBSSx5QkFBVyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLFNBQVMsRUFBRSxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsQ0FBQyxDQUFDLENBQUE7QUFDbEYsQ0FBQyxDQUFBIiwiZmlsZSI6ImFwaS1pbmJvdW5kL2FkYXB0ZXIvb3JkZXIuYWRhcHRlci5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIFxyXG5cIlRoZSBhZGFwdGVyIGhhbmRsZXMgaW5jb21pbmcgc2lnbmFscywgc3VjaCBhcyBhbiBIVFRQIEdFVCBjYWxsLCBcclxuYW5kIHRyYW5zbGF0ZXMgdGhlIGluY29taW5nIERUTyBpbnRvIGEgbW9kZWwgYmVmb3JlIGludm9raW5nIGEgUG9ydC5cIiBcclxuaHR0cHM6Ly9kem9uZS5jb20vYXJ0aWNsZXMvaGV4YWdvbmFsLWFyY2hpdGVjdHVyZS1pdC13b3JrcyA+IEluYm91bmQgR2F0ZWF3YXlcclxuXHJcbkluIG91ciBjYXNlLCBzaW5jZSB3ZSBhbHJlYWR5IGhhdmUgdGhlIGNvbnRyb2xsZXJzLCB0aGUgYWRhcHRlciB3aWxsIHNpbXBseSBjb252ZXJ0XHJcbnRoZSBPcmRlckRUTyB0byBvcmRlciBNb2RlbC5cclxuKi9cclxuaW1wb3J0IHtPcmRlckRUTywgVXBkYXRlRFRPfSBmcm9tICcuLi9kdG8vb3JkZXIuZHRvJ1xyXG5pbXBvcnQge0luZ3JlZGllbnRNb2RlbCwgSXRlbU1vZGVsLCBDb25zdW1lck1vZGVsLCBVcGRhdGVNb2RlbH0gZnJvbSAnLi4vLi4vYXBpLWNvcmUvY29tbW9ucy9tb2RlbC9vcmRlci5tb2RlbCdcclxuaW1wb3J0IE9yZGVyTW9kZWwgZnJvbSAnLi4vLi4vYXBpLWNvcmUvY29tbW9ucy9tb2RlbC9vcmRlci5tb2RlbCdcclxuXHJcbmV4cG9ydCBjb25zdCBvcmRlckFkYXB0ZXIgPSAob3JkZXJEVE86IE9yZGVyRFRPKTogT3JkZXJNb2RlbCA9PiB7XHJcbiAgICAvLyBpdGVyYXRlIG92ZXIgT3JkZXJEVE8gb2JqZWN0IHRvIGNyZWF0ZSBPcmRlck1vZGVsIG9iamVjdCBhdm9pZGluZyB0eXBlIGNvbnN0cmFpbnRzXHJcbiAgICBsZXQgY29uc3VtZXJzID0gW11cclxuICAgIG9yZGVyRFRPLmNvbnN1bWVycy5mb3JFYWNoKGNvbnN1bWVyID0+IHtcclxuICAgICAgICBsZXQgaXRlbXMgPSBbXVxyXG4gICAgICAgIGNvbnN1bWVyLml0ZW1zLmZvckVhY2goaXRlbSA9PiB7XHJcbiAgICAgICAgICAgIGxldCBpbmdyZWRpZW50cyA9IG51bGxcclxuICAgICAgICAgICAgaWYgKGl0ZW0uaW5ncmVkaWVudHMgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgIGluZ3JlZGllbnRzID0gW11cclxuICAgICAgICAgICAgICAgIGl0ZW0uaW5ncmVkaWVudHMuZm9yRWFjaChpbmdyZWRpZW50ID0+IGluZ3JlZGllbnRzLnB1c2gobmV3IEluZ3JlZGllbnRNb2RlbChpbmdyZWRpZW50KSkpXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaXRlbXMucHVzaChuZXcgSXRlbU1vZGVsKE9iamVjdC5hc3NpZ24oe30sIGl0ZW0sIHsgaW5ncmVkaWVudHM6IGluZ3JlZGllbnRzIH0pKSlcclxuICAgICAgICB9KVxyXG4gICAgICAgIGNvbnN1bWVycy5wdXNoKG5ldyBDb25zdW1lck1vZGVsKE9iamVjdC5hc3NpZ24oe30sIGNvbnN1bWVyLCB7IGl0ZW1zOiBpdGVtcyB9KSkpXHJcbiAgICB9KVxyXG4gICAgcmV0dXJuIG5ldyBPcmRlck1vZGVsKE9iamVjdC5hc3NpZ24oe30sIG9yZGVyRFRPLCB7IGNvbnN1bWVyczogY29uc3VtZXJzIH0pKVxyXG59XHJcblxyXG5leHBvcnQgY29uc3QgdXBkYXRlQWRhcHRlciA9ICh1cGRhdGVEVE86IFVwZGF0ZURUTyk6IFVwZGF0ZU1vZGVsID0+IHtcclxuICAgIC8vIGl0ZXJhdGUgb3ZlciBPcmRlckRUTyBvYmplY3QgdG8gY3JlYXRlIE9yZGVyTW9kZWwgb2JqZWN0IGF2b2lkaW5nIHR5cGUgY29uc3RyYWludHNcclxuICAgIGxldCBjb25zdW1lcnMgPSBbXVxyXG4gICAgdXBkYXRlRFRPLmNvbnN1bWVycy5mb3JFYWNoKGNvbnN1bWVyID0+IHtcclxuICAgICAgICBsZXQgaXRlbXMgPSBbXVxyXG4gICAgICAgIGNvbnN1bWVyLml0ZW1zLmZvckVhY2goaXRlbSA9PiB7XHJcbiAgICAgICAgICAgIGxldCBpbmdyZWRpZW50cyA9IG51bGxcclxuICAgICAgICAgICAgaWYgKGl0ZW0uaW5ncmVkaWVudHMgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgIGluZ3JlZGllbnRzID0gW11cclxuICAgICAgICAgICAgICAgIGl0ZW0uaW5ncmVkaWVudHMuZm9yRWFjaChpbmdyZWRpZW50ID0+IGluZ3JlZGllbnRzLnB1c2gobmV3IEluZ3JlZGllbnRNb2RlbChpbmdyZWRpZW50KSkpXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaXRlbXMucHVzaChuZXcgSXRlbU1vZGVsKE9iamVjdC5hc3NpZ24oe30sIGl0ZW0sIHsgaW5ncmVkaWVudHM6IGluZ3JlZGllbnRzIH0pKSlcclxuICAgICAgICB9KVxyXG4gICAgICAgIGNvbnN1bWVycy5wdXNoKG5ldyBDb25zdW1lck1vZGVsKE9iamVjdC5hc3NpZ24oe30sIGNvbnN1bWVyLCB7IGl0ZW1zOiBpdGVtcyB9KSkpXHJcbiAgICB9KVxyXG4gICAgcmV0dXJuIG5ldyBVcGRhdGVNb2RlbChPYmplY3QuYXNzaWduKHt9LCB1cGRhdGVEVE8sIHsgY29uc3VtZXJzOiBjb25zdW1lcnMgfSkpXHJcbn0iXX0=
