"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const inversify_1 = require("inversify");
const types_1 = require("../../config/types");
let RestaurantHandler = class RestaurantHandler {
    constructor(restaurantPortOutbound) {
        this.restaurantPortOutbound = restaurantPortOutbound;
        this.save = (restaurant) => __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.restaurantPortOutbound.delete();
                yield this.restaurantPortOutbound.save(restaurant);
            }
            catch (err) {
                throw err;
            }
        });
    }
    get() {
        try {
            return this.restaurantPortOutbound.get();
        }
        catch (err) {
            throw err;
        }
    }
};
RestaurantHandler = __decorate([
    inversify_1.injectable(),
    __param(0, inversify_1.inject(types_1.types.RestaurantPortOutbound)),
    __metadata("design:paramtypes", [Object])
], RestaurantHandler);
exports.default = RestaurantHandler;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9hcGktY29yZS9oYW5kbGVyL3Jlc3RhdXJhbnQuaGFuZGxlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEseUNBQStDO0FBQy9DLDhDQUEwQztBQU0xQyxJQUFxQixpQkFBaUIsR0FBdEMsTUFBcUIsaUJBQWlCO0lBR2xDLFlBQ2tELHNCQUE4QztRQUE5QywyQkFBc0IsR0FBdEIsc0JBQXNCLENBQXdCO1FBR2hHLFNBQUksR0FBRyxDQUFPLFVBQTJCLEVBQWlCLEVBQUU7WUFDeEQsSUFBSTtnQkFDQSxNQUFNLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxNQUFNLEVBQUUsQ0FBQTtnQkFDMUMsTUFBTSxJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFBO2FBQ3JEO1lBQUMsT0FBTSxHQUFHLEVBQUU7Z0JBQ1QsTUFBTSxHQUFHLENBQUE7YUFDWjtRQUNMLENBQUMsQ0FBQSxDQUFBO0lBVEcsQ0FBQztJQVdMLEdBQUc7UUFDQyxJQUFJO1lBQ0EsT0FBTyxJQUFJLENBQUMsc0JBQXNCLENBQUMsR0FBRyxFQUFFLENBQUE7U0FDM0M7UUFBQyxPQUFPLEdBQUcsRUFBRTtZQUNWLE1BQU0sR0FBRyxDQUFBO1NBQ1o7SUFDTCxDQUFDO0NBR0osQ0FBQTtBQXpCb0IsaUJBQWlCO0lBRHJDLHNCQUFVLEVBQUU7SUFLSixXQUFBLGtCQUFNLENBQUMsYUFBSyxDQUFDLHNCQUFzQixDQUFDLENBQUE7O0dBSnhCLGlCQUFpQixDQXlCckM7a0JBekJvQixpQkFBaUIiLCJmaWxlIjoiYXBpLWNvcmUvaGFuZGxlci9yZXN0YXVyYW50LmhhbmRsZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBpbmplY3RhYmxlLCBpbmplY3QgfSBmcm9tIFwiaW52ZXJzaWZ5XCI7XHJcbmltcG9ydCB7IHR5cGVzIH0gZnJvbSAnLi4vLi4vY29uZmlnL3R5cGVzJ1xyXG5pbXBvcnQgUmVzdGF1cmFudFBvcnRJbmJvdW5kIGZyb20gXCIuLi9wb3J0L2luYm91bmQvcmVzdGF1cmFudC5pbmJvdW5kLXBvcnRcIjtcclxuaW1wb3J0IFJlc3RhdXJhbnRQb3J0T3V0Ym91bmQgZnJvbSBcIi4uL3BvcnQvb3V0Ym91bmQvcmVzdGF1cmFudC5vdXRib3VuZC1wb3J0XCJcclxuaW1wb3J0IFJlc3RhdXJhbnRNb2RlbCBmcm9tIFwiLi4vY29tbW9ucy9tb2RlbC9yZXN0YXVyYW50Lm1vZGVsXCI7XHJcblxyXG5AaW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFJlc3RhdXJhbnRIYW5kbGVyIGltcGxlbWVudHMgUmVzdGF1cmFudFBvcnRJbmJvdW5kIHtcclxuXHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgQGluamVjdCh0eXBlcy5SZXN0YXVyYW50UG9ydE91dGJvdW5kKSBwcml2YXRlIHJlc3RhdXJhbnRQb3J0T3V0Ym91bmQ6IFJlc3RhdXJhbnRQb3J0T3V0Ym91bmRcclxuICAgICkgeyB9XHJcblxyXG4gICAgc2F2ZSA9IGFzeW5jIChyZXN0YXVyYW50OiBSZXN0YXVyYW50TW9kZWwpOiBQcm9taXNlPHZvaWQ+ID0+IHtcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICBhd2FpdCB0aGlzLnJlc3RhdXJhbnRQb3J0T3V0Ym91bmQuZGVsZXRlKClcclxuICAgICAgICAgICAgYXdhaXQgdGhpcy5yZXN0YXVyYW50UG9ydE91dGJvdW5kLnNhdmUocmVzdGF1cmFudClcclxuICAgICAgICB9IGNhdGNoKGVycikge1xyXG4gICAgICAgICAgICB0aHJvdyBlcnJcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0KCk6IFByb21pc2U8T2JqZWN0PiB7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMucmVzdGF1cmFudFBvcnRPdXRib3VuZC5nZXQoKVxyXG4gICAgICAgIH0gY2F0Y2ggKGVycikge1xyXG4gICAgICAgICAgICB0aHJvdyBlcnJcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG5cclxufSJdfQ==
