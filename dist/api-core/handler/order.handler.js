"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const inversify_1 = require("inversify");
const types_1 = require("../../config/types");
// import ResponseApi from "../commons/envelopes/response.model";
let OrderHandler = class OrderHandler {
    constructor(orderPortOutbound, managementSystemIntegrationPort, peddiCloudIntegrationPort) {
        this.orderPortOutbound = orderPortOutbound;
        this.managementSystemIntegrationPort = managementSystemIntegrationPort;
        this.peddiCloudIntegrationPort = peddiCloudIntegrationPort;
        this.save = (order) => __awaiter(this, void 0, void 0, function* () {
            let response = {
                id: undefined,
                msg: ''
            };
            try {
                yield this.managementSystemIntegrationPort.openTable(order.table);
                response.id = yield this.orderPortOutbound.save(order);
                return response;
            }
            catch (err) {
                response.msg = err.message;
                console.log(err);
                throw response;
            }
        });
        this.update = (id, update) => __awaiter(this, void 0, void 0, function* () {
            try {
                const order = yield this.getById(id);
                const fails = yield this.managementSystemIntegrationPort.addProductsToTable(update, order.table);
                yield this.orderPortOutbound.updateDocument(order, update, fails);
                return fails;
            }
            catch (err) {
                throw err;
            }
        });
        this.close = (id) => __awaiter(this, void 0, void 0, function* () {
            try {
                const order = yield this.getById(id);
                yield this.managementSystemIntegrationPort.closeTable(order.table);
                const token = yield this.peddiCloudIntegrationPort.authenticate();
                yield this.peddiCloudIntegrationPort.sendOrder(order, token)
                    .then(() => this.orderPortOutbound.delete(order._id))
                    .catch(err => { throw err; });
            }
            catch (err) {
                throw err;
            }
        });
    }
    getAll() {
        try {
            return this.orderPortOutbound.getAll();
        }
        catch (err) {
            throw err;
        }
    }
    getById(id) {
        try {
            return this.orderPortOutbound.getById(id);
        }
        catch (err) {
            throw err;
        }
    }
    send(id) {
        console.log("Chegou na camada core");
        // // order : OrderDTO;
        // let order = new OrderModel(this.orderPortOutbound.getById(id));
        // let restaurantToken = "";
        // //get restaurant infos to make the authenticate of peddi cloud api
        // // restaurantToken = this.restaurantPortOutbound.getById(order.restaurant_id_cloud).hash;
        // //tranfer order and restaurant infos from controller to the outside of the application
        // this.orderPortOutbound.send(order, order.restaurant_id_cloud, restaurantToken);
        // // return new Number();
        // // this.orderPortOutbound.save(order);
        // // throw new Error("Method not implemented.");
    }
};
OrderHandler = __decorate([
    inversify_1.injectable(),
    __param(0, inversify_1.inject(types_1.types.OrderPortOutbound)),
    __param(1, inversify_1.inject(types_1.types.ManagementSystemIntegrationPort)),
    __param(2, inversify_1.inject(types_1.types.PeddiCloudIntegrationPort)),
    __metadata("design:paramtypes", [Object, Object, Object])
], OrderHandler);
exports.default = OrderHandler;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9hcGktY29yZS9oYW5kbGVyL29yZGVyLmhhbmRsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLHlDQUErQztBQUMvQyw4Q0FBd0M7QUFPeEMsaUVBQWlFO0FBR2pFLElBQXFCLFlBQVksR0FBakMsTUFBcUIsWUFBWTtJQUU3QixZQUVZLGlCQUFvQyxFQUVwQywrQkFBZ0UsRUFFaEUseUJBQW9EO1FBSnBELHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBbUI7UUFFcEMsb0NBQStCLEdBQS9CLCtCQUErQixDQUFpQztRQUVoRSw4QkFBeUIsR0FBekIseUJBQXlCLENBQTJCO1FBbUJoRSxTQUFJLEdBQUcsQ0FBTyxLQUFpQixFQUFnQixFQUFFO1lBQzdDLElBQUksUUFBUSxHQUFHO2dCQUNYLEVBQUUsRUFBRSxTQUFTO2dCQUNiLEdBQUcsRUFBRSxFQUFFO2FBQ1YsQ0FBQTtZQUNELElBQUk7Z0JBQ0EsTUFBTSxJQUFJLENBQUMsK0JBQStCLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQTtnQkFDakUsUUFBUSxDQUFDLEVBQUUsR0FBRyxNQUFNLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3ZELE9BQU8sUUFBUSxDQUFBO2FBQ2xCO1lBQUMsT0FBTSxHQUFHLEVBQUU7Z0JBQ1YsUUFBUSxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUMsT0FBTyxDQUFDO2dCQUMzQixPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFBO2dCQUNoQixNQUFNLFFBQVEsQ0FBQTthQUNoQjtRQUNMLENBQUMsQ0FBQSxDQUFBO1FBRUQsV0FBTSxHQUFHLENBQU8sRUFBVSxFQUFFLE1BQW1CLEVBQXVCLEVBQUU7WUFDcEUsSUFBSTtnQkFDQSxNQUFNLEtBQUssR0FBRyxNQUFNLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUE7Z0JBQ3BDLE1BQU0sS0FBSyxHQUFHLE1BQU0sSUFBSSxDQUFDLCtCQUErQixDQUFDLGtCQUFrQixDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUE7Z0JBQ2hHLE1BQU0sSUFBSSxDQUFDLGlCQUFpQixDQUFDLGNBQWMsQ0FBQyxLQUFLLEVBQUUsTUFBTSxFQUFFLEtBQUssQ0FBQyxDQUFDO2dCQUNsRSxPQUFPLEtBQUssQ0FBQTthQUNmO1lBQUMsT0FBTSxHQUFHLEVBQUU7Z0JBQ1QsTUFBTSxHQUFHLENBQUE7YUFDWjtRQUNMLENBQUMsQ0FBQSxDQUFBO1FBRUQsVUFBSyxHQUFHLENBQU8sRUFBVSxFQUFpQixFQUFFO1lBQ3hDLElBQUk7Z0JBQ0EsTUFBTSxLQUFLLEdBQUcsTUFBTSxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFBO2dCQUNwQyxNQUFNLElBQUksQ0FBQywrQkFBK0IsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFBO2dCQUNsRSxNQUFNLEtBQUssR0FBRyxNQUFNLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxZQUFZLEVBQUUsQ0FBQTtnQkFDakUsTUFBTSxJQUFJLENBQUMseUJBQXlCLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUM7cUJBQ3ZELElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztxQkFDcEQsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFLEdBQUUsTUFBTSxHQUFHLENBQUEsQ0FBQSxDQUFDLENBQUMsQ0FBQTthQUNqQztZQUFDLE9BQU0sR0FBRyxFQUFFO2dCQUNULE1BQU0sR0FBRyxDQUFBO2FBQ1o7UUFFTCxDQUFDLENBQUEsQ0FBQTtJQXpETyxDQUFDO0lBRVQsTUFBTTtRQUNGLElBQUk7WUFDQSxPQUFPLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLEVBQUUsQ0FBQztTQUMxQztRQUFDLE9BQU8sR0FBRyxFQUFFO1lBQ1YsTUFBTSxHQUFHLENBQUE7U0FDWjtJQUNMLENBQUM7SUFFRCxPQUFPLENBQUMsRUFBVTtRQUNkLElBQUk7WUFDQSxPQUFPLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUM7U0FDN0M7UUFBQyxPQUFPLEdBQUcsRUFBRTtZQUNWLE1BQU0sR0FBRyxDQUFBO1NBQ1o7SUFDTCxDQUFDO0lBMkNELElBQUksQ0FBQyxFQUFVO1FBQ1gsT0FBTyxDQUFDLEdBQUcsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDO1FBRXJDLHVCQUF1QjtRQUN2QixrRUFBa0U7UUFFbEUsNEJBQTRCO1FBQzVCLHFFQUFxRTtRQUNyRSw0RkFBNEY7UUFFNUYseUZBQXlGO1FBQ3pGLGtGQUFrRjtRQUVsRiwwQkFBMEI7UUFDMUIseUNBQXlDO1FBQ3pDLGlEQUFpRDtJQUNyRCxDQUFDO0NBRUosQ0FBQTtBQXRGb0IsWUFBWTtJQURoQyxzQkFBVSxFQUFFO0lBSUosV0FBQSxrQkFBTSxDQUFDLGFBQUssQ0FBQyxpQkFBaUIsQ0FBQyxDQUFBO0lBRS9CLFdBQUEsa0JBQU0sQ0FBQyxhQUFLLENBQUMsK0JBQStCLENBQUMsQ0FBQTtJQUU3QyxXQUFBLGtCQUFNLENBQUMsYUFBSyxDQUFDLHlCQUF5QixDQUFDLENBQUE7O0dBUDNCLFlBQVksQ0FzRmhDO2tCQXRGb0IsWUFBWSIsImZpbGUiOiJhcGktY29yZS9oYW5kbGVyL29yZGVyLmhhbmRsZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBpbmplY3RhYmxlLCBpbmplY3QgfSBmcm9tIFwiaW52ZXJzaWZ5XCI7XHJcbmltcG9ydCB7dHlwZXN9IGZyb20gJy4uLy4uL2NvbmZpZy90eXBlcydcclxuaW1wb3J0IE9yZGVyUG9ydEluYm91bmQgZnJvbSBcIi4uL3BvcnQvaW5ib3VuZC9vcmRlci5pbmJvdW5kLXBvcnRcIjtcclxuaW1wb3J0IE9yZGVyUG9ydE91dGJvdW5kIGZyb20gXCIuLi9wb3J0L291dGJvdW5kL29yZGVyLm91dGJvdW5kLXBvcnRcIlxyXG5pbXBvcnQgTWFuYWdlbWVudFN5c3RlbUludGVncmF0aW9uUG9ydCBmcm9tIFwiLi4vLi4vYXBpLW91dGJvdW5kL2ludGVncmF0aW9uL21hbmFnZW1lbnQtc3lzdGVtcy9tZ210LXN5c3QuaW50ZWdyYXRpb24tcG9ydFwiXHJcbmltcG9ydCBQZWRkaUNsb3VkSW50ZWdyYXRpb25Qb3J0IGZyb20gXCIuLi8uLi9hcGktb3V0Ym91bmQvaW50ZWdyYXRpb24vZXh0ZXJuYWwtcGVkZGktYXBpL3BlZGRpLWNsb3VkLmludGVncmF0aW9uLXBvcnRcIlxyXG5pbXBvcnQgT3JkZXJNb2RlbCBmcm9tIFwiLi4vY29tbW9ucy9tb2RlbC9vcmRlci5tb2RlbFwiO1xyXG5pbXBvcnQge1VwZGF0ZU1vZGVsfSBmcm9tIFwiLi4vY29tbW9ucy9tb2RlbC9vcmRlci5tb2RlbFwiO1xyXG4vLyBpbXBvcnQgUmVzcG9uc2VBcGkgZnJvbSBcIi4uL2NvbW1vbnMvZW52ZWxvcGVzL3Jlc3BvbnNlLm1vZGVsXCI7XHJcblxyXG5AaW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIE9yZGVySGFuZGxlciBpbXBsZW1lbnRzIE9yZGVyUG9ydEluYm91bmQge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIEBpbmplY3QodHlwZXMuT3JkZXJQb3J0T3V0Ym91bmQpIFxyXG4gICAgICAgIHByaXZhdGUgb3JkZXJQb3J0T3V0Ym91bmQ6IE9yZGVyUG9ydE91dGJvdW5kLFxyXG4gICAgICAgIEBpbmplY3QodHlwZXMuTWFuYWdlbWVudFN5c3RlbUludGVncmF0aW9uUG9ydCkgXHJcbiAgICAgICAgcHJpdmF0ZSBtYW5hZ2VtZW50U3lzdGVtSW50ZWdyYXRpb25Qb3J0OiBNYW5hZ2VtZW50U3lzdGVtSW50ZWdyYXRpb25Qb3J0LFxyXG4gICAgICAgIEBpbmplY3QodHlwZXMuUGVkZGlDbG91ZEludGVncmF0aW9uUG9ydClcclxuICAgICAgICBwcml2YXRlIHBlZGRpQ2xvdWRJbnRlZ3JhdGlvblBvcnQ6IFBlZGRpQ2xvdWRJbnRlZ3JhdGlvblBvcnRcclxuICAgICAgICApIHsgfVxyXG5cclxuICAgIGdldEFsbCgpOiBQcm9taXNlPEFycmF5PGFueT4+IHtcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5vcmRlclBvcnRPdXRib3VuZC5nZXRBbGwoKTtcclxuICAgICAgICB9IGNhdGNoIChlcnIpIHtcclxuICAgICAgICAgICAgdGhyb3cgZXJyXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldEJ5SWQoaWQ6IFN0cmluZyk6IFByb21pc2U8YW55PiB7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMub3JkZXJQb3J0T3V0Ym91bmQuZ2V0QnlJZChpZCk7XHJcbiAgICAgICAgfSBjYXRjaCAoZXJyKSB7XHJcbiAgICAgICAgICAgIHRocm93IGVyclxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIFxyXG4gICAgc2F2ZSA9IGFzeW5jIChvcmRlcjogT3JkZXJNb2RlbCk6IFByb21pc2U8YW55PiA9PiB7ICAgXHJcbiAgICAgICAgbGV0IHJlc3BvbnNlID0ge1xyXG4gICAgICAgICAgICBpZDogdW5kZWZpbmVkLFxyXG4gICAgICAgICAgICBtc2c6ICcnXHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgIGF3YWl0IHRoaXMubWFuYWdlbWVudFN5c3RlbUludGVncmF0aW9uUG9ydC5vcGVuVGFibGUob3JkZXIudGFibGUpXHJcbiAgICAgICAgICAgIHJlc3BvbnNlLmlkID0gYXdhaXQgdGhpcy5vcmRlclBvcnRPdXRib3VuZC5zYXZlKG9yZGVyKTtcclxuICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlXHJcbiAgICAgICAgfSBjYXRjaChlcnIpIHtcclxuICAgICAgICAgICByZXNwb25zZS5tc2cgPSBlcnIubWVzc2FnZTtcclxuICAgICAgICAgICBjb25zb2xlLmxvZyhlcnIpXHJcbiAgICAgICAgICAgdGhyb3cgcmVzcG9uc2VcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgdXBkYXRlID0gYXN5bmMgKGlkOiBTdHJpbmcsIHVwZGF0ZTogVXBkYXRlTW9kZWwpOiBQcm9taXNlPEFycmF5PGFueT4+ID0+IHtcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICBjb25zdCBvcmRlciA9IGF3YWl0IHRoaXMuZ2V0QnlJZChpZClcclxuICAgICAgICAgICAgY29uc3QgZmFpbHMgPSBhd2FpdCB0aGlzLm1hbmFnZW1lbnRTeXN0ZW1JbnRlZ3JhdGlvblBvcnQuYWRkUHJvZHVjdHNUb1RhYmxlKHVwZGF0ZSwgb3JkZXIudGFibGUpXHJcbiAgICAgICAgICAgIGF3YWl0IHRoaXMub3JkZXJQb3J0T3V0Ym91bmQudXBkYXRlRG9jdW1lbnQob3JkZXIsIHVwZGF0ZSwgZmFpbHMpO1xyXG4gICAgICAgICAgICByZXR1cm4gZmFpbHNcclxuICAgICAgICB9IGNhdGNoKGVycikge1xyXG4gICAgICAgICAgICB0aHJvdyBlcnJcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgY2xvc2UgPSBhc3luYyAoaWQ6IFN0cmluZyk6IFByb21pc2U8dm9pZD4gPT4ge1xyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgIGNvbnN0IG9yZGVyID0gYXdhaXQgdGhpcy5nZXRCeUlkKGlkKVxyXG4gICAgICAgICAgICBhd2FpdCB0aGlzLm1hbmFnZW1lbnRTeXN0ZW1JbnRlZ3JhdGlvblBvcnQuY2xvc2VUYWJsZShvcmRlci50YWJsZSlcclxuICAgICAgICAgICAgY29uc3QgdG9rZW4gPSBhd2FpdCB0aGlzLnBlZGRpQ2xvdWRJbnRlZ3JhdGlvblBvcnQuYXV0aGVudGljYXRlKClcclxuICAgICAgICAgICAgYXdhaXQgdGhpcy5wZWRkaUNsb3VkSW50ZWdyYXRpb25Qb3J0LnNlbmRPcmRlcihvcmRlciwgdG9rZW4pXHJcbiAgICAgICAgICAgICAgICAudGhlbigoKSA9PiB0aGlzLm9yZGVyUG9ydE91dGJvdW5kLmRlbGV0ZShvcmRlci5faWQpKVxyXG4gICAgICAgICAgICAgICAgLmNhdGNoKGVyciA9PiB7dGhyb3cgZXJyfSlcclxuICAgICAgICB9IGNhdGNoKGVycikge1xyXG4gICAgICAgICAgICB0aHJvdyBlcnJcclxuICAgICAgICB9XHJcbiAgICAgICAgXHJcbiAgICB9XHJcblxyXG4gICAgc2VuZChpZDogU3RyaW5nKTogdm9pZCB7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCJDaGVnb3UgbmEgY2FtYWRhIGNvcmVcIik7XHJcblxyXG4gICAgICAgIC8vIC8vIG9yZGVyIDogT3JkZXJEVE87XHJcbiAgICAgICAgLy8gbGV0IG9yZGVyID0gbmV3IE9yZGVyTW9kZWwodGhpcy5vcmRlclBvcnRPdXRib3VuZC5nZXRCeUlkKGlkKSk7XHJcblxyXG4gICAgICAgIC8vIGxldCByZXN0YXVyYW50VG9rZW4gPSBcIlwiO1xyXG4gICAgICAgIC8vIC8vZ2V0IHJlc3RhdXJhbnQgaW5mb3MgdG8gbWFrZSB0aGUgYXV0aGVudGljYXRlIG9mIHBlZGRpIGNsb3VkIGFwaVxyXG4gICAgICAgIC8vIC8vIHJlc3RhdXJhbnRUb2tlbiA9IHRoaXMucmVzdGF1cmFudFBvcnRPdXRib3VuZC5nZXRCeUlkKG9yZGVyLnJlc3RhdXJhbnRfaWRfY2xvdWQpLmhhc2g7XHJcblxyXG4gICAgICAgIC8vIC8vdHJhbmZlciBvcmRlciBhbmQgcmVzdGF1cmFudCBpbmZvcyBmcm9tIGNvbnRyb2xsZXIgdG8gdGhlIG91dHNpZGUgb2YgdGhlIGFwcGxpY2F0aW9uXHJcbiAgICAgICAgLy8gdGhpcy5vcmRlclBvcnRPdXRib3VuZC5zZW5kKG9yZGVyLCBvcmRlci5yZXN0YXVyYW50X2lkX2Nsb3VkLCByZXN0YXVyYW50VG9rZW4pO1xyXG5cclxuICAgICAgICAvLyAvLyByZXR1cm4gbmV3IE51bWJlcigpO1xyXG4gICAgICAgIC8vIC8vIHRoaXMub3JkZXJQb3J0T3V0Ym91bmQuc2F2ZShvcmRlcik7XHJcbiAgICAgICAgLy8gLy8gdGhyb3cgbmV3IEVycm9yKFwiTWV0aG9kIG5vdCBpbXBsZW1lbnRlZC5cIik7XHJcbiAgICB9XHJcblxyXG59Il19
