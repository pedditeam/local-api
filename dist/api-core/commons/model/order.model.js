"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// const { BaseDTO, fields } = require('dtox')
// Define order mapping
// const ORDER_MAPPING = {
//     table: Number(),
//     guests: Number(),
//     amount_price: Number(),
//     status: Number(),
//     restaurant_id_cloud: Number(),
//     // waiter: fields.WaiterDTO()
// };
// const WAITER_MAPPING = {
//     cloud_id: fields.number(),
//     mgmt_id: fields.string(),
//     name: fields.string()
// };
// // Define a DTO which represents a single order
// export default class OrderDTO extends BaseDTO {
//     constructor(data: any) {
//         super(data, ORDER_MAPPING);
//     }
// }
// class WaiterDTO extends BaseDTO {
//     constructor(data: any) {
//       super(data, WAITER_MAPPING);
//     }
// }
class OrderModel {
    constructor(incoming) {
        this.mongo_id = incoming.mongo_id;
        this.table = incoming.table;
        this.guests = incoming.guests;
        this.order_price = incoming.amount_price;
        this.status = incoming.status;
        this.restaurant_cloud_id = incoming.restaurant_cloud_id;
        this.waiter_cloud_id = incoming.waiter_cloud_id;
        this.created_at = incoming.created_at;
        this.updated_at = incoming.updated_at;
        this.consumers = incoming.consumers;
    }
}
exports.default = OrderModel;
class UpdateModel {
    constructor(incoming) {
        this.order_price = incoming.order_price;
        this.updated_at = incoming.updated_at;
        this.consumers = incoming.consumers;
    }
}
exports.UpdateModel = UpdateModel;
class ConsumerModel {
    constructor(incoming) {
        this.card = incoming.card;
        this.items = incoming.items;
    }
}
exports.ConsumerModel = ConsumerModel;
class ItemModel {
    constructor(incoming) {
        this.base_cloud_id = incoming.base_cloud_id;
        this.mgmt_id = incoming.mgmt_id;
        this.item_price = incoming.item_price;
        this.ingredients = incoming.ingredients;
    }
}
exports.ItemModel = ItemModel;
class IngredientModel {
    constructor(incoming) {
        this.cloud_id = incoming.cloud_id;
        this.mgmt_id = incoming.mgmt_id;
        this.action = incoming.action;
        this.ingredient_price = incoming.ingredient_price;
    }
}
exports.IngredientModel = IngredientModel;
//Contract
// {  
//     "table":3,
//     "guests":2,
//     "amount_price":66.30,
//     "status":1,
//     "restaurant_id_cloud":1,
//     "waiter_id_cloud":1,
//     "created_at":"12/02/2018 12:50",
//     "updated_at":"12/02/2018 12:51",
//     "consumers":[  
//         {  
//             "card":508,
//             "items":[  
//                 {  
//                     "base_cloud_id":33,
//                     "item_price": 19.4,
//                     "ingredients":null
//                 },
//                 {  
//                     "base_cloud_id":101,
//                     "item_price": 4.50,
//                     "ingredients":null
//                 }
//             ]
//         },
//         {  
//             "card":603,
//             "items":[  
//                 {  
//                     "base_cloud_id":10,
//                     "item_price": 16.00,
//                     "ingredients":[  
//                         {  
//             		        "id":1,
//             		        "action":1,
//             		        "ingredient_price":2.5
//             	        }
//                     ]
//                 },
//                 {  
//                     "base_cloud_id":71,
//                     "item_price": 25.00,
//                     "ingredients":[  
//                         {  
//             		        "id":5,
//             		        "action":1,
//             	    	    "ingredient_price":2.5
//             	        },
//                         {  
//                             "id":14,
//                             "action":1,
//                             "ingredient_price":2.5
//                         }
//                     ]
//                 }
//             ]
//         }
//     ]
// }

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9hcGktY29yZS9jb21tb25zL21vZGVsL29yZGVyLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBR0EsOENBQThDO0FBRTlDLHVCQUF1QjtBQUN2QiwwQkFBMEI7QUFDMUIsdUJBQXVCO0FBQ3ZCLHdCQUF3QjtBQUN4Qiw4QkFBOEI7QUFDOUIsd0JBQXdCO0FBQ3hCLHFDQUFxQztBQUNyQyxvQ0FBb0M7QUFDcEMsS0FBSztBQUVMLDJCQUEyQjtBQUMzQixpQ0FBaUM7QUFDakMsZ0NBQWdDO0FBQ2hDLDRCQUE0QjtBQUM1QixLQUFLO0FBRUwsa0RBQWtEO0FBQ2xELGtEQUFrRDtBQUNsRCwrQkFBK0I7QUFDL0Isc0NBQXNDO0FBQ3RDLFFBQVE7QUFDUixJQUFJO0FBRUosb0NBQW9DO0FBQ3BDLCtCQUErQjtBQUMvQixxQ0FBcUM7QUFDckMsUUFBUTtBQUNSLElBQUk7QUFFSixNQUFxQixVQUFVO0lBWTNCLFlBQVksUUFBa0I7UUFDMUIsSUFBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUMsUUFBUSxDQUFDO1FBQ2xDLElBQUksQ0FBQyxLQUFLLEdBQUcsUUFBUSxDQUFDLEtBQUssQ0FBQztRQUM1QixJQUFJLENBQUMsTUFBTSxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUM7UUFDOUIsSUFBSSxDQUFDLFdBQVcsR0FBRyxRQUFRLENBQUMsWUFBWSxDQUFDO1FBQ3pDLElBQUksQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQztRQUM5QixJQUFJLENBQUMsbUJBQW1CLEdBQUcsUUFBUSxDQUFDLG1CQUFtQixDQUFDO1FBQ3hELElBQUksQ0FBQyxlQUFlLEdBQUcsUUFBUSxDQUFDLGVBQWUsQ0FBQztRQUNoRCxJQUFJLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQyxVQUFVLENBQUM7UUFDdEMsSUFBSSxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUMsVUFBVSxDQUFDO1FBQ3RDLElBQUksQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDLFNBQVMsQ0FBQztJQUV4QyxDQUFDO0NBRUo7QUExQkQsNkJBMEJDO0FBRUQsTUFBYSxXQUFXO0lBTXBCLFlBQVksUUFBbUI7UUFDM0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxRQUFRLENBQUMsV0FBVyxDQUFBO1FBQ3ZDLElBQUksQ0FBQyxVQUFVLEdBQUcsUUFBUSxDQUFDLFVBQVUsQ0FBQztRQUN0QyxJQUFJLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQyxTQUFTLENBQUM7SUFDeEMsQ0FBQztDQUNKO0FBWEQsa0NBV0M7QUFFRCxNQUFhLGFBQWE7SUFLdEIsWUFBWSxRQUFxQjtRQUM3QixJQUFJLENBQUMsSUFBSSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7UUFDMUIsSUFBSSxDQUFDLEtBQUssR0FBRyxRQUFRLENBQUMsS0FBSyxDQUFDO0lBQ2hDLENBQUM7Q0FFSjtBQVZELHNDQVVDO0FBRUQsTUFBYSxTQUFTO0lBT2xCLFlBQVksUUFBaUI7UUFDekIsSUFBSSxDQUFDLGFBQWEsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDO1FBQzVDLElBQUksQ0FBQyxPQUFPLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQztRQUNoQyxJQUFJLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQyxVQUFVLENBQUM7UUFDdEMsSUFBSSxDQUFDLFdBQVcsR0FBRyxRQUFRLENBQUMsV0FBVyxDQUFDO0lBQzVDLENBQUM7Q0FFSjtBQWRELDhCQWNDO0FBRUQsTUFBYSxlQUFlO0lBT3hCLFlBQVksUUFBdUI7UUFDL0IsSUFBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUMsUUFBUSxDQUFDO1FBQ2xDLElBQUksQ0FBQyxPQUFPLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQztRQUNoQyxJQUFJLENBQUMsTUFBTSxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUM7UUFDOUIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQztJQUN0RCxDQUFDO0NBRUo7QUFkRCwwQ0FjQztBQUdELFVBQVU7QUFDVixNQUFNO0FBQ04saUJBQWlCO0FBQ2pCLGtCQUFrQjtBQUNsQiw0QkFBNEI7QUFDNUIsa0JBQWtCO0FBQ2xCLCtCQUErQjtBQUMvQiwyQkFBMkI7QUFDM0IsdUNBQXVDO0FBQ3ZDLHVDQUF1QztBQUN2QyxzQkFBc0I7QUFDdEIsY0FBYztBQUNkLDBCQUEwQjtBQUMxQiwwQkFBMEI7QUFDMUIsc0JBQXNCO0FBQ3RCLDBDQUEwQztBQUMxQywwQ0FBMEM7QUFDMUMseUNBQXlDO0FBQ3pDLHFCQUFxQjtBQUNyQixzQkFBc0I7QUFDdEIsMkNBQTJDO0FBQzNDLDBDQUEwQztBQUMxQyx5Q0FBeUM7QUFDekMsb0JBQW9CO0FBQ3BCLGdCQUFnQjtBQUNoQixhQUFhO0FBQ2IsY0FBYztBQUNkLDBCQUEwQjtBQUMxQiwwQkFBMEI7QUFDMUIsc0JBQXNCO0FBQ3RCLDBDQUEwQztBQUMxQywyQ0FBMkM7QUFDM0Msd0NBQXdDO0FBQ3hDLDhCQUE4QjtBQUM5QixnQ0FBZ0M7QUFDaEMsb0NBQW9DO0FBQ3BDLCtDQUErQztBQUMvQyx5QkFBeUI7QUFDekIsd0JBQXdCO0FBQ3hCLHFCQUFxQjtBQUNyQixzQkFBc0I7QUFDdEIsMENBQTBDO0FBQzFDLDJDQUEyQztBQUMzQyx3Q0FBd0M7QUFDeEMsOEJBQThCO0FBQzlCLGdDQUFnQztBQUNoQyxvQ0FBb0M7QUFDcEMsK0NBQStDO0FBQy9DLDBCQUEwQjtBQUMxQiw4QkFBOEI7QUFDOUIsdUNBQXVDO0FBQ3ZDLDBDQUEwQztBQUMxQyxxREFBcUQ7QUFDckQsNEJBQTRCO0FBQzVCLHdCQUF3QjtBQUN4QixvQkFBb0I7QUFDcEIsZ0JBQWdCO0FBQ2hCLFlBQVk7QUFDWixRQUFRO0FBQ1IsSUFBSSIsImZpbGUiOiJhcGktY29yZS9jb21tb25zL21vZGVsL29yZGVyLm1vZGVsLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtPcmRlckRUT30gZnJvbSBcIi4uLy4uLy4uL2FwaS1pbmJvdW5kL2R0by9vcmRlci5kdG9cIjtcclxuaW1wb3J0IHtDb25zdW1lckRUTywgSXRlbURUTywgSW5ncmVkaWVudERUTywgVXBkYXRlRFRPfSBmcm9tIFwiLi4vLi4vLi4vYXBpLWluYm91bmQvZHRvL29yZGVyLmR0b1wiO1xyXG5cclxuLy8gY29uc3QgeyBCYXNlRFRPLCBmaWVsZHMgfSA9IHJlcXVpcmUoJ2R0b3gnKVxyXG5cclxuLy8gRGVmaW5lIG9yZGVyIG1hcHBpbmdcclxuLy8gY29uc3QgT1JERVJfTUFQUElORyA9IHtcclxuLy8gICAgIHRhYmxlOiBOdW1iZXIoKSxcclxuLy8gICAgIGd1ZXN0czogTnVtYmVyKCksXHJcbi8vICAgICBhbW91bnRfcHJpY2U6IE51bWJlcigpLFxyXG4vLyAgICAgc3RhdHVzOiBOdW1iZXIoKSxcclxuLy8gICAgIHJlc3RhdXJhbnRfaWRfY2xvdWQ6IE51bWJlcigpLFxyXG4vLyAgICAgLy8gd2FpdGVyOiBmaWVsZHMuV2FpdGVyRFRPKClcclxuLy8gfTtcclxuXHJcbi8vIGNvbnN0IFdBSVRFUl9NQVBQSU5HID0ge1xyXG4vLyAgICAgY2xvdWRfaWQ6IGZpZWxkcy5udW1iZXIoKSxcclxuLy8gICAgIG1nbXRfaWQ6IGZpZWxkcy5zdHJpbmcoKSxcclxuLy8gICAgIG5hbWU6IGZpZWxkcy5zdHJpbmcoKVxyXG4vLyB9O1xyXG5cclxuLy8gLy8gRGVmaW5lIGEgRFRPIHdoaWNoIHJlcHJlc2VudHMgYSBzaW5nbGUgb3JkZXJcclxuLy8gZXhwb3J0IGRlZmF1bHQgY2xhc3MgT3JkZXJEVE8gZXh0ZW5kcyBCYXNlRFRPIHtcclxuLy8gICAgIGNvbnN0cnVjdG9yKGRhdGE6IGFueSkge1xyXG4vLyAgICAgICAgIHN1cGVyKGRhdGEsIE9SREVSX01BUFBJTkcpO1xyXG4vLyAgICAgfVxyXG4vLyB9XHJcblxyXG4vLyBjbGFzcyBXYWl0ZXJEVE8gZXh0ZW5kcyBCYXNlRFRPIHtcclxuLy8gICAgIGNvbnN0cnVjdG9yKGRhdGE6IGFueSkge1xyXG4vLyAgICAgICBzdXBlcihkYXRhLCBXQUlURVJfTUFQUElORyk7XHJcbi8vICAgICB9XHJcbi8vIH1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIE9yZGVyTW9kZWwge1xyXG4gICAgcHVibGljIG1vbmdvX2lkPzogU3RyaW5nO1xyXG4gICAgcHVibGljIHRhYmxlOiBOdW1iZXI7XHJcbiAgICBwdWJsaWMgZ3Vlc3RzOiBOdW1iZXI7XHJcbiAgICBwdWJsaWMgb3JkZXJfcHJpY2U6IE51bWJlcjtcclxuICAgIHB1YmxpYyBzdGF0dXM6IE51bWJlcjtcclxuICAgIHB1YmxpYyByZXN0YXVyYW50X2Nsb3VkX2lkOiBOdW1iZXI7XHJcbiAgICBwdWJsaWMgd2FpdGVyX2Nsb3VkX2lkOiBOdW1iZXI7XHJcbiAgICBwdWJsaWMgY3JlYXRlZF9hdDogU3RyaW5nO1xyXG4gICAgcHVibGljIHVwZGF0ZWRfYXQ6IFN0cmluZztcclxuICAgIHB1YmxpYyBjb25zdW1lcnM6IEFycmF5PENvbnN1bWVyTW9kZWw+O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKGluY29taW5nOiBPcmRlckRUTykge1xyXG4gICAgICAgIHRoaXMubW9uZ29faWQgPSBpbmNvbWluZy5tb25nb19pZDtcclxuICAgICAgICB0aGlzLnRhYmxlID0gaW5jb21pbmcudGFibGU7XHJcbiAgICAgICAgdGhpcy5ndWVzdHMgPSBpbmNvbWluZy5ndWVzdHM7XHJcbiAgICAgICAgdGhpcy5vcmRlcl9wcmljZSA9IGluY29taW5nLmFtb3VudF9wcmljZTtcclxuICAgICAgICB0aGlzLnN0YXR1cyA9IGluY29taW5nLnN0YXR1cztcclxuICAgICAgICB0aGlzLnJlc3RhdXJhbnRfY2xvdWRfaWQgPSBpbmNvbWluZy5yZXN0YXVyYW50X2Nsb3VkX2lkO1xyXG4gICAgICAgIHRoaXMud2FpdGVyX2Nsb3VkX2lkID0gaW5jb21pbmcud2FpdGVyX2Nsb3VkX2lkO1xyXG4gICAgICAgIHRoaXMuY3JlYXRlZF9hdCA9IGluY29taW5nLmNyZWF0ZWRfYXQ7XHJcbiAgICAgICAgdGhpcy51cGRhdGVkX2F0ID0gaW5jb21pbmcudXBkYXRlZF9hdDtcclxuICAgICAgICB0aGlzLmNvbnN1bWVycyA9IGluY29taW5nLmNvbnN1bWVycztcclxuXHJcbiAgICB9XHJcblxyXG59XHJcblxyXG5leHBvcnQgY2xhc3MgVXBkYXRlTW9kZWwge1xyXG5cclxuICAgIHB1YmxpYyBvcmRlcl9wcmljZTogTnVtYmVyO1xyXG4gICAgcHVibGljIHVwZGF0ZWRfYXQ6IFN0cmluZztcclxuICAgIHB1YmxpYyBjb25zdW1lcnM6IEFycmF5PENvbnN1bWVyTW9kZWw+O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKGluY29taW5nOiBVcGRhdGVEVE8pIHtcclxuICAgICAgICB0aGlzLm9yZGVyX3ByaWNlID0gaW5jb21pbmcub3JkZXJfcHJpY2VcclxuICAgICAgICB0aGlzLnVwZGF0ZWRfYXQgPSBpbmNvbWluZy51cGRhdGVkX2F0O1xyXG4gICAgICAgIHRoaXMuY29uc3VtZXJzID0gaW5jb21pbmcuY29uc3VtZXJzO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgY2xhc3MgQ29uc3VtZXJNb2RlbCB7XHJcblxyXG4gICAgcHVibGljIGNhcmQ6IFN0cmluZztcclxuICAgIHB1YmxpYyBpdGVtczogQXJyYXk8SXRlbU1vZGVsPjtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihpbmNvbWluZzogQ29uc3VtZXJEVE8pIHtcclxuICAgICAgICB0aGlzLmNhcmQgPSBpbmNvbWluZy5jYXJkO1xyXG4gICAgICAgIHRoaXMuaXRlbXMgPSBpbmNvbWluZy5pdGVtcztcclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBJdGVtTW9kZWwge1xyXG5cclxuICAgIHB1YmxpYyBiYXNlX2Nsb3VkX2lkOiBOdW1iZXI7XHJcbiAgICBwdWJsaWMgbWdtdF9pZDogU3RyaW5nO1xyXG4gICAgcHVibGljIGl0ZW1fcHJpY2U6IE51bWJlcjtcclxuICAgIHB1YmxpYyBpbmdyZWRpZW50czogQXJyYXk8SW5ncmVkaWVudE1vZGVsPjtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihpbmNvbWluZzogSXRlbURUTykge1xyXG4gICAgICAgIHRoaXMuYmFzZV9jbG91ZF9pZCA9IGluY29taW5nLmJhc2VfY2xvdWRfaWQ7XHJcbiAgICAgICAgdGhpcy5tZ210X2lkID0gaW5jb21pbmcubWdtdF9pZDtcclxuICAgICAgICB0aGlzLml0ZW1fcHJpY2UgPSBpbmNvbWluZy5pdGVtX3ByaWNlO1xyXG4gICAgICAgIHRoaXMuaW5ncmVkaWVudHMgPSBpbmNvbWluZy5pbmdyZWRpZW50cztcclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBJbmdyZWRpZW50TW9kZWwge1xyXG5cclxuICAgIHB1YmxpYyBjbG91ZF9pZDogTnVtYmVyO1xyXG4gICAgcHVibGljIG1nbXRfaWQ6IFN0cmluZztcclxuICAgIHB1YmxpYyBhY3Rpb246IE51bWJlcjtcclxuICAgIHB1YmxpYyBpbmdyZWRpZW50X3ByaWNlOiBOdW1iZXI7XHJcblxyXG4gICAgY29uc3RydWN0b3IoaW5jb21pbmc6IEluZ3JlZGllbnREVE8pIHtcclxuICAgICAgICB0aGlzLmNsb3VkX2lkID0gaW5jb21pbmcuY2xvdWRfaWQ7XHJcbiAgICAgICAgdGhpcy5tZ210X2lkID0gaW5jb21pbmcubWdtdF9pZDtcclxuICAgICAgICB0aGlzLmFjdGlvbiA9IGluY29taW5nLmFjdGlvbjtcclxuICAgICAgICB0aGlzLmluZ3JlZGllbnRfcHJpY2UgPSBpbmNvbWluZy5pbmdyZWRpZW50X3ByaWNlO1xyXG4gICAgfVxyXG5cclxufVxyXG5cclxuXHJcbi8vQ29udHJhY3RcclxuLy8geyAgXHJcbi8vICAgICBcInRhYmxlXCI6MyxcclxuLy8gICAgIFwiZ3Vlc3RzXCI6MixcclxuLy8gICAgIFwiYW1vdW50X3ByaWNlXCI6NjYuMzAsXHJcbi8vICAgICBcInN0YXR1c1wiOjEsXHJcbi8vICAgICBcInJlc3RhdXJhbnRfaWRfY2xvdWRcIjoxLFxyXG4vLyAgICAgXCJ3YWl0ZXJfaWRfY2xvdWRcIjoxLFxyXG4vLyAgICAgXCJjcmVhdGVkX2F0XCI6XCIxMi8wMi8yMDE4IDEyOjUwXCIsXHJcbi8vICAgICBcInVwZGF0ZWRfYXRcIjpcIjEyLzAyLzIwMTggMTI6NTFcIixcclxuLy8gICAgIFwiY29uc3VtZXJzXCI6WyAgXHJcbi8vICAgICAgICAgeyAgXHJcbi8vICAgICAgICAgICAgIFwiY2FyZFwiOjUwOCxcclxuLy8gICAgICAgICAgICAgXCJpdGVtc1wiOlsgIFxyXG4vLyAgICAgICAgICAgICAgICAgeyAgXHJcbi8vICAgICAgICAgICAgICAgICAgICAgXCJiYXNlX2Nsb3VkX2lkXCI6MzMsXHJcbi8vICAgICAgICAgICAgICAgICAgICAgXCJpdGVtX3ByaWNlXCI6IDE5LjQsXHJcbi8vICAgICAgICAgICAgICAgICAgICAgXCJpbmdyZWRpZW50c1wiOm51bGxcclxuLy8gICAgICAgICAgICAgICAgIH0sXHJcbi8vICAgICAgICAgICAgICAgICB7ICBcclxuLy8gICAgICAgICAgICAgICAgICAgICBcImJhc2VfY2xvdWRfaWRcIjoxMDEsXHJcbi8vICAgICAgICAgICAgICAgICAgICAgXCJpdGVtX3ByaWNlXCI6IDQuNTAsXHJcbi8vICAgICAgICAgICAgICAgICAgICAgXCJpbmdyZWRpZW50c1wiOm51bGxcclxuLy8gICAgICAgICAgICAgICAgIH1cclxuLy8gICAgICAgICAgICAgXVxyXG4vLyAgICAgICAgIH0sXHJcbi8vICAgICAgICAgeyAgXHJcbi8vICAgICAgICAgICAgIFwiY2FyZFwiOjYwMyxcclxuLy8gICAgICAgICAgICAgXCJpdGVtc1wiOlsgIFxyXG4vLyAgICAgICAgICAgICAgICAgeyAgXHJcbi8vICAgICAgICAgICAgICAgICAgICAgXCJiYXNlX2Nsb3VkX2lkXCI6MTAsXHJcbi8vICAgICAgICAgICAgICAgICAgICAgXCJpdGVtX3ByaWNlXCI6IDE2LjAwLFxyXG4vLyAgICAgICAgICAgICAgICAgICAgIFwiaW5ncmVkaWVudHNcIjpbICBcclxuLy8gICAgICAgICAgICAgICAgICAgICAgICAgeyAgXHJcbi8vICAgICAgICAgICAgIFx0XHQgICAgICAgIFwiaWRcIjoxLFxyXG4vLyAgICAgICAgICAgICBcdFx0ICAgICAgICBcImFjdGlvblwiOjEsXHJcbi8vICAgICAgICAgICAgIFx0XHQgICAgICAgIFwiaW5ncmVkaWVudF9wcmljZVwiOjIuNVxyXG4vLyAgICAgICAgICAgICBcdCAgICAgICAgfVxyXG4vLyAgICAgICAgICAgICAgICAgICAgIF1cclxuLy8gICAgICAgICAgICAgICAgIH0sXHJcbi8vICAgICAgICAgICAgICAgICB7ICBcclxuLy8gICAgICAgICAgICAgICAgICAgICBcImJhc2VfY2xvdWRfaWRcIjo3MSxcclxuLy8gICAgICAgICAgICAgICAgICAgICBcIml0ZW1fcHJpY2VcIjogMjUuMDAsXHJcbi8vICAgICAgICAgICAgICAgICAgICAgXCJpbmdyZWRpZW50c1wiOlsgIFxyXG4vLyAgICAgICAgICAgICAgICAgICAgICAgICB7ICBcclxuLy8gICAgICAgICAgICAgXHRcdCAgICAgICAgXCJpZFwiOjUsXHJcbi8vICAgICAgICAgICAgIFx0XHQgICAgICAgIFwiYWN0aW9uXCI6MSxcclxuLy8gICAgICAgICAgICAgXHQgICAgXHQgICAgXCJpbmdyZWRpZW50X3ByaWNlXCI6Mi41XHJcbi8vICAgICAgICAgICAgIFx0ICAgICAgICB9LFxyXG4vLyAgICAgICAgICAgICAgICAgICAgICAgICB7ICBcclxuLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiaWRcIjoxNCxcclxuLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiYWN0aW9uXCI6MSxcclxuLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiaW5ncmVkaWVudF9wcmljZVwiOjIuNVxyXG4vLyAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbi8vICAgICAgICAgICAgICAgICAgICAgXVxyXG4vLyAgICAgICAgICAgICAgICAgfVxyXG4vLyAgICAgICAgICAgICBdXHJcbi8vICAgICAgICAgfVxyXG4vLyAgICAgXVxyXG4vLyB9Il19
